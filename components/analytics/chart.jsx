import { useState, useEffect } from 'react';
import NProgress from 'nprogress';
import {Line} from 'react-chartjs-2';
import * as ChartAnnotation from 'chartjs-plugin-annotation';
import { fetchTemperatureLogs, fetchHumidityLogs } from '../../lib/api/logs';
import { _momentDateFormat } from '../../utils/globalFunc';

const LineChart = props => {
    const { shipmentDetails, dateRange, rgbColor, type, container_id, shipment_id } = props;
    const [format, setFormat] = useState('YYYY-MM-DD HH:mm:ss');
    const [labels, setLabels] = useState([]);
    const [values, setValues] = useState([]);
    const [label, setLabel] = useState([]);

    useEffect(() => {
        _fetchChartData(false);
    }, []);

    useEffect(() => {
        if(dateRange[0].startDate && dateRange[0].endDate){
			_fetchChartData(true);
		}
    }, [dateRange]);

    const _fetchChartData = async (withDate) => {
        NProgress.start();
        try {
            const chartLabels = [];
            const chartValue = [];
            if(type == 'temp'){
                setLabel('Temperature');
                // Fetch and Save temperature logs
                let temperatureLogs = [];
                if(withDate){
                    temperatureLogs = await fetchTemperatureLogs({container: container_id, shipment: shipment_id, start_date:_momentDateFormat(dateRange[0].startDate, "YYYY-MM-DD"), end_date:_momentDateFormat(dateRange[0].endDate, "YYYY-MM-DD")});
                } else{
                    temperatureLogs = await fetchTemperatureLogs({container: container_id, shipment: shipment_id, start_date:null, end_date:null});
                }
                temperatureLogs.data.map((log,i) => {
                    chartLabels.push(_momentDateFormat(log.createdAt, format));
                    chartValue.push(log.temperature);
                });
            } else{
                setLabel('Humidity');
                // Fetch and Save humidity logs
                let humidityLogs = [];
                if(withDate){
                    humidityLogs = await fetchHumidityLogs({container: container_id, shipment: shipment_id, start_date:_momentDateFormat(dateRange[0].startDate, "YYYY-MM-DD"), end_date:_momentDateFormat(dateRange[0].endDate, "YYYY-MM-DD")});
                } else{
                    humidityLogs = await fetchHumidityLogs({container: container_id, shipment: shipment_id, start_date:null, end_date:null});
                }
                humidityLogs.data.map((log,i) => {
                    chartLabels.push(_momentDateFormat(log.createdAt, format));
                    chartValue.push(log.humidity);
                });
            }
            setLabels(chartLabels);
            setValues(chartValue);

            NProgress.done();
        } catch (err) {
            console.error("Error while fething logs => ", err);
            NProgress.done();
        }
    }

    const chartData = {
        labels: labels,
        datasets: [
        {
            label: label,
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba('+rgbColor+',0.4)',
            borderColor: 'rgba('+rgbColor+',1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba('+rgbColor+',1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba('+rgbColor+',1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 3,
            pointHitRadius: 10,
            data: values
          }
        ]
    };

    const chartOpts = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    min: type == 'temp' ? -50 : 0,
                    max: type == 'temp' ? 50 : 100
                }
            }]
        },
        annotation: {
            annotations: [{
                type: 'line',
                mode: 'horizontal',
                scaleID: 'y-axis-0',
                value: type == 'temp' ? shipmentDetails.temperature_alert_max : shipmentDetails.humidity_alert_max,
                borderColor: 'rgb(255,0,0)',
                borderWidth: 4,
                label: {
                    enabled: true,
                    content: type == 'temp' ? 'Max Temperature ' + shipmentDetails.temperature_alert_max + ' °C' : 'Max Humidity ' + shipmentDetails.humidity_alert_max + '%'
                }
            },{
                type: 'line',
                mode: 'horizontal',
                scaleID: 'y-axis-0',
                value: type == 'temp' ? shipmentDetails.temperature_alert_min : shipmentDetails.humidity_alert_min,
                borderColor: 'rgb(255,0,0)',
                borderWidth: 4,
                label: {
                    enabled: true,
                    content: type == 'temp' ? 'Min Temperature ' + shipmentDetails.temperature_alert_min + ' °C' : 'Min Humidity ' + shipmentDetails.humidity_alert_min + '%'
                }
            }]
        }
    };

    return (
        <div className="shipment-table-listing table-responsive mt-2 mb-2">
            <Line data={chartData} options={chartOpts} plugins={[ChartAnnotation]} />
        </div>
    )
}

export default LineChart;
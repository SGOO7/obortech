import { Formik } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";

import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import Select from 'react-select'

const AddShipmentCategoryMapschema = Yup.object().shape({
    category_id: Yup.object().shape({
        value: Yup.string().required(`Please select a category`),
        label: Yup.string()
    })
});

const AddShipmentCategoryMapModal = ({ setValues, event_categories, onCategorySubmit, isOpen, toggle, values, editMode, string }) => {

    let eventCategories = [];
    event_categories.map((category,i) => {
        eventCategories.push({label:category.name, value:category.id});
    });

    if (typeof window === 'undefined') {
        return null;
    } else {
        return (
            <Modal isOpen={isOpen} toggle={toggle} className="customModal document">
                <ModalHeader toggle={toggle}>
                    <span className="modal-title text-dark font-weight-bold" id="exampleModalLabel">{editMode === "shipmentCategory" ? string.updateEvntCatTxt : string.addEvntCatTxt}</span>
                </ModalHeader>
                <ModalBody>
                <Formik
                    enableReinitialize={true}
                    initialValues={{
                        category_id: values.category_id,
                    }}
                    validationSchema={AddShipmentCategoryMapschema}
                    onSubmit={(val) => {
                        onCategorySubmit();
                    }}
                >
                    {({ errors, touched, handleChange, handleSubmit, values }) => {
                        return (
                            <form onSubmit={handleSubmit}>
                                <div className="row ml-0 mr-0 content-block">
                                    <div className="form-group col-md-12 p-0">
                                        <label htmlFor="name" className="col-md-12 col-form-label pl-0">{string.selectCategory}</label>
                                        <Select
                                            value={values.category_id}
                                            options={eventCategories}
                                            name="category_id"
                                            onChange={(event) => {
                                                handleChange({ target: { name: "category_id", value: event } });
                                                setValues(event);
                                            }}
                                        />
                                        {errors.category_id?.value ? (
                                            <FormHelperMessage
                                                message={errors.category_id?.value}
                                                className="error"
                                            />
                                        ) : null}
                                    </div>
                                </div>
                                <ModalFooter>
                                    <button
                                        className="btn btn-primary large-btn" type="submit">{editMode === "eventCategory" ? string.updateBtnTxt : string.insertBtnTxt}</button>
                                </ModalFooter>
                            </form>
                        )
                    }}
                    </Formik>
                </ModalBody>
            </Modal>
        );
    }
}

AddShipmentCategoryMapModal.propTypes = {
};

AddShipmentCategoryMapModal.defaultProps = {
};

export default AddShipmentCategoryMapModal;
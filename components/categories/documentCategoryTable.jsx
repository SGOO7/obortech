import { useState } from 'react';
import { Collapse } from 'reactstrap';

const DocumentCategoryTable = props => {
    const { setEditMode, setDeleteMode, document_categories, string } = props;
    const [openAcc, setOpenAcc] = useState();
    const _toggleAcc = idx => {
        if(idx === openAcc) {
            setOpenAcc(null);
        }else {
            setOpenAcc(idx);
        }
    }
    return (
        <table className="table eventCat">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{string.tableColName}</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {document_categories.length > 0 && document_categories.map((category, i) => {
                    return (
                        <>
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{category.name}</td>
                                <td>
                                    <i className="fa fa-pencil-alt" onClick={() => setEditMode('documentCategory', i, null, category)}></i>
                                    <i className="fa fa-trash" onClick={() => setDeleteMode('documentCategory', i)}></i>
                                    <i className="fa fa-caret-down" onClick={() => _toggleAcc(i)}></i>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="3" className="eventCatExpand">
                                <Collapse isOpen={openAcc === i}>
                                    <div style={{textAlign: 'right'}}>
                                        <i className="fa fa-plus pull-right" onClick={() => setEditMode('d_event', i, null, null)}/>
                                    </div>
                                    <table className="table">
                                        <tbody>
                                            {category.events?.map((event,j)=>{
                                                return (
                                                    <tr key={j}>
                                                        <td>{event.name}</td>
                                                        <td>
                                                            <i className="fa fa-pencil-alt" onClick={() => setEditMode('d_event', i, j, event)}></i>
                                                            <i className="fa fa-trash" onClick={() => setDeleteMode('d_event', j, i)}></i>
                                                        </td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                </Collapse>
                                </td>
                            </tr>
                        </>
                    );
                })}
                {document_categories.length == 0 && <tr>
                    <td colSpan="3" className="text-center">{string.noData}</td>
                </tr>}
            </tbody>
        </table>
    )
}

export default DocumentCategoryTable;
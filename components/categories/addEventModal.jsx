import { Formik } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";

import { values } from "lodash";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

const AddCategoryschema = Yup.object().shape({
    name: Yup.string()
    .trim()
    .required(`Event Name ${string.errors.required}`),
});

const AddCategoryModal = ({ handleChange, onEventSubmit, isOpen, toggle, values, editMode, string }) => {
    if (typeof window === 'undefined') {
        return null;
    } else {
        return (
            <Modal isOpen={isOpen} toggle={toggle} className="customModal document">
                <ModalHeader toggle={toggle}>
                    <span className="modal-title text-dark font-weight-bold" id="exampleModalLabel">{editMode === "event" || editMode === "d_event" ? string.updateEvntTxt : string.addEvntTxt}</span>
                </ModalHeader>
                <ModalBody>
                <Formik
                    enableReinitialize={true}
                    initialValues={{
                        name: values.name,
                    }}
                    validationSchema={AddCategoryschema}
                    onSubmit={(val) => {
                        values.name = val.name;
                        onEventSubmit();
                    }}
                    >
                    {({ errors, touched, handleChange, handleSubmit, values }) => (
                    <form onSubmit={handleSubmit}>
                        <div className="row ml-0 mr-0 content-block">
                            <div className="form-group col-md-12 p-0">
                                <label htmlFor="name" className="col-md-12 col-form-label pl-0">{string.eventNameTxt}</label>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    className="form-control"
                                    placeholder={string.eventNameTxt}
                                    onChange={handleChange}
                                    value={values.name}
                                />
                                {errors.name && touched.name ? (
                                <FormHelperMessage
                                    message={errors.name}
                                    className="error"
                                />
                                ) : null}
                            </div>
                        </div>
                        <ModalFooter>
                            <button 
                            // data-dismiss="modal" 
                            // onClick={onEventSubmit} 
                            className="btn btn-primary large-btn" type="submit">{editMode === "event" || editMode === "d_event" ? string.updateBtnTxt : string.insertBtnTxt}</button>
                        </ModalFooter>
                    </form>
                    )}
                    </Formik>
                </ModalBody>
            </Modal>
        );
    }
}

AddCategoryModal.propTypes = {
};

AddCategoryModal.defaultProps = {
};

export default AddCategoryModal;
import { useState } from 'react';
import { Collapse } from 'reactstrap';

const ShipmentCategoryTable = props => {
    const [openAcc, setOpenAcc] = useState();
    const { setEditMode, setDeleteMode, shipment_categories, string } = props;
    const _toggleAcc = idx => {
        if(idx === openAcc) {
            setOpenAcc(null);
        }else {
            setOpenAcc(idx);
        }
    }
    return (
        <table className="table eventCat">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{string.tableColName}</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {shipment_categories.length > 0 && shipment_categories.map((category, i) => {
                    return (
                        <>
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{category.name}</td>
                                <td>
                                    <i className="fa fa-pencil-alt" onClick={() => setEditMode('shipmentCategory', i, null, category)}></i>
                                    <i className="fa fa-trash" onClick={() => setDeleteMode('shipmentCategory', i)}></i>
                                    <i className="fa fa-caret-down" onClick={() => _toggleAcc(i)}></i>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="3" className="eventCatExpand">
                                <Collapse isOpen={openAcc === i}>
                                    <div style={{textAlign: 'right', marginBottom: '10px'}}>
                                        <button className="btn btn-primary btn-sm add-evnt-cat-btn" onClick={() => setEditMode('eventCategoryMap', i, null, null)}>Add Event Category</button>
                                        <button className="btn btn-primary btn-sm" onClick={() => setEditMode('documentCategoryMap', i, null, null)}>Add Document Category</button>
                                    </div>
                                    <table className="table">
                                        <tbody>
                                            {category.shipment_event_categories?.map((category,j)=>{
                                                return (
                                                    <tr key={j}>
                                                        <td>{category.event_category.name}</td>
                                                        <td>Event</td>
                                                        <td>
                                                            <i className="fa fa-trash" onClick={() => setDeleteMode('eventCategoryMap', j, i)}></i>
                                                        </td>
                                                    </tr>
                                                );
                                            })}
                                            {category.shipment_document_categories?.map((category,k)=>{
                                                return (
                                                    <tr key={k}>
                                                        <td>{category.document_category?.name}</td>
                                                        <td>Document</td>
                                                        <td>
                                                            <i className="fa fa-trash" onClick={() => setDeleteMode('documentCategoryMap', k, i)}></i>
                                                        </td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                </Collapse>
                                </td>
                            </tr>
                        </>
                    );
                })}
                {shipment_categories.length == 0 && <tr>
                    <td colSpan="3" className="text-center">{string.noData}</td>
                </tr>}
            </tbody>
        </table>
    )
}

export default ShipmentCategoryTable;
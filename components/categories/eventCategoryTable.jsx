import { useState } from 'react';
import { Collapse } from 'reactstrap';

const EventCategoryTable = props => {
    const { setEditMode, setDeleteMode, event_categories, string } = props;
    const [openAcc, setOpenAcc] = useState();
    const _toggleAcc = idx => {
        if(idx === openAcc) {
            setOpenAcc(null);
        }else {
            setOpenAcc(idx);
        }
    }
    return (
        <table className="table eventCat">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{string.tableColName}</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {event_categories.length > 0 && event_categories.map((category, i) => {
                    return (
                        <>
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{category.name}</td>
                                <td>
                                    <i className="fa fa-pencil-alt" onClick={() => setEditMode('eventCategory', i, null, category)}></i>
                                    <i className="fa fa-trash" onClick={() => setDeleteMode('eventCategory', i)}></i>
                                    <i className="fa fa-caret-down" onClick={() => _toggleAcc(i)}></i>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="3" className="eventCatExpand">
                                <Collapse isOpen={openAcc === i}>
                                    <div style={{textAlign: 'right'}}>
                                        <i className="fa fa-plus pull-right" onClick={() => setEditMode('event', i, null, null)}/>
                                    </div>
                                    <table className="table">
                                        <tbody>
                                            {category.events?.map((event,j)=>{
                                                return (
                                                    <tr key={j}>
                                                        <td>{event.name}</td>
                                                        <td>
                                                            <i className="fa fa-pencil-alt" onClick={() => setEditMode('event', i, j, event)}></i>
                                                            <i className="fa fa-trash" onClick={() => setDeleteMode('event', j, i)}></i>
                                                        </td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                </Collapse>
                                </td>
                            </tr>
                        </>
                    );
                })}
                {event_categories.length == 0 && <tr>
                    <td colSpan="3" className="text-center">{string.noData}</td>
                </tr>}
            </tbody>
        </table>
    )
}

export default EventCategoryTable;
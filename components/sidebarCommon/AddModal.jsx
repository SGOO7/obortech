import { values } from "lodash";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

const AddModal = ({ handleChange, onSubmit, isOpen, toggle, values, editMode }) => {
    if (typeof window === 'undefined') {
        return null;
    } else {
        return (
            <Modal isOpen={isOpen} toggle={toggle} className="customModal document">
                <ModalHeader toggle={toggle}>
                    <h5 className="modal-title text-dark font-weight-bold" id="exampleModalLabel">{editMode === "eventCategory" ? "UPDATE" : "ADD"} Folder</h5>
                </ModalHeader>
                <ModalBody>
                    <form onSubmit={onSubmit}>
                        <div className="row ml-0 mr-0 content-block">
                            <div className="form-group col-md-12 p-0">
                                <label htmlFor="name" className="col-md-12 col-form-label pl-0">Folder Name</label>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    className="form-control"
                                    placeholder="Name"
                                    // onChange={handleChange}
                                />
                            </div>
                        </div>
                        <ModalFooter>
                            <button data-dismiss="modal" onClick={onSubmit} className="btn btn-primary large-btn" type="submit">{editMode === "eventCategory" ? "UPDATE" : "INSERT"}</button>
                        </ModalFooter>
                    </form>
                </ModalBody>
            </Modal>
        );
    }
}

AddModal.propTypes = {

};

AddModal.defaultProps = {

};

export default AddModal;
import React,{ useState} from "react";
import "./request.css";
import { Modal,ModalHeader,ModalBody } from "reactstrap";
import Button from "../../components/common/form-elements/button/Button";
import Checkbox from "../../components/common/form-elements/checkbox/index";
import CustomSelect from "../../components/common/form-elements/select/CustomSelect";
import Input from "../../components/common/form-elements/input/Input";
function SendSubmissionRequest(){
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    const [shipment,setShipment] =useState(['Mongolia-China KYL 1-56','Certificate of Origin']);
    return(
       <div className="request-block">
        <Button className="common-modal-btn" onClick={toggle}>Send Submission Request</Button>
        <Modal isOpen={modal} toggle={toggle} className="request-modal common-model modal-lg customModal">
        <div className="common-modal-wrap">  
           <ModalHeader toggle={toggle} className="modal-center-Header justify-content-center title-profile">SEND SUBMISSION REQUEST</ModalHeader>
           <ModalBody className="request-body-wrap">
              <div className="request-body d-flex justify-content-between">
                <div className="request-content half-card">
                   <h6 className="modal-sub-heading">SELECT SHIPMENT</h6>
                      <CustomSelect shipment
                        className="requet-select"
                        name="shipment_name"
                      >
                      <option value="">Mongolia-China KYL 1-56</option>;
                      {shipment.map((shipment, i) => {
                        return (
                          <option key={i} value={shipment}>{shipment}</option>
                        );
                      })}
                      </CustomSelect>
                </div>
                <div className="request-content half-card">
                  <h6 className="modal-sub-heading">WHO CAN VIEW THE SUBMISSION?</h6>
                  <div className="setting-card setting-content-wrap">
                        <div className="chekbox-wrap">
                          <div className="custom-checkbox">
                            <Checkbox 
                              value="Standard & Metrology MN"
                              id="MongoliaAustralia"
                              className="notification-check custom-control-input" checked/>
                              <label className="custom-control-label" for="MongoliaAustralia">Standard & Metrology MN</label>
                          </div>
                          <div className="custom-checkbox">
                            <Checkbox 
                              value="Precom"
                              id="ChinaMongoliaKYL"
                              className="notification-check custom-control-input" checked/>
                              <label className="custom-control-label" for="ChinaMongoliaKYL">Precom</label>
                          </div>
                          <div className="custom-checkbox">
                            <Checkbox 
                              value="Mongolian Express"
                              id="UBCHPRE"
                              className="notification-check custom-control-input" checked/>
                              <label className="custom-control-label" for="UBCHPRE">Mongolian Express</label>
                          </div>
                          <div className="custom-checkbox">
                            <Checkbox 
                              value="Customs Office MN"
                              id="UBMNENCNPRE"
                              className="notification-check custom-control-input" />
                              <label className="custom-control-label" for="UBMNENCNPRE">Customs Office MN</label>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
              <div className="request-body d-flex justify-content-between">
                <div className="custom-checkbox request-btn half-card">
                    <Checkbox 
                      value="UBMN-ENCN PRE-12"
                       id="2"
                       type="radio"
                       className="notification-check custom-control-input" checked/>
                       <label className="custom-control-label modal-sub-heading" for="2">SELECT DOCUMENT TYPE</label>
                       <CustomSelect shipment
                        className="requet-select"
                        name="shipment_name"
                      >
                      <option value="">Certificate of Origin</option>;
                      {shipment.map((shipment, i) => {
                        return (
                          <option key={i} value={shipment}>{shipment}</option>
                        );
                      })}
                      </CustomSelect>
                  </div>
                  <div className="custom-checkbox request-btn half-card disable-edit">
                    <Checkbox 
                      value="UBMN-ENCN PRE-12"
                       id="2"
                       type="radio"
                       className="notification-check custom-control-input" />
                       <label className="custom-control-label modal-sub-heading" for="2">SELECT EVENT TYPE</label>
                       <CustomSelect shipment
                        className="requet-select"
                        name="shipment_name"
                      >
                      <option value="">Actual container stuffed</option>;
                      {shipment.map((shipment, i) => {
                        return (
                          <option key={i} value={shipment}>{shipment}</option>
                        );
                      })}
                      </CustomSelect>
                  </div>
              </div>
              <div className="request-body d-flex justify-content-between">
                  <div className="half-card request-recipients">
                      <div className="recipients-header d-flex justify-content-between">
                        <h6 className="modal-sub-heading">RECIPIENTS</h6>
                        <Button className="recipients-btn">Add <span className="right-arrow"></span></Button>
                      </div>
                      <div className="recipients-fliter-wrap">
                         <Input 
                         type="text"
                         className="recipients-filter"
                         placeholder="Type username to filter"
                         />
                          <div className="setting-card setting-content-wrap">
                          <div className="chekbox-wrap">
                            <div className="custom-checkbox">
                              <Checkbox 
                                value="Tsolmon Baatar_UAY4440 (Driver)"
                                id="Tsolmon"
                                className="notification-check custom-control-input" checked/>
                                <label className="custom-control-label" for="Tsolmon">Tsolmon Baatar_UAY4440 (Driver)</label>
                            </div>
                            <div className="custom-checkbox">
                              <Checkbox 
                                value="Enkhtstesteg (Farmer)"
                                id="Enkhtstesteg"
                                className="notification-check custom-control-input" checked/>
                                <label className="custom-control-label" for="Enkhtstesteg">Enkhtstesteg (Farmer)</label>
                            </div>
                            <div className="custom-checkbox">
                              <Checkbox 
                                value="Batuush Garmaa_UBE8356 (Driver)"
                                id="Batuush"
                                className="notification-check custom-control-input" checked/>
                                <label className="custom-control-label" for="Batuush">Batuush Garmaa_UBE8356 (Driver)</label>
                            </div>
                            <div className="custom-checkbox">
                              <Checkbox 
                                value="Bolortsetseg Dashchoilon (Farmer)"
                                id="Bolortsetseg"
                                className="notification-check custom-control-input" />
                                <label className="custom-control-label" for="Bolortsetseg">Bolortsetseg Dashchoilon (Farmer)</label>
                            </div>
                            <div className="custom-checkbox">
                            <Checkbox 
                              value="Sukhbaataar Maralmaa_UZO8554 (Driver)"
                              id="Sukhbaataar"
                              className="notification-check custom-control-input" />
                              <label className="custom-control-label" for="Sukhbaataar">Sukhbaataar Maralmaa_UZO8554 (Driver)</label>
                          </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div className="half-card request-recipients">
                  <div className="recipients-header d-flex justify-content-between">
                        <h6 className="modal-sub-heading">SELECTED RECIPIENTS</h6>
                        <Button className="recipients-btn disable-edit">Remove<span className="right-arrow"></span></Button>
                   </div>
                   <div className="recipients-fliter-wrap">
                         <Input 
                         type="text"
                         className="recipients-filter"
                         placeholder="Type username to filter"
                         />
                          <div className="setting-card setting-content-wrap">
                          <div className="chekbox-wrap">
                            <div className="custom-checkbox">
                              <Checkbox 
                                value="Tsolmon Baatar_UAY4440 (Driver)"
                                id="Tsolmon1"
                                className="notification-check custom-control-input" checked/>
                                <label className="custom-control-label" for="Tsolmon1">Tsolmon Baatar_UAY4440 (Driver)</label>
                            </div>
                            <div className="custom-checkbox">
                              <Checkbox 
                                value="Enkhtstesteg (Farmer)"
                                id="Enkhtstesteg1"
                                className="notification-check custom-control-input" checked/>
                                <label className="custom-control-label" for="Enkhtstesteg1">Enkhtstesteg (Farmer)</label>
                            </div>
                            <div className="custom-checkbox">
                              <Checkbox 
                                value="Batuush Garmaa_UBE8356 (Driver)"
                                id="Batuush1"
                                className="notification-check custom-control-input" checked/>
                                <label className="custom-control-label" for="Batuush1">Batuush Garmaa_UBE8356 (Driver)</label>
                            </div>
                            <div className="custom-checkbox">
                              <Checkbox 
                                value="Bolortsetseg Dashchoilon (Farmer)"
                                id="Bolortsetseg1"
                                className="notification-check custom-control-input" />
                                <label className="custom-control-label" for="Bolortsetseg1">Bolortsetseg Dashchoilon (Farmer)</label>
                            </div>
                            <div className="custom-checkbox">
                            <Checkbox 
                              value="Sukhbaataar Maralmaa_UZO8554 (Driver)"
                              id="Sukhbaataar1"
                              className="notification-check custom-control-input" />
                              <label className="custom-control-label" for="Sukhbaataar1">Sukhbaataar Maralmaa_UZO8554 (Driver)</label>
                          </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
              <div className="request-btn-wrap d-flex">
                <Button className="send-btn" >Send</Button>
              </div>
           </ModalBody>
         </div>
        </Modal>
       </div>
    )
};
export default SendSubmissionRequest;
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getAllCountries } from "../../redux/actions/countryActions";
import { getAllCities } from "../../redux/actions/cityActions";
import NProgress from "nprogress";
import {
    completeOnobarding,
    clearOnboardingSignupState,
    existsByName,
    existsByUsername
} from "../../redux/actions/signupActions";
import {
    verifyUserExists,
    clearUserExistsState
} from "../../redux/actions/userCheckActions";
import { signupVerifedUser } from "../../redux/actions/signupVerifiedUserActions";
import {
    sendOtp,
    verifyOtp
} from "../../redux/actions/mobileVerificationActions";
import { getAllRoles } from "../../redux/actions/roleActions";
import { getAllTypes } from "../../redux/actions/userTypeActions";
import { verifyEmailId } from "../../redux/actions/emailVerificationActions";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import * as string from "../../utils/stringConstants/language/eng.json";
import notify from "../../lib/notifier";
import OnboardingStepOne from "./onboardingStepOne";
import OnboardingStepTwo from "./onboardingStepTwo";
import OnboardingStepThree from "./onboardingStepThree";
import OnboardingStepFour from "./onboardingStepFour";
import Stepper from "./stepper";
import "./onboarding.css";

function Signup(props) {
    const { isSignupOpen, closeSignupModal } = props;

    NProgress.configure({ easing: "ease", speed: 1000 });

    const [currentStep, updateCurrentStep] = useState(1);
    const [onboardingObject, updateOnboardingObject] = useState({});
    const [isEmailVerified, setEmailVerified] = useState(false);
    const [isMobileVerified, setMobileVerified] = useState(false);
    const [isEmailVerifying, setIsEmailVerifying] = useState(false);

    const countryStore = useSelector(state => state.countries);
    const cityStore = useSelector(state => state.cities);
    const rolesStore = useSelector(state => state.roles);
    const typesStore = useSelector(state => state.types);
    const signupStore = useSelector(state => state.signup);
    const mobileStore = useSelector(state => state.mobile);
    const emailStore = useSelector(state => state.email);
    const userStore = useSelector(state => state.user);
    const signedUserStore = useSelector(state => state.signedUser);

    const dispatch = useDispatch();

    const _handleNext = data => {
        NProgress.start();
        if (data.currentStep === 1) _isUserExistsAndVerified(data);
        if (data.currentStep === 2) _submitVerified(data);
        if (data.currentStep === 4) _submitFinalUser(data);
        _updateState(data);
        if (currentStep < 5) updateCurrentStep(currentStep + 1);
    };

    const _updateState = data => {
        updateOnboardingObject({
            ...onboardingObject,
            ...data
        });
        NProgress.done();
    };

    const _isUserExistsAndVerified = data => {
        const payload = {
            email: data.emailId,
            mobile: data.mobile
        };
        dispatch(verifyUserExists(payload));
    };

    const _submitVerified = data => {
        const updatedData = { ...onboardingObject, ...data };
        const payload = {
            email: updatedData.emailId,
            mobile: updatedData.mobile,
            isEmailVerified,
            isPhoneVerified: isMobileVerified
        };
        dispatch(signupVerifedUser(payload));
    };

    const _submitFinalUser = data => {
        const updatedData = { ...onboardingObject, ...data };
        const user = userStore.payload.user;

        const payload = {
            // existing
            id: user.id,
            // email: user.email,
            // mobile: user.mobile,
            // isEmailVerified: user.isEmailVerified,
            // isPhoneVerified: user.isPhoneVerified,

            // new
            username: updatedData.username,
            name: updatedData.organization,
            streetAddress: updatedData.address,
            country_id: updatedData.country,
            city_id: parseInt(updatedData.city),
            role_id: parseInt(updatedData.userRole),
            user_type_id: parseInt(updatedData.type),
            password: updatedData.password
        };
        console.log(payload);
        dispatch(completeOnobarding(payload));
    };

    const _handleBack = (data = {}) => {
        if (currentStep === 3 || currentStep === 4) {
            updateOnboardingObject({
                ...onboardingObject,
                ...data
            });
        }
        updateCurrentStep(currentStep - 1);
    };

    useEffect(() => {
        if (emailStore.payload) {
            if (emailStore?.payload?.accepted?.length > 0)
                notify(`Email sent to ${emailStore?.payload?.accepted[0]}`);
        } else if (emailStore.error) notify("Failed to send email!");
    }, [emailStore]);

    useEffect(() => {
        if (signupStore.payload)
            if (signupStore.payload.message)
                notify(signupStore.payload.message);
    }, [signupStore]);

    useEffect(() => {
        if (userStore.payload) {
            if (userStore.payload.level) {
                updateCurrentStep(userStore.payload.level);
            }
            if (userStore?.payload?.user) {
                notify(userStore?.payload.message);
            }
            if (userStore?.payload?.user) {
                setEmailVerified(userStore?.payload?.user.isEmailVerified);
                setMobileVerified(userStore?.payload?.user.isPhoneVerified);
                setIsEmailVerifying(false);
            }
        }
        NProgress.done();
    }, [userStore]);

    const _verifyEmailId = payload => {
        dispatch(verifyEmailId(payload));
    };

    const _sendOtp = payload => {
        console.log("in otp", payload);
        dispatch(sendOtp(payload));
    };

    const _verifyOtp = payload => {
        dispatch(verifyOtp(payload));
    };
    const _verifyOrganizationName = name => {
        dispatch(existsByName(name));
    };

    const _verifyUsername = username => {
        dispatch(existsByUsername(username));
    };

    const _fetchCities = code => {
        dispatch(getAllCities(code));
    };

    const _reinitializeWithData = () => {
        if (props.user) {
            const userEmailVerified = {
                emailId: props.user.id,
                mobile: props.user.mobile
            };
            setIsEmailVerifying(false);
            setEmailVerified(props.user.idVerified);
            setMobileVerified(props.user.numVerified);
            _handleNext(userEmailVerified);
        }
    };

    const _notifyCountryResponses = countryStore => {
        if (countryStore.error !== null) notify(`${string.onboarding.failedToLoadCountries}`);
    };

    const _notifyRolesResponses = rolesStore => {
        if (rolesStore.error !== null) notify(`${string.onboarding.failedToLoadRoles}`);
    };

    const _notifyTypesResponses = typesStore => {
        if (typesStore.error !== null) notify(`${string.onboarding.failedToLoadTypes}`);
    };

    useEffect(() => {
        NProgress.start();
        _reinitializeWithData();
        dispatch(getAllCountries());
        dispatch(getAllRoles());
        dispatch(getAllTypes());

        _notifyCountryResponses(countryStore);
        _notifyRolesResponses(rolesStore);
        _notifyTypesResponses(typesStore);
        NProgress.done();
    }, []);

    const _modalClosingActions = () => {
        dispatch(clearUserExistsState());
        dispatch(clearOnboardingSignupState());
        props.router.push("/");
        closeSignupModal();
        NProgress.done();
    };
    return (
        <div className="root-container">
            <Modal
                isOpen={isSignupOpen}
                className="customModal common-model onbordingModal"
                toggle={() => {}}
                size={"lg"}
                modalClassName={"bgModal"}
                centered={true}
            >
                <ModalHeader
                    className="modal-header"
                    toggle={_modalClosingActions}
                >
                    <span>
                        <strong className="font-black">
                            {currentStep === 1
                                ? string.onboarding.signup
                                : currentStep === 2
                                ? string.onboarding.emailAndMobile
                                : currentStep === 3
                                ? string.onboarding.password
                                : currentStep === 4 &&
                                  string.onboarding.userInfo}
                        </strong>
                    </span>
                </ModalHeader>
                <ModalBody className="modal-body">
                    <div className="signup">
                        {currentStep === 1 || currentStep === 2 ? (
                            <OnboardingStepOne
                                currentStep={currentStep}
                                onboardingObject={onboardingObject}
                                mobileStore={mobileStore}
                                userStore={userStore}
                                isEmailVerified={isEmailVerified}
                                setEmailVerified={setEmailVerified}
                                isMobileVerified={isMobileVerified}
                                setMobileVerified={setMobileVerified}
                                isEmailVerifying={isEmailVerifying}
                                setIsEmailVerifying={setIsEmailVerifying}
                                _verifyEmailId={_verifyEmailId}
                                _sendOtp={_sendOtp}
                                _verifyOtp={_verifyOtp}
                                _handleNext={_handleNext}
                                _handleBack={_handleBack}
                            />
                        ) : currentStep === 3 ? (
                            <OnboardingStepTwo
                                currentStep={currentStep}
                                onboardingObject={onboardingObject}
                                _handleNext={_handleNext}
                                _handleBack={_handleBack}
                            />
                        ) : currentStep === 4 ? (
                            <OnboardingStepThree
                                currentStep={currentStep}
                                onboardingObject={onboardingObject}
                                countries={countryStore.payload}
                                cities={cityStore.payload}
                                roles={rolesStore.payload}
                                types={typesStore.payload}
                                signupPayload={signupStore.payload}
                                _fetchCities={_fetchCities}
                                _verifyOrganizationName={
                                    _verifyOrganizationName
                                }
                                _verifyUsername={_verifyUsername}
                                _handleNext={_handleNext}
                                _handleBack={_handleBack}
                            />
                        ) : (
                            currentStep === 5 && <OnboardingStepFour />
                        )}
                        {currentStep === 5 || (
                            <Stepper currentStep={currentStep} />
                        )}
                    </div>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default Signup;

import React, { useEffect } from "react";
import { Col, Form, FormGroup, Label } from "reactstrap";
import PropTypes from "prop-types";
import { useFormik } from "formik";
import * as Yup from "yup";
import * as string from "../../utils/stringConstants/language/eng.json";
import notify from "../../lib/notifier";
import NProgress from "nprogress";
import Button from "../common/form-elements/button/Button";
import Input from "../common/form-elements/input/Input";
import CustomSelect from "../common/form-elements/select/CustomSelect";
import FormHelperMessage from "../common/form-elements/formHelperMessage";
function OnboardingStepThree({
  currentStep,
  onboardingObject,
  countries,
  cities,
  roles,
  types,
  signupPayload,
  _fetchCities,
  _verifyOrganizationName,
  _verifyUsername,
  _handleNext,
  _handleBack,
}) {
  const formik = useFormik({
    initialValues: {
      organization: onboardingObject?.organization || "",
      type: onboardingObject?.type || "",
      country: onboardingObject?.country || "",
      address: onboardingObject?.address || "",
      city: onboardingObject?.city || "",
      username: onboardingObject?.username || "",
      userRole: onboardingObject?.userRole || "",
    },

    validationSchema: Yup.object({
      organization: Yup.string().required(
        `${string.onboarding.organization} ${string.errors.required}`
      ),
      type: Yup.string().required(`${string.onboarding.type} ${string.errors.required}`),
      country: Yup.string().required(`${string.onboarding.country} ${string.errors.required}`),
      address: Yup.string().required(`${string.onboarding.address} ${string.errors.required}`),
      city: Yup.string().required(`${string.onboarding.city} ${string.errors.required}`),
      username: Yup.string().required(`${string.onboarding.username} ${string.errors.required}`),
      userRole: Yup.string().required(`${string.onboarding.userRole} ${string.errors.required}`),
    }),

    onSubmit: (values) => {
      NProgress.start();
      _handleNext({
        ...values,
        currentStep,
      });
    },
  });

  useEffect(() => {
    _fetchCities(formik.values.country);
    return;
  }, [formik.values.country]);

  return (
    <Form onSubmit={formik.handleSubmit} className="form-space thridForm">
      <FormGroup row>
        <Col sm={4}>
          <Label for="organization" className="signup-label-font">
          {string.onboarding.organization}
          </Label>
          <Input
            type="text"
            name="organization"
            id="organization"
            className="form-control"
            value={formik.values.organization}
            onChange={formik.handleChange}
            onBlur={() => _verifyOrganizationName(formik.values.organization)}
          />
          {formik.errors.organization && (
            <>
              <FormHelperMessage
                className="err"
                message={formik.errors.organization}
              />
              <br />
            </>
          )}
          {signupPayload?.nameExists && (
            <FormHelperMessage
              className="err"
              message={string.onboarding.validations.organizationNameAlreadyUsed}
            />
          )}
        </Col>
        <Col sm={4}>
          <Label for="type" className="signup-label-font">
          {string.onboarding.selectType}
          </Label>
          <CustomSelect
            name="type"
            id="type"
            className="form-control user-bg-color"
            value={formik.values.type}
            onChange={formik.handleChange}
          >
            <option value="">{string.onboarding.selectType}</option>
            {types?.map((type) => {
              return (
                <option key={type.id} value={type.id}>
                  {type.name}
                </option>
              );
            })}
          </CustomSelect>
          {formik.errors.type && (
            <FormHelperMessage className="err" message={formik.errors.type} />
          )}
        </Col>
        <Col sm={4}>
          <Label for="country" className="signup-label-font">
          {string.onboarding.country}
          </Label>
          <CustomSelect
            name="country"
            id="country"
            className="form-control user-bg-color"
            value={formik.values.country}
            onChange={formik.handleChange}
          >
            <option value=""> {string.onboarding.selectCounty}</option>
            {countries?.map((country) => {
              return (
                <option key={country.Code} value={country.Code}>
                  {country.Name}
                </option>
              );
            })}
          </CustomSelect>
          {formik.errors.country && (
            <FormHelperMessage
              className="err"
              message={formik.errors.country}
            />
          )}
        </Col>
      </FormGroup>

      <FormGroup row>
        <Col sm={8}>
          <Label for="address" className="signup-label-font">
          {string.onboarding.streetAddress}
         </Label>
          <Input
            type="address"
            name="address"
            id="address"
            className="form-control"
            value={formik.values.address}
            onChange={formik.handleChange}
          />
          {formik.errors.address && (
            <FormHelperMessage
              className="err"
              message={formik.errors.address}
            />
          )}
        </Col>
        <Col sm={4}>
          <Label for="city" className="signup-label-font">
            City
          </Label>
          <CustomSelect
            name="city"
            id="city"
            className="form-control user-bg-color"
            value={formik.values.city}
            onChange={formik.handleChange}
            disabled={cities?.length > 0 ? false : true}
          >
            <option value="">Select City</option>
            {cities?.map((city) => {
              return (
                <option key={city.ID} value={city.ID}>
                  {city.Name}
                </option>
              );
            })}
          </CustomSelect>
          {formik.errors.city && (
            <FormHelperMessage className="err" message={formik.errors.city} />
          )}
        </Col>
      </FormGroup>
      <FormGroup row className="flex-content-center">
        <Col sm={4}>
          <Label for="username" className="signup-label-font">
          {string.onboarding.username}
          </Label>
          <Input
            type="username"
            name="username"
            id="username"
            className="form-control"
            value={formik.values.username}
            onChange={formik.handleChange}
            onBlur={() => _verifyUsername(formik.values.username)}
          />
          {formik.errors.username && (
            <>
              <FormHelperMessage
                className="err"
                message={formik.errors.username}
              />
              <br />
            </>
          )}
          {signupPayload?.usernameExists && (
            <FormHelperMessage
              className="err"
             message= {string.onboarding.validations.usernameAlreadyUsed}
            />
          )}
        </Col>
        <Col sm={4}>
          <Label for="userRole" className="signup-label-font">
          {string.onboarding.userRole}
          </Label>
          <CustomSelect
            name="userRole"
            id="userRole"
            className="form-control user-bg-color"
            value={formik.values.userRole}
            onChange={formik.handleChange}
          >
            <option value="">{string.onboarding.selectRole}</option>
            {roles?.map((role) => {
              return (
                <option key={role.id} value={role.id}>
                  {role.name}
                </option>
              );
            })}
          </CustomSelect>
          {formik.errors.userRole && (
            <FormHelperMessage
              className="err"
              message={formik.errors.userRole}
            />
          )}
        </Col>
      </FormGroup>
      {currentStep === 5 || (
        <div
          className={
            currentStep === 1 ? "flexbox-center" : "flexbox-space-evenly"
          }
        >
          {currentStep === 1 || (
            <Button
              type="button"
              className="btn btn-secondary large-btn"
              onClick={() => _handleBack(formik.values)}
            >
            {string.onboarding.btn.back}
            </Button>
          )}
          <Button type="submit" className="btn btn-primary large-btn">
          {string.onboarding.btn.submit}
          </Button>
        </div>
      )}
    </Form>
  );
}

OnboardingStepThree.propTypes = {
  currentStep: PropTypes.number,
  onboardingObject: PropTypes.object,
  countries: PropTypes.array,
  roles: PropTypes.array,
  types: PropTypes.array,
  signupPayload: PropTypes.object,
  _verifyOrganizationName: PropTypes.func,
  _verifyUsername: PropTypes.func,
  _handleNext: PropTypes.func,
  _handleBack: PropTypes.func,
};

export default OnboardingStepThree;

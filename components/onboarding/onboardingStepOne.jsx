import React, { useState, useEffect } from "react";
import { InputGroup, InputGroupAddon, Col, Form, FormGroup } from "reactstrap";
import { Modal, ModalHeader } from "reactstrap";
import PropTypes from "prop-types";
import { useFormik } from "formik";
import * as Yup from "yup";
import * as string from "../../utils/stringConstants/language/eng.json";
import notify from "../../lib/notifier";
import NProgress from "nprogress";
import Button from "../common/form-elements/button/Button";
import Input from "../common/form-elements/input/Input";
import FormHelperMessage from "../common/form-elements/formHelperMessage";

function OnboardingStepOne({
  currentStep,
  onboardingObject,
  mobileStore,
  emailStore,
  userStore,
  isEmailVerified,
  setEmailVerified,
  isMobileVerified,
  setMobileVerified,
  isEmailVerifying,
  setIsEmailVerifying,
  _verifyEmailId,
  _sendOtp,
  _verifyOtp,
  _handleNext,
  _handleBack,
}) {
  NProgress.configure({ easing: "ease", speed: 1000 });

  const [verifyEmailMessage, setVerifyEmailMessage] = useState(true);
  const [verifyMobileMessage, setVerifyMobileMessage] = useState(true);

  // Formik Validations and initialization
  const phoneRegExp = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/; // 903 538 2399 || 9035382399
  const otpRegExp = /^\(?([0-9]{6})\)?[-. ]?([0]{0})[-. ]?([0]{0})$/; // 123456 (RegEx will changed, temporary used)
  // const otpRegExp = /^[0-9]{6}(\s*,*,\s*[0-9]{6})*/;

  const formik = useFormik({
    initialValues: {
      emailId: onboardingObject?.emailId || "",
      mobile: onboardingObject?.mobile || "",
      otp: "",
    },
    validationSchema: Yup.object({
      emailId: Yup.string()
        .email()
        .required(
          `${string.onboarding.validations.emailId} ${string.errors.required}`
        ),
      mobile: Yup.string()
        .matches(
          phoneRegExp,
          `${string.onboarding.validations.mobileNo} ${string.onboarding.validations.isNotValid}`
        )
        .required(
          `${string.onboarding.validations.mobileNo} ${string.errors.required}`
        ),
      otp: Yup.string().matches(
        otpRegExp,
        `${string.onboarding.validations.otp} ${string.onboarding.validations.isNotValid}`
      ),
    }),

    onSubmit: (values) => {
      NProgress.start();
      _handleNext({
        ...values,
        currentStep,
      });
    },
  });

  const _handleVerification = (event) => {
    const { name } = event.target;
    console.log("in hndle", name, verifyMobileMessage)
    switch (name) {
      case "emailId":
        if (verifyEmailMessage) {
          setIsEmailVerifying(true);
          _verifyEmailId({
            emailId: formik.values.emailId,
            mobile: formik.values.mobile,
            numVerified: isMobileVerified,
          });
        }
        setVerifyEmailMessage(!verifyEmailMessage);
        break;
      case "mobile":
        if (verifyMobileMessage) {
          NProgress.start();
          _sendOtp({
            number: parseInt(formik.values.mobile.trim()),
            countrycode: "+91",
            ismobile: "false",
          });
          setVerifyMobileMessage(!verifyMobileMessage);
        }
        break;
      case "verify":
        if (!verifyMobileMessage) {
          NProgress.start();
          console.log("in verify")
          _verifyOtp({
            number: parseInt(formik.values.mobile.trim()),
            otp: parseInt(formik.values.otp.trim()),
            ismobile: "false",
          });
        }
        setMobileVerified(!isMobileVerified);
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    if (userStore.payload) {
      notify(userStore.payload.message);
      NProgress.done();
    } else if (userStore.error) {
      if (userStore.error.exception) {
        notify(`500 - ${JSON.stringify(userStore.error)}`);
      } else {
        notify(userStore.error.message);
      }
      NProgress.done();
    }
  }, [userStore]);

  useEffect(() => {
    if (mobileStore.payload) {
      if (mobileStore.payload.verified) {
        console.log("in resend")
        setMobileVerified(mobileStore.payload.verified);
        setVerifyMobileMessage(!verifyMobileMessage);
      }
      notify(mobileStore.payload.message);
      NProgress.done();
    } else if (mobileStore.error) {
      if (!mobileStore.error.verified) {
        setMobileVerified(mobileStore.error.verified);
      }
      if (mobileStore.error.exception) {
        notify(`500 - ${JSON.stringify(mobileStore.error)}`);
      } else {
        notify(mobileStore.error.message);
      }
      NProgress.done();
    }
    return;
  }, [mobileStore]);

  return (
    <Form onSubmit={formik.handleSubmit} className="form-space first">
      <FormGroup row className="flex-content-center">
        <div className="signUp-field">
          <div className="input-wrap d-flex">
            <label className="signup-label-font" htmlFor="signup-email">
              {string.onboarding.validations.emailId}
            </label>
            {currentStep === 2 && (
              <FormHelperMessage
                className={
                  isEmailVerified
                    ? "success padding-left"
                    : "warning padding-left"
                }
              >
                {isEmailVerified ? (
                  <>
                    <i className="fas fa-check"></i>
                    {string.onboarding.validations.verified}
                  </>
                ) : (
                  <>
                    <i className="fas fa-exclamation-triangle"></i>
                    {string.onboarding.validations.notVerified}
                  </>
                )}
              </FormHelperMessage>
            )}
          </div>
          <Input
            type="text"
            onChange={formik.handleChange}
            className="form-control form-control-signup"
            id="signup-email"
            name="emailId"
            value={formik.values.emailId}
            disabled={currentStep === 2 && true}
          />
          {formik.errors.emailId && (
            <FormHelperMessage
              className="err"
              message={formik.errors.emailId}
            />
          )}
        </div>

        <div className="signUp-field">
          <div className="input-wrap d-flex">
            <label className="signup-label-font" htmlFor="signup-mobile">
              {string.onboarding.validations.mobileNo}{" "}
              {currentStep === 1 && (
                <span className="font-fade-out">
                  {string.onboarding.validations.useCountryCode}
                </span>
              )}
            </label>
            {currentStep === 2 && (
              <FormHelperMessage
                className={
                  isMobileVerified
                    ? "success padding-left"
                    : "warning padding-left"
                }
              >
                {isMobileVerified ? (
                  <>
                    <i className="fas fa-check"></i>
                    {string.onboarding.validations.verified}
                  </>
                ) : (
                  <>
                    <i className="fas fa-exclamation-triangle"></i>
                    {string.onboarding.validations.notVerified}
                  </>
                )}
              </FormHelperMessage>
            )}
          </div>
          <Input
            type="text"
            onChange={formik.handleChange}
            className="form-control form-control-signup"
            id="signup-mobile"
            name="mobile"
            value={formik.values.mobile}
            disabled={currentStep === 2 && true}
          />
          {formik.errors.mobile && (
            <>
              <FormHelperMessage
                className="err"
                message={formik.errors.mobile}
              />
              <br />
            </>
          )}
          {currentStep === 1 && (
            <span className="font-fade-out">
              {string.onboarding.validations.useOnlyNumbers}
            </span>
          )}
        </div>
      </FormGroup>

      {currentStep === 2 && (
        <>
          <FormGroup row className="flex-content-center margin-t-3em">
            <div className="signUp-field">
              {verifyEmailMessage || (
                <div className="verification-container">
                  <span className="verification-link text-center">
                  {string.onboarding.VerificationlinkSent}
                   <br /> {string.onboarding.toYourEmail}
                  
                  </span>
                </div>
              )}
              <Button
                type="button"
                className="linear-gradient-btn"
                name="emailId"
                onClick={(event) => _handleVerification(event)}
                disabled={isEmailVerified}
              >
                {verifyEmailMessage
                  ? "Verify E-mail"
                  : "Resend the verification link"}
              </Button>
              {isEmailVerifying && (
                <Modal
                  isOpen={isEmailVerifying}
                  className="customModal"
                  toggle={() => {}}
                  size={"sm"}
                >
                  <ModalHeader
                    className="modal-header"
                    toggle={() => {}}
                  ></ModalHeader>
                  <span className="grid-place-items-center-verification">
                  {string.onboarding.pleaseVerifyEmail}
                     <br />{string.onboarding.beforeContinue}
                  </span>
                </Modal>
              )}
            </div>
            <div className="signUp-field">
              {verifyMobileMessage || (
                <div className="verification-container">
                  <span className="verification-link">
                  {string.onboarding.VerificationCodeSent}
                   <br /> {string.onboarding.viaSms}
                  </span>
                </div>
              )}
              <Button 
                type="button"
                className="linear-gradient-btn"
                name="mobile"
                onClick={(event) => _handleVerification(event)}
                // title="Please verify Email Id"
                disabled={isMobileVerified}
              >
                {verifyMobileMessage
                  ? `${string.onboarding.verifyPhoneNo}`
                  : `${string.onboarding.resendTheVerificationCode}`}
              </Button>
            </div>
          </FormGroup>

          {verifyMobileMessage || (
            <FormGroup row className="flex-content-center">
              <Col sm={4}></Col>
              <Col sm={4}>
                <InputGroup size="sm">
                  <Input
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.otp}
                    className="form-control form-control-signup"
                    id="signup-mobile-verify"
                    name="otp"
                    placeholder="Enter verification code"
                  />
                  <InputGroupAddon addonType="append">
                    <Button
                      type="button"
                      className="onboarding-verify-btn"
                      name="verify"
                      onClick={(event) => _handleVerification(event)}
                    >
                      <span
                        className="onboarding-verify-btn-icon"
                        // onClick={(event) => _handleVerification(event)}
                      >
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </Button>
                  </InputGroupAddon>
                </InputGroup>
                {formik.errors.otp && (
                  <FormHelperMessage
                    className="err"
                    message={formik.errors.otp}
                  />
                )}
              </Col>
            </FormGroup>
          )}
        </>
      )}
      <div
        className={
          currentStep === 1 ? "flexbox-center" : "flexbox-space-evenly"
        }
      >
        {currentStep === 1 || (
          <Button
            type="button"
            className="btn btn-secondary large-btn"
            onClick={_handleBack}
          >
            {string.onboarding.btn.back}
          </Button>
        )}
        <Button
          type="submit"
          className="btn btn-primary large-btn"
          disabled={
            currentStep === 2 ? !isEmailVerified || !isMobileVerified : false
          }
        >
          {string.onboarding.btn.next}
        </Button>
      </div>
    </Form>
  );
}
OnboardingStepOne.propTypes = {
  currentStep: PropTypes.number,
  onboardingObject: PropTypes.object,
  mobileStore: PropTypes.object,
  emailStore: PropTypes.object,
  userStore: PropTypes.object,
  isEmailVerified: PropTypes.bool,
  setEmailVerified: PropTypes.func,
  isMobileVerified: PropTypes.bool,
  setMobileVerified: PropTypes.func,
  isEmailVerifying: PropTypes.bool,
  setIsEmailVerifying: PropTypes.func,
  _verifyEmailId: PropTypes.func,
  _sendOtp: PropTypes.func,
  _verifyOtp: PropTypes.func,
  _handleNext: PropTypes.func,
  _handleBack: PropTypes.func,
};

export default OnboardingStepOne;

import { Grid } from "@material-ui/core";
import React from "react";
// import User from "../../static/assets/user-onboarding.png";
// import User from "../../static/assets/user-onboarding.svg";

function OnboardingStepFour() {
  return (
    <div className="grid-place-items-center">
      <div className="userSuccess">
        <i className="fa fa-user" />
        <span className="successUser">
          <i className="fa fa-check"/>
        </span>
      </div>
      <p className="thank-font">
      {string.onboarding.thanksForSignin} <br /> {string.onboarding.yourInfoUnderReview}{" "}
         
      </p>
    </div>
  );
}

export default OnboardingStepFour;

import React from "react";
import PropTypes from "prop-types";
import { useFormik } from "formik";
import * as Yup from "yup";
import * as string from "../../utils/stringConstants/language/eng.json";
import Button from "../common/form-elements/button/Button";
import Input from "../common/form-elements/input/Input";
import FormHelperMessage from "../common/form-elements/formHelperMessage";
import { Label, Form } from "reactstrap";

function OnboardingStepTwo({
  currentStep,
  onboardingObject,
  _handleNext,
  _handleBack,
}) {
  const formik = useFormik({
    initialValues: {
      password: onboardingObject?.password || "",
      passwordConfirmation: onboardingObject?.passwordConfirmation || "",
    },

    validationSchema: Yup.object({
      password: Yup.string()
        .required(`${string.onboarding.password} ${string.errors.required}`)
        .min(8, `${string.onboarding.validations.passwordShouldBeEight}`)
        .matches(/[0-9a-zA-Z]/, `${string.onboarding.validations.passwordDoesNotMatch}`),
      passwordConfirmation: Yup.string()
        .oneOf([Yup.ref("password"), null], `${string.onboarding.validations.passwordsMustMatch}`)
        .required(`${string.onboarding.validations.passwordConfirmation} ${string.errors.required}`),
    }),

    onSubmit: (values) => {
      _handleNext({ ...values, currentStep });
    },
  });
  return (
    <Form onSubmit={formik.handleSubmit} className="form-space second">
      <div className="flexbox-top">
        <div className="form-group signUp-field">
          <Label className="signup-label-font" for="signup-password">
          {string.onboarding.passWord}
          </Label>
          <Input
            type="password"
            onChange={formik.handleChange}
            className="form-control form-control-signup"
            id="signup-password"
            name="password"
            value={formik.values.password}
          />
          {formik.errors.password && (
            <>
              <FormHelperMessage
                className="err"
                message={formik.errors.password}
              />
              <br />
            </>
          )}
          <span className="font-fade-out">
          {string.onboarding.minimumChar}
          </span>
        </div>
        <div className="form-group signUp-field">
          <Label
            className="signup-label-font"
            for="signup-passwordConfirmation"
          >
          {string.onboarding.validations.passwordConfirmation}
          </Label>
          <Input
            type="password"
            onChange={formik.handleChange}
            className="form-control form-control-signup"
            id="signup-passwordConfirmation"
            name="passwordConfirmation"
            value={formik.values.passwordConfirmation}
          />
          {formik.errors.passwordConfirmation && (
            <FormHelperMessage
              className="err"
              message={formik.errors.passwordConfirmation}
            />
          )}
        </div>
      </div>
      {currentStep === 5 || (
        <div
          className={
            currentStep === 1 ? "flexbox-center" : "flexbox-space-evenly"
          }
        >
          {currentStep === 1 || (
            <Button
              type="button"
              className="btn btn-secondary large-btn"
              onClick={() => _handleBack(formik.values)}
            >
            {string.onboarding.btn.back}
             
            </Button>
          )}
          <Button type="submit" className="btn btn-primary large-btn">
          {string.onboarding.btn.next}
          </Button>
        </div>
      )}
    </Form>
  );
}

OnboardingStepTwo.propTypes = {
  currentStep: PropTypes.number,
  onboardingObject: PropTypes.object,
  _handleNext: PropTypes.func,
  _handleBack: PropTypes.func,
};

export default OnboardingStepTwo;

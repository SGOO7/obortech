import React from "react";
const Stepper = ({ currentStep }) => {
  return (
    <div className="flex-content-center margin-tb">
      <span
        className={
          currentStep === 1 ? "step-dots step-dots-active" : "step-dots"
        }
      ></span>
      <span
        className={
          currentStep === 2 ? "step-dots step-dots-active" : "step-dots"
        }
      ></span>
      <span
        className={
          currentStep === 3 ? "step-dots step-dots-active" : "step-dots"
        }
      ></span>
      <span
        className={
          currentStep === 4 ? "step-dots step-dots-active" : "step-dots"
        }
      ></span>
    </div>
  );
};

export default Stepper;

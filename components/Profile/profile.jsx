import React, { useState } from "react";
import ProfileCard from "../Profile/ProfileCard";
import ProfileDetails from "../Profile/profileDetails";
import { Modal,ModalHeader } from "reactstrap";
import Button from "../../components/common/form-elements/button/Button";
import Link from 'next/link';
import "./profile.css";
import "../../static/css/modal.css"
function Profile(props) {
  const {
    buttonLabel,
   
  } = props;

  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  return (
    <div className="profile-modal-wrap"> 
    <Button className="modal-btn" onClick={toggle}>Profile</Button>
    <Modal isOpen={modal} toggle={toggle} className="modal-lg customModal profile-model">
    
      <div className="profile-block d-flex flex-column align-items-center">
        <div className="details-wrap">
        <ModalHeader className=" modal-Header justify-content-center title-profile" toggle={toggle}>PROFILE</ModalHeader>
        
          <div className="card-wrap"> 
            <ProfileCard
              heading="Mongolian Express"
              caption="Freight Forwarder"
              label="level 1"
              icon="fa fa-pencil-alt icon"
            />
            <ProfileCard
              heading="Jester_Tamir001"
              caption="Logistics Manager"
              label="level 1"
              icon="fa fa-pencil-alt icon"
            />
          </div>
          <div className="profile-details">
            <ProfileDetails heading="User ID" caption="MNX001" />
            <ProfileDetails heading="E-mail:" caption="Jester@monex.mn" />
            <ProfileDetails heading="Mobile phone:" caption="976-99058265" />
            <ProfileDetails heading="Country:" caption="Mongolia" />
            <ProfileDetails heading="City:" caption="Ulaanbaatar" />
            <ProfileDetails
             className="street-block"
              heading="Street address: "
              caption="4th floor, Khasvuu plaza, Automzamchidiin Street, 
                1st khoroo, Sukhbaatar district,"
            />
          </div>
          <div className="profile-authentication d-flex justify-content-between">
            <ProfileDetails
              heading="Two-factor Authentication: "
              caption="Enabled"
            />
            <ProfileDetails heading="SMS Authentication:" caption="Enabled" />
          </div>
        </div>
        <Button className="btn btn-primary large-btn">Edit Profile</Button>
      </div>
      
    </Modal>

    </div>
    
  );
}
export default Profile;

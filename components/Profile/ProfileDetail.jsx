import React from 'react';
function ProfileDetails(props){
    return(
        <p>{props.heading}: <span>{props.caption}</span></p>
    )
}
export default ProfileDetails;
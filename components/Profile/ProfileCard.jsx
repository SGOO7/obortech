import React from 'react';
function ProfileCard(props){
    return(
        <div className="profile-card d-flex justify-content-between">
            <div className="d-flex">
              <i className={props.icon} aria-hidden="true"></i>
              <div className="text-wrap">
                <h3>{props.heading}</h3>
                <span>{props.caption}</span>
              </div>
            </div>
            <div className="label-wrap">
              <span className="label-holder">{props.label}</span>
            </div>
          </div>
    )
}
export default ProfileCard;
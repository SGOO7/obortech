const folder = [
  ///////////////// 1
  {
    id: 1,
    name: "fdgdfgdadd",
    subFolders: [
      {
        id: 2,
        name: "dsfdsf",
        parent: 1,
        shipments: [
          {
            id: 3,
            parent: 2,
            shipment: { id: 1, name: "Shipment A" },
          },
          {
            id: 4,
            parent: 2,
            shipment: { id: 1, name: "Shipment A" },
          },
        ],
      },
      {
        id: 5,
        name: "Jalaj",
        parent: 1,
        shipments: [
          {
            id: 15,
            parent: 5,
            shipment: { id: 1, name: "Shipment A" },
          },
        ],
      },
    ],

    shipments: [],
  },
  ///////////////// 2
  {
    id: 9,
    name: "Folder Test",

    subFolders: [
      {
        id: 13,
        name: "Sub-2",
        parent: 9,

        shipments: [
          { id: 14, parent: 13, shipment: { id: 1, name: "Shipment A" } },
        ],
      },
    ],

    shipments: [
      {
        id: 10,
        name: null,
        parent: 9,

        shipment: { id: 1, name: "Shipment A" },
      },
    ],
  },
  ///////////////// 3
  {
    id: 6,
    name: "NewFolderTest1-V1",
    subFolders: [
      {
        name: "SubFolder",
        parent: 6,

        shipments: [
          {
            id: 16,
            parent: 8,

            shipment: { id: 2, name: "sdsfd" },
          },
          {
            id: 17,
            parent: 8,

            shipment: { id: 3, name: "Jalaj" },
          },
        ],
      },
    ],
    shipments: [
      {
        id: 11,
        name: null,
        parent: 6,

        shipment: { id: 3, name: "Jalaj" },
      },
    ],
  },
  ///////////////// 4
  {
    id: 12,
    name: "issue-fix-folder-",
    subFolders: [],
    shipments: [],
  },
];

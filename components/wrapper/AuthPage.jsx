import Footer from "../HomeFooter";
import Header from "../HomeHeader";
import Sidebar from "../Sidebar";
import SeoMetaData from "../SeoMetaData";
import string from "../../utils/stringConstants/language/eng.json";

const AuthPage = (props) => {
  return (
    <div id="page-top">
      <SeoMetaData meta_data={{ title: props.title }} />
      {/* Page Wrapper */}
      <div id="wrapper">
        {/* Sidebar */}
        <Sidebar />
        {/* End of Sidebar */}
        {/* Content Wrapper */}
        <div id="content-wrapper" className="d-flex flex-column">
          {/* Main Content */}
          <div id="content">
            {/* Topbar */}
            <Header user={props.user} />
            {props.children}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default AuthPage;

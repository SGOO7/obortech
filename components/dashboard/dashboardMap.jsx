import { MapContainer, TileLayer, Marker, Popup, Polyline } from 'react-leaflet'
import L from 'leaflet'
import 'leaflet/dist/leaflet.css';
import { useEffect, useState } from 'react';

const DashboardMap = ({ mapMarker, polylines, startMarker, endMarker}) => {
    const [zoom, setZoom] = useState(20);

    if (typeof window === 'undefined') {
        return null;
    }else {

        // const _getMarkers = data => {
            const completedIcon = L.icon({
                iconUrl: "/static/img/red.png",
                iconSize: [21, 30], // size of the icon
                iconAnchor: [21, 30], // point of the icon which will correspond to marker's location
                popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
            })
            const notCompletedIcon = L.icon({
                iconUrl: "/static/img/green.png",
                iconSize: [21, 30], // size of the icon
                iconAnchor: [21, 30], // point of the icon which will correspond to marker's location
                popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
            })
            const oneIcon = L.icon({
                iconUrl: '/static/img/1.png',
                iconSize: [20, 20],
                iconAnchor: [20, 51],
                popupAnchor: [0, -51]
            });

            const twoIcon = L.icon({
                iconUrl: '/static/img/2.png',
                iconSize: [20, 20],
                iconAnchor: [-5, 21],
                popupAnchor: [0, -51]
            });
        // }
        return (
            // <div className>
                <MapContainer center={[polylines[polylines.length - 1].fromLat, polylines[polylines.length - 1].fromLong]} zoom={zoom} scrollWheelZoom={false}>
                    <TileLayer
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=cd3cfc9365144a9d81dab3e08e080c68"
                    />
                    {/* Current truck position marker */}
                    <Marker position={mapMarker} icon={completedIcon}>
                        <Popup>
                            Current
                        </Popup>
                    </Marker>
                    {/* Start position marker */}
                    <Marker position={startMarker.pos} icon={oneIcon}>
                        <Popup>
                            {startMarker.name}
                        </Popup>
                    </Marker>
                    {/* End position marker */}
                    <Marker position={endMarker.pos} icon={twoIcon}>
                        <Popup>
                            {endMarker.name}
                        </Popup>
                    </Marker>
                    {
                        polylines?.map(({fromLat, fromLong, toLat, toLong}, idx) => {
                            if(toLat){
                                return (
                                    <Polyline
                                        key={idx}
                                        positions={[
                                            [fromLat, fromLong],[toLat, toLong]
                                        ]}
                                        color={'red'}
                                    />
                                )
                            } else{
                                return null;
                            }
                        })
                    }
                </MapContainer>
            // </div>
        )
    }
}

export default DashboardMap;
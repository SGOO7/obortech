import { useEffect, useState } from "react";
import DragSortableList from "react-drag-sortable";
import PropTypes from "prop-types";
import CustomSelect from "../common/form-elements/select/CustomSelect";

export const DraggableItem = (props) => {
  const [list, setList] = useState([]);
  const { className } = props;
  const renderItem = (selectedRoad, i) => {
    return (
      // <div style={{ touchAction: 'none' }}>
      <tr key={i} id={selectedRoad?.road_id}>
        <td>
          <i className="fa fa-sort"></i>
        </td>
        <td width="200">
          <CustomSelect
            className="form-control"
            onChange={(event) => {
              event.preventDefault();
              let tmpSelectedRoads = props.listItems;
              tmpSelectedRoads[i].road_id = event.target.value;
              props.setState({
                shipment: Object.assign({}, props.shipment, {
                  selectedRoads: tmpSelectedRoads,
                }),
              });
            }}
            value={selectedRoad.road_id || "0"}
            options={props.roads}
          />
        </td>
        <td width="150">
          <span>
            {selectedRoad.road_id
              ? props.roads.find((x) => x.id == selectedRoad.road_id).latitude
              : "Select Road"}
          </span>
        </td>
        <td width="150">
          <span>
            {selectedRoad.road_id
              ? props.roads.find((x) => x.id == selectedRoad.road_id).longitude
              : "Select Road"}
          </span>
        </td>
        <td width="150">
          {selectedRoad.road_id
            ? props.roads.find((x) => x.id == selectedRoad.road_id).radius
            : "Select Road"}
        </td>
        <td width="30">
          {/* <i className="saveBtn"></i> */}
          <i
            className="deleteBtn"
            onClick={(event) => {
              event.preventDefault();
              let selectedRoads = props.listItems;
              let shipment = props.shipment;
              selectedRoads.splice(i, 1);
              shipment.selectedRoads = selectedRoads;
              props.setState({
                shipment: Object.assign({}, props.shipment, { selectedRoads }),
              });
            }}
          ></i>
        </td>
      </tr>
      // </div>
    );
  };
  useEffect(() => {
    const items = [];
    props.listItems.map((it, i) => {
      items.push({ content: renderItem(it, i) });
    });
    setList(items);
  }, [props.listItems.length, props.shipment]);
  const placeholder = () => (
    <div className="placeholderContent">PLACEHOLDER</div>
  );
  const onSort = (list) => {
    const arr = props.shipment.selectedRoads.map((rd) => {
      const selectRank = list.find(
        (srd) => srd.content.props.id === rd.road_id
      );
      if (selectRank) {
        rd.order = selectRank.rank;
      }
      return rd;
    });
    props.setState({
      shipment: Object.assign({}, props.shipment, {
        selectedRoads: arr.sort((a, b) => {
          return a.order - b.order;
        }),
      }),
    });
  };
  return (
    <>
      <DragSortableList
        items={list}
        onSort={onSort}
        dropBackTransitionDuration={0.3}
        placeholder={placeholder}
      />
    </>
  );
};

DraggableItem.propTypes = {
  className: PropTypes.string,
};

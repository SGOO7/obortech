import Link from "next/link";
import { useRouter } from "next/router";
import notify from "../lib/notifier";
import React, { useState, useEffect, useRef } from "react";

import DeleteModal from "./common/DeleteModal";
import {
    fetchFolders,
    fetchSideMenuShipments,
    assignShipment,
    releaseShipment,
    addFolder,
    updateFolder,
    removeFolder,
} from "../lib/api/shipment";
// import Select from "react-select";
import AdvanceSelect from "./common/form-elements/select/AdvanceSelect";
import Input from "./common/form-elements/input/Input";

import { useOutsideClick } from "../utils/customHooks/useClickOutside";

function Sidebar() {

    const [openDelete, setOpenDelete] = useState(false);
    const [sideMenuData, setSideMenuData] = useState([]);
    const [shipmentOption, setShipmentOption] = useState([]);
    const [deleteArgs, setDeleteArgs] = useState();
    const [subFolderEditMode, setSubFolderEditMode] = useState(false);
    const [subFolderEditValue, setSubFolderEditValue] = useState(null);
    const [folderEditMode, setFolderEditMode] = useState(false);
    const [folderEditValue, setFolderEditValue] = useState(null);
    const [folderValue, setFolderValue] = useState(null);
    const moreOptionRef = useRef(null);
    const moreOptionSubRef = useRef(null);
    const moreOptionSubShipmentRef = useRef(null);
    const moreOptionShipmetRef = useRef(null);
    const moreOutside = useOutsideClick(moreOptionRef);
    const moreOutsideSub = useOutsideClick(moreOptionSubRef);
    const moreOutsideSubShipment = useOutsideClick(moreOptionSubShipmentRef);
    const moreOutsideShipment = useOutsideClick(moreOptionShipmetRef);

    const router = useRouter();
    let { shipment_id } = router.query;
    if (typeof window === 'undefined') {
        return null;
    } else {
        if (!shipment_id) {
            if (window.localStorage.getItem("shipment_id")) {
                shipment_id = window.localStorage.getItem("shipment_id");
            }
        }
    }

    const _toggleEdit = (E, id, subFolderId, idx) => {
        let arr = sideMenuData;
        if (subFolderId) {
            _manualToggleCollapse(`collapse-${subFolderId}-${idx}`, false, "show");
            _manualToggleCollapse(`collapsed${subFolderId}`, false, "collapsed");
            arr = arr.map((it) => {
                if (it.id === id) {
                    it.subFolders = it.subFolders.map((subFolder) => {
                        if (subFolder.id === subFolderId) {
                            setFolderValue(subFolder.name);
                            setSubFolderEditValue(subFolder.name);
                            setSubFolderEditMode(true);
                            // subFolder.name = null
                            subFolder.edit = true;
                        }
                        return subFolder;
                    });
                    return it;
                }
            });
        } else {
            _manualToggleCollapse(`collapse-${id}-${idx}`, false, "show");
            _manualToggleCollapse(`collapsed${id}`, false, "collapsed");
            arr = arr.map((it) => {
                if (it.id === id) {
                    setFolderValue(it.name);
                    setFolderEditValue(it.name);
                    setFolderEditMode(true);
                    // it.name = null;
                    it.edit = true;
                }
                return it;
            });
        }
    };

    const _manualToggleCollapse = (id, open, className) => {
        const elem = document.getElementById(id);
        if (elem) {
            if (open) {
                elem.classList.add(className);
            } else {
                elem.classList.remove(className);
            }
        }
    };

    const _toggleAdd = (e, id, subFolderId, isShip, idx) => {
        let arr = sideMenuData;
        const newObj = {
            id: Math.floor(Math.random() * 1000),
            name: null,
            subFolders: [],
            shipments: [],
        };
        if (subFolderId) {
            _manualToggleCollapse(`collapsed${subFolderId}`, false, "collapsed");
            _manualToggleCollapse(`collapse${subFolderId}`, true, "show");
            _manualToggleCollapse(`collapse-${subFolderId}-${idx}`, false, "show");

            arr = arr.map((it) => {
                if (it.id === id) {
                    if (it.subFolders.some((subFolder) => subFolder.id === subFolderId)) {
                        it.subFolders = it.subFolders.map((subFolder) => {
                            if (subFolder.id === subFolderId) {
                                subFolder.shipments = subFolder.shipments.concat({
                                    id: Math.floor(Math.random() * 1000),
                                    name: null,
                                    shipment: {
                                        name: null,
                                    },
                                    edit: true,
                                    parent: subFolderId,
                                });
                            }
                            return subFolder;
                        });
                    } else {
                        it.shipments = it.shipments.concat({
                            id: Math.floor(Math.random() * 1000),
                            name: null,
                            shipment: {
                                name: null,
                            },
                            edit: true,
                            parent: id,
                        });
                    }
                }
                return it;
            });
        } else if (id) {
            _manualToggleCollapse(`collapse${id}`, true, "show");
            _manualToggleCollapse(`collapse-${id}-${idx}`, false, "show");
            _manualToggleCollapse(`collapsed${id}`, false, "collapsed");
            arr = arr.map((it) => {
                if (it.id === id) {
                    if (isShip) {
                        it.shipments = it.shipments.concat({
                            id: Math.floor(Math.random() * 1000),
                            name: null,
                            shipment: {
                                name: null,
                            },
                            edit: true,
                            parent: id,
                        });
                    } else {
                        it.subFolders = it.subFolders.concat({
                            ...newObj,
                            edit: true,
                            parent: id,
                        });
                    }
                }
                return it;
            });
            // setOpenAdd(!openAdd)
        } else {
            arr = arr.concat({ ...newObj, edit: true });
        }
        setSideMenuData(arr);
    };

    const _toggleDelete = (e, id, deleteId, subFolderId, idx) => {
        setDeleteArgs({
            mainId: id,
            deleteId,
            subFolderId,
            idx,
        });
        setOpenDelete(!openDelete);
    };

    const _deleteRecord = (e) => {
        const args = deleteArgs;
        let arr = sideMenuData;
        if (args) {
            if (args.deleteId) {
                if (args.subFolderId) {
                    arr = arr.map((it) => {
                        if (it.id === args.mainId) {
                            it.subFolders = it.subFolders.map((subIt) => {
                                if (subIt.id === args.subFolderId) {
                                    subIt.shipments = subIt.shipments.filter(
                                        (shipment) => shipment.id !== args.deleteId
                                    );
                                    releaseShipment({
                                        parent: args.subFolderId,
                                        shipment_id: args.deleteId,
                                    });
                                }
                                return subIt;
                            });
                        }
                        return it;
                    });
                    _manualToggleCollapse(
                        `collapse-${args.subFolderId}-${args.idx}`,
                        false,
                        "show"
                    );
                } else {
                    arr = arr.map((it) => {
                        it.shipments = it.shipments.filter(
                            (shipment) => shipment.id !== args.deleteId
                        );
                        releaseShipment({
                            parent: args.mainId,
                            shipment_id: args.deleteId,
                        });
                        return it;
                    });
                    _manualToggleCollapse(
                        `collapse-${args.deleteId}-${args.idx}`,
                        false,
                        "show"
                    );
                }
            } else {
                if (args.subFolderId) {
                    arr = arr.map((it) => {
                        if (it.id === args.mainId) {
                            it.subFolders = it.subFolders.filter((subIt) => {
                                if (subIt.id != args.subFolderId) {
                                    return subIt;
                                }
                            });
                            removeFolder({ id: args.subFolderId });
                        }
                        return it;
                    });
                    _manualToggleCollapse(
                        `collapse-${args.subFolderId}-${args.idx}`,
                        false,
                        "show"
                    );
                } else {
                    arr = arr.filter((it) => {
                        if (it.id !== args.mainId) {
                            return it;
                        }
                    });
                    removeFolder({ id: args.mainId });
                    _manualToggleCollapse(
                        `collapse-${args.mainId}-${args.idx}`,
                        false,
                        "show"
                    );
                }
            }
            setSideMenuData(arr);
            _toggleDelete(e);
        }
    };

    const fetchSideMenu = async () => {
        const data = await fetchFolders();
        if (data.length > 0) {
            setSideMenuData(data);
        }
    };

    const fetchShipment = async () => {
        const shipments = await fetchSideMenuShipments();
        if (shipments.length > 0) {
            let shipmentoptions = [];
            shipments.map((shipment) => {
                let optoinObj = {
                    label: shipment.shipment.name,
                    value: shipment.shipment.id
                }
                shipmentoptions.push(optoinObj);
            })
            setShipmentOption(shipmentoptions);
        }
    };

    useEffect(() => {
        // _manualToggleCollapse();
        fetchSideMenu();
        fetchShipment();
    }, []);

    useEffect(() => {
        const {
            query: { shipment_id },
        } = router;
        const folders = [...sideMenuData];

        if (shipment_id) {
            if (folders.length > 0) {
                folders.forEach((folder) => {
                    const folderCollapsed = `collapsed${folder.id}`;
                    const folderCollapse = `collapse${folder.id}`;
                    if (folder.subFolders.length > 0)
                        folder.subFolders.forEach((subFolder) => {
                            const subFolderCollapsed = `collapsed${subFolder.id}`;
                            const subFolderCollapse = `collapse${subFolder.id}`;
                            subFolder.shipments.forEach((shipment) => {
                                if (shipment["shipment"]["id"])
                                    if (shipment["shipment"]["id"] == shipment_id) {
                                        // Folder Expand
                                        _manualToggleCollapse(folderCollapsed, true, "collapsed");
                                        _manualToggleCollapse(folderCollapse, true, "show");

                                        // Sub Folder Expand
                                        _manualToggleCollapse(
                                            subFolderCollapsed,
                                            true,
                                            "collapsed"
                                        );
                                        _manualToggleCollapse(subFolderCollapse, true, "show");
                                    }
                            });
                        });
                    if (folder.shipments.length > 0)
                        folder.shipments.forEach((shipment) => {
                            const subShipmentCollapsed = `collapsed${shipment.id}`;
                            const subShipmentCollapse = `collapse${shipment.id}`;
                            if (shipment["shipment"]["id"])
                                if (shipment["shipment"]["id"] == shipment_id) {
                                    // Folder Expand
                                    _manualToggleCollapse(folderCollapsed, true, "collapsed");
                                    _manualToggleCollapse(folderCollapse, true, "show");

                                    // Sub Folder Expand
                                    _manualToggleCollapse(subShipmentCollapsed, true, "collapsed");
                                    _manualToggleCollapse(subShipmentCollapse, true, "show");
                                }
                        });
                });
            }
        }
    }, [sideMenuData]);

    useEffect(() => {
        if (moreOutside && document.getElementById(`collapse-${moreOptionRef?.current?.id}`)) {
            document.getElementById(`collapse-${moreOptionRef?.current?.id}`).classList.remove("show");
        }
        if (moreOutsideSubShipment && document.getElementById(`collapse-${moreOptionSubShipmentRef?.current?.id}`)) {
            document.getElementById(`collapse-${moreOptionSubShipmentRef?.current?.id}`).classList.remove("show");
        }
        if (moreOutsideSub && document.getElementById(`collapse-${moreOptionSubRef?.current?.id}`)) {
            document.getElementById(`collapse-${moreOptionSubRef?.current?.id}`).classList.remove("show");
        }
        if (moreOutsideShipment && document.getElementById(`collapse-${moreOptionShipmetRef?.current?.id}`)) {
            document.getElementById(`collapse-${moreOptionShipmetRef?.current?.id}`).classList.remove("show");
        }
    }, [
        moreOutside,
        moreOutsideSub,
        moreOutsideSubShipment,
        moreOutsideShipment,
    ]);

    const _handleInputBlur = async (e, type, subFolderId) => {
        const {
            target: { label, value, id },
        } = e;
        const arr = sideMenuData.map(async (it) => {
            if (type === "subFolder") {
                it.subFolders = it.subFolders.map(async (subFolder) => {
                    if (subFolder.id === parseInt(id)) {
                        if (subFolderEditMode) {
                            if (value) {
                                subFolder.name = value;
                                await updateFolder({ id: subFolder.id, name: value });
                                await fetchSideMenu();
                            } else {
                                subFolder.name = subFolderEditValue;
                            }
                            subFolder.edit = false;
                            setSubFolderEditMode(false);
                            setSubFolderEditValue(null);
                            setFolderValue(null);
                        } else {
                            if (value) {
                                subFolder.name = value;
                                const response = await addFolder({
                                    parent: subFolder.parent,
                                    name: value,
                                });
                                if (response.error != undefined) {
                                    notify("Folder name already exist!");
                                } else {
                                    await fetchSideMenu();
                                    subFolder.edit = false;
                                    setFolderValue(null);
                                }
                            } else {
                                // remove entry
                                setDeleteArgs({
                                    mainId: subFolder.parent,
                                    deleteId: null,
                                    subFolderId: false,
                                    idx: null,
                                });
                                _deleteRecord(e);
                                await fetchSideMenu();
                            }
                        }
                    }
                    return subFolder;
                });
                return it;
            } else if (type === "shipment") {
                if (subFolderId) {
                    it.subFolders = it.subFolders.map((subFolder) => {
                        if (subFolderId === subFolder.id) {
                            subFolder.shipments = subFolder.shipments.map(
                                async (shipment) => {
                                    if (shipment.id === parseInt(id)) {
                                        if (label) {
                                            shipment.shipment.name = label;
                                            await assignShipment({
                                                parent: shipment.parent,
                                                shipment_id: value,
                                            });
                                            await fetchSideMenu();
                                        }
                                    }
                                    return shipment;
                                }
                            );
                        }
                        return subFolder;
                    });
                } else {
                    it.shipments = it.shipments.map(async (shipment) => {
                        if (shipment.id === parseInt(id)) {
                            if (label) {
                                shipment.shipment.name = label;
                                await assignShipment({
                                    parent: shipment.parent,
                                    shipment_id: value,
                                });
                                await fetchSideMenu();
                            }
                        }
                        return shipment;
                    });
                }
            } else {
                if (it.id === parseInt(id)) {
                    if (folderEditMode) {
                        if (value) {
                            it.name = value;
                            await updateFolder({ id: it.id, name: value });
                            await fetchSideMenu();
                        } else {
                            it.name = folderEditValue;
                        }
                        it.edit = false;
                        setFolderEditMode(false);
                        setFolderEditValue(null);
                        setFolderValue(null);
                    } else {
                        if (value) {
                            it.name = value;
                            it.edit = false;
                            const response = await addFolder({ parent: null, name: value });
                            if (response.error != undefined) {
                                notify("Folder name already exist!");
                            } else {
                                setFolderValue(null);
                                await fetchSideMenu();
                            }
                        } else {
                            // remove entry
                            setDeleteArgs({
                                mainId: parseInt(id),
                                deleteId: null,
                                subFolderId: false,
                                idx: null,
                            });
                            _deleteRecord(e);
                            await fetchSideMenu();
                        }
                    }
                }
            }
            return it;
        });
    };

    if (typeof window === "undefined") {
        return null;
    } else {
        return (
            <>
                <ul
                    className="navbar-nav sidebar sidebar-dark accordion customAccordion"
                    id="accordionSidebar"
                >
                    {/* Sidebar - Brand */}
                    <Link href="/shipment">
                        <a className="sidebar-brand d-flex align-items-center o-hidden">
                            <img src="/static/img/logo.png" alt="OBORTECH" />
                        </a>
                    </Link>
                    {/* Divider */}
                    <hr className="sidebar-divider my-0" />
                    {/* Topbar Search */}
                    <form className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div className="input-group">
                            <Input
                                type="text"
                                className="form-control bg-light border-0 small"
                                placeholder="Shipment, Cargo, Container, Truck"
                                aria-label="Search"
                                aria-describedby="basic-addon2"
                            />
                            <div className="input-group-append">
                                <button className="btn btn-dark" type="button">
                                    <i className="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    {/* Nav Item - Dashboard */}
                    <li className="nav-item title">
                        <span>SHIPMENTS</span>
            &nbsp;&nbsp;
            <a style={{ cursor: "pointer" }} onClick={_toggleAdd}>
                            <i className="fa fa-plus" />
                        </a>
                    </li>
                    {sideMenuData.length > 0 &&
                        sideMenuData.map((menu, idx) => (
                            <>
                                <li className="nav-item row" key={idx}>
                                    <div className="col-sm-10">
                                        <span
                                            id={`collapsed${menu.id}`}
                                            style={{ cursor: "pointer" }}
                                            className={
                                                menu.subFolders?.length > 0 ||
                                                    menu.shipments?.length > 0
                                                    ? "nav-link collapsed"
                                                    : "nav-link"
                                            }
                                            data-toggle={
                                                menu.subFolders?.length > 0 ||
                                                    menu.shipments?.length > 0
                                                    ? "collapse"
                                                    : ""
                                            }
                                            data-target={`#collapse${menu.id}`}
                                            aria-expanded="true"
                                            aria-controls={`collapse${menu.id}`}
                                        >
                                            <span>
                                                {menu.edit != undefined && menu.edit ? (
                                                    <Input
                                                        type="text"
                                                        value={folderValue || ""}
                                                        onChange={(e) => {
                                                            setFolderValue(e.target.value);
                                                        }}
                                                        onBlur={_handleInputBlur}
                                                        onKeyPress={(e) => {
                                                            e?.which === 13 && _handleInputBlur(e);
                                                        }}
                                                        id={menu.id}
                                                    />
                                                ) : (
                                                        menu.name
                                                    )}
                                            </span>
                                        </span>
                                    </div>
                                    <div
                                        className="col-sm-2"
                                        ref={moreOptionRef}
                                        id={`${menu.id}-${idx}`}
                                    >
                                        <span
                                            data-toggle="collapse"
                                            data-target={`#collapse-${menu.id}-${idx}`}
                                            style={{ color: "#fff", cursor: "pointer" }}
                                        >
                                            <b>...</b>
                                        </span>
                                        <ul
                                            id={`collapse-${menu.id}-${idx}`}
                                            className="collapse dropdown-menu"
                                            style={{ margin: "0", padding: "10px" }}
                                        >
                                            <li
                                                className="collapse-item"
                                                onClick={(e) => _toggleEdit(e, menu.id, null, idx)}
                                            >
                                                Edit folder
                      </li>
                                            <li
                                                className="collapse-item"
                                                onClick={(e) =>
                                                    _toggleAdd(e, menu.id, null, false, idx)
                                                }
                                            >
                                                Add subfolder
                      </li>
                                            <li
                                                className="collapse-item"
                                                onClick={(e) => _toggleAdd(e, menu.id, null, true, idx)}
                                            >
                                                Add shipment
                      </li>
                                            <li
                                                className="collapse-item"
                                                onClick={(e) =>
                                                    _toggleDelete(e, menu.id, null, false, idx)
                                                }
                                            >
                                                Delete folder
                      </li>
                                        </ul>
                                    </div>
                                    <div
                                        id={`collapse${menu.id}`}
                                        className="collapse"
                                        aria-labelledby="headingTwo"
                                        data-parent="#accordionSidebar"
                                    >
                                        <div className="collapse-inner rounded row">
                                            {menu.subFolders?.map((subIt, idx) => (
                                                <>
                                                    <div className="col-sm-10">
                                                        <span
                                                            id={`collapsed${subIt.id}`}
                                                            style={{ cursor: "pointer" }}
                                                            className={
                                                                subIt.shipments?.length > 0
                                                                    ? `nav-link ${document.getElementById(`collapse${subIt.id}`)?.className?.includes('show') ? '' : "collapsed"}`
                                                                    : "collapse-item"
                                                            }
                                                            data-toggle={
                                                                subIt.shipments?.length > 0 ? "collapse" : ""
                                                            }
                                                            data-target={`#collapse${subIt.id}`}
                                                            aria-expanded="true"
                                                            aria-controls={`collapse${subIt.id}`}
                                                        >
                                                            {subIt.edit != undefined && subIt.edit ? (
                                                                <Input
                                                                    type="text"
                                                                    value={folderValue || ""}
                                                                    onChange={(e) => {
                                                                        setFolderValue(e.target.value);
                                                                    }}
                                                                    onBlur={(e) =>
                                                                        _handleInputBlur(e, "subFolder")
                                                                    }
                                                                    onKeyPress={(e) => {
                                                                        e?.which === 13 && _handleInputBlur(e);
                                                                    }}
                                                                    id={subIt.id}
                                                                />
                                                            ) : (
                                                                    subIt.name
                                                                )}
                                                        </span>
                                                    </div>
                                                    <div
                                                        className="col-sm-2 dropdown"
                                                        ref={moreOptionSubRef}
                                                        id={`${subIt.id}-${idx}`}
                                                    >
                                                        <span
                                                            data-toggle="collapse"
                                                            data-target={`#collapse-${subIt.id}-${idx}`}
                                                            style={{ color: "#fff", cursor: "pointer" }}
                                                        >
                                                            <b>...</b>
                                                        </span>
                                                        <ul
                                                            id={`collapse-${subIt.id}-${idx}`}
                                                            className="collapse dropdown-menu"
                                                            style={{ margin: "0", padding: "10px" }}
                                                        >
                                                            <li
                                                                onClick={(e) =>
                                                                    _toggleEdit(e, menu.id, subIt.id, idx)
                                                                }
                                                            >
                                                                Edit subfolder
                              </li>
                                                            <li
                                                                onClick={(e) =>
                                                                    _toggleAdd(e, menu.id, subIt.id, true, idx)
                                                                }
                                                            >
                                                                Add shipment
                              </li>
                                                            <li
                                                                onClick={(e) =>
                                                                    _toggleDelete(e, menu.id, null, subIt.id, idx)
                                                                }
                                                            >
                                                                Delete subfolder
                              </li>
                                                        </ul>
                                                    </div>
                                                    <div
                                                        id={`collapse${subIt.id}`}
                                                        className="collapse"
                                                        aria-labelledby="headingThree"
                                                    >
                                                        <div className="collapse-inner rounded row">
                                                            {subIt.shipments?.map((shipment, idx) => (
                                                                <>
                                                                    <>
                                                                        <div className={(shipment.shipment.id == shipment_id) ? 'col-sm-10 active-shipment' : 'col-sm-10'}>
                                                                            {shipment.shipment.name ? (
                                                                                <Link
                                                                                    href={
                                                                                        "/containers/" +
                                                                                        shipment.shipment.id
                                                                                    }>
                                                                                    <a>{shipment.shipment.name}</a>
                                                                                </Link>
                                                                            ) : (
                                                                                    <AdvanceSelect
                                                                                        className="basic-single"
                                                                                        classNamePrefix="select"
                                                                                        defaultValue={shipmentOption[0]}
                                                                                        isClearable={true}
                                                                                        isSearchable={true}
                                                                                        name="shipment"
                                                                                        options={shipmentOption}
                                                                                        onChange={(e) =>
                                                                                            _handleInputBlur(
                                                                                                {
                                                                                                    target: {
                                                                                                        label: e?.label,
                                                                                                        value: e?.value,
                                                                                                        id: shipment?.id,
                                                                                                    },
                                                                                                },
                                                                                                "shipment",
                                                                                                subIt.id
                                                                                            )
                                                                                        }
                                                                                    />
                                                                                )}
                                                                        </div>
                                                                        <div
                                                                            className="col-sm-2 dropdown"
                                                                            ref={moreOptionSubShipmentRef}
                                                                            id={`${shipment.id}-${idx}`}
                                                                        >
                                                                            <span
                                                                                data-toggle="collapse"
                                                                                data-target={`#collapse-${shipment.id}-${idx}`}
                                                                                style={{
                                                                                    color: "#fff",
                                                                                    cursor: "pointer",
                                                                                }}
                                                                            >
                                                                                <b>...</b>
                                                                            </span>
                                                                            <ul
                                                                                id={`collapse-${shipment.id}-${idx}`}
                                                                                className="collapse dropdown-menu"
                                                                                style={{ margin: "0", padding: "10px" }}
                                                                            >
                                                                                <li
                                                                                    onClick={(e) =>
                                                                                        _toggleDelete(
                                                                                            e,
                                                                                            menu.id,
                                                                                            shipment.id,
                                                                                            subIt.id,
                                                                                            idx
                                                                                        )
                                                                                    }
                                                                                >
                                                                                    Delete shipment
                                        </li>
                                                                            </ul>
                                                                        </div>
                                                                    </>
                                                                </>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </>
                                            ))}
                                            {menu.shipments?.length > 0 &&
                                                menu.shipments?.map((shipment, idx) => (
                                                    <>
                                                        <div className={(shipment.shipment.id == shipment_id) ? 'col-sm-10 pl-2 active-shipment' : 'col-sm-10 pl-2'}>
                                                            {shipment.shipment?.name ? (
                                                                <Link
                                                                    href={"/containers/" + shipment.shipment?.id}
                                                                >
                                                                    <a>{shipment.shipment?.name}</a>
                                                                </Link>
                                                            ) : (
                                                                    <AdvanceSelect
                                                                        className="basic-single"
                                                                        classNamePrefix="select"
                                                                        defaultValue={shipmentOption[0]}
                                                                        isClearable={true}
                                                                        isSearchable={true}
                                                                        name="shipment"
                                                                        options={shipmentOption}
                                                                        onChange={(e) =>
                                                                            _handleInputBlur(
                                                                                {
                                                                                    target: {
                                                                                        label: e?.label,
                                                                                        value: e?.value,
                                                                                        id: shipment?.id,
                                                                                    },
                                                                                },
                                                                                "shipment"
                                                                            )
                                                                        }
                                                                    />
                                                                )}
                                                        </div>
                                                        <div
                                                            className="col-sm-2 dropdown"
                                                            ref={moreOptionShipmetRef}
                                                            id={`${shipment.id}-${idx}`}
                                                        >
                                                            <span
                                                                data-toggle="collapse"
                                                                data-target={`#collapse-${shipment.id}-${idx}`}
                                                                style={{ color: "#fff", cursor: "pointer" }}
                                                            >
                                                                <b>...</b>
                                                            </span>
                                                            <ul
                                                                id={`collapse-${shipment.id}-${idx}`}
                                                                className="collapse dropdown-menu"
                                                                style={{ margin: "0", padding: "10px" }}
                                                            >
                                                                <li
                                                                    onClick={(e) =>
                                                                        _toggleDelete(
                                                                            e,
                                                                            menu.id,
                                                                            shipment.id,
                                                                            null,
                                                                            idx
                                                                        )
                                                                    }
                                                                >
                                                                    Delete shipment
                                </li>
                                                            </ul>
                                                        </div>
                                                    </>
                                                ))}
                                        </div>
                                    </div>
                                </li>
                            </>
                        ))}
                </ul>
                {openDelete && (
                    <DeleteModal
                        toggle={_toggleDelete}
                        isOpen={openDelete}
                        onDeleteEntry={(e) => _deleteRecord(e)}
                    />
                )}
            </>
        );
    }
}

export default Sidebar;

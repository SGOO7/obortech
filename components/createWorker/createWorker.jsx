import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";
import { useFormik } from "formik";
import string from "../../utils/stringConstants/language/eng.json";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import * as Yup from "yup";
import "./worker.css"
function CreateWorker (){
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    const formik = useFormik({
        initialValues: {
            firstName: "",
            lastName: "",
            cellNo: "",
            email: "",
            role:"",
            uniqueId:"",
            password:"",
            isActive:""
          }, 
          validationSchema: Yup.object({
            firstName: Yup.string().required(`First Name ${string.errors.required}`),
            lastName: Yup.string().required(`Last Name ${string.errors.required}`),
            cellNo: Yup.string().required(`Cell No ${string.errors.required}`),
            email: Yup.string().required(`Email ${string.errors.required}`),
            role: Yup.string().required(`Role ${string.errors.required}`),
            uniqueId: Yup.string().required(`Unique Id ${string.errors.required}`),
            password: Yup.string().required(`Password ${string.errors.required}`),
            isActive: Yup.string().required(`isActive ${string.errors.required}`),
         }),
    })
    return(
        <div className="wroker-wrap">
          <Button  onClick={toggle} className="common-modal-btn ">Create Workers</Button>
          <Modal isOpen={modal} toggle={toggle} className="worker-modal common-model modal-lg customModal">
          <ModalHeader toggle={toggle} className="modal-center-Header">Create Workers</ModalHeader>
          <ModalBody className="common-modal-wrap">
          <form  onSubmit={formik.handleSubmit} >
            <div className="text-field-wrap d-flex justify-content-between">
                <div className="text-field-wrapper">
                    <Input type="text" className="input-field"
                        placeholder="First Name *"
                        name="firstName"
                        onChange={formik.handleChange}
                        value={formik.values.firstName}
                        />
                        {formik.errors.firstName ? (
                            <FormHelperMessage
                            className="err"
                            message={formik.errors.firstName}
                            />
                        ) : null}
                </div>  
                <div className="text-field-wrapper">
                    <Input 
                        type="text" className="input-field"
                        placeholder="Last Name *"
                        name="lastName"
                        onChange={formik.handleChange}
                        value={formik.values.lastName}
                    />
                    {formik.errors.lastName ? (
                        <FormHelperMessage
                        className="err"
                        message={formik.errors.lastName}
                        />
                    ) : null}
                </div>
            </div>
            
            <div className="text-field-wrap d-flex justify-content-between">
                <div className="text-field-wrapper">  
                <Input type="text" className="input-field"
                    placeholder="Cell No *"
                    name="cellNo"
                    onChange={formik.handleChange}
                    value={formik.values.cellNo}
                    />
                    {formik.errors.cellNo ? (
                        <FormHelperMessage
                        className="err"
                        message={formik.errors.cellNo}
                        />
                    ) : null}
                </div> 
                <div className="text-field-wrapper">
                    <Input 
                        type="email" className="input-field"
                        placeholder="Email *"
                        name="email"
                        onChange={formik.handleChange}
                        value={formik.values.email}
                    />
                    {formik.errors.email ? (
                        <FormHelperMessage
                        className="err"
                        message={formik.errors.email}
                        />
                    ) : null}
                </div>
            </div>
            <div className="text-field-wrap d-flex justify-content-between">
                <div className="text-field-wrapper">    
                    <Input type="text" className="input-field"
                        placeholder="Role *"
                        name="role"
                        onChange={formik.handleChange}
                            value={formik.values.role}
                        />
                        {formik.errors.role ? (
                            <FormHelperMessage
                            className="err"
                            message={formik.errors.role}
                            />
                        ) : null}
                 </div>
                 <div className="text-field-wrapper">
                        <Input 
                            type="text" className="input-field"
                            placeholder="Unique Id *"
                            name="uniqueId"
                            onChange={formik.handleChange}
                            value={formik.values.uniqueId}
                        />
                        {formik.errors.uniqueId ? (
                            <FormHelperMessage
                            className="err"
                            message={formik.errors.uniqueId}
                            />
                        ) : null}
                 </div>
            </div>
            <div className="text-field-wrap d-flex justify-content-between">
                <div className="text-field-wrapper">   
                    <Input type="password" className="input-field"
                        placeholder="Password *"
                        name="password"
                        onChange={formik.handleChange}
                        value={formik.values.password}
                        />
                        {formik.errors.password ? (
                            <FormHelperMessage
                            className="err"
                            message={formik.errors.password}
                            />
                        ) : null}
                </div> 
                <div className="text-field-wrapper">
                        <Input 
                            type="text" className="input-field"
                            placeholder="isActive *"
                            name="isActive"
                            onChange={formik.handleChange}
                            value={formik.values.isActive}
                        />
                        {formik.errors.isActive ? (
                            <FormHelperMessage
                            className="err"
                            message={formik.errors.isActive}
                            />
                        ) : null}
                </div>
            </div>
            <div className="text-field-wrap d-flex justify-content-between">
            <Button  onClick={toggle} type="submit" className="input-field workers-btn">Save</Button>
            <Button  onClick={toggle}  className="input-field workers-btn">Cancel</Button>
            </div>

          </form>
          </ModalBody>
          </Modal>
        </div>
    )
};
export default CreateWorker;
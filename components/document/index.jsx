const Document = props => {
    return (
        <div className="container-fluid">
            <div className="row d-flex shipment-listing">
                <div className="tab-pane fade show active mt-3 w-100" id="event" role="tabpanel" aria-labelledby="event-listing">
                    <div className="row">
                        <div className="col-md-12 event-mini-stats">
                            <span>Cargo ID: 45G1 (B)</span>
                            <span>Container ID: CSNU-635059-9 (B))</span>
                            <span>Group: 1</span>
                            <span>Destination: Ulaanbaatar MN - Brisbane AU</span>
                        </div>

                        <div class="shipment-table-listing table-responsive mt-2 w-100 col-md-12">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Date</th>
                                        <th scope="col">Submitter</th>
                                        <th scope="col">Type of document</th>
                                        <th scope="col">File name</th>
                                        <th scope="col">Document seen by</th>
                                        <th scope="col" className="text-center">Actions</th>
                                        <th scope="col">Hash</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="150">2020-10-08 18:13:58</td>
                                        <td>Monex (Freight Forwarder)</td>
                                        <td><b>Inventory Document</b></td>
                                        <td><a href="#">household-goods-descriptive-inventory.pdf</a></td>
                                        <td width="200"><select className="form-control"><option>Monex (Freight Forwarder)</option></select></td>
                                        <td className="action-menu">
                                            <div class="comment-added position-relative have-hiden-content">
                                                <i className="far fa-comment-alt" data-name="2"></i> 
                                                <i className="fa fa-check ml-2"></i>   
                                            </div>
                                            
                                        </td>
                                        <td><a href="#">View</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Document;
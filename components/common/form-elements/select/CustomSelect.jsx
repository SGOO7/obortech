import React from "react";
import PropTypes from "prop-types";
import "./Select.css";

const CustomSelect = (props) => {
  const {
    children,
    options,
    value,
    onChange,
    className,
    id,
    name,
    defaultOptionText = "Select One",
  } = props;

  return (
    <select
      {...props}
      // onChange={onChange ? onChange : null}
      className={`default-css ${className}`}
      id={id ? id : null}
      name={name ? name : null}
      // value={value ? value : null}
    >
      {children ? (
        children
      ) : (
        <>
          <option value="">{defaultOptionText}</option>
          {options?.map((options, i) => {
            return (
              <option key={i} value={options.id}>
                {options.name}
              </option>
            );
          })}
        </>
      )}
    </select>
  );
};

CustomSelect.propTypes = {
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func,
  className: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  children: PropTypes.node,
  defaultOptionText: PropTypes.string,
};

export default CustomSelect;

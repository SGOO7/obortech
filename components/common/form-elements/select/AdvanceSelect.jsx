import React from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import "./Select.css";

const AdvanceSelect = (props) => {
	return <Select {...props} />;
};

AdvanceSelect.propTypes = {
	options: PropTypes.array,
	onChange: PropTypes.func,
	className: PropTypes.string,
	name: PropTypes.string,
	value: PropTypes.string,
	children: PropTypes.node,
};

export default AdvanceSelect;

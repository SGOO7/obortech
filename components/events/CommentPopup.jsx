import { useEffect, useRef, useState } from "react";
import { useOutsideClick } from "../../utils/customHooks/useClickOutside";
const { _momentDateFormat } = require("../../utils/globalFunc");

const CommentPopup = ({toggle, comments, _addComment, shipment_event_id, user, isOpen, customCSS = {}}) => {
    const [comment, setComment] = useState('');
    const commentRef = useRef(null)
    const isOutside = useOutsideClick(commentRef)

    const _onSubmit = (comment) => {
        const new_comment = _addComment(comment, shipment_event_id);
        comments.push({
            comment:comment,
            createdAt:new_comment.createdAt,
            user:{
                official_name:user.official_name
            }
        })
        setComment('');
    }

    useEffect(() => {
        if(!!isOpen && !!isOutside){
            toggle(null)
        }
    }, [isOutside])

    return (
        <div className="small-action-popup text-left" style={customCSS} ref={commentRef}>
            <i className="fa fa-times-circle close-btn" onClick={() => toggle(null)}></i>
            <div className="main-content-body">
                {comments.map((comment,i) => {
                    return (
                        <div key={i} className="comment-box">
                            <p className="text-dark-50">{comment.user.official_name}</p>
                            <p className="text-dark">{comment.comment}</p>
                            <p className="text-warning">{_momentDateFormat(comment.createdAt)}</p>
                        </div>
                    );
                })}
                {comments.length == 0 &&
                    <p>No comments!</p>
                }

                <div className="add-comment d-flex align-items-center">
                    <input
                        type="text"
                        name="comment"
                        value={comment || ''}
                        onChange={(event) => {
                            setComment(event.target.value);
                        }}
                        placeholder="Add comment..."
                    />
                    <button onClick={() => {_onSubmit(comment)}}>
                        <i className="fa fa-arrow-right"></i>
                    </button>
                </div>
            </div>
        </div>
    )
};

export default CommentPopup;

import Link from 'next/link'
import { useState } from "react";
import DeleteModal from "../common/DeleteModal";
import AcceptDocumentPopup from "./AcceptDocumentPopup";
import BounceCircle from "./BounceCircle";
import CommentPopup from "./CommentPopup";

const { _momentDateFormat, _momentGetDiff } = require("../../utils/globalFunc");

const EventType = ({ acceptedDocument, shipment_event, updatedAt, id, user_id, created_by, _addComment, _seenDocument, _acceptDocument, seenDocument, canSeeDocument, user, _onDeleteEntry, auth_user, commentOpen, setCommentOpen, acceptOpen, setAcceptOpen, createdBy }) => {

    let bounceClass = 'bg-black';
    if (shipment_event.event_id === 26) {
        bounceClass = "bg-blue blue-fill";
    } else if (shipment_event.event_id === "borderIn") {
        bounceClass = "bg-yellow";
    } else if (shipment_event.event_id === 21) {
        bounceClass = "bg-red";
    } else if (shipment_event.event_id === 23) {
        bounceClass = "bg-red red-fill"
    }else if (shipment_event.event_id === 5) {
        bounceClass = "bg-blue";
    } else if (shipment_event.event_id === 20) {
        bounceClass = "bg-red";
    } else if (shipment_event.event_id === 24) {
        bounceClass = "bg-black black-fill";
    } else if (shipment_event.event_id === 25) {
        bounceClass = 'bg-violet'
    }else {
        bounceClass ="bg-black";
    }

    const [ deleteIndex, setDeleteIndex ] = useState(0);
    const [ deleteOpen, setDeleteOpen ] = useState(false);

    const _toggleDelete = (id) => {
        if(id == 0){
            _onDeleteEntry(deleteIndex);
        }
        setDeleteIndex(id);
        setDeleteOpen(!deleteOpen)
    };

    const _toggleComment = (id) => {
        setCommentOpen(id)
        setAcceptOpen(null);
    }

    const _toggleDocument = (id) => {
        setAcceptOpen(id);
        setCommentOpen(null)
    }

    //Event name, Show road name with border events
    let eventName = <h4 className="timeline-title">{shipment_event.event.name}</h4>;
    if(shipment_event.event.id == 27 || shipment_event.event.id == 28){
        eventName = <h4 className="timeline-title">{shipment_event.event.name}: {shipment_event.road.name}</h4>
    }

    let canDelete = false;
    let canComment = false;

    //Show/hide delete button
    if(auth_user.role_id == process.env.ADMIN){
        canDelete = true;
    } else {
        const createdMins = _momentGetDiff(new Date(), shipment_event.createdAt, 'minutes');
        if(createdBy == user_id && parseInt(createdMins) < 5){
            canDelete = true;
        }
    }

    if(shipment_event.event.type == 'document' && canSeeDocument){
        canComment = true;
    } else if (shipment_event.event.type == 'event' && shipment_event.event.event_category_id != process.env.ALERT_EVENTS_CATEGORY) {
        canComment = true;
    }

    return (

        <div className="vertical-timeline-item vertical-timeline-element" key={id}>
            <div>
                <span className="vertical-timeline-element-date">{created_by == 0 ? 'Obortech' : user == null ? 'Invalid User' : user.official_name}</span>
                <BounceCircle className={bounceClass} />
                <div className="vertical-timeline-element-content row">
                    <div className="list-content col-sm-7 pl-0">
                        {eventName}
                        <p>{_momentDateFormat(updatedAt)}</p>
                    </div>
                    <div className="action-menu col-sm-5 d-flex justify-content-end align-items-center">
                        <div className="document-attached">
                            {shipment_event.attachment ? (
                                <Link href={shipment_event.attachment}>
                                    {shipment_event.event.type=='document' ?
                                        <>
                                            {canSeeDocument && (
                                                <a href={shipment_event.attachment} target="_blank" onClick={() => _seenDocument(shipment_event.id, seenDocument)}><i className="far fa-file-alt text-warning"></i></a>
                                            )}
                                        </>
                                    :
                                        <a target="_blank"><i className="fa fa-camera event-image"></i></a>
                                    }
                                </Link>
                            ) : <i className={"fa fa-camera text-custom-hidden"}></i>}
                        </div>

                        {/* COMMENTS */}
                        <div className="comment-added position-relative have-hiden-content">
                            {canComment ? (
                                <>
                                    <i className="far fa-comment-alt" data-name={shipment_event.shipment_event_comments.length} onClick={() => _toggleComment(id)}></i>
                                    {
                                        commentOpen === id && (
                                            <CommentPopup
                                                toggle={(id) => _toggleComment(id)}
                                                comments={shipment_event.shipment_event_comments}
                                                _addComment={_addComment}
                                                shipment_event_id={shipment_event.id}
                                                user={auth_user}
                                                isOpen={commentOpen === id}
                                            />
                                        )
                                    }
                                </>
                            ) : <i className={"fa fa-camera text-custom-hidden"}></i>}
                        </div>

                        {/* ACCEPT DOCUMENT ONLY FOR DOCUMENT EVENTS */}
                        <div className="accepted-sec position-relative have-hiden-content">
                            {(shipment_event.event.type == 'document' && canSeeDocument) ? (
                                <>
                                    <i className={acceptedDocument ? "fa fa-check text-dark":"fa fa-check text-custom-light"} onClick={() => _toggleDocument(id)}></i>
                                    {
                                        acceptOpen === id && (
                                            <AcceptDocumentPopup
                                                toggle={(id) => _toggleDocument(id)}
                                                documentAcceptedUsers={shipment_event.document_accepted_users}
                                                _acceptDocument={_acceptDocument}
                                                shipment_event_id={shipment_event.id}
                                                user={auth_user}
                                                isOpen={acceptOpen === id}
                                            />
                                        )
                                    }
                                </>
                            ): <i className={"fa fa-check text-custom-hidden"}></i> }
                        </div>

                        {/* Delete button should be visible to admin only, will be removed after 5 mins if not admin */}
                        <div className="delete-sec">
                            {canDelete ? (
                                <i onClick={() => {_toggleDelete(id)}} className="far fa-trash-alt"></i>
                            ) : <i className={"fa fa-trash text-custom-hidden"}></i> }
                        </div>

                    </div>
                </div>
            </div>
            <DeleteModal
                isOpen={deleteOpen}
                toggle={() => {_toggleDelete(0)}}
                onDeleteEntry={() => {
                    _toggleDelete(0);
                }}
            />
        </div>
    )
}

export default EventType;

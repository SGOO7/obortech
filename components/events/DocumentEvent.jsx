import Link from 'next/link'
import CommentPopup from "./CommentPopup";
import AcceptDocumentPopup from "./AcceptDocumentPopup";

const { _momentDateFormat } = require("../../utils/globalFunc");

const DocumentEvent = ({ shipment_event, updatedAt, user, _seenDocument, _acceptDocument, acceptedDocument, seenDocument, commentOpen, setCommentOpen, _addComment, auth_user, _toggleHashView, acceptOpen, setAcceptOpen }) => {

    const _toggleComment = (id) => {
        setCommentOpen(id)
        setAcceptOpen(null);
    }

    const _toggleDocument = (id) => {
        setAcceptOpen(id);
        setCommentOpen(null)
    }

    return (
        <tr>
            <td width="150">{_momentDateFormat(updatedAt)}</td>
            <td>{user.official_name}</td>
            <td><b>{shipment_event.event.name}</b></td>
            <td>
                <Link href={shipment_event.attachment}>
                    <a href={shipment_event.attachment} target="_blank" onClick={() => _seenDocument(shipment_event.id, seenDocument)}>
                        {shipment_event.attachment.split('/').pop()}
                    </a>
                </Link>
            </td>
            <td width="200">
                {shipment_event.document_seen_users.length == 0 ? 'No record!' :
                    <select className="form-control">
                        {shipment_event.document_seen_users.map((user,i) => {
                            return (
                                <option key={i}>{user.user.official_name}</option>
                            )
                        })}
                    </select>
                }
            </td>
            <td className="action-menu">
                <div className="comment-added position-relative have-hiden-content">
                    <i className="far fa-comment-alt" data-name={shipment_event.shipment_event_comments.length} onClick={() => _toggleComment(shipment_event.id)}></i>
                    {
                        commentOpen === shipment_event.id && (
                            <CommentPopup
                                toggle={(id) => _toggleComment(id)}
                                comments={shipment_event.shipment_event_comments}
                                _addComment={_addComment}
                                shipment_event_id={shipment_event.id}
                                user={auth_user}
                                isOpen={commentOpen === shipment_event.id}
                                customCSS={{top:"25px",left:"-260px"}}
                            />
                        )
                    }
                    <i className={acceptedDocument ? "fa fa-check ml-2 text-dark":"fa fa-check text-custom-light ml-2"} onClick={()=> _toggleDocument(shipment_event.id)}></i>
                    {
                        acceptOpen === shipment_event.id && (
                            <AcceptDocumentPopup
                                toggle={(id) => _toggleDocument(id)}
                                documentAcceptedUsers={shipment_event.document_accepted_users}
                                _acceptDocument={_acceptDocument}
                                shipment_event_id={shipment_event.id}
                                user={auth_user}
                                isOpen={acceptOpen === shipment_event.id}
                                customCSS={{top:"25px",left:"-260px"}}
                            />
                        )
                    }
                </div>
            </td>
            <td><a style={{cursor:'pointer'}} onClick={() => _toggleHashView(shipment_event.file_hash)}>View</a></td>
        </tr>
    )
}

export default DocumentEvent;

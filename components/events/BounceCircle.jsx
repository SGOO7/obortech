const BounceCircle = ({className}) => {
    const typeClass = `badge badge-dot badge-dot-xl ${className}`
    return (
        <span className="vertical-timeline-element-icon bounce-in">
            <i className={typeClass}> </i>
        </span>
    )
}

export default BounceCircle;
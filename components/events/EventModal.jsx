import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { useState } from 'react';
import ImageCrop from "../ImageCrop";

const EventModal = ({isOpen, toggle, eventParticipantFilters, transportEvents, _submitEvent, auth_user}) => {
    const [event_type, setEventType] = useState(transportEvents[0]?.id || 0);
    const [event_time, setEventTime] = useState('');
    const [event_users, setEventUsers] = useState([]);
    const [document_users, setDocumentUsers] = useState([]);
    const [event_comment, setEventComment] = useState('');
    const [event_file, setEventFile] = useState(null);
    const [inputImg, setInputImg] = useState('');
    const [blob, setBlob] = useState(null);
    const getBlob = (blob) => {
        // pass blob up from the ImageCropper component
        setBlob(blob)
    }
    const _onImageChange = e => {
        const file = e.target.files[0]
        const reader = new FileReader()
        setEventFile(file);
        reader.addEventListener('load', () => {
            setInputImg(reader.result)
        }, false)

        if (file) {
            reader.readAsDataURL(file)
        }
    }
    return (
        <Modal isOpen={isOpen} toggle={toggle} className="customModal document modal-lg" id="documentModal">
            <ModalHeader toggle={toggle}>
                <h5 className="modal-title text-dark font-weight-bold" id="exampleModalLabel">SUBMIT EVENT</h5>
            </ModalHeader>
            <ModalBody>
                <form className="form-container addEvent">
                    {/* row */}
                    <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-12">
                        <div className="form-group">
                            <select
                                value={event_type || 0}
                                className="form-control"
                                onChange={(event) => {
                                    setEventType(event.target.value);
                                }}
                            >
                                {   transportEvents.length > 0 &&
                                    transportEvents.map(item => (
                                        <option key={item.id} value={item.id} >{item.name}</option>
                                    ))
                                }
                            </select>
                        </div>
                    </div>

                    <div className="col-md-6">
                        <div className="form-group">
                            <input
                                type="datetime-local" className="form-control"
                                onChange={(event) => {
                                    setEventTime(event.target.value);
                                }}
                            />
                        </div>
                    </div>
                    </div>
                    {/* //row */}

                    {/* row */}
                    <div className="row ml-0 mr-0 content-block">
                        <div className="col-md-6">
                            <div className="form-group pointListing">
                                <h5 className="pointsheading">Who can view this event?</h5>
                                {   eventParticipantFilters?.length > 0 &&
                                    eventParticipantFilters?.map((item,i) => {
                                        if(item.user.id == auth_user.id){
                                            return false;
                                        }
                                        return (
                                            <span key={i}>
                                                <input
                                                    type="checkbox"
                                                    onClick={(event) => {
                                                        if(event_users.filter(function(e) { return e === item.user.id }).length == 0) {
                                                            setEventUsers([...event_users, item.user.id]);
                                                        } else{
                                                            setEventUsers(event_users.filter(function( obj ) {return obj !== item.user.id}));
                                                        }
                                                    }}
                                                /><label htmlFor="4Event">{item.user.official_name}</label><br />
                                            </span>
                                        );
                                    })
                                }
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="col-form-label float-right font-weight-bold">Add picture</label>
                                <div className="custom-file">

                                    <input
                                        type="file"
                                        className="custom-file-input"
                                        id="customFileLangHTML"
                                            onChange={_onImageChange}
                                        fileName={event_file?.name}
                                    />
                                    <label className="custom-file-label" htmlFor="customFileLangHTML"></label>
                                </div>
                                {
                                    event_file && (
                                        <p>
                                            {event_file.name}
                                        </p>
                                    )
                                }
                                <p><small>Supported file format: JPEG, JPG, PNG</small></p>
                            </div>
                        </div>
                        <div className="row">
                            {
                                inputImg && (
                                    <ImageCrop
                                        getBlob={getBlob}
                                        inputImg={inputImg}
                                    />
                                )
                            }
                        </div>

                    </div>
                    {/* //row */}

                    {/* row */}
                    <div className="row ml-0 mr-0 content-block">
                        <div className="col-md-12">
                            <div className="form-group">
                                <textarea
                                    className="form-control"
                                    onChange={(event)=>{
                                        setEventComment(event.target.value);
                                    }}
                                ></textarea>
                            </div>
                        </div>
                    </div>
                    {/* //row */}
                </form>
            </ModalBody>
            <ModalFooter>
                <button className="btn btn-primary large-btn" onClick={()=>{_submitEvent(parseInt(event_type), event_time, event_users, document_users, event_comment, event_file, 'event')}}>SUBMIT</button>
            </ModalFooter>
        </Modal>

    )
}

export default EventModal;
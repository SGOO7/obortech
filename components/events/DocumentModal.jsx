import { useState } from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { Progress } from 'reactstrap';

const DocumentModal = ({isOpen, toggle, eventParticipantFilters, documentEvents, _submitEvent, auth_user}) => {
    const [event_type, setEventType] = useState(documentEvents[0]?.id || 0);
    const [event_users, setEventUsers] = useState([]);
    const [document_users, setDocumentUsers] = useState([]);
    const [event_comment, setEventComment] = useState('');
    const [event_file, setEventFile] = useState(null);
    const [progressVal, setProgressVal ] = useState(0);
    const [intervalId, setIntervalId ] = useState();
    let pgVal = 0;
    const _handleDocUpload = (e) => {
        const interval = setInterval(setProgress, 1000)
        setIntervalId(interval)
    }
    const setProgress = (val = pgVal) => {
        if (val !== 100) {
            pgVal=pgVal+10
            setProgressVal(pgVal)
        } else {
            clearInterval(intervalId)
        }
    }
    return (
        <Modal isOpen={isOpen} toggle={toggle} className="customModal document modal-lg" id="documentModal">
            <ModalHeader toggle={toggle}>
                <h5 className="modal-title text-dark font-weight-bold" id="exampleModalLabel">SUBMIT DOCUMENT</h5>
            </ModalHeader>
            <ModalBody>
            <form className="form-container addEvent">
                    {/* row */}
                    <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-6">
                        <div className="form-group">
                            <select
                                value={event_type || 0}
                                className="form-control"
                                onChange={(event) => {
                                    setEventType(event.target.value);
                                }}
                            >
                                {
                                    documentEvents.map(item => (
                                        <option key={item.id} value={item.id} >{item.name}</option>
                                    ))
                                }
                            </select>
                        </div>
                    </div>

                    <div className="col-md-6">
                        <div className="form-group">
                            <div class="custom-file">
                                <input
                                    type="file"
                                    className="custom-file-input"
                                    id="customFileLangHTML"
                                    onChange={(event)=>{
                                        setEventFile(event.target.files[0]);
                                    }}
                                />
                                <label class="custom-file-label" for="choosefile"></label>
                            </div>
                            <p>{event_file && event_file.name}</p>
                            <p><small>Supported file format: JPEG, JPG, PNG</small></p>
                            {
                                progressVal > 0 && (
                                    <>
                                        <Progress value={progressVal} />
                                        {
                                            progressVal < 100 && (
                                                <p>Uploading document please wait</p>
                                            )
                                        }
                                    </>
                                )
                            }
                        </div>
                    </div>
                    </div>
                    {/* //row */}

                    {/* row */}
                    <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-6">
                        <div className="form-group pointListing">
                            <h5 className="pointsheading">Who can see your event ?</h5>
                            {
                                eventParticipantFilters.map((item,i) => {
                                    if(item.user.id == auth_user.id){
                                        return false;
                                    }
                                    return (
                                        <span key={i}>
                                            <input
                                                type="checkbox"
                                                onClick={(event) => {
                                                    if(event_users.filter(function(e) { return e === item.user.id }).length == 0) {
                                                        setEventUsers([...event_users, item.user.id]);
                                                    } else{
                                                        setEventUsers(event_users.filter(function( obj ) {return obj !== item.user.id}));
                                                        if(document_users.filter(function(e) { return e === item.user.id }).length != 0) {
                                                            document.getElementById(item.user.id + "_view_document").click();
                                                        }
                                                    }
                                                }}
                                                id={item.user.id + "_view_event"}
                                            /><label htmlFor="4Event">{item.user.official_name}</label><br />
                                        </span>
                                    );
                                })
                            }
                        </div>
                    </div>

                    <div className="col-md-6">
                    <div className="form-group pointListing">
                            <h5 className="pointsheading">Who can view your document ?</h5>
                            {
                                eventParticipantFilters.map((item,i) => {
                                    if(item.user.id == auth_user.id){
                                        return false;
                                    }
                                    return (
                                        <span key={i}>
                                            <input
                                                type="checkbox"
                                                onClick={(event) => {
                                                    if(document_users.filter(function(e) { return e === item.user.id }).length == 0) {
                                                        setDocumentUsers([...document_users, item.user.id]);
                                                        if(event_users.filter(function(e) { return e === item.user.id }).length == 0) {
                                                            document.getElementById(item.user.id + "_view_event").click();
                                                        }
                                                    } else{
                                                        setDocumentUsers(document_users.filter(function( obj ) {return obj !== item.user.id}));
                                                    }
                                                }}
                                                id={item.user.id + "_view_document"}
                                            /><label htmlFor="4Event">{item.user.official_name}</label><br />
                                        </span>
                                    );
                                })
                            }
                        </div>
                    </div>
                    </div>
                    {/* //row */}

                    {/* row */}
                    <div className="row ml-0 mr-0 content-block">
                        <div className="col-md-12">
                            <div className="form-group">
                                <textarea
                                    className="form-control"
                                    onChange={(event)=>{
                                        setEventComment(event.target.value);
                                    }}
                                ></textarea>
                            </div>
                        </div>
                    </div>
                    {/* //row */}

                </form>
            </ModalBody>
            <ModalFooter>
                <button className="btn btn-primary large-btn" onClick={()=>{_submitEvent(parseInt(event_type), null, event_users, document_users, event_comment, event_file, 'document')}}>SUBMIT</button>
            </ModalFooter>
        </Modal>
    )
}

export default DocumentModal;
import { useOutsideClick } from "../../utils/customHooks/useClickOutside";
import { useEffect, useRef, useState } from "react";

const AcceptDocumentPopup = ({toggle, documentAcceptedUsers, _acceptDocument, shipment_event_id, user, isOpen, customCSS = {}}) => {
    const [accepted, setAccepted] = useState(false);
    const acceptRef = useRef(null)
    const isOutside = useOutsideClick(acceptRef)

    const _onSubmit = () => {
        _acceptDocument(shipment_event_id);
        documentAcceptedUsers.push({
            user:{
                official_name:user.official_name,
                id: user.id
            },
        });
        setAccepted(true);
        toggle();
    }

    useEffect(() => {
        if (!!isOpen && !!isOutside) {
            toggle(null)
        }
    }, [isOutside])

    return (
        <div className="small-action-popup text-left" style={customCSS} ref={acceptRef}>
            <i className="fa fa-times-circle close-btn" onClick={() => toggle(null)}></i>
            <div className="main-content-body">
                <p className="text-dark">Document accepted by</p>
                {documentAcceptedUsers.map((document_accept,i) => {
                    if(!accepted && document_accept.user.id == parseInt(user.id)){
                        setAccepted(true);
                    }
                    return (
                        <p key={i} className="text-dark-50">{document_accept.user.official_name} {document_accept.user.id == user.id ? "(You)":""}</p>
                    )
                })}

                {documentAcceptedUsers.length == 0 && 
                    <p>No one!</p>
                }
                {!accepted && <p 
                    className="accept-btn"
                    onClick={(event) => {
                        event.preventDefault();
                        _onSubmit();
                    }}
                    style={{cursor:'pointer'}}
                >
                    <a className="text-warning">Accept Document</a>
                </p>}
            </div>
        </div>
    )
};

export default AcceptDocumentPopup;
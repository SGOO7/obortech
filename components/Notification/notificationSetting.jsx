import React,{ useState}  from 'react';
import { Modal,ModalHeader } from "reactstrap";
import Button from "../../components/common/form-elements/button/Button";
import Checkbox from "../../components/common/form-elements/checkbox/index";
import "./setting.css";
import "../../static/css/modal.css";

import SettingCard from './settingCard';
function NotificationSetting(props){
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    return(
        <div className="notification-model">
          <Button className="notification-btn" onClick={toggle}>Notification Settings</Button>
          <Modal isOpen={modal} toggle={toggle} className="notification-model common-model modal-lg customModal largeModal">
            <div className="setting-wrap">
              <ModalHeader className="setting-header" toggle={toggle}>
                SHIPMENTS FOR NOTIFICATION
                {/*  */}
               <div className="modalHeadernput form-control">
                <label> <input type="checkbox" className="default-css"></input>Mongolia-Australia 1-3 <span>(SEARCH & SELECT)</span></label>
               </div>

              </ModalHeader> 
              <div className="setting-body">
                {/* content row started */}
                <div className="row">
                  {/* ORGANIZATIONS START */}
                  <div className="col-md-4">
                    <div className="block-title">ORGANIZATIONS</div>
                    <div className="setting-card setting-content-wrap">
                      <label> <input type="checkbox" className="default-css"></input>Standard & Metrology MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Precom</label>
                      <label> <input type="checkbox" className="default-css"></input>Mongolian Express</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>

                    </div>
                  </div>
                  {/* ORGANIZATIONS END */}

                  {/* DOCUMENTS START */}
                  <div className="col-md-4">
                    <div className="block-title">DOCUMENTS</div>
                    <div className="withNoBox">
                     <label> <input type="checkbox" className="default-css"></input>Document acceptance</label>
                     <label> <input type="checkbox" className="default-css"></input>Document acceptance</label>
                     <label> <input type="checkbox" className="default-css"></input>New document submission</label>
                    </div>
                    <div className="setting-card setting-content-wrap withSmallHeight">
                      <label> <input type="checkbox" className="default-css"></input>Certificate of Conformity</label>
                      <label> <input type="checkbox" className="default-css"></input>Packing List</label>
                      <label> <input type="checkbox" className="default-css"></input>Invoice</label>
                      <label> <input type="checkbox" className="default-css"></input>Bill of Lading</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                    </div>
                  </div>
                  {/* DOCUMENTS END */}

                  {/* EVENTS START */}
                  <div className="col-md-4">
                    <div className="block-title">EVENTS</div>
                    <div className="withNoBox">
                     <label> <input type="checkbox" className="default-css"></input>Comments on events</label>
                     <label> <input type="checkbox" className="default-css"></input>New event submission</label>
                    </div>
                    <div className="setting-card setting-content-wrap withSmallHeight">
                      <label> <input type="checkbox" className="default-css"></input>Container opened</label>
                      <label> <input type="checkbox" className="default-css"></input>Temperature breached</label>
                      <label> <input type="checkbox" className="default-css"></input>Humidity breached</label>
                      <label> <input type="checkbox" className="default-css"></input>Shipment finished</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                      <label> <input type="checkbox" className="default-css"></input>Customs Office MN</label>
                    </div>
                  </div>
                  {/* EVENTS END */}

                  {/* FOOTER START */}
                  <div className="switch-wrap d-flex col-md-12">
                    <div className="custom-switch">                    
                      <Checkbox  value="Get notifications on E-mail" id="Cargoreceivedx" className="notification-check custom-control-input" />
                      <label className="custom-control-label" for="Cargoreceivedx">Get notifications on E-mail</label>
                    </div>
                    <div className="switch-btns">
                      <Button className="btn btn-primary large-btn" > Set to default </Button>
                      <Button className="btn btn-primary large-btn" > Apply </Button>
                    </div>  
                  </div>
                  {/* FOOTER END */}

                </div>
                {/* //content row end */}              
                 
            </div>
            </div>
          </Modal>
        </div>
    )
}
export default  NotificationSetting;

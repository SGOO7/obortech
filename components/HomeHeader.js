import PropTypes from 'prop-types';
import Link from 'next/link';
import React, { useState, useEffect } from 'react';
import Router, { useRouter } from 'next/router';
import Moment from 'react-moment';
import Profile from "./Profile/profile";
import Notification from "./Notification/notificationSetting";
import SendSubmissionRequest from "./sendSubmissionRequest/sendSubmissionRequest";
import CreateWorker from "./createWorker/createWorker";
import { fetchShipmentEvents } from "../lib/api/shipment-event";


function Header({ user }) {
    const router = useRouter();
    let { shipment_id } = router.query;
    const [eventCount, setEventCount] = useState(0);
    const [timer, setTimer] = useState(false);

    //Check if user is admin
    const ifAdmin = true;
    // const ifAdmin = (user.role_id==1) ? true : false;

    if (typeof window === 'undefined') {
        return null;
    } else {
        // If on another page then fetch shipment_id from cookie
        if (!shipment_id) {
            if (window.localStorage.getItem("shipment_id")) {
                shipment_id = window.localStorage.getItem("shipment_id");
            }
        }

        useEffect(() => {
            if (router.pathname != '/event') {
                _fetchEvents();
                if (!timer) {
                    setTimer(true);
                    setInterval(async () => {
                        _fetchEvents();
                    }, process.env.EVENT_TIMER || 60000);
                }
            }
        }, []);

        /**
           * Request for fetching all @events
           */
        const _fetchEvents = async () => {
            const shipmentEvents = await fetchShipmentEvents({
                container_id: 0,
                shipment_id: parseInt(shipment_id),
                user_id: parseInt(user.id),
                event_category: 0,
                participant_id: 0,
                mark_as_view: false,
                viewed: 0
            });
            setEventCount(shipmentEvents.length);
        };

        return (
            <nav className="navbar navbar-expand navbar-light topbar mb-4 static-top" style={{ padding: 0 }}>
                {/* Sidebar Toggle (Topbar) */}
                <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
                    <i className="fa fa-bars"></i>
                </button>
                {/* Topbar Navbar */}
                <ul className="navbar-nav ml-auto align-items-center">
                    <li className="menu-item-option">
                        {shipment_id && <Link href={"/containers/" + parseInt(shipment_id)}>
                            <a>
                                <i className="fa fa-calendar-alt"></i><span>CONTAINERS</span>
                            </a>
                        </Link>}
                        {shipment_id && <Link href={"/event/" + parseInt(shipment_id)}>
                            <a>
                                <i className="fa fa-calendar-alt"></i><span>
                                    EVENTS <span className={eventCount > 0 ? "badge badge-danger badge-counter" : "badge badge-danger badge-counter d-none"}>{eventCount}</span>
                                </span>
                            </a>
                        </Link>}
                        {shipment_id && <Link href={"/document/" + parseInt(shipment_id)}>
                            <a>
                                <i className="fa fa-file-alt"></i><span>DOCUMENTS</span>
                            </a>
                        </Link>}
                    </li>
                    {/* Nav Item - Alerts */}

                    <li className="nav-item dropdown no-arrow mx-1">
                        <a className="nav-link notification" href="#" data-toggle="modal" data-target="#notificationModal">
                            <i className="fas fa-bell fa-fw"></i>
                            {/* Counter - Alerts */}
                            <span className="badge badge-danger badge-counter">3+</span>
                        </a>
                    </li>
                    <div className="topbar-divider d-none d-sm-block"></div>
                    {/* Nav Item - User Information */}
                    <li className="nav-item dropdown no-arrow">
                        <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span className="mr-2 d-none d-lg-inline text-gray-600 small text-capitalize">{user.official_name}</span>
                            <i className="fas fa-angle-down fa-sm"></i>
                        </a>
                        {/* Dropdown - User Information */}
                        <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

                            {ifAdmin &&
                                <>
                                    <Link href="javascript:void(0)" as="">
                                        <a className="dropdown-item"><SendSubmissionRequest /></a>
                                    </Link>
                                    <Link href="javascript:void(0)" as="">
                                        <a className="dropdown-item"><CreateWorker /></a>
                                    </Link>
                                    <Link href="javascript:void(0)" as="">
                                        <a className="dropdown-item"><Notification /></a>
                                    </Link>
                                    <Link href="javascript:void(0)" as="">
                                        <a className="dropdown-item"> <Profile /></a>
                                    </Link>
                                    <Link href="/shipment" as="/shipment">
                                        <a className="dropdown-item">Shipments</a>
                                    </Link>
                                    <Link href="/group">
                                        <a className="dropdown-item">Groups</a>
                                    </Link>
                                    <Link href="/truck">
                                        <a className="dropdown-item">Trucks</a>
                                    </Link>
                                    <Link href="/container">
                                        <a className="dropdown-item">Containers</a>
                                    </Link>
                                    <Link href="/cargo">
                                        <a className="dropdown-item">Cargos</a>
                                    </Link>
                                    <Link href="/category">
                                        <a className="dropdown-item">Categories</a>
                                    </Link>
                                    <Link href="/participant">
                                        <a className="dropdown-item">Participants</a>
                                    </Link>
                                    <Link href="/station">
                                        <a className="dropdown-item">Stations</a>
                                    </Link>
                                    <Link href="/device">
                                        <a className="dropdown-item">Devices</a>
                                    </Link>
                                    <div className="dropdown-divider"></div>
                                </>
                            }
                            <a className="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>
        );
    }
}

Header.propTypes = {
    user: PropTypes.shape({
        displayName: PropTypes.string,
        email: PropTypes.string.isRequired,
        isAdmin: PropTypes.number,
        avatarUrl: PropTypes.string,
        isGithubConnected: PropTypes.bool,
    }),
};

Header.defaultProps = {
    user: null
};

export default Header;

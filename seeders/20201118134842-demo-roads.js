'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('roads', [{
      name: 'Road A',
      latitude: 123.33,
      longitude: 332.67,
      radius: 100
    },{
      name: 'Road B',
      latitude: 125.33,
      longitude: 52.67,
      radius: 150
    },{
      name: 'Road C',
      latitude: 6623.33,
      longitude: 32.67,
      radius: 10
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};

Create seeder -- npx sequelize-cli seed:generate --name demo-user
Run seeder -- npx sequelize-cli db:seed:all
Run one seeder -- npm sequelize-cli db:seed --seed {seed file name}
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [{
      participant_category_id: 1,
      official_name: 'John Doe',
      username: 'johndoe',
      email: 'johndoe@temp.com',
      password: '827ccb0eea8a706c4c34a16891f84e7b',
      status: 1
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};

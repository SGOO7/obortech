#!/usr/local/bin/python2.7

from PIL import Image
import sys

try:

    image1 = Image.open(sys.argv[1])
    im1 = image1.convert('RGB')
    im1.save(sys.argv[2])

except:
    e = sys.exc_info()[0]
    print "I/O error({0}): {1}".format(e.errno, e.strerror)


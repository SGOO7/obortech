import NProgress from "nprogress";
import withAuth from "../lib/withAuth";
import {
    fetchShipmentEvents,
    seenShipmentEventDocument,
    acceptShipmentEventDocument,
    addShipmentEventComment,
    fetchShipmentDocuments
} from "../lib/api/shipment-event";
import { fetchShipment } from "../lib/api/shipment";
import AuthPage from "../components/wrapper/AuthPage";
import DocumentEvent from "../components/events/DocumentEvent";
import DocumentHash from "../components/events/DocumentHash";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import string from "../utils/stringConstants/language/eng.json";
// updated
import Button from "../components/common/form-elements/button/Button";
import CustomSelect from "../components/common/form-elements/select/CustomSelect";
var interval = null;

const DocumentPage = (props) => {
    const router = useRouter();
	const { shipment_id } = router.query;
	const [user, setUser] = useState(props.user || {});
	const [user_id, setUserId] = useState(props.user.id);
	const [participant_id, setParticipantId] = useState(0);
	const [event_category, setEventCategory] = useState(0);
	const [eventType, setEventType] = useState([]);
	const [shipment, setShipment] = useState({});
	const [shipmentSelections, setShipmentSelections] = useState([]);
	const [shipmentCargos, setShipmentCargos] = useState([]);
	const [shipmentGroups, setShipmentGroups] = useState([]);
	const [selectedContainer, setSelectedContainer] = useState(null);
	const [destinationA, setDestinationA] = useState("");
	const [destinationB, setDestinationB] = useState("");
	const [commentOpen, setCommentOpen] = useState();
	const [acceptOpen, setAcceptOpen] = useState();
	const [hashViewOpen, setHashViewOpen] = useState(false);
	const [documentHash, setDocumentHash] = useState('');
    const [documentCategories, setDocumentCategories] = useState([]);
    const [timer, setTimer] = useState(false);

	useEffect(() => {
		_fetchShipment();
	}, []);

	useEffect(() => {
		_fetchDocuments();
	}, [event_category, participant_id, selectedContainer]);

	useEffect(() => {
		if (shipment.id != undefined) {
			// Check if logged in user is part of shipment or not
			if (user.role_id != process.env.ADMIN && shipment.shipment_participants.filter(function(e) {
					return parseInt(e.user.id) === parseInt(user.id);
				}).length == 0
			) {
				router.push("/404");
			}
            const documentCategories = [];
            shipment.shipment_category?.shipment_document_categories?.map((docCategory, i) => {
                documentCategories.push(docCategory.document_category_id);
            })
            setDocumentCategories(documentCategories);
			setSelectedContainer(shipment.shipment_selections[0].selection_containers[0].container.id);

            if(!timer){
				setTimer(true);
				interval = setInterval(async() => {
                    const shipmentEvents = await fetchShipmentDocuments({
                        container_id: shipment.shipment_selections[0].selection_containers[0].container.id,
                        shipment_id: parseInt(shipment_id),
                        user_id: parseInt(user_id)
                    });
                    setEventType(shipmentEvents);
				}, process.env.EVENT_TIMER || 60000);
			}

			_fetchDocuments();
		}
	}, [shipment]);


	/**
	 * Add Comment by user to document
	 */
    const _addComment = async (comment, shipment_event_id) => {
		NProgress.start();
		try {
			const new_comment = await addShipmentEventComment({
				comment,
				user_id: user.id,
				shipment_event_id,
			});
			NProgress.done();
            _fetchDocuments();
			return new_comment;
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	const _toggleHashView = (hash = '') => {
		if(hash){
			setDocumentHash(hash);
		} else{
			setDocumentHash('');
		}
        setHashViewOpen(!hashViewOpen)
    };

    /**
     * Accept Documment
     */
    const _acceptDocument = async (shipment_event_id, acceptedDocument) => {
		NProgress.start();
		try {
            if(!acceptedDocument){
                await acceptShipmentEventDocument({
                    user_id: parseInt(user.id),
                    shipment_event_id,
                });
                _fetchDocuments();
            }
			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

    /**
     * Seen Document
     */
    const _seenDocument = async (shipment_event_id, seenDocument) => {
        NProgress.start();
		try {
            if(!seenDocument){
                await seenShipmentEventDocument({
                    user_id: user.id,
                    shipment_event_id,
                });
                _fetchDocuments();
            }
			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

    /**
	 * Request for fetching all @events
	 */
	const _fetchDocuments = async () => {
		NProgress.start();
		try {
			if (selectedContainer != null && documentCategories.length > 0) {
				const shipmentEvents = await fetchShipmentDocuments({
					container_id: selectedContainer,
					shipment_id: parseInt(shipment_id),
					user_id: parseInt(user_id)
				});
				setEventType(shipmentEvents);
			}
			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	/**
	 * Request for fetching selected @shipment and then @events of that particualr @shipment
	 */
	const _fetchShipment = async () => {
		NProgress.start();
		try {
			const shipment_details = await fetchShipment({ shipment_id });
			setShipment(shipment_details);

			setDestinationA(shipment_details.shipment_roads[0].road.name);
			setDestinationB(shipment_details.shipment_roads[shipment_details.shipment_roads.length - 1].road.name);
			setShipmentSelections(shipment_details.shipment_selections);
			const containerID = shipment_details.shipment_selections[0].selection_containers[0].container.id;
			const shipmentCargos = [];
			const shipmentGroups = [];
            shipment_details.shipment_selections.map((selection, i) => {
				if (selection.selection_containers[0].container.id == containerID) {
					selection.selection_cargos.map((cargo) => {
						shipmentCargos.push(cargo.cargo.cargoID);
					});
					selection.selection_groups.map((group) => {
						shipmentGroups.push(group.group.groupID);
					});
				}
			});
			setShipmentCargos(shipmentCargos);
			setShipmentGroups(shipmentGroups);

			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	const _changeContainer = (container_id) => {
		const shipmentCargos = [];
		const shipmentGroups = [];
		shipment.shipment_selections.map((selection, i) => {
			if (
				selection.selection_containers[0].container.id == container_id
			) {
				selection.selection_cargos.map((cargo) => {
					shipmentCargos.push(cargo.cargo.cargoID);
				});
				selection.selection_groups.map((group) => {
					shipmentGroups.push(group.group.groupID);
				});
			}
		});
		setSelectedContainer(container_id);
		setShipmentCargos(shipmentCargos);
		setShipmentGroups(shipmentGroups);
	};

    return (
        <AuthPage user={user} title={string.documentPageTitle}>
            {/* <Document/> */}
            <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                    <div className="tab-pane fade show active mt-3 w-100" id="event" role="tabpanel" aria-labelledby="event-listing">
                        <div className="row">
                            <div className="col-md-12 event-mini-stats">
								<span>
									Container:
									<CustomSelect
										className="ml-1"
										onChange={(event) => {
											_changeContainer(
												event.target.value
											);
										}}
									>
										{shipmentSelections.map(
											(selection, i) => {
												return (
													<option
														key={i}
														value={
															selection
																.selection_containers[0]
																.container.id
														}
													>
														{
															selection
																.selection_containers[0]
																.container
																.containerID
														}
													</option>
												);
											}
										)}
									</CustomSelect>
								</span>
								<span>Cargo: {shipmentCargos.join(", ")}</span>
								<span>Group: {shipmentGroups.join(", ")}</span>
								<span>
									Destination: {destinationA} - {destinationB}
								</span>
							</div>

                            <div className="shipment-table-listing mt-2 w-100 col-md-12">
                                <table className="table">
                                    <thead className="thead-dark">
                                        <tr>
                                            <th width="140" scope="col">Date</th>
                                            <th width="100" scope="col">Submitter</th>
                                            <th width="150" scope="col">Type of document</th>
                                            <th width="100" scope="col">File name</th>
                                            <th width="100" scope="col">Document seen by</th>
                                            <th width="120" scope="col" className="text-center">Actions</th>
                                            <th width="80" scope="col">Hash</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {eventType?.map((ev) => (
                                            <DocumentEvent
                                                shipment_event={ev.shipment_event}
                                                updatedAt={ev.updatedAt}
                                                user={ev.user}
                                                _seenDocument={_seenDocument}
                                                _acceptDocument={_acceptDocument}
                                                acceptedDocument={ev.shipment_event.document_accepted_users.filter(function(e) {return (e.user_id === parseInt(user.id))}).length == 0 ? false:true}
                                                seenDocument={ev.shipment_event.document_seen_users.filter(function(e) {return (e.user_id === ev.user.id)}).length == 0 ? false:true}
                                                setCommentOpen={(id) =>
                                                    setCommentOpen(id)
                                                }
                                                commentOpen={commentOpen}
                                                _addComment={_addComment}
												auth_user={user}
												_toggleHashView={_toggleHashView}
												setAcceptOpen={(id) =>
													setAcceptOpen(id)
												}
												acceptOpen={acceptOpen}
                                            />
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
				<DocumentHash
					isOpen={hashViewOpen}
					toggle={() => {_toggleHashView()}}
					documentHash={documentHash}
				/>
            </div>
        </AuthPage>
    )
}

export default withAuth(DocumentPage, { loginRequired: true });

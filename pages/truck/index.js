import PropTypes from "prop-types";
import Head from "next/head";
import Header from "../../components/HomeHeader";
import Footer from "../../components/HomeFooter";
import Sidebar from "../../components/Sidebar";
import DeleteModal from "./DeleteModal";
import AddModal from "./AddModal";
import EditModal from "./EditModal";
import NProgress from "nprogress";
import notify from "../../lib/notifier";

import withAuth from "../../lib/withAuth";
import {
  fetchTrucks,
  addTruck,
  removeTruck,
  updateTruck,
} from "../../lib/api/truck";
// updated
import Button from "../../components/common/form-elements/button/Button";

class TruckPage extends React.Component {
  static getInitialProps() {
    const truckPage = true;
    return { truckPage };
  }

  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
  };

  async componentDidMount() {
    NProgress.start();
    try {
      const trucks = await fetchTrucks();
      this.setState({ trucks });
      NProgress.done();
    } catch (err) {
      this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
      NProgress.done();
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      trucks: [],
      truck: {},
      deleteMode: "",
      selectedIndex: "",
      editMode: "",
    };
  }

  // submit truck function to check submitted details
  onTruckSubmit = (event) => {
    // event.preventDefault();
    let { truck } = this.state;
    const { truckID } = truck;

    if (!truckID) {
      notify("TruckID is required");
      return;
    }
    this.addTruckData(truck);
  };

  // add truck function
  addTruckData = async (data) => {
    NProgress.start();
    try {
      await addTruck({ truckID: data.truckID });
      const trucks = await fetchTrucks();
      $("#truckModal").modal("hide");
      this.setState({ trucks, truck: {} });
      notify("Truck added successfully!");
      NProgress.done();
    } catch (err) {
      console.error(err);
      notify("Error adding truck!");
      NProgress.done();
    }
  };

  // Function to delete entry from popup
  onDeleteEntry = async (event) => {
    event.preventDefault();
    let { deleteMode, trucks, selectedIndex } = this.state;
    if (deleteMode == "truck") {
      // delete truck data
      let trucks_data = trucks[selectedIndex];
      await removeTruck({ id: trucks_data.id });
      trucks.splice(selectedIndex, 1);
      this.setState({ trucks });
      notify("Truck deleted successfully!");
    }
  };

  // update truck function
  updateTruck = async () => {
    NProgress.start();
    let { truck, selectedIndex } = this.state;
    try {
      await updateTruck(truck);
      const trucks = await fetchTrucks();
      $("#editTruckModal").modal("hide");
      this.setState({ trucks, truck: {} });
      notify("Truck updated successfully!");
      NProgress.done();
    } catch (err) {
      console.error(err);
      notify("Error adding Truck!");
      NProgress.done();
    }
  };

  // set delete mode upon selecting delete icon
  setDeleteMode = (mode, i) => {
    if (mode) {
      this.setState({ deleteMode: mode });
      this.setState({ selectedIndex: i });
      $("#deleteModal").modal("show");
    }
  };

  setEditMode = (mode, i) => {
    if (mode) {
      this.setState({ editMode: mode });
      this.setState({ selectedIndex: i });
      if (mode == "truck") {
        let { trucks } = this.state;
        let truck = trucks[i];
        this.setState({ truck });
        $("#editTruckModal").modal("show");
      }
    }
  };

  render() {
    let { user, trucks, truck } = this.state;

    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - Manage Trucks</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <div
                    className="tab-pane fade show active mt-3 w-100"
                    id="truck"
                    role="tabpanel"
                    aria-labelledby="truck-listing"
                  >
                    <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
                      <h4 className="text-dark">Trucks Listing</h4>
                      <Button
                        className="btn btn-primary large-btn"
                        data-toggle="modal"
                        data-target="#truckModal"
                      >
                        SUBMIT TRUCK
                      </Button>
                    </div>
                    <div className="shipment-table-listing table-responsive mt-2 w-100">
                      <table className="table">
                        <thead className="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">TruckID</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {trucks.map((truck, i) => {
                            return (
                              <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{truck.truckID}</td>
                                <td>
                                  <i
                                    className="fa fa-pencil-alt"
                                    onClick={() => this.setEditMode("truck", i)}
                                  ></i>
                                  <i
                                    className="fa fa-trash"
                                    onClick={() =>
                                      this.setDeleteMode("truck", i)
                                    }
                                  ></i>
                                </td>
                              </tr>
                            );
                          })}
                          {trucks.length == 0 && (
                            <tr>
                              <td colSpan="5" className="text-center">
                                No data available!!
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <div
          className="modal fade customModal document"
          id="deleteModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <DeleteModal onDeleteEntry={this.onDeleteEntry} />
        </div>
        <div
          className="modal fade customModal document"
          id="truckModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <AddModal
            state={this.setState.bind(this)}
            onTruckSubmit={this.onTruckSubmit.bind(this)}
          />
        </div>
        <div
          className="modal fade customModal document"
          id="editTruckModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <EditModal
            truck={truck}
            state={this.setState.bind(this)}
            updateTruck={this.updateTruck.bind(this)}
          />
        </div>
        <Footer />
      </div>
    );
  }
}

export default withAuth(TruckPage, { loginRequired: true });

import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";

const EditTruckschema = Yup.object().shape({
  truckID: Yup.string()
    .trim()
    .required(`TruckID ${string.errors.required}`),
});
function EditModal({ truck, state, updateTruck }) {
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              EDIT TRUCK
            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </Button>
          </div>
          <div className="modal-body">
            <Formik
              enableReinitialize={true}
              initialValues={{
                truckID: truck.truckID || "",
              }}
              validationSchema={EditTruckschema}
              onSubmit={(values) => {
                state({
                  truck: Object.assign({}, truck, { truckID: values.truckID }),
                });
                updateTruck();
                $("#truckModal").modal("hide");
              }}
            >
              {({ errors, touched, handleChange, handleSubmit, values }) => (
                <form className="form-container" onSubmit={handleSubmit}>
                  <div className="row ml-0 mr-0 content-block">
                    <div className="form-group col-md-12 p-0">
                      <label
                        htmlFor="truckID"
                        className="col-md-12 col-form-label pl-0"
                      >
                        TruckID
                      </label>
                      <Input
                        type="text"
                        name="truckID"
                        id="truckID"
                        value={values.truckID}
                        onChange={handleChange}
                        className="form-control"
                        placeholder="TruckID"
                        // onChange={(event) => {
                        //     state({
                        //         truck: Object.assign({}, truck, { truckID: event.target.value })
                        //     });
                        // }}
                      />
                      {errors.truckID && touched.truckID ? (
                        <FormHelperMessage
                          message={errors.truckID}
                          className="error"
                        />
                      ) : null}
                    </div>
                  </div>
                  <div className="modal-footer">
                    <Button
                      className="btn btn-primary large-btn"
                      // onClick={updateTruck}
                      type="submit"
                    >
                      UPDATE
                    </Button>
                  </div>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

EditModal.propTypes = {};

EditModal.defaultProps = {};

export default EditModal;

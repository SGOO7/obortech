import PropTypes from "prop-types";
import Link from "next/link";
import Head from "next/head";
import Header from "../components/HomeHeader";
import Footer from "../components/HomeFooter";
import Sidebar from "../components/Sidebar";
import NProgress from "nprogress";
import withAuth from "../lib/withAuth";
import { DraggableItem } from "../components/shipments/DraggableItem";
// updated
// import Select from "react-select";
import Button from "../components/common/form-elements/button/Button";
import Input from "../components/common/form-elements/input/Input";
import Checkbox from "../components/common/form-elements/checkbox";
import AdvanceSelect from "../components/common/form-elements/select/AdvanceSelect";
import CustomSelect from "../components/common/form-elements/select/CustomSelect";
class ShipmentPage extends React.Component {
  static getInitialProps() {
    const shipmentPage = true;
    return { shipmentPage };
  }

  static propTypes = {
    user: PropTypes.shape({
      _id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
  };

  async componentDidMount() {
    NProgress.done();
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      shipments: {},
    };
  }
  render() {
    const { user, shipments } = this.state;
    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - Shipment Listing</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <ul className="nav nav-tabs w-100" id="myTab" role="tablist">
                    <li className="nav-item">
                      <a
                        className="nav-link active"
                        id="all-containers2"
                        data-toggle="tab"
                        href="#all2"
                        role="tab"
                        aria-controls="all"
                        aria-selected="true"
                      >
                        All Shipments
                      </a>
                    </li>
                  </ul>

                  <div className="tab-content w-100" id="myTabContent">
                    <div
                      className="tab-pane fade show active mt-3"
                      id="all2"
                      role="tabpanel"
                      aria-labelledby="all-containers"
                    >
                      <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter">
                        <h4 className="text-dark">Shipment listing</h4>
                        <Button
                          className="btn btn-primary large-btn"
                          data-toggle="modal"
                          data-target="#shipmentModal"
                        >
                          SUBMIT SHIPMENT
                        </Button>
                      </div>
                      <div className="shipment-table-listing table-responsive mt-2">
                        <table className="table">
                          <thead className="thead-dark">
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Shipment name</th>
                              <th scope="col">CargoID</th>
                              <th scope="col">Users</th>
                              <th scope="col">Temperature alert</th>
                              <th scope="col">Humidity alert</th>
                              <th scope="col">Sealing alert</th>
                              <th scope="col"></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>4</td>
                              <td>MONEX TEST8</td>
                              <td>10004</td>
                              <td style={{ width: "40%" }}>
                                China logistics, Obortech, Oyu Tolgoi,
                              </td>
                              <td align="center">-42°C - 42°C (0)</td>
                              <td align="center">10% - 75% (0)</td>
                              <td align="center">0%</td>
                              <td>
                                <i className="fa fa-pencil-alt"></i>
                                <i
                                  className="fa fa-trash"
                                  data-toggle="modal"
                                  data-target="#deleteModal"
                                ></i>
                                <i className="fa fa-stopwatch"></i>
                              </td>
                            </tr>
                            <tr>
                              <td>14</td>
                              <td>UBMN-EHCN 1-6</td>
                              <td>PRE01-HS16029011</td>
                              <td>
                                Obortech, Monex (Freight Forwarder), Demo User,
                                State Inspection MN (GOV), Precom (Shipper), ETB
                                Trading (Consignee), Customs MN (GOV), Chamber
                                of Commerce MN (NGO), Standard & Metrology MN
                                (GOV),
                              </td>
                              <td align="center">-40°C - 5°C (12)</td>
                              <td align="center">0% - 50% (12)</td>
                              <td align="center">9%</td>
                              <td>
                                <i className="fa fa-pencil-alt"></i>
                                <i
                                  className="fa fa-trash"
                                  data-toggle="modal"
                                  data-target="#deleteModal"
                                ></i>
                                <i className="fa fa-check"></i>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <div
          className="modal fade customModal document"
          id="deleteModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-md" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <Button
                  className="close"
                  type="button"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </Button>
              </div>
              <div className="modal-body text-center mb-5">
                <p>
                  <strong>DELETE RECORD ?</strong>
                </p>
                <Button
                  className="btn btn-primary large-btn"
                  type="button"
                  data-dismiss="modal"
                >
                  DELETE
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade customModal"
          id="shipmentModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5
                  className="modal-title text-dark font-weight-bold"
                  id="exampleModalLabel"
                >
                  ADD SHIPMENT
                </h5>
                <Button
                  className="close"
                  type="button"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </Button>
              </div>
              <div className="modal-body">
                <div className="shiment-name">
                  <Input
                    type="text"
                    onChange={(event) => {
                      this.setState({
                        shipment: Object.assign({}, shipment, {
                          name: event.target.value,
                        }),
                      });
                    }}
                    placeholder="Type shipment name"
                    name="name"
                  />
                </div>
                {/* form secction */}
                <form className="form-container">
                  {/* row */}
                  <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-8 pl-0">
                      <div className="row m-0">
                        <div className="form-group col-md-6 pl-0">
                          <label
                            htmlFor="email-address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Event category
                          </label>
                          <div className="col-md-12 position-relative p-0">
                            <CustomeSelect
                              className="form-control"
                              disabled=""
                              name="event_category_id"
                              onChange={(event) => {
                                this.setState({
                                  shipment: Object.assign({}, shipment, {
                                    event_category_id: event.target.value,
                                  }),
                                });
                              }}
                            >
                              <option>Coal transportation Mongolia</option>
                              <option>Option 1</option>
                            </CustomeSelect>
                          </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Document category
                          </label>
                          <div className="col-md-12 position-relative p-0">
                            <CustomeSelect
                              className="form-control"
                              disabled=""
                              onChange={(event) => {
                                this.setState({
                                  shipment: Object.assign({}, shipment, {
                                    document_category_id: event.target.value,
                                  }),
                                });
                              }}
                              name="document_category_id"
                            >
                              <option>Coal transportation Mongolia</option>
                              <option>Option 1</option>
                            </CustomeSelect>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 pr-0">
                      <div className="form-group row m-0">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0 pr-0"
                        >
                          Shipment template
                        </label>
                        <div className="col-md-12 position-relative p-0">
                          <CustomSelect
                            className="form-control"
                            onChange={(event) => {
                              this.setState({
                                shipment: Object.assign({}, shipment, {
                                  template_id: event.target.value,
                                }),
                              });
                            }}
                            name="template_id"
                          >
                            <option>Coal transportation Mongolia</option>
                            <option>Option 1</option>
                          </CustomSelect>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* //row */}
                  {/* row */}
                  <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-8 pl-0">
                      <div className="row m-0">
                        <div className="form-group col-md-6 pl-0">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Participant category
                          </label>
                          <div className="col-md-12 position-relative p-0">
                            <CustomSelect
                              className="form-control"
                              onChange={(event) => {
                                this.setState({
                                  shipment: Object.assign({}, shipment, {
                                    participant_category_id: event.target.value,
                                  }),
                                });
                              }}
                              name="participant_category_id"
                            >
                              <option>Mongolia participants</option>
                              <option>Option 1</option>
                            </CustomSelect>
                          </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Participants
                          </label>
                          <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                            <div
                              className="selected-value form-control"
                              onChange={(event) => {
                                this.setState({
                                  shipment: Object.assign({}, shipment, {
                                    participant_id: event.target.value,
                                  }),
                                });
                              }}
                              name="participant_id"
                            >
                              <span className="seleted-text">
                                Type participant name
                              </span>
                              <i className="fas fa-angle-down fa-sm"></i>
                            </div>
                            {/* dropdown */}
                            <div className="drop-content full-width">
                              <div className="tab-content" id="myTabContent">
                                <Input
                                  type="text"
                                  placeholder="Type participant name"
                                  id="myInput"
                                />
                                <a href="#about">1 </a>
                                <a href="#base">2 </a>
                                <a href="#blog">3</a>
                                <a href="#contact">4</a>
                                <a href="#custom">5 </a>
                                <a href="#support">6</a>
                                <a href="#tools">7</a>
                              </div>
                            </div>
                            {/* add btn */}
                            <div className="add-btn">
                              <Button className="btn">
                                <i className="fas fa-plus fa-sm"></i>
                              </Button>
                            </div>
                            {/* //add btn */}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 pr-0">
                      <div className="form-group row m-0">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Selected participants
                        </label>
                        <div className="col-md-12 position-relative p-0">
                          <CustomSelect className="form-control">
                            <option>Chamber of Commerce MN (GOV)</option>
                            <option>Option 1</option>
                          </CustomSelect>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* //row */}
                  {/* row */}
                  <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-8 pl-0">
                      {/* row */}
                      <div className="row m-0">
                        <div className="form-group col-md-6 pl-0">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Add group
                          </label>
                          <div className="col-md-12 position-relative p-0 d-flex">
                            <Input
                              type="text"
                              onChange={(event) => {
                                this.setState({
                                  shipment: Object.assign({}, shipment, {
                                    new_group_id: event.target.value,
                                  }),
                                });
                              }}
                              name="new_group_id"
                              placeholder="Type group ID"
                              className="form-control radius-0"
                            />
                            {/* add btn */}
                            <div className="add-btn pl-0">
                              <Button className="btn radius-0 border-left-0">
                                <i className="fas fa-plus fa-sm"></i>
                              </Button>
                            </div>
                            {/* //add btn */}
                          </div>
                          <div className="custom-control custom-checkbox small col-md-12 mt-3">
                            <Checkbox
                              type="checkbox"
                              className="custom-control-input"
                              id="Humidity"
                            />
                            <label
                              className="custom-control-label"
                              htmlFor="Humidity"
                            >
                              No groups
                            </label>
                          </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Select group
                          </label>
                          <div className="col-md-12 position-relative p-0">
                            <CustomSelect
                              className="form-control"
                              onChange={(event) => {
                                this.setState({
                                  shipment: Object.assign({}, shipment, {
                                    group_id: event.target.value,
                                  }),
                                });
                              }}
                              name="group_id"
                            >
                              <option>1</option>
                              <option>2</option>
                            </CustomSelect>
                          </div>
                        </div>
                      </div>
                      {/* //row */}
                      {/* row */}
                      <div className="row m-0">
                        <div className="form-group col-md-6 pl-0">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Add truck
                          </label>
                          <div className="col-md-12 position-relative p-0 d-flex">
                            <Input
                              type="text"
                              name=""
                              placeholder="Type truck ID"
                              className="form-control radius-0"
                            />
                            {/* add btn */}
                            <div className="add-btn pl-0">
                              <Button className="btn radius-0 border-left-0">
                                <i className="fas fa-plus fa-sm"></i>
                              </Button>
                            </div>
                            {/* //add btn */}
                          </div>
                          <div className="custom-control custom-checkbox small col-md-12 mt-3">
                            <Checkbox
                              type="checkbox"
                              className="custom-control-input"
                              id="Humidity"
                            />
                            <label
                              className="custom-control-label"
                              htmlFor="Humidity"
                            >
                              No trucks
                            </label>
                          </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Select truck
                          </label>
                          <div className="col-md-12 position-relative p-0">
                            <CustomSelect className="form-control" disabled="">
                              <option>UBE-4440</option>
                              <option>2</option>
                            </CustomSelect>
                          </div>
                        </div>
                      </div>
                      {/* //row */}
                      {/* row */}
                      <div className="row m-0">
                        <div className="form-group col-md-6 pl-0">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Add container
                          </label>
                          <div className="col-md-12 position-relative p-0 d-flex">
                            <Input
                              type="text"
                              name=""
                              placeholder="Type container ID"
                              className="form-control radius-0"
                            />
                            {/* add btn */}
                            <div className="add-btn pl-0">
                              <button className="btn radius-0 border-left-0">
                                <i className="fas fa-plus fa-sm"></i>
                              </button>
                            </div>
                            {/* //add btn */}
                          </div>
                          <div className="custom-control custom-checkbox small col-md-12 mt-3">
                            <Checkbox
                              type="checkbox"
                              className="custom-control-input"
                              id="Humidity"
                            />
                            <label
                              className="custom-control-label"
                              htmlFor="Humidity"
                            >
                              No containers
                            </label>
                          </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Select container
                          </label>
                          <div className="col-md-12 position-relative p-0">
                            <CustomSelect className="form-control" disabled="">
                              <option>CSNU-68502485 -9</option>
                              <option>2</option>
                            </CustomSelect>
                          </div>
                        </div>
                      </div>
                      {/* //row */}
                      {/* row */}
                      <div className="row m-0">
                        <div className="form-group col-md-6 pl-0">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Add cargo
                          </label>
                          <div className="col-md-12 position-relative p-0 d-flex">
                            <Input
                              type="text"
                              name=""
                              placeholder="Type cargo ID"
                              className="form-control radius-0"
                            />
                            {/* add btn */}
                            <div className="add-btn pl-0">
                              <Button className="btn radius-0 border-left-0">
                                <i className="fas fa-plus fa-sm"></i>
                              </Button>
                            </div>
                            {/* //add btn */}
                          </div>
                        </div>
                        <div className="form-group col-md-6">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Cargo ID
                          </label>
                          <div className="col-md-12 position-relative p-0">
                            <CustomSelect className="form-control" disabled="">
                              <option>PRE001-587621</option>
                              <option>2</option>
                            </CustomSelect>
                          </div>
                        </div>
                      </div>
                      {/* //row */}
                    </div>
                    <div className="col-md-4 pr-0 right-form-field">
                      {/* section 1 */}
                      <div className="data-block">
                        <div className="form-group row m-0">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Temperature alert (°C)
                          </label>
                          <div className="col-md-6 pl-0">
                            <Input
                              type="text"
                              name=""
                              className="form-control radius-0"
                              placeholder="Min range"
                            />
                          </div>
                          <div className="col-md-6 pl-0 pr-0">
                            <Input
                              type="text"
                              name=""
                              className="form-control radius-0"
                              placeholder="Max range"
                            />
                          </div>
                        </div>
                        <div className="form-group row m-0">
                          <div className="col-md-6 pl-0">
                            <label
                              htmlFor="email_address"
                              className="col-md-12 col-form-label pl-0 pr-0"
                            >
                              Interval (minutes)
                            </label>
                            <Input
                              type="text"
                              name=""
                              className="form-control radius-0"
                              placeholder="Minutes"
                            />
                          </div>
                          <div className="col-md-6 pl-0 pr-0">
                            <label
                              htmlFor="email_address"
                              className="col-md-12 col-form-label pl-0 pr-0"
                            >
                              Allowed occurance
                            </label>
                            <Input
                              type="text"
                              name=""
                              className="form-control radius-0"
                              placeholder="Times"
                            />
                          </div>
                        </div>
                      </div>
                      {/* //section 1 */}
                      {/* section 1 */}
                      <div className="data-block">
                        <div className="form-group row m-0">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Humidity alert (%)
                          </label>
                          <div className="col-md-6 pl-0">
                            <Input
                              type="text"
                              name=""
                              className="form-control radius-0"
                              placeholder="Min range"
                            />
                          </div>
                          <div className="col-md-6 pl-0 pr-0">
                            <Input
                              type="text"
                              name=""
                              className="form-control radius-0"
                              placeholder="Max range"
                            />
                          </div>
                        </div>
                        <div className="form-group row m-0">
                          <div className="col-md-6 pl-0">
                            <label
                              htmlFor="email_address"
                              className="col-md-12 col-form-label pl-0 pr-0"
                            >
                              Interval (minutes)
                            </label>
                            <Input
                              type="text"
                              name=""
                              className="form-control radius-0"
                              placeholder="Minutes"
                            />
                          </div>
                          <div className="col-md-6 pl-0 pr-0">
                            <label
                              htmlFor="email_address"
                              className="col-md-12 col-form-label pl-0 pr-0"
                            >
                              Allowed occurance
                            </label>
                            <Input
                              type="text"
                              name=""
                              className="form-control radius-0"
                              placeholder="Times"
                            />
                          </div>
                        </div>
                      </div>
                      {/* //section 1 */}
                      {/* section 1 */}
                      <div className="data-block">
                        <div className="form-group row m-0">
                          <label
                            htmlFor="email_address"
                            className="col-md-12 col-form-label pl-0"
                          >
                            Sealing alert (ambient %)
                          </label>
                          <div className="col-md-6 pl-0">
                            <Input
                              type="text"
                              name=""
                              className="form-control radius-0"
                              placeholder="Breaking point"
                            />
                          </div>
                        </div>
                      </div>
                      {/* //section 1 */}
                    </div>
                  </div>
                  {/* //row */}
                </form>
                {/* //form secction */}
              </div>
              <div className="modal-footer">
                <Button
                  className="btn btn-primary large-btn"
                  type="button"
                  data-dismiss="modal"
                  data-toggle="modal"
                  data-target="#shipmentstep2Modal"
                >
                  NEXT
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade customModal"
          id="shipmentstep2Modal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5
                  className="modal-title text-dark font-weight-bold"
                  id="exampleModalLabel"
                >
                  ADD SHIPMENT
                </h5>
                <Button
                  className="close"
                  type="button"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </Button>
              </div>
              <div className="modal-body">
                <form className="form-container">
                  <div className="row ml-0 mr-0 content-block">
                    <div className="form-group col-md-4 pl-0">
                      <label
                        htmlFor="email_address"
                        className="col-md-12 col-form-label pl-0"
                      >
                        Select group
                      </label>
                      <div className="col-md-12 position-relative p-0">
                        <CustomSelect className="form-control" disabled="">
                          <option>1</option>
                          <option>Option 1</option>
                        </CustomSelect>
                      </div>
                    </div>

                    <div className="form-group col-md-4">
                      <label
                        htmlFor="email_address"
                        className="col-md-12 col-form-label pl-0"
                      >
                        Select truck
                      </label>
                      <div className="col-md-12 position-relative p-0">
                        <CustomSelect className="form-control" disabled="">
                          <option>UBE-4440</option>
                          <option>Option 1</option>
                        </CustomSelect>
                      </div>
                    </div>

                    <div className="form-group col-md-4  pr-0">
                      <label
                        htmlFor="email_address"
                        className="col-md-12 col-form-label pl-0"
                      >
                        Select truck
                      </label>
                      <div className="col-md-12 position-relative p-0">
                        <CustomSelect className="form-control" disabled="">
                          <option>UBE-4440</option>
                          <option>Option 1</option>
                        </CustomSelect>
                      </div>
                    </div>
                  </div>
                  <div className="row ml-0 mr-0 content-block">
                    <div className="form-group col-md-4 pl-0">
                      <label
                        htmlFor="email_address"
                        className="col-md-12 col-form-label pl-0"
                      >
                        Select cargo
                      </label>
                      <div className="col-md-12 position-relative p-0">
                        <CustomSelect className="form-control" disabled="">
                          <option>CSNU-68502485 -9</option>
                          <option>Option 1</option>
                        </CustomSelect>
                      </div>
                    </div>

                    <div className="form-group col-md-5">
                      <label
                        htmlFor="email_address"
                        className="col-md-12 col-form-label pl-0"
                      >
                        Connect device
                      </label>
                      <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                        <div className="selected-value form-control">
                          <span className="seleted-text">Type device name</span>
                          <i className="fas fa-angle-down fa-sm"></i>
                        </div>
                        <div className="drop-content full-width">
                          <div className="tab-content" id="myTabContent">
                            <Input
                              type="text"
                              placeholder="Type participant name"
                              id="myInput"
                            />
                            <a href="#about">1 </a>
                            <a href="#base">2 </a>
                            <a href="#blog">3</a>
                            <a href="#contact">4</a>
                            <a href="#custom">5 </a>
                            <a href="#support">6</a>
                            <a href="#tools">7</a>
                          </div>
                        </div>
                        <div className="add-btn mr-1">
                          <Button className="btn">
                            <i className="fas fa-plus fa-sm"></i>
                          </Button>
                        </div>
                        <div className="add-btn">
                          <Button className="btn">
                            <i className="fas fa-minus fa-sm"></i>
                          </Button>
                        </div>
                      </div>
                    </div>

                    <div className="form-group col-md-3  pr-0 d-flex align-items-end">
                      <div className="select-all">
                        <label>
                          <Checkbox type="checkbox" name="" /> All
                        </label>
                      </div>
                      <div className="">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Data interval
                        </label>
                        <div className="col-md-12 position-relative p-0">
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Minutes"
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="shipment-table-listing table-responsive">
                    <table className="table">
                      <thead className="thead-dark">
                        <tr>
                          <th scope="col"></th>
                          <th scope="col">Road name</th>
                          <th scope="col">Latitude</th>
                          <th scope="col">Longitude</th>
                          <th scope="col">Radius</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colSpan="6">
                            <DraggableItem className="table__drag" />
                          </td>
                        </tr>

                        {/* <tr>
												<td><i className="fa fa-sort"></i></td>
												<td><select className="form-control"><option>Select road name</option></select></td>
												<td><span>Insert latitude</span></td>
												<td><span>Insert longitude</span></td>
												<td>Insert radius</td>
												<td>
													<i className="fa fa-print"></i>
													<i className="fa fa-trash"></i>
												</td>
												</tr> */}
                      </tbody>
                    </table>
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <Button
                  className="btn btn-secondary large-btn"
                  type="button"
                  data-dismiss="modal"
                  data-toggle="modal"
                  data-target="#shipmentModal"
                >
                  Back
                </Button>
                <Button
                  className="btn btn-secondary large-btn"
                  type="button"
                  data-dismiss="modal"
                >
                  SAVE AS TEMPLATE
                </Button>
                <Button
                  className="btn btn-primary large-btn"
                  type="button"
                  data-dismiss="modal"
                >
                  SUBMIT
                </Button>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withAuth(ShipmentPage, { loginRequired: true });

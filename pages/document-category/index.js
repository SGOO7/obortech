import PropTypes from "prop-types";
import Head from "next/head";
import Header from "../../components/HomeHeader";
import Footer from "../../components/HomeFooter";
import Sidebar from "../../components/Sidebar";
import DeleteModal from "./DeleteModal";
import AddModal from "./AddModal";
import EditModal from "./EditModal";
import NProgress from "nprogress";
import notify from "../../lib/notifier";

import withAuth from "../../lib/withAuth";
import {
  fetchDocumentCategories,
  addDocumentCategory,
  removeDocumentCategory,
  updateDocumentCategory,
} from "../../lib/api/document-category";

class DocumentCategoryPage extends React.Component {
  static getInitialProps() {
    const documentCategoryPage = true;
    return { documentCategoryPage };
  }

  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
    document_category: {},
  };

  async componentDidMount() {
    NProgress.start();
    try {
      const document_categories = await fetchDocumentCategories();
      // const documents = await fetchEvent();
      this.setState({ document_categories });
      NProgress.done();
    } catch (err) {
      this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
      NProgress.done();
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      document_categories: [],
      document_category: {},
      deleteMode: "",
      selectedIndex: "",
      editMode: "",
    };
  }

  // submit category function to check submitted details
  onCategorySubmit = (e) => {
    e.preventDefault();
    const { document_category } = this.state;
    const { name } = document_category;

    if (!name) {
      notify("Category Name is required");
      return;
    }

    this.addCategory(document_category);
  };

  // Function to delete entry from popup
  onDeleteEntry = async (e) => {
    e.preventDefault();
    let {
      deleteMode,
      document_categories,
      documents,
      selectedIndex,
    } = this.state;
    // check which category to delete
    if (deleteMode == "document") {
      // delete document data
      let documents_data = documents[selectedIndex];
      await removeEvents({ id: documents_data.id });
      documents.splice(selectedIndex, 1);
      this.setState({ documents });
      notify("Events deleted successfully!");
    } else if (deleteMode == "document_category") {
      // delete document category data
      let category = document_categories[selectedIndex];
      await removeDocumentCategory({ id: category.id });
      document_categories.splice(selectedIndex, 1);
      this.setState({ document_categories });
      notify("Category deleted successfully!");
    }
  };

  // add document category function
  addCategory = async (data) => {
    NProgress.start();
    try {
      const category = await addDocumentCategory({ name: data.name });
      let { document_categories } = this.state;
      document_categories.push(category);
      this.setState({ document_categories, document_category: {} });
      notify("Category added successfully!");
      NProgress.done();
    } catch (err) {
      console.error(err);
      notify("Error adding category!");
      NProgress.done();
    }
  };

  // update document category function
  updateCategory = async () => {
    NProgress.start();
    let { document_category, selectedIndex } = this.state;
    try {
      await updateDocumentCategory(document_category);
      let { document_categories } = this.state;
      document_categories[selectedIndex] = document_category;
      this.setState({ document_categories, document_category: {} });
      notify("Category updated successfully!");
      NProgress.done();
    } catch (err) {
      console.error(err);
      notify("Error adding category!");
      NProgress.done();
    }
  };

  // set delete mode upon selecting delete icon
  setDeleteMode = (mode, i) => {
    if (mode) {
      this.setState({ deleteMode: mode });
      this.setState({ selectedIndex: i });
      $("#deleteModal").modal("show");
    }
  };

  setEditMode = (mode, i) => {
    if (mode) {
      this.setState({ editMode: mode });
      this.setState({ selectedIndex: i });
      if (mode == "document") {
        let { documents } = this.state;
        let document = documents[i];
        this.setState({ document });
        $("#editEventsModal").modal("show");
      } else if (mode == "document_category") {
        let { document_categories } = this.state;
        let document_category = document_categories[i];
        this.setState({ document_category });
        $("#editDocumentCategoryModal").modal("show");
      }
    }
  };

  render() {
    const { user, document_categories, document_category } = this.state;
    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - Manage Event</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <div
                    className="tab-pane fade show active mt-3 w-100"
                    id="document_category"
                    role="tabpanel"
                    aria-labelledby="document-listing"
                  >
                    <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 document-filter ">
                      <h4 className="text-dark">Category Listing</h4>
                      <Button
                        className="btn btn-primary large-btn"
                        data-toggle="modal"
                        data-target="#documentCategoryModal"
                      >
                        SUBMIT CATEGORY
                      </Button>
                    </div>
                    <div className="shipment-table-listing table-responsive mt-2 w-100">
                      <table className="table">
                        <thead className="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {document_categories.map((category, i) => {
                            return (
                              <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{category.name}</td>
                                <td>
                                  <i
                                    className="fa fa-pencil-alt"
                                    onClick={() =>
                                      this.setEditMode("document_category", i)
                                    }
                                  ></i>
                                  <i
                                    className="fa fa-trash"
                                    onClick={() =>
                                      this.setDeleteMode("document_category", i)
                                    }
                                  ></i>
                                </td>
                              </tr>
                            );
                          })}
                          {document_categories.length == 0 && (
                            <tr>
                              <td colSpan="3" className="text-center">
                                No data available!!
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <div
          className="modal fade customModal document"
          id="deleteModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <DeleteModal onDeleteEntry={this.onDeleteEntry} />
        </div>
        {/* ADD document popup */}
        <div
          className="modal fade customModal document"
          id="documentModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5
                  className="modal-title text-dark font-weight-bold"
                  id="exampleModalLabel"
                >
                  DOCUMENTS
                </h5>
                <Button
                  className="close"
                  type="button"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </Button>
              </div>
              <div className="modal-body">
                {/* form secction */}
                <form className="form-container">
                  {/* row */}
                  <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          New document type
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              Type document type
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn">
                            <Button className="btn">
                              <i className="fas fa-plus fa-sm"></i>
                            </Button>
                          </div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Delete document type
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              CSNU-68502485 -9
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn">
                            <Button className="btn">
                              <i className="fas fa-minus fa-sm"></i>
                            </Button>
                          </div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* //row */}

                  {/* row */}
                  <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          New category
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              Type category name
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn">
                            <Button className="btn">
                              <i className="fas fa-plus fa-sm"></i>
                            </Button>
                          </div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Select category
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              CSNU-68502485 -9
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn"></div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* //row */}

                  {/* row */}
                  <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Add document types to the category
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              Type document type name
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn">
                            <Button className="btn">
                              <i className="fas fa-plus fa-sm"></i>
                            </Button>
                          </div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Added document types
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              Document type name
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn"></div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* //row */}
                </form>
                {/* //form secction */}
              </div>
              <div className="modal-footer">
                <Button
                  className="btn btn-primary large-btn"
                  type="button"
                  data-dismiss="modal"
                >
                  SAVE CATEGORY
                </Button>
                <Button
                  className="btn btn-secondary large-btn"
                  type="button"
                  data-dismiss="modal"
                >
                  DELETE CATEGORY
                </Button>
              </div>
            </div>
          </div>
        </div>
        {/* //ADD document popup */}

        {/* ADD document popup */}
        <div
          className="modal fade customModal document"
          id="eventModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5
                  className="modal-title text-dark font-weight-bold"
                  id="exampleModalLabel"
                >
                  EVENT
                </h5>
                <Button
                  className="close"
                  type="button"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </Button>
              </div>
              <div className="modal-body">
                {/* form secction */}
                <form className="form-container">
                  {/* row */}
                  <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          New document
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              Type document name
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn">
                            <Button className="btn">
                              <i className="fas fa-plus fa-sm"></i>
                            </Button>
                          </div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Delete document
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              CSNU-68502485 -9
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn">
                            <Button className="btn">
                              <i className="fas fa-minus fa-sm"></i>
                            </Button>
                          </div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* //row */}

                  {/* row */}
                  <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          New category
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              Type category name
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn">
                            <Button className="btn">
                              <i className="fas fa-plus fa-sm"></i>
                            </Button>
                          </div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Select category
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              CSNU-68502485 -9
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn"></div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* //row */}

                  {/* row */}
                  <div className="row ml-0 mr-0 content-block">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Add documents to the category
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              Type document name
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn">
                            <Button className="btn">
                              <i className="fas fa-plus fa-sm"></i>
                            </Button>
                          </div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label
                          htmlFor="email_address"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Added documents
                        </label>
                        <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                          <div className="selected-value form-control">
                            <span className="seleted-text">Event name</span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content full-width">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type document name"
                                id="myInput"
                              />
                              <a href="#about">1 </a>
                              <a href="#base">2 </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">5 </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                          {/* add btn */}
                          <div className="add-btn"></div>
                          {/* //add btn */}
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* //row */}
                </form>
                {/* //form secction */}
              </div>
              <div className="modal-footer">
                <Button
                  className="btn btn-primary large-btn"
                  type="button"
                  data-dismiss="modal"
                >
                  SAVE CATEGORY
                </Button>
                <Button
                  className="btn btn-secondary large-btn"
                  type="button"
                  data-dismiss="modal"
                >
                  DELETE CATEGORY
                </Button>
              </div>
            </div>
          </div>
        </div>
        {/* //ADD document popup */}
        <div
          className="modal fade customModal document"
          id="documentCategoryModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <AddModal
            state={this.setState.bind(this)}
            onCategorySubmit={this.onCategorySubmit.bind(this)}
          />
        </div>
        <div
          className="modal fade customModal document"
          id="editDocumentCategoryModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <EditModal
            document_category={document_category}
            state={this.setState.bind(this)}
            updateCategory={this.updateCategory.bind(this)}
          />
        </div>
        <Footer />
      </div>
    );
  }
}

export default withAuth(DocumentCategoryPage, { loginRequired: true });

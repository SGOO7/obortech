import PropTypes from 'prop-types';
import Head from 'next/head';
import Header from '../../components/HomeHeader';
import Footer from '../../components/HomeFooter';
import Sidebar from '../../components/Sidebar';
import NProgress from 'nprogress';
import notify from '../../lib/notifier';

import withAuth from '../../lib/withAuth';
import { fetchAPICredentials, saveAPICredentials } from '../../lib/api/api-key';

class APIKeyPage extends React.Component {
  	static getInitialProps() {
		const apiKeyPage = true;
		return { apiKeyPage };
  	}

  	static propTypes = {
		user: PropTypes.shape({
			id: PropTypes.string,
		})
  	};

  	static defaultProps = {
		user: null,
  	};

  	async componentDidMount() {
		NProgress.start();
		try {
			const apiCredentials = await fetchAPICredentials();
			this.setState({ apiCredentials });
			NProgress.done();
		} catch (err) {
			this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
			NProgress.done();
		}
  	}

  	constructor(props) {
		super(props);

		this.state = {
			user: props.user || {},
			apiCredentials: {},
		};
	}
	 
	onSubmit = (event) => {
		event.preventDefault();
		const { apiCredentials } = this.state;
		const { value } = apiCredentials;
	
		if (!value) {
			notify('API key is required');
			return;
		}
	
		this.saveData(apiCredentials);
	};

	// save function
	saveData = async (data) => {
		NProgress.start();
		try {
			await saveAPICredentials(data);
			notify('API key saved successfully!');
			NProgress.done();
		} catch (err) {
			console.error(err);
			notify('Error saving API key!');
			NProgress.done();
		}
	};

  	render() {
		const { user, apiCredentials } = this.state;
		return (
			<div id="page-top">
				<Head>
					<title>{process.env.APP_NAME} - Manage API Credentials</title>
				</Head>
				{/* Page Wrapper */}
				<div id="wrapper">
					{/* Sidebar */}
					<Sidebar />
					{/* End of Sidebar */}
					{/* Content Wrapper */}
					<div id="content-wrapper" className="d-flex flex-column">
						{/* Main Content */}
						<div id="content">
							{/* Topbar */}
							<Header user={user} />
							{/* End of Topbar */}
							<div className="container-fluid">
								<div className="row d-flex shipment-listing">
                                    <form className="form-container" onSubmit={this.onSubmit}>
                                        <div className="row ml-0 mr-0 content-block">
                                            <div className="form-group col-md-12 p-0">
                                                <label htmlFor="name" className="col-md-12 col-form-label pl-0">API Key</label>
                                                <input 
                                                    type="text" 
                                                    name="apiKey" 
                                                    id="apiKey" 
                                                    className="form-control" 
                                                    placeholder="Enter API Key" 
                                                    value={apiCredentials.value || ''}
                                                    onChange={(event) => {
                                                        this.setState({
                                                            apiCredentials: Object.assign({}, apiCredentials, { value: event.target.value })
                                                        });
                                                    }}
                                                />                     
                                            </div>
                                            <button className="btn btn-primary large-btn" onClick={this.onSubmit} type="button" data-dismiss="modal">UPDATE</button>
                                        </div>
                                    </form>
								</div>
							</div>
						</div>
						{/* End of Main Content */}
					</div>
					{/* End of Content Wrapper */}
				</div>
				{/* End of Page Wrapper */}
				<Footer />
			</div>
		);
	}
}

export default withAuth(APIKeyPage, { loginRequired: true });
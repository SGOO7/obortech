import PropTypes from "prop-types";
import NProgress from "nprogress";
import notify from "../lib/notifier";
import withAuth from "../lib/withAuth";
import {
	addShipmentEvent,
	fetchShipmentEvents,
	removeShipmentEvent,
    seenShipmentEventDocument,
	addShipmentEventComment,
	acceptShipmentEventDocument,
} from "../lib/api/shipment-event";
import { fetchShipment } from "../lib/api/shipment";
import { fetchEvents } from "../lib/api/event";
import { fetchEventCategories } from "../lib/api/event-category";
import { fetchShipmentEventCategories, fetchShipmentDocumentCategories } from "../lib/api/shipment-category";
import AuthPage from "../components/wrapper/AuthPage";
import EventType from "../components/events/EventType";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import EventModal from "../components/events/EventModal";
import DocumentModal from "../components/events/DocumentModal";
import string from "../utils/stringConstants/language/eng.json";
// updated
import Button from "../components/common/form-elements/button/Button";
import CustomSelect from "../components/common/form-elements/select/CustomSelect";
var interval = null;

const EventPage = (props) => {
	const arr = [1, 2, 3, 4];
	const router = useRouter();
	const { shipment_id } = router.query;

	const [user, setUser] = useState(props.user || {});
	const [user_id, setUserId] = useState(props.user.id);
	const [participant_id, setParticipantId] = useState(0);
	const [event_category, setEventCategory] = useState(0);
	const [shipment_category, setShipmentCategory] = useState(0);
	const [events, setEvents] = useState([]);
	const [event, setEvent] = useState({});
	const [deleteMode, setDeleteMode] = useState("");
	const [editMode, setEditMode] = useState("");
	const [loading, setLoading] = useState(false);
	const [eventOpen, setEventOpen] = useState(false);
	const [documentOpen, setDocumentOpen] = useState(false);
	const [eventParticipantFilters, setEventParticipantFilters] = useState([]);
	const [eventFilters, setEventFilters] = useState([]);
	const [eventType, setEventType] = useState([]);
	const [documentEvents, setDocumentEvents] = useState([]);
	const [transportEvents, setTransportEvents] = useState([]);
	const [shipment, setShipment] = useState({});
	const [shipmentSelections, setShipmentSelections] = useState([]);
	const [shipmentCargos, setShipmentCargos] = useState([]);
	const [shipmentGroups, setShipmentGroups] = useState([]);
	const [selectedContainer, setSelectedContainer] = useState(null);
	const [destinationA, setDestinationA] = useState("");
	const [destinationB, setDestinationB] = useState("");
	const [commentOpen, setCommentOpen] = useState();
	const [acceptOpen, setAcceptOpen] = useState();
	const [timer, setTimer] = useState(false);

	useEffect(() => {
		_fetchShipment();
	}, []);

	useEffect(() => {
		_fetchEvents();
	}, [event_category, participant_id, selectedContainer]);

	useEffect(() => {
		if (shipment.id != undefined) {

			// Check if logged in user is part of shipment or not
			if (user.role_id != process.env.ADMIN && shipment.shipment_participants.filter(function(e) {
					return parseInt(e.user.id) === parseInt(user.id);
				}).length == 0
			) {
				router.push("/404");
			}
			setSelectedContainer(
				shipment.shipment_selections[0].selection_containers[0]
					.container.id
			);
			if(!timer){
				setTimer(true);
				interval = setInterval(async() => {
					const shipmentEvents = await fetchShipmentEvents({
						container_id: shipment.shipment_selections[0].selection_containers[0].container.id,
						shipment_id: parseInt(shipment_id),
						user_id: parseInt(user_id),
						event_category: parseInt(event_category),
						participant_id: parseInt(participant_id),
					});
					setEventType(shipmentEvents);
				}, process.env.EVENT_TIMER || 60000);
			}
			_fetchEvents();
		}
	}, [shipment]);

	/**
	 * Request for fetching all @events
	 */
	const _fetchEvents = async () => {
		NProgress.start();
		try {
			if (selectedContainer != null) {
				const shipmentEvents = await fetchShipmentEvents({
					container_id: selectedContainer,
					shipment_id: parseInt(shipment_id),
					user_id: parseInt(user_id),
					event_category: parseInt(event_category),
					participant_id: parseInt(participant_id),
				});
				setEventType(shipmentEvents);
			}
			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	/**
	 * Request for fetching all transport @events
	 */
	const _fetchTransportEvents = async (event_categories) => {
		NProgress.start();
		let transportEvntCategories = [1];
		try {
			if (event_categories) {
				event_categories.map((category) => {
					transportEvntCategories.push(category.event_category_id);
				});
			}
			const transport_events = await fetchEvents({
				categories: transportEvntCategories,
                type: 'event'
			});
			setTransportEvents(transport_events);
			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	/**
	 * Request for fetching all document @events
	 */
	const _fetchDocumentEvents = async (document_categories) => {
		NProgress.start();
		let documentEvntCategories = [2];
		try {
            if (document_categories) {
				document_categories.map((category) => {
					documentEvntCategories.push(category.document_category_id);
				});
			}
			const document_events = await fetchEvents({
				categories: documentEvntCategories,
                type: 'document'
			});
			setDocumentEvents(document_events);
			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	/**
	 * Request for fetching selected @shipment and then @events of that particualr @shipment
	 */
	const _fetchShipment = async () => {
		NProgress.start();
		try {
			const shipment_details = await fetchShipment({ shipment_id });
			setEventParticipantFilters(shipment_details.shipment_participants);
			setShipment(shipment_details);

			const event_categories = await fetchShipmentEventCategories({
				shipment_category_id: shipment_details.shipment_category_id,
			});
			setEventFilters(event_categories);

            //Fetch events
			_fetchTransportEvents(event_categories);

            //Fetch document events
            const document_categories = await fetchShipmentDocumentCategories({
				shipment_category_id: shipment_details.shipment_category_id,
			});
            _fetchDocumentEvents(document_categories);

			setDestinationA(shipment_details.shipment_roads[0].road.name);
			setDestinationB(shipment_details.shipment_roads[shipment_details.shipment_roads.length - 1].road.name);
			setShipmentSelections(shipment_details.shipment_selections);
			const containerID = shipment_details.shipment_selections[0].selection_containers[0].container.id;
			const shipmentCargos = [];
			const shipmentGroups = [];
			shipment_details.shipment_selections.map((selection, i) => {
				if (selection.selection_containers[0].container.id == containerID) {
					selection.selection_cargos.map((cargo) => {
						shipmentCargos.push(cargo.cargo.cargoID);
					});
					selection.selection_groups.map((group) => {
						shipmentGroups.push(group.group.groupID);
					});
				}
			});
			setShipmentCargos(shipmentCargos);
			setShipmentGroups(shipmentGroups);

			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	const _changeContainer = (container_id) => {
		const shipmentCargos = [];
		const shipmentGroups = [];
		shipment.shipment_selections.map((selection, i) => {
			if (
				selection.selection_containers[0].container.id == container_id
			) {
				selection.selection_cargos.map((cargo) => {
					shipmentCargos.push(cargo.cargo.cargoID);
				});
				selection.selection_groups.map((group) => {
					shipmentGroups.push(group.group.groupID);
				});
			}
		});
		if(container_id){
			setSelectedContainer(container_id);
			clearInterval(interval);
			interval = setInterval(async() => {
				const shipmentEvents = await fetchShipmentEvents({
					container_id: parseInt(container_id),
					shipment_id: parseInt(shipment_id),
					user_id: parseInt(user_id),
					event_category: parseInt(event_category),
					participant_id: parseInt(participant_id),
				});
				setEventType(shipmentEvents);
			}, process.env.EVENT_TIMER || 60000);
		}
		setShipmentCargos(shipmentCargos);
		setShipmentGroups(shipmentGroups);
	};

	const _submitEvent = async (
		event_id,
		start_date,
		selectedEventtUsers,
		selectedDocumenttUsers,
		comment,
		file,
        type
	) => {
		if (event_id == 0) {
			notify("Please select Event Type");
			return;
		}

        if(type == 'document' && !file){
            notify("Please select Document");
			return;
        }

		let formData = new FormData();
		formData.append("event_id", event_id);
		formData.append("start_date", start_date);
		formData.append("selectedEventUsers", selectedEventtUsers);
		formData.append("selectedDocumentUsers", selectedDocumenttUsers);
		formData.append("comment", comment);
		formData.append("file", file);
		formData.append("container_id", selectedContainer);
		formData.append("shipment_id", shipment_id);
		formData.append("user_id", user.id);
        formData.append("type", type);

		NProgress.start();
		try {
			await addShipmentEvent(formData);
			_fetchEvents();
			setEventOpen(false);
			setDocumentOpen(false);
			NProgress.done();
		} catch (err) {
			notify("Error adding Event");
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	const _addComment = async (comment, shipment_event_id) => {
		NProgress.start();
		try {
			const new_comment = await addShipmentEventComment({
				comment,
				user_id: user.id,
				shipment_event_id,
			});
			NProgress.done();
			return new_comment;
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	const _acceptDocument = async (shipment_event_id) => {
		NProgress.start();
		try {
			await acceptShipmentEventDocument({
				user_id: user.id,
				shipment_event_id,
			});
			_fetchEvents();
			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

    /**
     * Seen Document
     */
    const _seenDocument = async (shipment_event_id, seenDocument) => {
        NProgress.start();
		try {
            if(!seenDocument){
                await seenShipmentEventDocument({
                    user_id: user.id,
                    shipment_event_id,
                });
                _fetchEvents();
            }
			NProgress.done();
		} catch (err) {
			console.error("Error while fething events => ", err);
			NProgress.done();
		}
	};

	/**
	 * Validations for event add & update.
	 * @param e current instance of button submit.
	 */
	const _onEventsSubmit = (e) => {
		e.preventDefault();
		const {
			username,
			official_name,
			password,
			repeat_password,
			event_category_id,
		} = event;

		if (!event_category_id) {
			notify("Please select Category");
			return;
		}
		if (!username) {
			notify("Username is required");
			return;
		}
		if (!password) {
			notify("Password is required");
			return;
		}
		if (!repeat_password) {
			notify("Repeat Password is required");
			return;
		} else {
			if (password != repeat_password) {
				notify("Password does not match Repeat Password");
				return;
			}
		}
		if (!official_name) {
			notify("Official Name is required");
			return;
		}
		//API Call for submit event.
		_submitEventsData(event);
	};

	/**
	 * Request for event add & update.
	 * @param data contains all info about event.
	 */
	const _submitEventsData = async (data) => {
		NProgress.start();
		try {
			let eventRes = {};
			if (editMode === "event") {
				notify("Event updated successfully!");
			} else {
				notify("Event added successfully!");
			}
		} catch (err) {
			console.error("Error in saving event => ", err);
			notify("Error saving event!");
			NProgress.done();
		}
	};

	/**
	 * Request for deleting @event.
	 * @param e is instance of button clicked.
	 */
	const _onDeleteEntry = async (id) => {
		NProgress.start();
		try {
			await removeShipmentEvent({ id: id });
			_fetchEvents();
			notify("Event deleted successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error deleting event!");
			NProgress.done();
		}
	};

	// set delete mode upon selecting delete icon
	const _setDeleteMode = (mode, i) => {
		if (mode) {
			setDeleteMode(mode);
		}
	};

	const _setEditMode = (mode, i) => {
		if (mode) {
			setEditMode(mode);
			if (mode == "event") {
				const eventSelected = events[i];
				setEvent(eventSelected);
			}
		}
	};

	const _toggleDocument = () => {
		setDocumentOpen(!documentOpen);
	};

	const _toggleEvent = () => {
		setEventOpen(!eventOpen);
	};
	const cargos = ["45G1 (B)", "45G1 (B)", "45G1 (B)", "45G1 (B)", "45G1 (B)"];
	return (
		<AuthPage user={user} title={string.eventPageTitle}>
			<div className="container-fluid">
				<div className="row d-flex shipment-listing">
					<div
						className="tab-pane fade show active mt-3 w-100"
						id="event"
						role="tabpanel"
						aria-labelledby="event-listing"
					>
						<div className="row">
							{/* event mini stats */}
							<div className="col-md-12 event-mini-stats">
								<span>
									Container:
									<CustomSelect
										className="ml-1"
										onChange={(event) => {
											_changeContainer(
												event.target.value
											);
										}}
									>
										{shipmentSelections.map(
											(selection, i) => {
												return (
													<option
														key={i}
														value={
															selection.selection_containers[0].container.id
														}
													>
														{ selection.selection_containers[0].container.containerID }
													</option>
												);
											}
										)}
									</CustomSelect>
								</span>
								<span>Cargo: {shipmentCargos.join(", ")}</span>
								<span>Group: {shipmentGroups.join(", ")}</span>
								<span>
									Destination: {destinationA} - {destinationB}
								</span>
							</div>
							{/* //event mini stats */}

							{/* event filter */}
							<div className="col-md-12 event-filter">
								<div className="d-flex col-md-10 p-0 mt-3">
									<CustomSelect
										value={participant_id || 0}
										className="form-control"
										onChange={(event) => {
											setParticipantId(
												event.target.value
											);
										}}
									>
										<option value="0">
											Show for all Participants
										</option>
										{eventParticipantFilters?.length > 0 && eventParticipantFilters?.map((item) => (
											<option
												key={item.user.id}
												value={item.user.id}
											>
												{item.user.official_name}
											</option>
										))}
									</CustomSelect>
									<CustomSelect
										className="form-control"
										onChange={(event) => {
											setEventCategory(
												event.target.value
											);
										}}
									>
										<option value="0">Show all Events</option>
										<option value="1">Transport Events</option>
										<option value="2">Document Events</option>
										<option value="3">Alert Events</option>
										{/*eventFilters.map((item) => (
											<option
												key={item.event_category_id}
												value={item.event_category_id}
											>
												{item.event_category.name}
											</option>
										))*/}
									</CustomSelect>
									<Button className="btn btn-primary large-btn" onClick={_toggleEvent}>
										SUBMIT EVENT
									</Button>
									<Button className="btn btn-primary large-btn" onClick={_toggleDocument}>
										SUBMIT DOCUMENT
									</Button>
								</div>
							</div>
							{/* //event filter */}
						</div>

						<div className="row d-flex event-listing">
							<div className="col-md-10">
								<div className="main-card mb-3 card">
									<div className="card-body">
										<div className="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
											{eventType?.map((ev) => (
												<EventType
													shipment_event={
														ev.shipment_event
													}
													updatedAt={ev.updatedAt}
                                                    createdBy={ev.created_by}
													id={ev.shipment_event.id}
                                                    created_by={ev.created_by}
													user_id={ev.user_id}
													road_id={ev.road_id}
													_addComment={_addComment}
                                                    _seenDocument={_seenDocument}
													acceptedDocument={ev.shipment_event.document_accepted_users.filter(function(e) {return (e.user_id === parseInt(user.id))}).length == 0 ? false:true}
                                                    seenDocument={ev.shipment_event.document_seen_users.filter(function(e) {return (e.user_id === parseInt(user.id))}).length == 0 ? false:true}
                                                    canSeeDocument={ev.shipment_event.event_document_users.filter(function(e) {return (e.user_id === parseInt(user.id))}).length == 0 ? false:true}
													_acceptDocument={
														_acceptDocument
													}
													user={ev.user}
													_onDeleteEntry={
														_onDeleteEntry
													}
													key={ev.id}
													auth_user={user}
													setCommentOpen={(id) =>
														setCommentOpen(id)
													}
													commentOpen={commentOpen}
													setAcceptOpen={(id) =>
														setAcceptOpen(id)
													}
													acceptOpen={acceptOpen}
												/>
											))}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				{eventOpen && (
					<EventModal
						isOpen={eventOpen}
						toggle={_toggleEvent}
						eventParticipantFilters={eventParticipantFilters}
						transportEvents={transportEvents}
						_submitEvent={_submitEvent}
						auth_user={user}
					/>
				)}
				{documentOpen && (
					<DocumentModal
						isOpen={documentOpen}
						toggle={_toggleDocument}
						eventParticipantFilters={eventParticipantFilters}
						documentEvents={documentEvents}
						_submitEvent={_submitEvent}
						auth_user={user}
					/>
				)}
			</div>
		</AuthPage>
	);
};

EventPage.getInitialProps = (ctx) => {
	const eventPage = true;
	return { eventPage };
};

EventPage.propTypes = {
	user: PropTypes.shape({
		id: PropTypes.string,
	}),
};

EventPage.defaultProps = {
	user: null,
	event_category: {},
};

export default withAuth(EventPage, { loginRequired: true });

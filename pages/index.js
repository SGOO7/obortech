import PropTypes from "prop-types";
import Link from "next/link";
import Head from "next/head";
import Header from "../components/HomeHeader";
import Footer from "../components/HomeFooter";
import Sidebar from "../components/Sidebar";
import NProgress from "nprogress";

import withAuth from "../lib/withAuth";

class IndexPage extends React.Component {
  static getInitialProps() {
    const indexPage = true;
    return { indexPage };
  }

  static propTypes = {
    user: PropTypes.shape({
      _id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
  };

  async componentDidMount() {
    NProgress.done();
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      shipment: {},
    };
  }

  render() {
    const { user, shipment } = this.state;

    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - Admin Dashboard</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              {/* Begin Page Content */}
              <div className="container-fluid">
                {/* Content Row */}
                <div className="row d-flex step-wrapper">
                  <div className="steps col-xl-9">
                    <div className="bs-wizard">
                      <div className="col-xs-3 bs-wizard-step complete">
                        <div className="in-out-state">
                          <span className="active"></span>
                          <span className=""></span>
                          <span className=""></span>
                        </div>
                        <div className="progress">
                          <div className="progress-bar"></div>
                        </div>
                        <a
                          className="bs-wizard-dot"
                          data-toggle="tooltip"
                          data-placement="right"
                          id="hints"
                          data-html="true"
                          title="<strong>Border In</strong>   <span>2020-10-11 6:13:28</span> <br/><strong>Border Out</strong>  <span>2020-10-11 15:13:28</span> <br/><strong>Traveled</strong>  <span>168.8 km</span>"
                        >
                          <span>
                            40d <br />
                            23h
                          </span>
                        </a>
                        <div className="bs-wizard-info text-center">
                          Ulaanbaatar MN
                        </div>
                      </div>
                      <div className="col-xs-3 bs-wizard-step complete">
                        {/* complete */}
                        <div className="progress">
                          <div className="progress-bar"></div>
                        </div>
                        <a
                          className="bs-wizard-dot"
                          data-toggle="tooltip"
                          data-placement="right"
                          id="hints"
                          data-html="true"
                          title="<strong>Border In</strong>   <span>2020-10-11 6:13:28</span> <br/><strong>Border Out</strong>  <span>2020-10-11 15:13:28</span> <br/><strong>Traveled</strong>  <span>168.8 km</span>"
                        >
                          <span>
                            1d <br />
                            3h
                          </span>
                        </a>
                        <div className="bs-wizard-info text-center">
                          Ulaanbaatar MN
                        </div>
                      </div>
                      <div className="col-xs-3 bs-wizard-step complete">
                        {/* complete */}
                        <div className="progress">
                          <div className="progress-bar"></div>
                        </div>
                        <a className="bs-wizard-dot">
                          <span>
                            10d <br />
                            1h
                          </span>
                        </a>
                        <div className="bs-wizard-info text-center">
                          Ulaanbaatar MN
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="steps-total col-xl-3">
                    <p>
                      <strong>Total Duration:</strong> 2 days, 0 hours, 8
                      minutes
                    </p>
                    <p>
                      <strong>Traveled km:</strong> 639.61 km
                    </p>
                    <p>
                      <strong>Started:</strong> 2020-10-09 19:14:17 GMT +8
                    </p>
                  </div>
                </div>
                <div className="row d-flex map-content">
                  <div className="col-lg-5 pl-0">
                    <form action="" method="">
                      <div className="form-group row">
                        <label
                          htmlFor="email_address"
                          className="col-md-4 col-form-label"
                        >
                          Cargo:
                        </label>
                        <div className="col-md-8 position-relative">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              PRE0001-UBE 4440{" "}
                              <span className="text-red">(2)</span>
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          <div className="drop-content">
                            <ul
                              className="nav nav-tabs"
                              id="myTab"
                              role="tablist"
                            >
                              <li className="nav-item">
                                <a
                                  className="nav-link active"
                                  id="all-containers"
                                  data-toggle="tab"
                                  href="#all"
                                  role="tab"
                                  aria-controls="all"
                                  aria-selected="true"
                                >
                                  All containers
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  className="nav-link"
                                  id="truck-containers"
                                  data-toggle="tab"
                                  href="#truck"
                                  role="tab"
                                  aria-controls="truck"
                                  aria-selected="false"
                                >
                                  Truck containers
                                </a>
                              </li>
                            </ul>
                            <div className="tab-content" id="myTabContent">
                              <div
                                className="tab-pane fade show active"
                                id="all"
                                role="tabpanel"
                                aria-labelledby="all-containers"
                              >
                                <Input
                                  type="text"
                                  placeholder="Type container ID"
                                  id="myInput"
                                />
                                <a href="#about">
                                  CSNU-635059-9 <span>(2)</span>
                                </a>
                                <a href="#base">
                                  CSNU-735059-9 <span>(3)</span>
                                </a>
                                <a href="#blog">CSNU-735059-9</a>
                                <a href="#contact">CSNU-735059-9</a>
                                <a href="#custom">
                                  CSNU-735059-9 <span>(1)</span>
                                </a>
                                <a href="#support">CSNU-735059-9</a>
                                <a href="#tools">CSNU-735059-9</a>
                              </div>
                              <div
                                className="tab-pane fade"
                                id="truck"
                                role="tabpanel"
                                aria-labelledby="truck-containers"
                              >
                                <Input
                                  type="text"
                                  placeholder="Type container ID"
                                  id="myInput"
                                />
                                <a href="#about">
                                  CSNU-635059-9 <span>(2)</span>
                                </a>
                                <a href="#base">CSNU-735059-9</a>
                                <a href="#blog">CSNU-735059-9</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group row">
                        <label
                          htmlFor="email_address"
                          className="col-md-4 col-form-label "
                        >
                          Container:
                        </label>
                        <div className="col-md-8 position-relative">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              PRE0001-UBE 4440{" "}
                              <span className="text-red">(2)</span>
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content">
                            <ul
                              className="nav nav-tabs"
                              id="myTab"
                              role="tablist"
                            >
                              <li className="nav-item">
                                <a
                                  className="nav-link active"
                                  id="all-containers1"
                                  data-toggle="tab"
                                  href="#all1"
                                  role="tab"
                                  aria-controls="all"
                                  aria-selected="true"
                                >
                                  All cargos
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  className="nav-link"
                                  id="truck-containers1"
                                  data-toggle="tab"
                                  href="#truck1"
                                  role="tab"
                                  aria-controls="truck"
                                  aria-selected="false"
                                >
                                  Container cargos
                                </a>
                              </li>
                            </ul>
                            <div className="tab-content" id="myTabContent">
                              <div
                                className="tab-pane fade show active"
                                id="all1"
                                role="tabpanel"
                                aria-labelledby="all-containers"
                              >
                                <Input
                                  type="text"
                                  placeholder="Type cargo ID"
                                  id="myInput"
                                />
                                <a href="#about">
                                  CSNU-635059-9 <span>(2)</span>
                                </a>
                                <a href="#base">
                                  CSNU-735059-9 <span>(3)</span>
                                </a>
                                <a href="#blog">CSNU-735059-9</a>
                                <a href="#contact">CSNU-735059-9</a>
                                <a href="#custom">
                                  CSNU-735059-9 <span>(1)</span>
                                </a>
                                <a href="#support">CSNU-735059-9</a>
                                <a href="#tools">CSNU-735059-9</a>
                              </div>
                              <div
                                className="tab-pane fade"
                                id="truck1"
                                role="tabpanel"
                                aria-labelledby="truck-containers"
                              >
                                <Input
                                  type="text"
                                  placeholder="Type container ID"
                                  id="myInput"
                                />
                                <a href="#about">
                                  CSNU-635059-9 <span>(2)</span>
                                </a>
                                <a href="#base">CSNU-735059-9</a>
                                <a href="#blog">CSNU-735059-9</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group row">
                        <label
                          htmlFor="email_address"
                          className="col-md-4 col-form-label"
                        >
                          Truck:
                        </label>
                        <div className="col-md-8 position-relative">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              PRE0001-UBE 4440{" "}
                              <span className="text-red">(2)</span>
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content">
                            <ul
                              className="nav nav-tabs"
                              id="myTab"
                              role="tablist"
                            >
                              <li className="nav-item">
                                <a
                                  className="nav-link active"
                                  id="all-containers2"
                                  data-toggle="tab"
                                  href="#all2"
                                  role="tab"
                                  aria-controls="all"
                                  aria-selected="true"
                                >
                                  All trucks
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  className="nav-link"
                                  id="truck-containers2"
                                  data-toggle="tab"
                                  href="#truck2"
                                  role="tab"
                                  aria-controls="truck"
                                  aria-selected="false"
                                >
                                  Group trucks
                                </a>
                              </li>
                            </ul>
                            <div className="tab-content" id="myTabContent">
                              <div
                                className="tab-pane fade show active"
                                id="all2"
                                role="tabpanel"
                                aria-labelledby="all-containers"
                              >
                                <Input
                                  type="text"
                                  placeholder="Type truck ID"
                                  id="myInput"
                                />
                                <a href="#about">
                                  CSNU-635059-9 <span>(2)</span>
                                </a>
                                <a href="#base">
                                  CSNU-735059-9 <span>(3)</span>
                                </a>
                                <a href="#blog">CSNU-735059-9</a>
                                <a href="#contact">CSNU-735059-9</a>
                                <a href="#custom">
                                  CSNU-735059-9 <span>(1)</span>
                                </a>
                                <a href="#support">CSNU-735059-9</a>
                                <a href="#tools">CSNU-735059-9</a>
                              </div>
                              <div
                                className="tab-pane fade"
                                id="truck2"
                                role="tabpanel"
                                aria-labelledby="truck-containers"
                              >
                                <Input
                                  type="text"
                                  placeholder="Type container ID"
                                  id="myInput"
                                />
                                <a href="#about">
                                  CSNU-635059-9 <span>(2)</span>
                                </a>
                                <a href="#base">CSNU-735059-9</a>
                                <a href="#blog">CSNU-735059-9</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group row">
                        <label
                          htmlFor="email_address"
                          className="col-md-4 col-form-label"
                        >
                          Group:
                        </label>
                        <div className="col-md-8 position-relative">
                          <div className="selected-value form-control">
                            <span className="seleted-text">
                              PRE0001-UBE 4440{" "}
                              <span className="text-red">(2)</span>
                            </span>
                            <i className="fas fa-angle-down fa-sm"></i>
                          </div>
                          {/* dropdown */}
                          <div className="drop-content">
                            <div className="tab-content" id="myTabContent">
                              <Input
                                type="text"
                                placeholder="Type container ID"
                                id="myInput"
                              />
                              <a href="#about">
                                1 <span>(2)</span>
                              </a>
                              <a href="#base">
                                2 <span>(3)</span>
                              </a>
                              <a href="#blog">3</a>
                              <a href="#contact">4</a>
                              <a href="#custom">
                                5 <span>(1)</span>
                              </a>
                              <a href="#support">6</a>
                              <a href="#tools">7</a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group row">
                        <label
                          htmlFor="email_address"
                          className="col-md-4 col-form-label"
                        >
                          Show alerts:
                        </label>
                        <div className="col-md-8 tem-type">
                          <span>
                            <Checkbox type="checkbox" name="" /> Sealing
                          </span>
                          <span>
                            <Checkbox type="checkbox" name="" /> Temperature
                          </span>
                          <span>
                            <Checkbox type="checkbox" name="" /> Humidity
                          </span>
                        </div>
                      </div>
                    </form>
                    {/* //form */}
                    {/* mini stats */}
                    <div className="stats-wrapper row m-0">
                      <div className="col-sm-6 br-0 bb-0">
                        <p className="title">HUMIDITY</p>
                        <p className="output">
                          <span className="green-text">30%</span>{" "}
                          <span className="text-red">(2)</span>
                        </p>
                      </div>
                      <div className="col-sm-6 bb-0">
                        <p className="title">TEMPERATURE</p>
                        <p className="output">
                          <span className="green-text">-3.0°C</span>{" "}
                          <span className="text-red">(3)</span>
                        </p>
                      </div>
                      <div className="col-sm-6 br-0">
                        <p className="title">SEALING</p>
                        <p className="output">
                          <span className="text-red">1 opened</span>
                        </p>
                      </div>
                      <div className="col-sm-6">
                        <p className="title">SMART LOCK</p>
                        <p className="output">
                          <span className="green-text">N/A</span>
                        </p>
                      </div>
                    </div>
                    {/* //mini stats */}
                  </div>
                  <div className="col-lg-7 pr-0">
                    <div className="map-wrapper">Map</div>
                  </div>
                </div>
              </div>
              {/* container-fluid */}
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <Footer />
      </div>
    );
  }
}

export default withAuth(IndexPage, { loginRequired: true });

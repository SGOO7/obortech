import Head from "next/head";
import { useRouter } from 'next/router';
import NProgress from "nprogress";
import PropTypes from "prop-types";
import Button from "../components/common/form-elements/button/Button";
import Checkbox from "../components/common/form-elements/checkbox";
import Input from "../components/common/form-elements/input/Input";
import Signup from "../components/onboarding";
import { decipher } from "../utils/decrypt"
import { initialize } from "passport";
import { login } from "../lib/api/auth";
import notify from "../lib/notifier";
import withAuth from "../lib/withAuth";

const LoginWithData = (props) => {
  const router = useRouter()
  return <LoginWithDataChild {...props} router={router} />
}

class LoginWithDataChild extends React.Component {
  static getInitialProps() {
    const loginPage = true;
    return { loginPage };
  }

  static defaultProps = {
    userdata: null,
  };

    static propTypes = {
        userdata: PropTypes.shape({
            _id: PropTypes.string.isRequired,
        }),
    };

    initializeRoute(router) {
        console.log(router)
        console.log(router.query)
        if(Object.keys(router.query).length) {
    
          const emailDeciper = decipher("email-verification")
          const user = JSON.parse(emailDeciper(router.query.email))
          this.setState({
            isSignupOpen: true,
            // user: {...router.query}
            user
          })
        }
      }

    async componentDidMount() {
        this.initializeRoute(this.props.router)
        NProgress.done();
    }

    constructor(props) {
        super(props);
        this.state = {
            // location: props.router,
            userdata: props.userdata || {},
            isSignupOpen: false,
            user: null
        };
    }

    onSubmit = (event) => {
        event.preventDefault();
        const { userdata } = this.state;
        const { username, password } = userdata;

        if (!username) {
            notify("Email is required");
            return;
        }

        if (!password) {
            notify("Password is required");
            return;
        }

        this.login(userdata);
    };

    login = async (data) => {
        NProgress.start();
        try {
            const userdata = await login(data);
            if (data.remember_me != undefined && data.remember_me == true) {
                window.localStorage.setItem("user", JSON.stringify(userdata));
            } else {
            }
            notify("Login Successful!");
            try {
                NProgress.done();
                window.location.href = "/shipment";
            } catch (err) {
                notify("Email/Password mismatch!");
                NProgress.done();
            }
        } catch (err) {
            notify("Email/Password mismatch!");
            NProgress.done();
        }
    };

    render() {
        const { userdata } = this.state;
        return (
            <div className="bg-gradient-primary">
                <Head>
                    <title>{process.env.APP_NAME} - Admin Login</title>
                </Head>
                <div className="container">
                    {/* Outer Row */}
                    <div className="row justify-content-center">
                        <div className="col-xl-10 col-lg-12 col-md-9">
                            <div className="card o-hidden border-0 shadow-lg my-5">
                                <div className="card-body p-0">
                                    {/* Nested Row within Card Body */}
                                    <div className="row">
                                        <div className="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                        <div className="col-lg-6">
                                            <div className="p-5">
                                                <div className="text-center">
                                                    <h1 className="h4 text-gray-900 mb-4">
                                                        Welcome Back!
                                                    </h1>
                                                </div>
                                                <form className="user" onSubmit={this.onSubmit}>
                                                    <div className="form-group">
                                                        <Input
                                                            type="text"
                                                            onChange={(event) => {
                                                                this.setState({
                                                                    userdata: Object.assign({}, userdata, {
                                                                        username: event.target.value,
                                                                    }),
                                                                });
                                                            }}
                                                            className="form-control form-control-user"
                                                            id="login-username"
                                                            name="username"
                                                            placeholder="Email"
                                                        />
                                                    </div>
                                                    <div className="form-group">
                                                        <Input
                                                            type="password"
                                                            onChange={(event) => {
                                                                this.setState({
                                                                    userdata: Object.assign({}, userdata, {
                                                                        password: event.target.value,
                                                                    }),
                                                                });
                                                            }}
                                                            name="password"
                                                            className="form-control form-control-user"
                                                            id="login-password"
                                                            placeholder="Password"
                                                        />
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="custom-control custom-checkbox small">
                                                            <Checkbox
                                                                onChange={(event) => {
                                                                    this.setState({
                                                                        userdata: Object.assign({}, userdata, {
                                                                            remember_me: !userdata.remember_me,
                                                                        }),
                                                                    });
                                                                }}
                                                                className="custom-control-input"
                                                                name="login-remember"
                                                                id="login-remember"
                                                            />
                                                            <label className="custom-control-label"htmlFor="login-remember">
                                                                Remember Me
                                                            </label>
                                                            <Button
                                                                type="button"
                                                                className="btn btn-primary float-right"
                                                                onClick={() => {
                                                                    this.setState({
                                                                        isSignupOpen: true,
                                                                    });
                                                                }}
                                                            >
                                                                Signup
                                                            </Button>
                                                        </div>
                                                    </div>
                                                    <Button type="submit" className="btn btn-primary btn-block">
                                                        Login
                                                    </Button>
                                                </form>
                                            </div>
                                            {/* component for signup modal */}
                                            {this.state.isSignupOpen && (
                                                <Signup
                                                    isSignupOpen={this.state.isSignupOpen}
                                                    closeSignupModal={() =>
                                                        this.setState({
                                                            isSignupOpen: false,
                                                        })
                                                    }
                                                    router={this.props.router}
                                                    user={this.state.user}
                                                />
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withAuth(LoginWithData, { logoutRequired: true });

import PropTypes from "prop-types";
import withAuth from '../lib/withAuth';
import { useRouter } from "next/router";
import {useState, useEffect, useCallback} from 'react';
import { _momentDateFormat } from '../utils/globalFunc';
import LineChart from '../components/analytics/chart';
import AuthPage from '../components/wrapper/AuthPage';
import string from '../utils/stringConstants/language/eng.json';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { DateRange } from 'react-date-range';
import { fetchShipment } from "../lib/api/shipment";
import Input from "../components/common/form-elements/input/Input";

const AnalyticsPage = (props) => {
	const { user } = props;
	const router = useRouter();
	const { shipment_id, container_id } = router.query;
	const [dateRange, setDateRange] = useState([
		{
			startDate: new Date(),
			endDate: null,
			key: 'selection'
		}
	]);
	const [dateSelected, setDateSelected] = useState(false);
	const [open, setOpen] = useState(false);
	const [timeFrameValue, setTimeFrameValue] = useState(null);
	const [format, setFormat] = useState('YYYY-MM-DD');
	const [shipmentDetails, setShipmentDetails] = useState({});

	useEffect(async () => {
		const shipment_details = await fetchShipment({shipment_id});
		setShipmentDetails(shipment_details);
	}, []);	

	useEffect(() => {

        //Show current month start and end date
        let date = new Date(), y = date.getFullYear(), m = date.getMonth();
        let firstDay = new Date(y, m, 1);
        let lastDay = new Date(y, m + 1, 0);
        let currentMonthRange = _momentDateFormat(firstDay, format) + ' - ' + _momentDateFormat(lastDay, format);
		setTimeFrameValue(currentMonthRange);

		if(dateRange[0].startDate && dateRange[0].endDate){
			let date = _momentDateFormat(dateRange[0].startDate, format) + ' - ' + _momentDateFormat(dateRange[0].endDate, format);
			setTimeFrameValue(date);
		}
    }, [dateRange]);

  	return (
		<AuthPage user={user} title={string.analyticsPageTitle}>
		  	<div className="container-fluid">
				<div className="row d-flex shipment-listing">
					<div className="tab-pane fade show active mt-3 col-md-12" id="all2" role="tabpanel" aria-labelledby="all-containers">
						<div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter">
							<h4 className="text-dark">{string.analyticsPageTitle}</h4>
                            <div className="analytics-time-input">
                                <span>Time Frame:</span>
                                <Input
                                    type="text"
                                    value={timeFrameValue || ''}
                                    onClick={(event) => {
                                        setOpen(!open);
                                    }}
                                    readOnly={true}
                                />
                                {open ? <div style={{position:'absolute',top:'100%',right:'0%'}}>
                                    <DateRange
                                        editableDateInputs={true}
                                        onChange={(item) => {
											setDateRange([item.selection])
											if(dateSelected){
												setOpen(!open);
											}
											setDateSelected(!dateSelected);
                                        }}
                                        moveRangeOnFirstSelection={false}
                                        ranges={dateRange}
                                    />
                                </div> : ''}
                            </div>
						</div>
						<LineChart shipmentDetails={shipmentDetails} dateRange={dateRange} rgbColor="126,253,255" type="temp" container_id={container_id} shipment_id={shipment_id} />
						<LineChart shipmentDetails={shipmentDetails} dateRange={dateRange} rgbColor="117,85,218" type="hum" container_id={container_id} shipment_id={shipment_id} />
					</div>
				</div>
			</div>
		</AuthPage>
	);
}

AnalyticsPage.getInitialProps = (ctx) => {
	const analyticsPage = true;
	return { analyticsPage };
  };

AnalyticsPage.propTypes = {
	user: PropTypes.shape({
		id: PropTypes.string,
	}),
};

AnalyticsPage.defaultProps = {
	user: null
};

export default withAuth(AnalyticsPage, { loginRequired: true });
import { Formik } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";

const EditDeviceschema = Yup.object().shape({
  deviceID: Yup.string()
    .trim()
    .required(`DeviceID ${string.errors.required}`),
});

function EditModal({ device, state, updateDevice }) {
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              EDIT DEVICE
            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </Button>
          </div>
          <div className="modal-body">
            <Formik
              enableReinitialize={true}
              initialValues={{
                deviceID: device.deviceID || "",
              }}
              validationSchema={EditDeviceschema}
              onSubmit={(values) => {
                state({
                  device: Object.assign({}, device, {
                    deviceID: values.deviceID,
                  }),
                });
                updateDevice();
                $("#editDeviceModal").modal("hide");
                values.deviceID = "";
              }}
            >
              {({ errors, touched, handleChange, handleSubmit, values }) => (
                <form className="form-container" onSubmit={handleSubmit}>
                  <div className="row ml-0 mr-0 content-block">
                    <div className="form-group col-md-12 p-0">
                      <label
                        htmlFor="deviceID"
                        className="col-md-12 col-form-label pl-0"
                      >
                        DeviceID
                      </label>
                      <Input
                        type="text"
                        name="deviceID"
                        id="deviceID"
                        value={device.deviceID || ""}
                        className="form-control"
                        placeholder="DeviceID"
                        onChange={handleChange}
                        // onChange={(event) => {
                        //     state({
                        //         device: Object.assign({}, device, { deviceID: event.target.value })
                        //     });
                        // }}
                      />
                      {errors.deviceID && touched.deviceID ? (
                        <FormHelperMessage
                          message={errors.deviceID}
                          className="error"
                        />
                      ) : null}
                    </div>
                  </div>
                  <div className="modal-footer">
                    <Button
                      className="btn btn-primary large-btn"
                      // onClick={updateDevice}
                      type="submit"
                      // data-dismiss="modal"
                    >
                      UPDATE
                    </Button>
                  </div>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

EditModal.propTypes = {};

EditModal.defaultProps = {};

export default EditModal;

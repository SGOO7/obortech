import PropTypes from "prop-types";
import Head from "next/head";
import Header from "../../components/HomeHeader";
import Footer from "../../components/HomeFooter";
import Sidebar from "../../components/Sidebar";
import DeleteModal from "./DeleteModal";
import AddModal from "./AddModal";
import EditModal from "./EditModal";
import NProgress from "nprogress";
import notify from "../../lib/notifier";

import withAuth from "../../lib/withAuth";
import {
  fetchDevices,
  addDevice,
  removeDevice,
  updateDevice,
} from "../../lib/api/device";
// updated
import Button from "../../components/common/form-elements/button/Button";
class DevicePage extends React.Component {
  static getInitialProps() {
    const devicePage = true;
    return { devicePage };
  }

  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
  };

  async componentDidMount() {
    NProgress.start();
    try {
      const devices = await fetchDevices();
      this.setState({ devices });
      NProgress.done();
    } catch (err) {
      this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
      NProgress.done();
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      devices: [],
      device: {},
      deleteMode: "",
      selectedIndex: "",
      editMode: "",
    };
  }

  // submit device function to check submitted details
  onDeviceSubmit = (event) => {
    // event.preventDefault();
    const { device } = this.state;
    const { deviceID } = device;

    // if (!deviceID) {
    // 	notify('DeviceID is required');
    // 	return;
    // }

    this.addDeviceData({ deviceID: deviceID });
  };

  // add device function
  addDeviceData = async (data) => {
    NProgress.start();
    try {
      const device = await addDevice(data);
      if (device.error != undefined) {
        notify(device.error);
        NProgress.done();
        return false;
      }
      let { devices } = this.state;
      devices.push(device);
      this.setState({ devices, device: {} });
      notify("Device added successfully!");
      NProgress.done();
    } catch (err) {
      console.error(err);
      notify("Error adding device!");
      NProgress.done();
    }
  };

  // Function to delete entry from popup
  onDeleteEntry = async (event) => {
    event.preventDefault();
    let { deleteMode, devices, selectedIndex } = this.state;
    if (deleteMode == "device") {
      // delete device data
      let devices_data = devices[selectedIndex];
      await removeDevice({ id: devices_data.id });
      devices.splice(selectedIndex, 1);
      this.setState({ devices });
      notify("Device deleted successfully!");
    }
  };

  // update device function
  updateDevice = async () => {
    NProgress.start();
    let { device, selectedIndex } = this.state;
    try {
      const updated_device = await updateDevice(device);
      if (updated_device.error != undefined) {
        notify(updated_device.error);
        NProgress.done();
        return false;
      }
      let { devices } = this.state;
      devices[selectedIndex] = device;
      this.setState({ devices, device: {} });
      notify("Device updated successfully!");
      NProgress.done();
    } catch (err) {
      console.error(err);
      notify("Error adding Device!");
      NProgress.done();
    }
  };

  // set delete mode upon selecting delete icon
  setDeleteMode = (mode, i) => {
    if (mode) {
      this.setState({ deleteMode: mode });
      this.setState({ selectedIndex: i });
      $("#deleteModal").modal("show");
    }
  };

  setEditMode = (mode, i) => {
    if (mode) {
      this.setState({ editMode: mode });
      this.setState({ selectedIndex: i });
      if (mode == "device") {
        let { devices } = this.state;
        let device = devices[i];
        this.setState({ device });
        $("#editDeviceModal").modal("show");
      }
    }
  };

  render() {
    const { user, devices, device } = this.state;
    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - Manage Devices</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <div
                    className="tab-pane fade show active mt-3 w-100"
                    id="device"
                    role="tabpanel"
                    aria-labelledby="device-listing"
                  >
                    <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
                      <h4 className="text-dark">Devices Listing</h4>
                      <Button
                        className="btn btn-primary large-btn"
                        data-toggle="modal"
                        data-target="#deviceModal"
                      >
                        SUBMIT DEVICE
                      </Button>
                    </div>
                    <div className="shipment-table-listing table-responsive mt-2 w-100">
                      <table className="table">
                        <thead className="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">DeviceID</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {devices.map((device, i) => {
                            return (
                              <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{device.deviceID}</td>
                                <td>
                                  <i
                                    className="fa fa-pencil-alt"
                                    onClick={() =>
                                      this.setEditMode("device", i)
                                    }
                                  ></i>
                                  <i
                                    className="fa fa-trash"
                                    onClick={() =>
                                      this.setDeleteMode("device", i)
                                    }
                                  ></i>
                                </td>
                              </tr>
                            );
                          })}
                          {devices.length == 0 && (
                            <tr>
                              <td colSpan="5" className="text-center">
                                No data available!!
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <div
          className="modal fade customModal document"
          id="deleteModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <DeleteModal onDeleteEntry={this.onDeleteEntry} />
        </div>
        <div
          className="modal fade customModal document"
          id="deviceModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <AddModal
            state={this.setState.bind(this)}
            onDeviceSubmit={this.onDeviceSubmit.bind(this)}
          />
        </div>
        <div
          className="modal fade customModal document"
          id="editDeviceModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <EditModal
            device={device}
            state={this.setState.bind(this)}
            updateDevice={this.updateDevice.bind(this)}
          />
        </div>
        <Footer />
      </div>
    );
  }
}

export default withAuth(DevicePage, { loginRequired: true });

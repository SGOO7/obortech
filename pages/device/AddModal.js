import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";

const AddDeviceschema = Yup.object().shape({
  deviceID: Yup.string()
    .trim()
    .required(`DeviceID ${string.errors.required}`),
});

function AddModal({ state, onDeviceSubmit }) {
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              ADD DEVICE
            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </Button>
          </div>
          <div className="modal-body">
            <Formik
              initialValues={{
                deviceID: "",
              }}
              validationSchema={AddDeviceschema}
              onSubmit={(values) => {
                state({
                  device: Object.assign({}, device, {
                    deviceID: values.deviceID,
                  }),
                });
                onDeviceSubmit();
                $("#deviceModal").modal("hide");
                values.deviceID = "";
              }}
            >
              {({ errors, touched, handleChange, handleSubmit, values }) => (
                <form className="form-container" onSubmit={handleSubmit}>
                  <div className="row ml-0 mr-0 content-block">
                    <div className="form-group col-md-12 p-0">
                      <label
                        htmlFor="deviceID"
                        className="col-md-12 col-form-label pl-0"
                      >
                        DeviceID
                      </label>
                      <Input
                        type="text"
                        name="deviceID"
                        id="deviceID"
                        className="form-control"
                        placeholder="DeviceID"
                        value={values.deviceID}
                        onChange={handleChange}
                        // onChange={(event) => {
                        //     state({
                        //         device: Object.assign({}, device, { deviceID: event.target.value })
                        //     });
                        // }}
                      />
                      {errors.deviceID && touched.deviceID ? (
                        <FormHelperMessage
                          message={errors.deviceID}
                          className="error"
                        />
                      ) : null}
                    </div>
                  </div>
                  <div className="modal-footer">
                    <Button
                      className="btn btn-primary large-btn"
                      // onClick={onDeviceSubmit}
                      type="submit"
                      // data-dismiss="modal"
                    >
                      INSERT
                    </Button>
                  </div>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

AddModal.propTypes = {};

AddModal.defaultProps = {};

export default AddModal;

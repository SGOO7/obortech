import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
// import Select from "react-select";
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";
import AdvanceSelect from "../../components/common/form-elements/select/AdvanceSelect";

const AddContainerschema = Yup.object().shape({
  containerID: Yup.string()
    .trim()
    .required(`${string.container.containerID} ${string.errors.required}`),
  cargo_id: Yup.array().min(1, `${string.container.selectCargo}`),
});

function AddModal({
  state,
  onContainerSubmit,
}) {
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              {string.container.addContainer}
            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">{string.cancelSign}</span>
            </Button>
          </div>
          <div className="modal-body">
            <Formik
              initialValues={{
                containerID: "",
              }}
              validationSchema={AddContainerschema}
              onSubmit={(values) => {
                state({
                  container: Object.assign({}, container, {
                    containerID: values.containerID,
                  }),
                });
                onContainerSubmit();
                $("#containerModal").modal("hide");
                values.containerID = "";
              }}
            >
              {({ errors, touched, handleChange, handleSubmit, values }) => (
                <form className="form-container" onSubmit={handleSubmit}>
                  <div className="row ml-0 mr-0 content-block">
                    <div className="form-group col-md-12 p-0">
                      <label
                        htmlFor="containerID"
                        className="col-md-12 col-form-label pl-0"
                      >
                        {string.container.containerID}
                      </label>
                      <Input
                        type="text"
                        name="containerID"
                        id="containerID"
                        className="form-control"
                        placeholder={string.container.containerID}
                        value={values.containerID}
                        onChange={handleChange}
                      // onChange={(event) => {
                      //     state({
                      //         container: Object.assign({}, container, { containerID: event.target.value })
                      //     });
                      // }}
                      />
                      {errors.containerID && touched.containerID ? (
                        <FormHelperMessage
                          message={errors.containerID}
                          className="error"
                        />
                      ) : null}
                    </div>
                    <div className="col-md-12 p-0">
                      <label className="col-md-12 col-form-label pl-0">
                        {string.container.selectCargo}
                      </label>
                      <AdvanceSelect
                        // value={defaultCargoOptions}
                        isMulti
                        options={cargoOptions}
                        name="cargo_id"
                        value={values.cargo_id}
                        // onChange={handleChange}
                        onChange={(event) => {
                          handleChange("cargo_id");
                          defaultCargoOptions = event;
                          values.cargo_id = defaultCargoOptions;
                          state({ defaultCargoOptions });
                        }}
                      />
                      {errors.cargo_id && touched.cargo_id ? (
                        <FormHelperMessage
                          message={errors.cargo_id}
                          className="error"
                        />
                      ) : null}
                    </div>
                  </div>
                  <div className="modal-footer">
                    <Button
                      className="btn btn-primary large-btn"
                      // onClick={onContainerSubmit}
                      type="submit"
                    // data-dismiss="modal"
                    >
                      {string.insertBtnTxt}
                    </Button>
                  </div>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

AddModal.propTypes = {};

AddModal.defaultProps = {};

export default AddModal;

import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
// import Select from "react-select";
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";
import AdvanceSelect from "../../components/common/form-elements/select/AdvanceSelect";

const EditContainerschema = Yup.object().shape({
  containerID: Yup.string()
    .trim()
    .required(`${string.container.containerID} ${string.errors.required}`),
  cargo_id: Yup.array().min(1, `${string.container.selectCargo}`),
});

function EditModal({
  container,
  state,
  updateContainer,
}) {
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              {string.container.editContainer}

            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">{string.cancelSign}</span>
            </Button>
          </div>
          <div className="modal-body">
            <Formik
              enableReinitialize={true}
              initialValues={{
                containerID: container.containerID || "",
              }}
              validationSchema={EditContainerschema}
              onSubmit={(values) => {
                state({
                  container: Object.assign({}, container, {
                    containerID: values.containerID,
                  }),
                });
                updateContainer();
                $("#editContainerModal").modal("hide");
              }}
            >
              {({ errors, touched, handleChange, handleSubmit, values }) => {
                return (
                  <form className="form-container" onSubmit={handleSubmit}>
                    <div className="row ml-0 mr-0 content-block">
                      <div className="form-group col-md-12 p-0">
                        <label
                          htmlFor="containerID"
                          className="col-md-12 col-form-label pl-0"
                        >
                          {string.container.containerID}
                        </label>
                        <Input
                          type="text"
                          name="containerID"
                          id="containerID"
                          // value={container.containerID || ""}
                          value={values.containerID}
                          onChange={handleChange}
                          className="form-control"
                          placeholder="ContainerID"
                        // onChange={(event) => {
                        //     state({
                        //         container: Object.assign({}, container, { containerID: event.target.value })
                        //     });
                        // }}
                        />
                        {errors.containerID && touched.containerID ? (
                          <FormHelperMessage
                            message={errors.containerID}
                            className="error"
                          />
                        ) : null}
                      </div>
                      <div className="col-md-12 p-0">
                        <label className="col-md-12 col-form-label pl-0">
                          {string.container.selectCargo}
                        </label>
                        <AdvanceSelect
                          // value={defaultCargoOptions}
                          isMulti
                          options={cargoOptions}
                          name="cargo_id"
                          value={values.cargo_id}
                          // onChange={handleChange}
                          onChange={(event) => {
                            handleChange("cargo_id");
                            defaultCargoOptions = event;
                            values.cargo_id = defaultCargoOptions;
                            state({ defaultCargoOptions });
                          }}
                        />
                        {errors.cargo_id && touched.cargo_id ? (
                          <FormHelperMessage
                            message={errors.cargo_id}
                            className="error"
                          />
                        ) : null}
                      </div>
                    </div>
                    <div className="modal-footer">
                      <Button
                        className="btn btn-primary large-btn"
                        // onClick={updateContainer}
                        type="submit"
                      // data-dismiss="modal"
                      >
                        {string.updateBtnTxt}
                      </Button>
                    </div>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

EditModal.propTypes = {};

EditModal.defaultProps = {};

export default EditModal;

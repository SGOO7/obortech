import PropTypes from "prop-types";
import Head from "next/head";
import Header from "../../components/HomeHeader";
import Footer from "../../components/HomeFooter";
import Sidebar from "../../components/Sidebar";
import DeleteModal from "./DeleteModal";
import AddModal from "./AddModal";
import EditModal from "./EditModal";
import NProgress from "nprogress";
import notify from "../../lib/notifier";

import withAuth from "../../lib/withAuth";
import {
  fetchContainers,
  addContainer,
  removeContainer,
  updateContainer,
} from "../../lib/api/container";
import { fetchCargos } from "../../lib/api/cargo";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Button from "../../components/common/form-elements/button/Button";

class ContainerPage extends React.Component {
  static getInitialProps() {
    const containerPage = true;
    return { containerPage };
  }

  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
  };

  async componentDidMount() {
    NProgress.start();
    try {
      const containers = await fetchContainers();
      this.setState({ containers });
      NProgress.done();
    } catch (err) {
      this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
      NProgress.done();
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      containers: [],
      container: {},
      deleteMode: "",
      selectedIndex: "",
      editMode: "",
    };
  }

  // submit container function to check submitted details
  onContainerSubmit = (event) => {
    // event.preventDefault();
    const { container } = this.state;
    const { containerID } = container;

    // if (!containerID) {
    // 	notify('ContainerID is required');
    // 	return;
    // }

    // if(defaultCargoOptions.length == 0){
    // 	notify('Please select Cargo');
    // 	return;
    // }

    this.addContainerData(container);
  };

  // add container function
  addContainerData = async (data) => {
    NProgress.start();
    try {
      let container = await addContainer({
        containerID: data.containerID,
      });
      let containers = await fetchContainers();
      this.setState({ containers, container: {} });
      notify(`${string.container.containerAddedSucessfully}`);
      NProgress.done();
    } catch (err) {
      console.log(err);
      notify(`${string.container.errorAddingContainer}`);
      NProgress.done();
    }
  };

  // Function to delete entry from popup
  onDeleteEntry = async (event) => {
    event.preventDefault();
    let { deleteMode, containers, selectedIndex } = this.state;
    if (deleteMode == "container") {
      // delete container data
      let containers_data = containers[selectedIndex];
      await removeContainer({ id: containers_data.id });
      containers.splice(selectedIndex, 1);
      this.setState({ containers });
      notify(`${string.container.containerDelatedSucessfully}`);

    }
  };

  // update container function
  updateContainer = async () => {
    NProgress.start();
    let { container, selectedIndex } = this.state;
    try {
      await updateContainer(container);
      let containers = await fetchContainers();
      this.setState({ containers, container: {} });
      notify(`${string.container.containerUpdatedSucessfully}`);

      NProgress.done();
    } catch (err) {
      console.log(err);
      notify(`${string.container.errorAddingContainer}`);
      NProgress.done();
    }
  };

  // set delete mode upon selecting delete icon
  setDeleteMode = (mode, i) => {
    if (mode) {
      this.setState({ deleteMode: mode });
      this.setState({ selectedIndex: i });
      $("#deleteModal").modal("show");
    }
  };

  setEditMode = (mode, i) => {
    if (mode) {
      this.setState({ editMode: mode });
      this.setState({ selectedIndex: i });
      if (mode == "container") {
        let { containers } = this.state;
        let container = containers[i];
        this.setState({ container });
        $("#editContainerModal").modal("show");
      }
    }
  };

  render() {
    let {
      user,
      containers,
      container
    } = this.state;

    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - {string.container.manageContainer}</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <div
                    className="tab-pane fade show active mt-3 w-100"
                    id="container"
                    role="tabpanel"
                    aria-labelledby="container-listing"
                  >
                    <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
                      <h4 className="text-dark">{string.container.containerListing}</h4>
                      <Button
                        className="btn btn-primary large-btn"
                        data-toggle="modal"
                        data-target="#containerModal"
                      >
                        {string.container.submitContainer}

                      </Button>
                    </div>
                    <div className="shipment-table-listing table-responsive mt-2 w-100">
                      <table className="table">
                        <thead className="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">{string.container.containerID}</th>
                            <th scope="col">{string.container.cargos}</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {containers.map((container, i) => {
                            return (
                              <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{container.containerID}</td>
                                <td>
                                  <i
                                    className="fa fa-pencil-alt"
                                    onClick={() =>
                                      this.setEditMode("container", i)
                                    }
                                  ></i>
                                  <i
                                    className="fa fa-trash"
                                    onClick={() =>
                                      this.setDeleteMode("container", i)
                                    }
                                  ></i>
                                </td>
                              </tr>
                            );
                          })}
                          {containers.length == 0 && (
                            <tr>
                              <td colSpan="5" className="text-center">
                                {string.cargo.noDataAvailable}

                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <div
          className="modal fade customModal document"
          id="deleteModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <DeleteModal onDeleteEntry={this.onDeleteEntry} />
        </div>
        <div
          className="modal fade customModal document"
          id="containerModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <AddModal
            state={this.setState.bind(this)}
            onContainerSubmit={this.onContainerSubmit.bind(this)}
          />
        </div>
        <div
          className="modal fade customModal document"
          id="editContainerModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <EditModal
            container={container}
            state={this.setState.bind(this)}
            updateContainer={this.updateContainer.bind(this)}
          />
        </div>
        <Footer />
      </div>
    );
  }
}

export default withAuth(ContainerPage, { loginRequired: true });

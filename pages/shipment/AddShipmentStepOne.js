import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import string from "../../utils/stringConstants/language/eng.json";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";
import CustomSelect from "../../components/common/form-elements/select/CustomSelect";

const AddShipmentStepOne = ({ mode, setState, state, user, changeStep }) => {
    let cargoOptions = [];
    state.cargos.map((cargo, i) => {
        cargoOptions.push({ label: cargo.cargoID, value: cargo.id });
    });

    let containerOptions = [];
    state.containers.map((container, i) => {
        containerOptions.push({
            label: container.containerID,
            value: container.id,
        });
    });
  // });
  const nextStep = (values) => {
    const {
      name,
      document_category_id,
      participant_category_id,
      participant_key,
      shipment_category_id,
      selectedParticipants,
      template_id,
    } = values;

    setState({
      shipment: {
        ...state.shipment,
        name,
        document_category_id,
        participant_category_id,
        participant_key,
        shipment_category_id,
        selectedParticipants,
        id: template_id,
      },
    });
    changeStep(2);
  };
  console.log("state", state);
  // Formik Validations and initialization
  const formik = useFormik({
    initialValues: {
      name: state?.shipment?.name || "",
      shipment_category_id: state?.shipment?.shipment_category_id || "",
      document_category_id: state?.shipment?.document_category_id || "",
      participant_category_id: state?.shipment?.participant_category_id || "",
      participant_key: state?.shipment?.participant_key || "",
      // template_id: state?.shipment?.template_id || "",
      template_id: state?.shipment?.id || "",
      selectedParticipants: state?.shipment?.selectedParticipants || [],
    },
    validationSchema: Yup.object({
      name: Yup.string()
        .min(3, string.errors.min3)
        .required(`${string.errors.required} ${string.errors.required}`),
      shipment_category_id: Yup.string().required(
        `${string.shipment.eventCategory} ${string.errors.required}`
      ),
      document_category_id: Yup.string().required(
        `${string.shipment.documentCategory} ${string.errors.required}`
      ),
      // participant_category_id: Yup.string()
      //   .required(`Participant category ${string.errors.required}`),
      // participant_key: Yup.string()
      //   .required(`Participant ${string.errors.required}`),
      // template_id: Yup.object({
      //   name: Yup.string().required(`Template ${string.errors.required}`)
      // }),
      // template_id: Yup.string().required(`Template ${string.errors.required}`),
      selectedParticipants: Yup.array()
        .min(1, `${string.shipment.participants} ${string.errors.required}`)
        .required(`${string.shipment.participants} ${string.errors.required}`),
      // .length(1, `Participant ${string.errors.required}`)
    }),
    onSubmit: (values) => {
      nextStep(values);
    },
  });
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div>
        {/* <div className="modal-header">
                    <h5 className="modal-title text-dark font-weight-bold" id="addShipmentStepOneModal">{mode == "add" ? "ADD":"EDIT"} SHIPMENT (1/3)</h5>
                    <button className="close" type="button" data-dismiss="modal" aria-label="Close" onClick={() => changeStep(0)}>
                        <span aria-hidden="true">×</span>
                    </button>
                </div> */}
        <div>
          {/* form secction */}
          <form className="form-container" onSubmit={formik.handleSubmit}>
            <div className="shiment-name">
              <Input
                type="text"
                value={formik.values.name}

                onChange={formik.handleChange}
                placeholder={string.shipment.typeShipmentName}
                name="name"
              />
              {formik.errors.name ? (
                <FormHelperMessage
                  className="err"
                  message={formik.errors.name}
                />
              ) : null}
            </div>
            {/* row */}
            <div className="row ml-0 mr-0 content-block">
              <div className="col-md-6 pl-0">
                <div className="form-group row m-0">
                  <label
                    htmlFor="email_address"
                    className="col-md-12 col-form-label pl-0 pr-0"
                  >
                    {string.shipment.shipmentTemplate}

                  </label>
                  <div className="col-md-12 position-relative p-0">
                    <CustomSelect
                      className="form-control"

                      onChange={formik.handleChange}
                      value={formik.values.template_id}
                      name="template_id"
                    >
                      <option value="">{string.shipment.selectTemplate}</option>;
                      {state.shipments.map((shipment, i) => {
                        return (
                          <option value={shipment.id}>{shipment.name}</option>
                        );
                      })}
                    </CustomSelect>
                    {formik.errors.template_id ? (
                      <FormHelperMessage
                        className="err"
                        message={formik.errors.template_id}
                      />
                    ) : null}
                  </div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="form-group col-md-6 pl-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.participantcategory}

                    </label>
                    <div className="col-md-12 position-relative p-0">
                      <CustomSelect
                        className="form-control"

                        onChange={formik.handleChange}
                        name="participant_category_id"
                        options={state.participant_categories}
                      />
                      {formik.errors.participant_category_id ? (
                        <FormHelperMessage
                          className="err"
                          message={formik.errors.participant_category_id}
                        />
                      ) : null}
                    </div>
                  </div>
                  <div className="form-group col-md-6">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.participants}

                    </label>
                    <div className="col-md-12 position-relative pl-0 pr-0 d-flex">
                      <CustomSelect
                        className="form-control"

                        onChange={formik.handleChange}
                        name="participant_key"
                      >
                        <option value=""> {string.shipment.selectOne}</option>
                        {state.participants.map((category, i) => {
                          if (state.shipment.participant_category_id) {
                            if (
                              category.participant_category_id ==
                              state.shipment.participant_category_id
                            ) {
                              return (
                                <option key={i} value={i}>
                                  {category.official_name}
                                </option>
                              );
                            }
                          } else {
                            return (
                              <option key={i} value={i}>
                                {category.official_name}
                              </option>
                            );
                          }
                        })}
                      </CustomSelect>
                      {/* add btn */}
                      <div
                        className="add-btn"
                        onClick={(event) => {
                          event.preventDefault();
                          if (
                            state.shipment.selectedParticipants.filter(function (
                              e
                            ) {
                              return (
                                e.email ===
                                state.participants[
                                  formik.values.participant_key
                                ].email
                              );
                            }).length == 0
                          ) {
                            // setSelectedParticipants(state.shipment.selectedParticipants => [...state.shipment.selectedParticipants, state.participants[state.shipment.participant_key]]);
                            let selectedParticipants =
                              state.shipment.selectedParticipants;
                            let shipment = state.shipment;
                            selectedParticipants.push(
                              { label: state.participants[formik.values.participant_key].official_name, value: state.participants[formik.values.participant_key].id }
                            );
                            shipment.selectedParticipants = selectedParticipants;
                            formik.handleChange({
                              target: {
                                value: selectedParticipants,
                                name: "selectedParticipants",
                              },
                            });
                            setState({ shipment });
                          }
                        }}
                      >
                        <Button className="btn">
                          <i className="fas fa-plus fa-sm"></i>
                        </Button>
                      </div>
                      {/* //add btn */}
                    </div>
                    {formik.errors.participant_key ? (
                      <FormHelperMessage
                        className="err"
                        message={formik.errors.participant_key}
                      />
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="col-md-6 pr-0">
                <div className="row m-0">
                  <div className="form-group col-md-6 pl-0">
                    <label
                      htmlFor="email-address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.shipmentCategory}

                    </label>
                    <div className="col-md-12 position-relative p-0">
                      <CustomSelect
                        className="form-control"
                        value={formik.values.shipment_category_id}
                        name="shipment_category_id"

                        onChange={formik.handleChange}
                        options={state.shipment_categories}
                      />
                      {formik.errors.shipment_category_id ? (
                        <FormHelperMessage
                          className="err"
                          message={formik.errors.shipment_category_id}
                        />
                      ) : null}
                    </div>
                  </div>
                  <div className="form-group col-md-6">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.documentCategory}

                    </label>
                    <div className="col-md-12 position-relative p-0">
                      <CustomSelect
                        className="form-control"
                        value={formik.values.document_category_id}

                        onChange={formik.handleChange}
                        name="document_category_id"
                        options={state.document_categories}
                      />
                      {formik.errors.document_category_id ? (
                        <FormHelperMessage
                          className="err"
                          message={formik.errors.document_category_id}
                        />
                      ) : null}
                    </div>
                  </div>
                </div>
                <div className="form-group row m-0">
                  <label
                    htmlFor="email_address"
                    className="col-md-12 col-form-label pl-0"
                  >
                    {string.shipment.selectedParticipants}

                  </label>
                  <div className="col-md-12 position-relative p-0">
                    {state.shipment.selectedParticipants.map(
                      (participant, i) => {
                        return (
                          <button
                            key={i}
                            onClick={() => {
                              let participants =
                                state.shipment.selectedParticipants;
                              participants.splice(i, 1);
                              setState({
                                shipment: Object.assign({}, state.shipment, {
                                  selectedParticipants: participants,
                                }),
                              });
                            }}
                            type="button"
                            className="btn btn-default"
                          >
                            {participant.label}{" "}
                            <span className="fa fa-trash"></span>
                          </button>
                        );
                      }
                    )}
                    {formik.errors.selectedParticipants ? (
                      <FormHelperMessage
                        className="err"
                        message={formik.errors.selectedParticipants}
                      />
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
            {/* //row */}
            <div className="modal-footer">
              <Button
                className="btn btn-primary large-btn"
                type="submit"
              // onClick={nextStep}
              >
                {string.shipment.next}
              </Button>
            </div>
          </form>
          {/* //form secction */}
        </div>
      </div>
    );
  }
};

AddShipmentStepOne.propTypes = {};
AddShipmentStepOne.defaultProps = {};

export default AddShipmentStepOne;

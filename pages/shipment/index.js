import PropTypes from "prop-types";
import notify from "../../lib/notifier";
import Head from "next/head";
import Header from "../../components/HomeHeader";
import Footer from "../../components/HomeFooter";
import Sidebar from "../../components/Sidebar";
import AddShipmentStepOne from "./AddShipmentStepOne";
import AddShipmentStepTwo from "./AddShipmentStepTwo";
import DeleteModal from "./DeleteModal";
import ToggleModal from "./ToggleModal";
import NProgress from "nprogress";

import withAuth from "../../lib/withAuth";
import { fetchShipmentCategories } from "../../lib/api/shipment-category";
import { fetchDocumentCategories } from "../../lib/api/document-category";
import { fetchParticipantCategories } from "../../lib/api/participant-category";
import { fetchUsers } from "../../lib/api/user";
import { addTruck, fetchTrucks } from "../../lib/api/truck";
import { addContainer, fetchContainers } from "../../lib/api/container";
import { fetchRoads } from "../../lib/api/road";
import { addGroup, fetchGroups } from "../../lib/api/group";
import { addCargo, fetchCargos } from "../../lib/api/cargo";
import { addDevice, fetchDevices } from "../../lib/api/device";
import {
    addShipment,
    fetchShipments,
    removeShipment,
    startTracking,
    stopTracking,
    updateShipment,
} from "../../lib/api/shipment";
import AddShipmentStepThree from "./AddShipmentStepThree";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Button from "../../components/common/form-elements/button/Button";
import '../../static/css/modal.css';
class ShipmentPage extends React.Component {
    static getInitialProps() {
        const shipmentPage = true;
        return { shipmentPage };
    }

    static propTypes = {
        user: PropTypes.shape({
            _id: PropTypes.string,
        }),
    };

    static defaultProps = {
        user: null,
    };

    async componentDidMount() {
        NProgress.start();
        try {
            const shipments = await fetchShipments();
            this.setState({ shipments });
            const shipment_categories = await fetchShipmentCategories();
            this.setState({ shipment_categories });
            const document_categories = await fetchDocumentCategories();
            this.setState({ document_categories });
            const participant_categories = await fetchParticipantCategories();
            this.setState({ participant_categories });
            const participants = await fetchUsers();
            this.setState({ participants });
            const trucks = await fetchTrucks();
            this.setState({ trucks });
            const containers = await fetchContainers();
            this.setState({ containers });
            const roads = await fetchRoads();
            this.setState({ roads });
            const groups = await fetchGroups();
            this.setState({ groups });
            const cargos = await fetchCargos();
            this.setState({ cargos });
            const devices = await fetchDevices();
            this.setState({ devices });
            NProgress.done();
        } catch (err) {
            this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
            NProgress.done();
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            user: props.user || {},
            shipments: [],
            shipment: {
                selectedParticipants: [],
                selections: [],
                selectedRoads: [],
            },
            shipment_categories: [],
            document_categories: [],
            participant_categories: [],
            participants: [],
            trucks: [],
            containers: [],
            roads: [],
            groups: [],
            cargos: [],
            devices: [],
            deleteMode: "",
            selectedIndex: "",
            currentStep: 0,
            isEdit: false,
            isActive: false,
            deleteOpen: false,
            toggleOpen: false,
        };
    }
  // this function will be called from child component to add cargo
  addCargoData = async (cargoID) => {
    console.log("in add cargo");
    NProgress.start();
    let { cargos } = this.state;
    let cargo = await addCargo({ cargoID: cargoID });
    cargos.push(cargo);
    this.setState({ cargos });
    NProgress.done();
    notify(`${string.shipment.cargoidHasBeenAdded}`);

  };
  addDeviceData = async (deviceID) => {
    console.log("in add device");
    NProgress.start();
    let { devices } = this.state;
    let device = await addDevice({ deviceID: deviceID });
    devices.push(device);
    this.setState({ devices });
    NProgress.done();
    notify(`${string.shipment.deviceIdHasBeenAdded}`);
  };
  addGroupData = async (groupID) => {
    console.log("in add group");
    NProgress.start();
    let { groups } = this.state;
    let group = await addGroup({ groupID: groupID });
    groups.push(group);
    this.setState({ groups });
    NProgress.done();
    notify(`${string.shipment.groupIdHasBeenAdded}`);
  };
  addTruckData = async (truckID, containers) => {
    console.log("in add truck");
    NProgress.start();
    let { trucks } = this.state;
    let truck = await addTruck({ truckID: truckID, containers: containers });
    trucks.push(truck);
    this.setState({ trucks });
    NProgress.done();
    notify(`${string.shipment.truckIdHasBeenAdded}`);

  };
  addContainerData = async (containerID, cargos) => {
    console.log("in add");
    NProgress.start();
    let { containers } = this.state;
    let container = await addContainer({
      containerID: containerID,
      cargos: cargos,
    });
    containers.push(container);
    this.setState({ containers });
    NProgress.done();
    notify(`${string.shipment.containerIdHasBeenAdded}`);

  };

    // this function is used to save shipment data into shipment state from modal which can be use to pass into another model
    setShipmentData = (shipment, selections) => {
        this.setState({ shipment: shipment });
    };

  saveShipment = async (shipment) => {
    NProgress.start();
    try {
      if (this.state.isEdit) {
        await updateShipment(shipment);
        notify(`${string.shipment.shipmentDataSaved}`);

    } else {
        await addShipment(shipment);
        notify(`${string.shipment.shipmentDataAdded}`);

    }
    let shipments = await fetchShipments();
    this.setState({ shipments });
    const trucks = await fetchTrucks();
    this.setState({ trucks });
    const containers = await fetchContainers();
    this.setState({ containers });
    const cargos = await fetchCargos();
    this.setState({ cargos });
    const devices = await fetchDevices();
    this.setState({ devices });
      NProgress.done();
    } catch (err) {
      NProgress.done();
      console.log(err);
      notify(`${string.shipment.errorAddingShipmentData}`);

    }
    this.setState({
      currentStep: 0,
      isEdit: false,
    });
    // $("#shipmentstepModal").modal('hide');
  };

    // set delete mode upon selecting delete icon
    setDeleteMode = (mode, i) => {
        if (mode) {
            this.setState({ deleteMode: mode });
            this.setState({ selectedIndex: i });
            this.setState({ deleteOpen: true });
            // $("#deleteModal").modal('show');
        }
    };

    // set toggle status
    setToggleStatus = (i) => {
        let { shipments } = this.state;
        let shipment = shipments[i];
        this.setState({
            toggleOpen: true,
            selectedIndex: i,
            isActive: shipment.isActive ? true : false,
        });
    };

    editMode = (i) => {
        let { shipments } = this.state;
        let shipment = shipments[i];
        this.setState({ shipment: shipment, isEdit: true });
        this._toggleStep(1);
        // $("#editShipmentModal").modal('show');
    };

    addMode = () => {
        this.setState({
            isEdit: false,
            shipment: { selectedParticipants: [], selections: [], selectedRoads: [] },
        });
        this._toggleStep(1);
    };

  // Function to delete entry from popup
  onDeleteEntry = async (event) => {
    console.log("in delete");
    NProgress.start();
    event.preventDefault();
    let { deleteMode, shipments, selectedIndex } = this.state;
    if (deleteMode == "shipment") {
      // delete shipment data
      let shipments_data = shipments[selectedIndex];
      await removeShipment({ id: shipments_data.id });
      shipments.splice(selectedIndex, 1);
      this.setState({ shipments, deleteOpen: false });
      NProgress.done();
      notify(`${string.shipment.shipmentDeletedSuccessfully}`);

    }
  };

    // Function to toggle shipment status
    toggleStatus = async (event) => {
        NProgress.start();
        event.preventDefault();
        let { shipments, selectedIndex } = this.state;
        let shipments_data = shipments[selectedIndex];
        shipments_data.isActive
            ? await stopTracking({ id: shipments_data.id })
            : await startTracking({ id: shipments_data.id });
        shipments[selectedIndex].isActive = !shipments_data.isActive;
        this.setState({ shipments, toggleOpen: false });
        NProgress.done();
        notify(shipments_data.isActive ? "Shipment started!" : "Shipment stopped!");
    };

    _toggleStep = (step) => {
        this.setState({ currentStep: step });
    };

    render() {
        let {
            shipments,
            user,
            currentStep,
            isEdit,
            deleteOpen,
            toggleOpen,
            isActive,
        } = this.state;
        // NEEDS TO OPTIMIZE FOLLOWING LOGIC ONCE EDIT SHIPMENT IS DONE
        shipments?.length > 0 && shipments?.map((shipment, i) => {
            shipments[i].shipmentCargos = [];
            shipments[i].selectedParticipants = [];
            shipments[i].selectedRoads = [];
            let selections = [];
            shipment.shipment_selections.map((shipment_selection) => {
                let selection = {};
                selection.cargos = [];
                shipment_selection.selection_cargos.map((cargo) => {
                    shipments[i].shipmentCargos.push(cargo);
                    selection.cargos.push({
                        value: cargo.cargo.id,
                        label: cargo.cargo.cargoID,
                    });
                });
                selection.containers = [];
                shipment_selection.selection_containers.map((container) => {
                    selection.containers.push({
                        value: container.container.id,
                        label: container.container.containerID,
                    });
                });
                shipment_selection.selection_devices.map((device) => {
                    selection.device_data_interval = device.device_data_interval;
                    selection.device_id = device.device_id;
                });
                shipment_selection.selection_groups.map((group) => {
                    selection.group_id = group.group_id;
                });
                shipment_selection.selection_trucks.map((truck) => {
                    selection.truck_id = truck.truck_id;
                });
                selections.push(selection);
            });
            shipments[i].selectedRoads = shipment.shipment_roads;
            shipments[i].selections = selections;
            shipment.shipment_participants.map((participant) => {
                shipments[i].selectedParticipants.push({
                    value: participant.user.id,
                    label: participant.user.official_name,
                });
            });
        });
    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - {string.shipment.shipmentListing}</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <div
                    className="tab-pane fade show active mt-3 col-md-12"
                    id="all2"
                    role="tabpanel"
                    aria-labelledby="all-containers"
                  >
                    <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter">
                      <h4 className="text-dark">{string.shipment.shipmentListing}</h4>
                      <Button
                        className="btn btn-primary large-btn"
                        onClick={() => this.addMode()}
                      >
                        SUBMIT SHIPMENT
                      </Button>
                    </div>
                    <div className="shipment-table-listing table-responsive mt-2">
                      <table className="table text-center">
                        <thead className="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">{string.shipment.shipmentName}</th>
                            <th scope="col">{string.shipment.cargo}</th>
                            <th scope="col">{string.shipment.participants}</th>
                            <th scope="col">{string.shipment.temperatureAlert}</th>
                            <th scope="col">{string.shipment.humidityAlert}</th>
                            <th scope="col">{string.shipment.sealingAlert}</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {shipments?.length > 0 && shipments?.map((shipment, i) => {
                            let shipmentStatusClass = "fa fa-stopwatch";
                            if (shipment.isActive == 1) {
                              shipmentStatusClass =
                                "fa fa-stopwatch text-success";
                            } else if (shipment.isActive == 0) {
                              shipmentStatusClass =
                                "fa fa-stopwatch text-danger";
                            }

                                                        return (
                                                            <tr key={i}>
                                                                <td>{i + 1}</td>
                                                                <td>{shipment.name}</td>
                                                                <td>
                                                                    {shipment.shipmentCargos
                                                                        .map((el) => {
                                                                            return el.cargo.cargoID;
                                                                        })
                                                                        .join(", ")}
                                                                </td>
                                                                <td>
                                                                    {shipment.selectedParticipants
                                                                        .map((el) => {
                                                                            return el.label;
                                                                        })
                                                                        .join(", ")}
                                                                </td>
                                                                <td align="center">
                                                                    {shipment.temperature_alert_min}°C -{" "}
                                                                    {shipment.temperature_alert_max}°C ({shipment.temperature_allowed_occurances})
                                                                </td>
                                                                <td align="center">
                                                                    {shipment.humidity_alert_min}% -{" "}
                                                                    {shipment.humidity_alert_max}% ({shipment.humidity_allowed_occurances})
                                                                </td>
                                                                <td align="center">
                                                                    {shipment.ambience_threshold}%
                                                                </td>
                                                                <td>
                                                                    <i
                                                                        className="fa fa-pencil-alt"
                                                                        onClick={() => this.editMode(i)}
                                                                    ></i>
                                                                    <i
                                                                        className="fa fa-trash"
                                                                        onClick={() =>
                                                                            this.setDeleteMode("shipment", i)
                                                                        }
                                                                    ></i>
                                                                    <i
                                                                        className={shipmentStatusClass}
                                                                        onClick={() => this.setToggleStatus(i)}
                                                                    ></i>
                                                                </td>
                                                            </tr>
                                                        );
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* End of Main Content */}
                    </div>
                    {/* End of Content Wrapper */}
                </div>
                {/* End of Page Wrapper */}
                <div
                    className="modal fade customModal document"
                    id="deleteModal"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true"
                >
                    <DeleteModal
                        onDeleteEntry={this.onDeleteEntry}
                        isOpen={deleteOpen}
                        toggleDelete={() => this.setState({ deleteOpen: !deleteOpen })}
                    />
                </div>
                <div
                    className="modal fade customModal document"
                    id="toggleModal"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true"
                >
                    <ToggleModal
                        isActive={isActive}
                        onToggleStatus={this.toggleStatus}
                        isOpen={toggleOpen}
                        toggleStatus={() => this.setState({ toggleOpen: !toggleOpen })}
                    />
                </div>
                {currentStep > 0 && (
                    <Modal
                        isOpen={currentStep > 0}
                        toggle={() => this._toggleStep(0)}
                        size={"lg"}
                        className={currentStep < 3 ? "customModal width80" : "customModal"}
                        id="shipmentModal"
                    >
                        <ModalHeader toggle={() => this._toggleStep(0)}>
                            <h5 className="modal-title text-dark font-weight-bold" id="addShipmentStepTwoModal">
                                {isEdit ? "EDIT" : "ADD"} SHIPMENT ({currentStep}/3)
                            </h5>
                        </ModalHeader>
                        <ModalBody>
                            {currentStep === 1 ? (
                                <AddShipmentStepOne
                                    mode={isEdit ? "edit" : "add"}
                                    setState={this.setState.bind(this)}
                                    state={this.state}
                                    user={user}
                                    changeStep={this._toggleStep}
                                />
                            ) : currentStep === 2 ? (
                                <AddShipmentStepTwo
                                    mode={isEdit ? "edit" : "add"}
                                    setState={this.setState.bind(this)}
                                    state={this.state}
                                    addCargoData={this.addCargoData}
                                    addGroupData={this.addGroupData}
                                    addDeviceData={this.addDeviceData}
                                    addTruckData={this.addTruckData}
                                    addContainerData={this.addContainerData}
                                    changeStep={this._toggleStep}
                                />
                            ) : currentStep === 3 ? (
                                <AddShipmentStepThree
                                    mode={isEdit ? "edit" : "add"}
                                    setState={this.setState.bind(this)}
                                    state={this.state}
                                    changeStep={this._toggleStep}
                                    saveShipment={this.saveShipment}
                                />
                            ) : null}
                        </ModalBody>
                        {/* </div> */}
                    </Modal>
                )}
                <Footer />
            </div>
        );
    }
}

export default withAuth(ShipmentPage, { loginRequired: true });

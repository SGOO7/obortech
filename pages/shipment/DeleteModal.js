import { Modal, ModalHeader, ModalBody } from "reactstrap";
// updated
import Button from "../../components/common/form-elements/button/Button";

const DeleteModal = ({ onDeleteEntry, toggleDelete, isOpen }) => {
  return (
    <Modal isOpen={isOpen} toggle={toggleDelete} className="customModal">
      <ModalHeader toggle={toggleDelete}></ModalHeader>
      <ModalBody className="text-center mb-5">
        <p>
          <strong>DELETE RECORD ?</strong>
        </p>
        <Button
          className="btn btn-primary large-btn"
          type="button"
          data-dismiss="modal"
          onClick={onDeleteEntry}
        >
          DELETE
        </Button>
      </ModalBody>
    </Modal>
  );
};

DeleteModal.propTypes = {};

DeleteModal.defaultProps = {};

export default DeleteModal;

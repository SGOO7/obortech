import React, { useState } from "react";
import notify from "../../lib/notifier";
import { useFormik } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Input from "../../components/common/form-elements/input/Input";
import CustomSelect from "../../components/common/form-elements/select/CustomSelect";
import Button from "../../components/common/form-elements/button/Button";
import AdvanceSelect from "../../components/common/form-elements/select/AdvanceSelect";

const AddShipmentStepTwo = ({
    mode,
    setState,
    state,
    addCargoData,
    addGroupData,
    addDeviceData,
    addTruckData,
    addContainerData,
    changeStep,
}) => {
    const [cargoID, setCargoID] = useState("");
    const [deviceID, setDeviceID] = useState("");
    const [groupID, setGroupID] = useState("");
    const [containerID, setContainerID] = useState("");
    const [truckID, setTruckID] = useState("");
    const backStep = (event) => {
        event.preventDefault();
        changeStep(1);
    };

    let cargoOptions = [];
    state.cargos.map((cargo, i) => {
        if(cargo.is_available){
            cargoOptions.push({
                label: cargo.cargoID,
                value: cargo.id
            });
        }
    });

  const nextStep = (event) => {
    event.preventDefault();
    // Validate empty state
    if (state.shipment.selections.length == 0) {
      notify(`${string.shipment.noSelectionMade}`);
      return false;
    }
    state.shipment.selectedParticipants = state.shipment.selectedParticipants;
    changeStep(3);
  };
  console.log("state", state);
  const formik = useFormik({
    initialValues: {
      group_id: state?.shipment?.group_id || "",
      container_id: state?.shipment?.containers || "",
      device_id: state?.shipment?.device_id || "",
      truck_id: state?.shipment?.truck_id || "",
      cargo_id:
        state?.shipment?.cargo_id === ""
          ? []
          : state?.shipment?.cargo_id === "" || [],
      device_data_interval: state?.shipment?.device_data_interval || "",
    },
    validationSchema: Yup.object({
      container_id: Yup.object().shape({
        value: Yup.string().required(`${string.shipment.container} ${string.errors.required}`),
        label: Yup.string()
      }),
      group_id: Yup.string().required(`${string.shipment.group} ${string.errors.required}`),
      device_id: Yup.string().required(`${string.shipment.device} ${string.errors.required}`),
      truck_id: Yup.string().required(`${string.shipment.truck} ${string.errors.required}`),
      cargo_id: Yup.array().min(1, `${string.shipment.cargo} ${string.errors.required}`),
      device_data_interval: Yup.string().required(
        `${string.shipment.dataInterval} ${string.errors.required}`
      ),
    }),
    onSubmit: (values) => {
      console.log("vlues", values);
      let selection = {};
      selection.group_id = values.group_id;
      selection.cargos = values.cargo_id;
      selection.containers = new Array(values.container_id);
      selection.truck_id = values.truck_id;
      selection.device_id = values.device_id;
      selection.device_data_interval = values.device_data_interval;
      let selections = state.shipment.selections;
      let shipment = {
        ...state.shipment,
        group_id: values.group_id,
        cargos: values.cargo_id,
        containers: values.container_id,
        truck_id: values.truck_id,
        device_id: values.device_id,
        device_data_interval: values.device_data_interval,
      };
      selections.push(selection);
      shipment.selections = selections;
      setState({ shipment });
      // nextStep(values)
    },
  });
  console.log("formik.values", formik.values);
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div>
        <div>
          {/* form secction */}
          <form className="form-container" onSubmit={formik.handleSubmit}>
            {/* row */}
            <div className="row ml-0 mr-0 content-block">
              <div className="col-md-6 pl-0">
                <div className="row m-0">
                  <div className="form-group col-md-6 pl-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.addGroup}
                    </label>
                    <div className="col-md-12 position-relative p-0 d-flex">
                      <Input
                        type="text"
                        name="groupID"
                        value={groupID || ""}
                        placeholder={string.shipment.typeGroupId}

                        className="form-control radius-0"
                        onChange={(event) => {
                          setGroupID(event.target.value);
                        }}
                      />
                      {/* add btn */}
                      <div
                        className="add-btn pl-0"
                        onClick={(event) => {
                          event.preventDefault();
                          if (!groupID) {

                            notify(`${string.shipment.pleaseEnterGroupId}`);
                            return false;
                          }
                          // call parent class's function to add new Cargo
                          addGroupData(groupID);
                          setGroupID("");
                        }}
                      >
                        <Button className="btn radius-0 border-left-0">
                          <i className="fas fa-plus fa-sm"></i>
                        </Button>
                      </div>
                      {/* //add btn */}
                    </div>
                  </div>
                  <div className="form-group col-md-6">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.selectGroup}

                    </label>
                    <div className="col-md-12 position-relative p-0">
                      <CustomSelect
                        className="form-control"
                        // onChange={(event) => {
                        //   setState({
                        //     shipment: Object.assign({}, state.shipment, {
                        //       group_id: event.target.value,
                        //     }),
                        //   });
                        // }}
                        onChange={formik.handleChange}
                        name="group_id"
                      >
                        <option value=""> {string.shipment.selectOne}</option>
                        {state.groups.map((group, i) => {
                          return (
                            <option key={i} value={group.id}>
                              {group.groupID}
                            </option>
                          );
                        })}
                      </CustomSelect>
                      {formik.errors.group_id ? (
                        <FormHelperMessage
                          className="err"
                          message={formik.errors.group_id}
                        />
                      ) : null}
                    </div>
                  </div>
                </div>
                {/* //row */}
                {/* row */}
                <div className="row m-0">
                  <div className="form-group col-md-6 pl-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.addContainer}

                    </label>
                    <div className="col-md-12 position-relative p-0 d-flex">
                      <Input
                        type="text"
                        name="containerID"
                        placeholder="Type container ID"
                        className="form-control radius-0"
                        onChange={(event) => {
                          setContainerID(event.target.value);
                        }}
                      />
                      {/* add btn */}
                      {/* <div className="add-btn pl-0"><button className="btn radius-0 border-left-0"><i className="fas fa-plus fa-sm"></i></button></div> */}
                      <div
                        className="add-btn pl-0"
                        onClick={(event) => {
                          event.preventDefault();
                          if (!containerID) {
                            notify(`${string.shipment.pleaseEnterContainerId}`);
                            return false;
                          }
                          if (
                            state.shipment.cargos == undefined ||
                            state.shipment.cargos.length == 0
                          ) {
                            notify("Please select Cargo");
                            return;
                          }
                          // call parent class's function to add new Cargo
                          addContainerData(containerID, state.shipment.cargos);
                        }}
                      >
                        <Button className="btn radius-0 border-left-0">
                          <i className="fas fa-plus fa-sm"></i>
                        </Button>
                      </div>
                      {/* //add btn */}
                    </div>
                  </div>
                  <div className="form-group col-md-6">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.selectContainer}

                    </label>
                    <div className="col-md-12 position-relative p-0">
                      <AdvanceSelect
                        value={formik.values.container_id}
                        // isMulti
                        options={containerOptions}
                        name="continer_id"
                        // onChange={(event) => {
                        //   setState({
                        //     shipment: Object.assign({}, state.shipment, {
                        //       containers: event,
                        //     }),
                        //   });
                        // }}
                        onChange={(e) =>
                          formik.handleChange({
                            target: { value: e, name: "container_id" },
                          })
                        }
                      />
                      {formik.errors.container_id?.value ? (
                        <FormHelperMessage
                          className="err"
                          message={formik.errors.container_id?.value}
                        />
                      ) : null}
                    </div>
                  </div>
                </div>
                {/* //row */}
                {/* row */}
                <div className="row m-0">
                  <div className="form-group col-md-6 pl-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.addDevice}

                    </label>
                    <div className="col-md-12 position-relative p-0 d-flex">
                      <Input
                        type="text"
                        name="deviceID"
                        placeholder={string.shipment.typeDeviceId}

                        className="form-control radius-0"
                        onChange={(event) => {
                          setDeviceID(event.target.value);
                        }}
                      />
                      {/* add btn */}
                      <div
                        className="add-btn pl-0"
                        onClick={(event) => {
                          event.preventDefault();
                          if (!deviceID) {
                            notify(`${string.shipment.pleaseEnterDeviceId}`);
                            return false;
                          }
                          // call parent class's function to add new Cargo
                          addDeviceData(deviceID);
                        }}
                      >
                        <Button className="btn radius-0 border-left-0">
                          <i className="fas fa-plus fa-sm"></i>
                        </Button>
                      </div>
                      {/* //add btn */}
                    </div>
                  </div>
                  <div className="form-group col-md-6 pr-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.selectDevice}

                    </label>
                    <div className="col-md-12 position-relative p-0">
                      <CustomSelect
                        value={state.shipment.device_id || ""}
                        // onChange={(event) => {
                        //   setState({
                        //     shipment: Object.assign({}, state.shipment, {
                        //       device_id: event.target.value,
                        //     }),
                        //   });
                        // }}
                        onChange={formik.handleChange}
                        value={formik.values.device_id}
                        name="device_id"
                        className="form-control"
                      >
                        <option value="">{string.shipment.selectOne}</option>
                        {state.devices.map((device, i) => {
                          return (
                            <option key={i} value={device.id}>
                              {device.deviceID}
                            </option>
                          );
                        })}
                      </CustomSelect>
                      {formik.errors.device_id ? (
                        <FormHelperMessage
                          className="err"
                          message={formik.errors.device_id}
                        />
                      ) : (
                          ""
                        )}
                    </div>
                  </div>
                </div>
                {/* //row */}
              </div>
              <div className="col-md-6 pr-0">
                {/* row */}
                <div className="row m-0">
                  <div className="form-group col-md-6 pl-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      Add truck
                    </label>
                    <div className="col-md-12 position-relative p-0 d-flex">
                      <Input
                        type="text"
                        name="truckID"
                        value={truckID || ""}
                        placeholder={string.shipment.typeTruckId}
                        className="form-control radius-0"
                        onChange={(event) => {
                          setTruckID(event.target.value);
                        }}
                      />
                      {/* add btn */}
                      <div
                        className="add-btn pl-0"
                        onClick={(event) => {
                          event.preventDefault();
                          if (!truckID) {
                            notify("Please enter TruckID");
                            return false;
                          }
                          if (
                            state.shipment.containers == undefined ||
                            state.shipment.containers.length == 0
                          ) {
                            notify("Please select Container");
                            return;
                          }
                          // call parent class's function to add new Truck
                          addTruckData(truckID, state.shipment.containers);
                          setTruckID("");
                        }}
                      >
                        <Button className="btn radius-0 border-left-0">
                          <i className="fas fa-plus fa-sm"></i>
                        </Button>
                      </div>
                      {/* //add btn */}
                    </div>
                  </div>
                  <div className="form-group col-md-6">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      Select truck
                    </label>
                    <div className="col-md-12 position-relative p-0">
                      <CustomSelect
                        value={state.shipment.truck_id || ""}
                        className="form-control"
                        // onChange={(event) => {
                        //   setState({
                        //     shipment: Object.assign({}, state.shipment, {
                        //       truck_id: event.target.value,
                        //     }),
                        //   });
                        // }}
                        onChange={formik.handleChange}
                        value={formik.values.truck_id}
                        name="truck_id"
                      >
                        <option value="">{string.shipment.selectOne}</option>
                        {state.trucks.map((truck, i) => {
                          return (
                            <option key={i} value={truck.id}>
                              {truck.truckID}
                            </option>
                          );
                        })}
                      </CustomSelect>
                      {formik.errors.truck_id ? (
                        <FormHelperMessage
                          className="err"
                          message={formik.errors.truck_id}
                        />
                      ) : null}
                    </div>
                  </div>
                </div>
                {/* //row */}
                {/* row */}
                <div className="row m-0 ">
                  <div className="form-group col-md-6 pl-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.addCargo}

                    </label>
                    <div className="col-md-12 position-relative p-0 d-flex">
                      <Input
                        type="text"
                        name="cargoID"
                        placeholder={string.shipment.typeCargoId}
                        className="form-control radius-0"
                        onChange={(event) => {
                          setCargoID(event.target.value);
                        }}
                      />
                      {/* add btn */}
                      <div
                        className="add-btn pl-0"
                        onClick={(event) => {
                          event.preventDefault();
                          if (!cargoID) {
                            notify("Please enter CargoID");
                            return false;
                          }
                          // call parent class's function to add new Cargo
                          addCargoData(cargoID);
                        }}
                      >
                        <Button className="btn radius-0 border-left-0">
                          <i className="fas fa-plus fa-sm"></i>
                        </Button>
                      </div>
                      {/* //add btn */}
                    </div>
                  </div>
                  <div className="form-group col-md-6 pr-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.selectCargo}

                    </label>
                    <div className="col-md-12 position-relative p-0">
                      <AdvanceSelect
                        value={state.shipment.cargos}
                        isMulti
                        options={cargoOptions}
                        name="cargo_id"
                        // onChange={(event) => {
                        //   setState({
                        //     shipment: Object.assign({}, state.shipment, {
                        //       cargos: event,
                        //     }),
                        //   });
                        // }}
                        value={formik.values.cargo_id}
                        onChange={(e) =>
                          formik.handleChange({
                            target: { value: e, name: "cargo_id" },
                          })
                        }
                      />
                      {formik.errors.cargo_id ? (
                        <FormHelperMessage
                          className="err"
                          message={formik.errors.cargo_id}
                        />
                      ) : null}
                    </div>
                  </div>
                </div>
                <div className="row m-0">
                  <div className="form-group col-md-6 pl-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    >
                      {string.shipment.dataInterval}

                    </label>
                    <div className="col-md-12 position-relative p-0 d-flex">
                      <Input
                        type="text"
                        name="device_data_interval"
                        value={formik.values.device_data_interval}
                        placeholder={string.shipment.minutes}
                        className="form-control radius-0"
                        // onChange={(event) => {
                        //   setState({
                        //     shipment: Object.assign({}, state.shipment, {
                        //       device_data_interval: event.target.value,
                        //     }),
                        //   });
                        // }}
                        onChange={formik.handleChange}
                      />
                    </div>
                    {formik.errors.device_data_interval ? (
                      <FormHelperMessage
                        className="err"
                        message={formik.errors.device_data_interval}
                      />
                    ) : null}
                  </div>
                  <div className="form-group col-md-6 pr-0">
                    <label
                      htmlFor="email_address"
                      className="col-md-12 col-form-label pl-0"
                    ></label>
                    <div className="col-md-12 position-relative p-0 mt-1 d-flex">
                      <Button
                        className="col-md-12 btn btn-primary large-btn"
                        type="submit"
                      // onClick={(event) => {
                      //   event.preventDefault();
                      //   if (!state.shipment.truck_id) {
                      //     notify("Please select Truck");
                      //     return false;
                      //   }
                      //   if (
                      //     state.shipment.containers == undefined ||
                      //     state.shipment.containers.length == 0
                      //   ) {
                      //     notify("Please select Container");
                      //     return false;
                      //   }
                      //   if (
                      //     state.shipment.cargos == undefined ||
                      //     state.shipment.cargos.length == 0
                      //   ) {
                      //     notify("Please select Cargo");
                      //     return false;
                      //   }

                      //   if (!state.shipment.device_id) {
                      //     notify("Please select Device");
                      //     return false;
                      //   }
                      //   if (!state.shipment.device_data_interval) {
                      //     notify("Please mention Data Interval");
                      //     return false;
                      //   }
                      //   let selection = {};
                      //   selection.group_id = state.shipment.group_id;
                      //   selection.cargos = state.shipment.cargos;
                      //   selection.containers = new Array(
                      //     state.shipment.containers
                      //   );
                      //   selection.truck_id = state.shipment.truck_id;
                      //   selection.device_id = state.shipment.device_id;
                      //   selection.device_data_interval =
                      //     state.shipment.device_data_interval;
                      //   let selections = state.shipment.selections;
                      //   let shipment = state.shipment;
                      //   selections.push(selection);
                      //   shipment.selections = selections;
                      //   setState({ shipment });
                      //   // setSelections(state.shipment.selections => [...state.shipment.selections, selection]);
                      // }}
                      >
                        {string.shipment.add}

                      </Button>
                    </div>
                  </div>
                </div>
                {/* //row */}
              </div>
            </div>
            {/* //row */}
            <div className="shipment-table-listing table-responsive">
              <table className="table text-center">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">{string.shipment.group}</th>
                    <th scope="col">{string.shipment.truck}</th>
                    <th scope="col">{string.shipment.container}</th>
                    <th scope="col">{string.shipment.cargo}</th>
                    <th scope="col">{string.shipment.device}</th>
                    <th scope="col">{string.shipment.actions}</th>
                  </tr>
                </thead>
                <tbody>
                  {state.shipment.selections.length == 0 && (
                    <tr>
                      <td className="m-auto text-center" colSpan="6">
                        {string.shipment.noData}
                      </td>
                    </tr>
                  )}
                  {state.shipment.selections.map((selection, i) => {
                    return (
                      <tr key={i}>
                        <td>
                          {
                            state.groups.find((x) => x.id == selection.group_id)
                              ?.groupID
                          }
                        </td>
                        <td>
                          {
                            state.trucks.find((x) => x.id == selection.truck_id)
                              ?.truckID
                          }
                        </td>
                        <td>
                          {selection?.containers
                            ?.map((el) => {
                              return el.label;
                            })
                            .join(", ")}
                        </td>
                        <td>
                          {selection?.cargos
                            ?.map((el) => {
                              return el.label;
                            })
                            .join(", ")}
                        </td>
                        <td>
                          {
                            state.devices.find(
                              (x) => x.id == selection.device_id
                            )?.deviceID
                          }
                        </td>
                        <td>
                          <i
                            className="fa fa-trash"
                            onClick={(event) => {
                              event.preventDefault();
                              let selections = state.shipment.selections;
                              let shipment = state.shipment;
                              selections.splice(i, 1);
                              shipment.selections = selections;
                              setState({
                                shipment: Object.assign({}, state.shipment, {
                                  selections,
                                }),
                              });
                            }}
                          ></i>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </form>
          {/* //form secction */}
        </div>
        <div className="modal-footer">
          <Button
            className="btn btn-secondary large-btn"
            type="button"
            onClick={backStep}
          >
            Back
          </Button>

          <Button
            className="btn btn-primary large-btn"
            type="button"
            onClick={nextStep}
          >
            {string.shipment.next}
          </Button>
        </div>
      </div>
    );
  }
};

AddShipmentStepTwo.propTypes = {};
AddShipmentStepTwo.defaultProps = {};

export default AddShipmentStepTwo;

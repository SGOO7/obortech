import React, { useState } from "react";
import { DraggableItem } from "../../components/shipments/DraggableItem";
import notify from "../../lib/notifier";
import { useFormik } from "formik";
import * as Yup from "yup";
import string from "../../utils/stringConstants/language/eng.json";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";

const AddShipmentStepThree = ({
	mode,
	setState,
	state,
	saveShipment,
	changeStep,
}) => {
	const backStep = (event) => {
		event.preventDefault();
		changeStep(2);
	};

	const saveAsDraft = (formik) => {
		state.shipment.isDraft = 1;
		formik.handleSubmit(formik.values);
	};

	const submitShipment = (shipment) => {
		saveShipment(shipment);
	};
	const formik = useFormik({
		initialValues: {
			temperature_alert_min: state?.shipment?.temperature_alert_min || "",
			temperature_alert_max: state?.shipment?.temperature_alert_max || "",
			temperature_alert_interval: state?.shipment?.temperature_alert_interval || "",
			temperature_allowed_occurances: state?.shipment?.temperature_allowed_occurances || "",
			humidity_alert_min: state?.shipment?.humidity_alert_min || "",
			humidity_alert_max: state?.shipment?.humidity_alert_max || "",
			humidity_alert_interval: state?.shipment?.humidity_alert_interval || "",
			humidity_allowed_occurances: state?.shipment?.humidity_allowed_occurances || "",
			ambience_threshold: state?.shipment?.ambience_threshold || "",
		},
		validationSchema: Yup.object({
			temperature_alert_min: Yup.string().required(
				`${string.shipment.minimumTemprature} ${string.errors.required}`
			),
			temperature_alert_max: Yup.string().required(
				`${string.shipment.maximumTemprature} ${string.errors.required}`
			),
			temperature_alert_interval: Yup.string().required(
				`${string.shipment.alertInterval} ${string.errors.required}`
			),
			temperature_allowed_occurances: Yup.string().required(
				`${string.shipment.temperatureAllowedOccurence} ${string.errors.required}`
			),
			humidity_alert_min: Yup.string().required(
				`${string.shipment.minimumHumidity} ${string.errors.required}`
			),
			humidity_alert_max: Yup.string().required(
				`${string.shipment.maximumHumidityInterval} ${string.errors.required}`
			),
			humidity_alert_interval: Yup.string().required(
				`${string.shipment.humidityInterval} ${string.errors.required}`
			),
			humidity_allowed_occurances: Yup.string().required(
				`${string.shipment.humidityOccurence} ${string.errors.required}`
			),
			ambience_threshold: Yup.string().required(
				`${string.shipment.ambience} ${string.errors.required}`
			),
		}),
		onSubmit: (values, mode, type) => {
			const newShip = {
				...state.shipment,
				...values,
			};
			setState({
				shipment: {
					...newShip,
				},
			});
			submitShipment(newShip);
		},
	});
	if (typeof window === "undefined") {
		return null;
	} else {
		return (
			<div>
				<div>
					<form className="form-container" onSubmit={formik.handleSubmit}>
						<div className="row ml-0 mr-0 content-block">
							{/* Temperature aler and interval fields */}
							<div className="data-block mb-0">
								<div className="form-group row m-0">
									<label
										htmlFor="email_address"
										className="col-md-12 col-form-label pl-0"
									>
										{string.shipment.temperatureAlert}
									</label>
									<div className="col-md-3 pl-0">
                                        <label htmlFor="temperature_alert_min" className="col-md-12 col-form-label pl-0">
                                            Temperature alert (min °C)
                                        </label>
										<Input
											type="text"
											name="temperature_alert_min"
											value={
												formik.values.temperature_alert_min || ""
											}
											className="form-control radius-0"
											placeholder={string.shipment.minRange}
											onChange={formik.handleChange}
										/>
										{formik.errors.temperature_alert_min ? (
											<FormHelperMessage
												className="err"
												message={ formik.errors.temperature_alert_min }
											/>
										) : null}
									</div>
									<div className="col-md-3 pl-0">
                                        <label htmlFor="temperature_alert_max" className="col-md-12 col-form-label pl-0">
                                            Temperature alert (max °C)
                                        </label>
										<Input
											type="text"
											name="temperature_alert_max"
											value={
												formik.values.temperature_alert_max || ""
											}
											className="form-control radius-0"
											placeholder={string.shipment.maxRange}
											onChange={formik.handleChange}
										/>
										{formik.errors.temperature_alert_max ? (
											<FormHelperMessage
												className="err"
												message={ formik.errors.temperature_alert_max }
											/>
										) : null}
									</div>
									<div className="col-md-3 pl-0">
                                        <label htmlFor="temperature_alert_interval" className="col-md-12 col-form-label pl-0">
                                            Interval (minutes)
                                        </label>
										<Input
											type="text"
											name="temperature_alert_interval"
											value={
												formik.values.temperature_alert_interval || ""
											}
											className="form-control radius-0"
											placeholder={string.shipment.intervalMinutes}
											onChange={formik.handleChange}
										/>
										{formik.errors
											.temperature_alert_interval ? (
												<FormHelperMessage
													className="err"
													message={
														formik.errors
															.temperature_alert_interval
													}
												/>
											) : null}
									</div>
									<div className="col-md-3 pl-0">
                                        <label htmlFor="temperature_allowed_occurances" className="col-md-12 col-form-label pl-0">
                                            Allowed occurance
                                        </label>
										<Input
											type="text"
											name="temperature_allowed_occurances"
											value={
												formik.values.temperature_allowed_occurances || ""
											}
											className="form-control radius-0"
											placeholder={string.shipment.allowedOccurance}
											onChange={formik.handleChange}
										/>
										{formik.errors
											.temperature_allowed_occurances ? (
												<FormHelperMessage
													className="err"
													message={
														formik.errors
															.temperature_allowed_occurances
													}
												/>
											) : null}
									</div>
								</div>
							</div>
							{/* //Temperature aler and interval fields */}
							{/* Humidity aler and interval fields */}
							<div className="data-block mb-0">
								<div className="form-group row m-0">
									<label
										htmlFor="email_address"
										className="col-md-12 col-form-label pl-0"
									>
										{string.shipment.humidityAlert}
									</label>
									<div className="col-md-3 pl-0">
                                        <label htmlFor="humidity_alert_min" className="col-md-12 col-form-label pl-0">
                                            Humidity alert (min %)
                                        </label>
										<Input
											type="text"
											name="humidity_alert_min"
											value={
												formik.values.humidity_alert_min || ""
											}
											className="form-control radius-0"
											placeholder={string.shipment.minRange}
											onChange={formik.handleChange}
										/>
										{formik.errors.humidity_alert_min ? (
											<FormHelperMessage
												className="err"
												message={ formik.errors.humidity_alert_min }
											/>
										) : null}
									</div>
									<div className="col-md-3 pl-0">
                                        <label htmlFor="humidity_alert_max" className="col-md-12 col-form-label pl-0">
                                            Humidity alert (max %)
                                        </label>
										<Input
											type="text"
											name="humidity_alert_max"
											value={
												formik.values.humidity_alert_max || ""
											}
											className="form-control radius-0"
											placeholder={string.shipment.maxRange}
											onChange={formik.handleChange}
										/>
										{formik.errors.humidity_alert_max ? (
											<FormHelperMessage
												className="err"
												message={ formik.errors.humidity_alert_max }
											/>
										) : null}
									</div>
									<div className="col-md-3 pl-0">
                                        <label htmlFor="humidity_alert_interval" className="col-md-12 col-form-label pl-0">
                                            Interval (minutes)
                                        </label>
										<Input
											type="text"
											name="humidity_alert_interval"
											value={
												formik.values.humidity_alert_interval || ""
											}
											className="form-control radius-0"
											placeholder={string.shipment.intervalMinutes}
											onChange={formik.handleChange}
										/>
										{formik.errors
											.humidity_alert_interval ? (
												<FormHelperMessage
													className="err"
													message={
														formik.errors
															.humidity_alert_interval
													}
												/>
											) : null}
									</div>
									<div className="col-md-3 pl-0">
                                        <label htmlFor="humidity_allowed_occurances" className="col-md-12 col-form-label pl-0">
                                            Allowed occurance
                                        </label>
										<Input
											type="text"
											name="humidity_allowed_occurances"
											value={
												formik.values.humidity_allowed_occurances || ""
											}
											className="form-control radius-0"
											placeholder={string.shipment.allowedOccurance}
											onChange={formik.handleChange}
										/>
										{formik.errors
											.humidity_allowed_occurances ? (
												<FormHelperMessage
													className="err"
													message={
														formik.errors
															.humidity_allowed_occurances
													}
												/>
											) : null}
									</div>
								</div>
							</div>
							{/* //section 1 */}
							{/* section 1 */}
							<div className="data-block mb-0">
								<div className="form-group row m-0">
									<label
										htmlFor="email_address"
										className="col-md-12 col-form-label pl-0"
									>
										{string.shipment.sealingAlert}
									</label>
									<div className="col-md-12 pl-0">
										<Input
											type="text"
											name="ambience_threshold"
											value={
												formik.values.ambience_threshold || ""
											}
											className="form-control radius-0"
											placeholder={string.shipment.brakingPoints}
											onChange={formik.handleChange}
										/>
										{formik.errors.ambience_threshold ? (
											<FormHelperMessage
												className="err"
												message={ formik.errors.ambience_threshold }
											/>
										) : null}
									</div>
								</div>
							</div>
							{/* //section 1 */}
						</div>

						<div className="shipment-table-listing table-responsive">
							<table className="table">
								<thead className="thead-dark">
									<tr>
										<th scope="col"></th>
										<th scope="col" width="200">
											{string.shipment.stationName}
										</th>
										<th scope="col" width="150">
											{string.shipment.latitude}
										</th>
										<th scope="col" width="150">
											{string.shipment.longitude}
										</th>
										<th scope="col" width="150">
											{string.shipment.radius}
										</th>
										<th scope="col" width="70"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colSpan="6">
											{state.shipment.selectedRoads
												?.length > 0 && (
													<DraggableItem
														className="table__drag"
														listItems={
															state.shipment
																.selectedRoads
														}
														roads={state.roads}
														shipment={state.shipment}
														setState={setState}
													/>
												)}
										</td>
									</tr>
								</tbody>
							</table>
							<div
								className="add-btn"
								style={{ margin: "auto" }}
								onClick={(event) => {
									event.preventDefault();
									let selectedRoads = state.shipment.selectedRoads;
									let shipment = state.shipment;
									selectedRoads.push({});
									shipment.selectedRoads = selectedRoads;
									setState({ shipment });
								}}
							>
								<Button className="btn">
									<i className="fas fa-plus fa-sm"></i>
								</Button>
							</div>
						</div>
						<div className="modal-footer">
							<Button className="btn btn-secondary large-btn" type="button" onClick={backStep}>
								Back
							</Button>
							{mode == "add" && (
								<Button
									className="btn btn-secondary large-btn"
									type="button"
									onClick={() => saveAsDraft(formik)}
								>
									{string.shipment.saveAsDraft}
								</Button>
							)}
							<Button
								className="btn btn-primary large-btn"
								type="submit"
							>
								{string.shipment.submit}
							</Button>
						</div>
					</form>
				</div>
			</div>
		);
	}
};

AddShipmentStepThree.propTypes = {};
AddShipmentStepThree.defaultProps = {};

export default AddShipmentStepThree;

import { Modal, ModalHeader, ModalBody } from "reactstrap";
import Button from "../../components/common/form-elements/button/Button";

const ToggleModal = ({ isActive, onToggleStatus, toggleStatus, isOpen }) => {
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <Modal isOpen={isOpen} toggle={toggleStatus} className="customModal">
        <ModalHeader toggle={toggleStatus}></ModalHeader>
        <ModalBody className="text-center mb-5">
          <p>
            <strong>{isActive ? "STOP SHIPMENT?" : "START SHIPMENT?"}</strong>
          </p>
          <Button
            className="btn btn-primary large-btn"
            type="button"
            data-dismiss="modal"
            onClick={onToggleStatus}
          >
            {isActive ? "STOP" : "START"}
          </Button>
        </ModalBody>
      </Modal>
    );
  }
};

ToggleModal.propTypes = {};

ToggleModal.defaultProps = {};

export default ToggleModal;

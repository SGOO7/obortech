import App from 'next/app';
import Head from 'next/head'
import React, { useEffect } from 'react';
import Router from 'next/router';
import NProgress from 'nprogress';
import { Provider } from "react-redux";
import Notifier from '../components/Notifier';
import { useStore } from '../redux/store';
import '../static/css/card.css';
import '../static/css/table.css';
import '..//static/css/modal.css';

Router.onRouteChangeStart = (url) => {
    if (typeof window != 'undefined' && (window.location.pathname == url)) {
        window.location.reload();
    } else {
        NProgress.start();
    }
}
const MyApp = ({ Component, pageProps }) => {
    const store = useStore(pageProps.initialReduxState)

    useEffect(() => {
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles && jssStyles.parentNode) {
            jssStyles.parentNode.removeChild(jssStyles);
        }
    }, [])

    return (
        <Provider store={store}>
            <div>
                <Head>
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                </Head>
                <Component {...pageProps} />
                <Notifier />
            </div>
        </Provider>
    );
}

MyApp.getInitialProps = async ({ Component, ctx }) => {
    const pageProps = {};

    if (Component.getInitialProps) {
        Object.assign(pageProps, await Component.getInitialProps(ctx));
    }

    return { pageProps };
}

export default MyApp;

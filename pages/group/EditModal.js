import { useFormik } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";
function EditModal({ group, state, updateGroup }) {
  // Formik Validations and initialization
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      groupID: group.groupID,
    },
    validationSchema: Yup.object({
      groupID: Yup.string()
        .trim()
        .required(`GroupID ${string.errors.required}`),
    }),
    onSubmit: (values) => {
      state({
        group: Object.assign({}, group, { groupID: values.groupID }),
      });
      updateGroup();
      $("#editGroupModal").modal("hide");
    },
  });

  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              EDIT GROUP
            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </Button>
          </div>
          <div className="modal-body">
            <form className="form-container" onSubmit={formik.handleSubmit}>
              <div className="row ml-0 mr-0 content-block">
                <div className="form-group col-md-12 p-0">
                  <label
                    htmlFor="groupID"
                    className="col-md-12 col-form-label pl-0"
                  >
                    GroupID
                  </label>
                  <Input
                    type="text"
                    name="groupID"
                    id="groupID"
                    // value={group.groupID || ""}
                    value={formik.values.groupID}
                    onChange={formik.handleChange}
                    className="form-control"
                    placeholder="GroupID"
                    // onChange={(event) => {
                    //     state({
                    //         group: Object.assign({}, group, { groupID: event.target.value })
                    //     });
                    // }}
                  />
                  {formik.errors.groupID && formik.touched.groupID ? (
                    <FormHelperMessage
                      message={formik.errors.groupID}
                      className="error"
                    />
                  ) : null}
                </div>
              </div>
              <div className="modal-footer">
                <Button
                  className="btn btn-primary large-btn"
                  // onClick={updateGroup}
                  type="submit"
                  // data-dismiss="modal"
                >
                  UPDATE
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

EditModal.propTypes = {};

EditModal.defaultProps = {};

export default EditModal;

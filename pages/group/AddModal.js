import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";

const AddGroupschema = Yup.object().shape({
    groupID: Yup.string()
        .trim()
        .required(`GroupID ${string.errors.required}`),
});

function AddModal({ state, onGroupSubmit }) {
    if (typeof window === "undefined") {
        return null;
    } else {
        return (
            <div className="modal-dialog modal-md" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5
                            className="modal-title text-dark font-weight-bold"
                            id="exampleModalLabel"
                        >
                            ADD GROUP
            </h5>
                        <Button
                            className="close"
                            type="button"
                            data-dismiss="modal"
                            aria-label="Close"
                        >
                            <span aria-hidden="true">×</span>
                        </Button>
                    </div>
                    <div className="modal-body">
                        <Formik
                            initialValues={{
                                groupID: "",
                            }}
                            validationSchema={AddGroupschema}
                            onSubmit={(values) => {
                                state({
                                    group: Object.assign({}, group, { groupID: values.groupID }),
                                });
                                onGroupSubmit();
                                $("#groupModal").modal("hide");
                                values.groupID = "";
                            }}>
                            {({ errors, touched, handleChange, handleSubmit, values }) => (
                                <form className="form-container" onSubmit={handleSubmit}>
                                    <div className="row ml-0 mr-0 content-block">
                                        <div className="form-group col-md-12 p-0">
                                            <label
                                                htmlFor="groupID"
                                                className="col-md-12 col-form-label pl-0"
                                            >
                                                GroupID
                      </label>
                                            <Input
                                                type="text"
                                                name="groupID"
                                                id="groupID"
                                                className="form-control"
                                                placeholder="GroupID"
                                                value={values.groupID}
                                                onChange={handleChange}
                                            // onBlur={handleBlur}
                                            // onChange={(event) => {
                                            //     state({
                                            //         group: Object.assign({}, group, { groupID: event.target.value })
                                            //     });
                                            // }}
                                            />
                                            {errors.groupID && touched.groupID ? (
                                                <FormHelperMessage
                                                    message={errors.groupID}
                                                    className="error"
                                                />
                                            ) : null}
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <Button className="btn btn-primary large-btn" type="submit">
                                            INSERT
                    </Button>
                                    </div>
                                </form>
                            )}
                        </Formik>
                    </div>
                </div>
            </div>
        );
    }
}

AddModal.propTypes = {};

AddModal.defaultProps = {};

export default AddModal;

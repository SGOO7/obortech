import PropTypes from "prop-types";
import Head from "next/head";
import Header from "../../components/HomeHeader";
import Footer from "../../components/HomeFooter";
import Sidebar from "../../components/Sidebar";
import DeleteModal from "./DeleteModal";
import AddModal from "./AddModal";
import EditModal from "./EditModal";
import NProgress from "nprogress";
import notify from "../../lib/notifier";

import withAuth from "../../lib/withAuth";
import {
  fetchGroups,
  addGroup,
  removeGroup,
  updateGroup,
} from "../../lib/api/group";
// updated
import Button from "../../components/common/form-elements/button/Button";
class GroupPage extends React.Component {
  static getInitialProps() {
    const groupPage = true;
    return { groupPage };
  }

  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
  };

  async componentDidMount() {
    NProgress.start();
    try {
      const groups = await fetchGroups();
      this.setState({ groups });
      NProgress.done();
    } catch (err) {
      this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
      NProgress.done();
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      groups: [],
      group: {},
      deleteMode: "",
      selectedIndex: "",
      editMode: "",
    };
  }

  _resetInput = () => {
    this.setState({ group: {} });
  };

  // submit group function to check submitted details
  onGroupSubmit = (event) => {
    // event.preventDefault();
    const { group } = this.state;
    const { groupID } = group;

    // if (!groupID) {
    // 	notify('GroupID is required');
    // 	return;
    // }

    this.addGroupData({ groupID: groupID });
  };

  // add group function
  addGroupData = async (data) => {
    NProgress.start();
    try {
      const group = await addGroup(data);
      let { groups } = this.state;
      groups.push(group);
      this.setState({ groups });
      notify("Group added successfully!");
      NProgress.done();
    } catch (err) {
      console.error(err);
      notify("Error adding group!");
      NProgress.done();
    }
  };

  // Function to delete entry from popup
  onDeleteEntry = async (event) => {
    event.preventDefault();
    let { deleteMode, groups, selectedIndex } = this.state;
    if (deleteMode == "group") {
      // delete group data
      let groups_data = groups[selectedIndex];
      await removeGroup({ id: groups_data.id });
      groups.splice(selectedIndex, 1);
      this.setState({ groups });
      notify("Group deleted successfully!");
    }
  };

  // update group function
  updateGroup = async () => {
    NProgress.start();
    let { group, selectedIndex } = this.state;
    try {
      await updateGroup(group);
      let { groups } = this.state;
      groups[selectedIndex] = group;
      this.setState({ groups, group: {} });
      notify("Group updated successfully!");
      NProgress.done();
    } catch (err) {
      console.error(err);
      notify("Error adding Group!");
      NProgress.done();
    }
  };

  // set delete mode upon selecting delete icon
  setDeleteMode = (mode, i) => {
    if (mode) {
      this.setState({ deleteMode: mode });
      this.setState({ selectedIndex: i });
      $("#deleteModal").modal("show");
    }
  };

  setEditMode = (mode, i) => {
    if (mode) {
      this.setState({ editMode: mode });
      this.setState({ selectedIndex: i });
      if (mode == "group") {
        let { groups } = this.state;
        let group = groups[i];
        this.setState({ group });
        $("#editGroupModal").modal("show");
      }
    }
  };

  render() {
    const { user, groups, group } = this.state;
    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - Manage Groups</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <div
                    className="tab-pane fade show active mt-3 w-100"
                    id="group"
                    role="tabpanel"
                    aria-labelledby="group-listing"
                  >
                    <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
                      <h4 className="text-dark">Groups Listing</h4>
                      <Button
                        onClick={this._resetInput}
                        className="btn btn-primary large-btn"
                        data-toggle="modal"
                        data-target="#groupModal"
                      >
                        SUBMIT GROUP
                      </Button>
                    </div>
                    <div className="shipment-table-listing table-responsive mt-2 w-100">
                      <table className="table">
                        <thead className="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">GroupID</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {groups.map((group, i) => {
                            return (
                              <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{group.groupID}</td>
                                <td>
                                  <i
                                    className="fa fa-pencil-alt"
                                    onClick={() => this.setEditMode("group", i)}
                                  ></i>
                                  <i
                                    className="fa fa-trash"
                                    onClick={() =>
                                      this.setDeleteMode("group", i)
                                    }
                                  ></i>
                                </td>
                              </tr>
                            );
                          })}
                          {groups.length == 0 && (
                            <tr>
                              <td colSpan="5" className="text-center">
                                No data available!!
                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <div
          className="modal fade customModal document"
          id="deleteModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <DeleteModal onDeleteEntry={this.onDeleteEntry} />
        </div>
        <div
          className="modal fade customModal document"
          id="groupModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <AddModal
            group={group}
            state={this.setState.bind(this)}
            onGroupSubmit={this.onGroupSubmit.bind(this)}
          />
        </div>
        <div
          className="modal fade customModal document"
          id="editGroupModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <EditModal
            group={group}
            state={this.setState.bind(this)}
            updateGroup={this.updateGroup.bind(this)}
          />
        </div>
        <Footer />
      </div>
    );
  }
}

export default withAuth(GroupPage, { loginRequired: true });

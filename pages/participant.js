import PropTypes from "prop-types";
import Head from "next/head";
import Header from "../components/HomeHeader";
import Footer from "../components/HomeFooter";
import Sidebar from "../components/Sidebar";
import NProgress from "nprogress";
import notify from "../lib/notifier";

import withAuth from "../lib/withAuth";
import {
  fetchParticipantCategories,
  addParticipantCategory,
  removeParticipantCategory,
  updateParticipantCategory,
} from "../lib/api/participant-category";
import { fetchUsers, addUser, removeUser, updateUser } from "../lib/api/user";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../components/common/form-elements/formHelperMessage";
import string from "../utils/stringConstants/language/eng.json";
import Button from "../components/common/form-elements/button/Button";
import CustomSelect from "../components/common/form-elements/select/CustomSelect";
import Input from "../components/common/form-elements/input/Input";

class ParticipantPage extends React.Component {
  static getInitialProps() {
    const participantPage = true;
    return { participantPage };
  }

  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
    participant_category: {},
  };

  async componentDidMount() {
    NProgress.start();
    try {
      const participant_categories = await fetchParticipantCategories();
      const participants = await fetchUsers();
      this.setState({ participant_categories, participants });
      NProgress.done();
    } catch (err) {
      this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
      NProgress.done();
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      participant_categories: [],
      participant_category: {},
      participants: [],
      participant: {},
      deleteMode: "",
      selectedIndex: "",
      editMode: "",
      openParticipant: false,
      deleteOpen: false,
      openCategory: false,
    };
  }

  // submit participant function to check submitted details
  onParticipantSubmit = (event) => {
    // event.preventDefault();
    const { participant } = this.state;
    const {
      username,
      official_name,
      password,
      repeat_password,
      participant_category_id,
    } = participant;

    // if (!participant_category_id) {
    // 	notify('Please select Category');
    // 	return;
    // }
    // if (!username) {
    // 	notify('Username is required');
    // 	return;
    // }
    // if (!email) {
    // 	notify('Email is required');
    // 	return;
    // }
    // if (!password) {
    // 	notify('Password is required');
    // 	return;
    // }
    // if (!repeat_password) {
    // 	notify('Repeat Password is required');
    // 	return;
    // } else{
    // 	if(password != repeat_password){
    // 		notify('Password does not match Repeat Password');
    // 		return;
    // 	}
    // }
    // if (!official_name) {
    // 	notify('Official Name is required');
    // 	return;
    // }

    this.addParticipantData(participant);
  };

  // add participant function
  addParticipantData = async (data) => {
    NProgress.start();
    try {
      const participant = await addUser(data);
      const participants = await fetchUsers();
      this.setState({ participants, participant: {} });
      notify("Participant added successfully!");
      this.togglePrticipantModal();
    } catch (err) {
      console.error(err);
      notify("Error adding participant!");
      NProgress.done();
    }
  };

  // submit category function to check submitted details
  onCategorySubmit = (event) => {
    // event.preventDefault();
    const { participant_category } = this.state;
    const { name } = participant_category;

    // if (!name) {
    // 	notify('Category Name is required');
    // 	return;
    // }

    this.addCategory(participant_category);
  };

  // Function to delete entry from popup
  onDeleteEntry = async (event) => {
    event.preventDefault();
    let {
      deleteMode,
      participant_categories,
      participants,
      selectedIndex,
    } = this.state;
    // check which category to delete
    if (deleteMode == "participant") {
      // delete participant data
      let participants_data = participants[selectedIndex];
      await removeUser({ id: participants_data.id });
      participants.splice(selectedIndex, 1);
      this.setState({ participants });
      this.toggleDelete();
      notify("Participant deleted successfully!");
    } else if (deleteMode == "participant_category") {
      // delete participant category data
      let category = participant_categories[selectedIndex];
      await removeParticipantCategory({ id: category.id });
      participant_categories.splice(selectedIndex, 1);
      this.setState({ participant_categories });
      this.toggleDelete();
      notify("Category deleted successfully!");
    }
  };

  // add participant category function
  addCategory = async (data) => {
    NProgress.start();
    try {
      const category = await addParticipantCategory(data);
      let { participant_categories } = this.state;
      participant_categories.push(category);
      this.setState({ participant_categories, participant_category: {} });
      notify("Category added successfully!");
      this.toggleCategory();
    } catch (err) {
      console.error(err);
      notify("Error adding category!");
      NProgress.done();
    }
  };

  // update participant category function
  updateCategory = async () => {
    NProgress.start();
    let { participant_category, selectedIndex } = this.state;
    try {
      await updateParticipantCategory(participant_category);
      let { participant_categories } = this.state;
      participant_categories[selectedIndex] = participant_category;
      this.setState({ participant_categories, participant_category: {} });
      notify("Category updated successfully!");
      this.toggleCategory();
    } catch (err) {
      console.error(err);
      notify("Error adding category!");
      NProgress.done();
    }
  };

  // update participant function
  updateUser = async () => {
    NProgress.start();
    let { participant, selectedIndex } = this.state;
    try {
      await updateUser(participant);
      const participants = await fetchUsers();
      this.setState({ participants, participant: {} });
      notify("Participant updated successfully!");
      this.togglePrticipantModal();
    } catch (err) {
      console.error(err);
      notify("Error adding category!");
      NProgress.done();
    }
  };

  // set delete mode upon selecting delete icon
  setDeleteMode = (mode, i) => {
    if (mode) {
      this.setState({ deleteMode: mode });
      this.setState({ selectedIndex: i });
      this.toggleDelete();
      // $("#deleteModal").modal('show');
    }
  };

  // set add mode upon click on SUBMIT PARTICIPANT button
  setAddMode = (mode, i) => {
    if (mode) {
      this.setState({ editMode: "" });
      this.togglePrticipantModal();
    }
  };

  setEditMode = (mode, i) => {
    if (mode) {
      this.setState({ editMode: mode });
      this.setState({ selectedIndex: i });
      if (mode == "participant") {
        let { participants } = this.state;
        let participant = participants[i];
        this.setState({ participant });
        this.togglePrticipantModal();
        // $("#editParticipantModal").modal('show');
      } else if (mode == "participant_category") {
        let { participant_categories } = this.state;
        let participant_category = participant_categories[i];
        this.setState({ participant_category });
        this.toggleCategory();
        // $("#editParticipantCategoryModal").modal('show');
      }
    }
  };

  togglePrticipantModal = () => {
    this.setState({
      openParticipant: !this.state.openParticipant,
    });
  };

  toggleDelete = () => {
    this.setState({
      deleteOpen: !this.state.deleteOpen,
    });
  };

  toggleCategory = () => {
    this.setState({
      openCategory: !this.state.openCategory,
    });
  };

  render() {
    const {
      user,
      participant_categories,
      participant_category,
      participants,
      participant,
      deleteOpen,
      openParticipant,
      editMode,
      openCategory,
    } = this.state;

    const AddParticipantschema = Yup.object().shape({
      participant_category_id: Yup.string().required(`Please select Category`),
      username: Yup.string()
        .trim()
        .required(`Username ${string.errors.required}`),
      email: Yup.string()
        .trim()
        .email(`Email ${string.errors.email}`)
        .required(`Email ${string.errors.required}`),
      password:
        editMode === "participant"
          ? Yup.string()
          : Yup.string()
              .trim()
              .required(`Password ${string.errors.required}`),
      repeat_password:
        editMode === "participant"
          ? Yup.string().when("password", {
              is: (val) => (val && val.length > 0 ? true : false),
              then: Yup.string().oneOf(
                [Yup.ref("password")],
                "Both password need to be the same"
              ),
            })
          : Yup.string()
              .when("password", {
                is: (val) => (val && val.length > 0 ? true : false),
                then: Yup.string().oneOf(
                  [Yup.ref("password")],
                  "Both password need to be the same"
                ),
              })
              .required(`Repeat Password ${string.errors.required}`),
      official_name: Yup.string()
        .trim()
        .required(`Official Name ${string.errors.required}`),
    });

    const AddParticipantCategoryschema = Yup.object().shape({
      name: Yup.string()
        .trim()
        .required(`Category Name ${string.errors.required}`),
    });

    return (
      <div id="page-top">
		  
        <Head>
          <title>{process.env.APP_NAME} - Manage Participants</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <ul className="nav nav-tabs w-100" id="myTab" role="tablist">
                    <li className="nav-item">
                      <a
                        className="nav-link active"
                        id="participants"
                        data-toggle="tab"
                        href="#participant"
                        role="tab"
                        aria-controls="participants"
                        aria-selected="true"
                      >
                        All Participants
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        className="nav-link"
                        id="participant_categories"
                        data-toggle="tab"
                        href="#participant_category"
                        role="tab"
                        aria-controls="participant_category"
                      >
                        Participant Categories
                      </a>
                    </li>
                  </ul>
			
                  <div className="tab-content w-100" id="myTabContent">
                    <div
                      className="tab-pane fade show active mt-3 w-100"
                      id="participant"
                      role="tabpanel"
                      aria-labelledby="participant-listing"
                    >
                      <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
                        <h4 className="text-dark">Participants Listing</h4>
                        <Button
                          className="btn btn-primary large-btn"
                          onClick={this.setAddMode}
                        >
                          SUBMIT PARTICIPANT
                        </Button>
                      </div>
                      <div className="shipment-table-listing table-responsive mt-2 w-100">
                        <table className="table">
                          <thead className="thead-dark">
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Username</th>
                              <th scope="col">Official Name</th>
                              <th scope="col">Category Name</th>
                              <th scope="col"></th>
                            </tr>
                          </thead>
                          <tbody>
                            {participants.map((participant, i) => {
                              return (
                                <tr key={i}>
                                  <td>{i + 1}</td>
                                  <td>{participant.username}</td>
                                  <td>{participant.official_name}</td>
                                  <td>
                                    {participant.participant_category.name}
                                  </td>
                                  <td>
                                    <i
                                      className="fa fa-pencil-alt"
                                      onClick={() =>
                                        this.setEditMode("participant", i)
                                      }
                                    ></i>
                                    <i
                                      className="fa fa-trash"
                                      onClick={() =>
                                        this.setDeleteMode("participant", i)
                                      }
                                    ></i>
                                  </td>
                                </tr>
                              );
                            })}
                            {participants.length == 0 && (
                              <tr>
                                <td colSpan="5" className="text-center">
                                  No data available!!
                                </td>
                              </tr>
                            )}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div
                      className="tab-pane fade mt-3 w-100"
                      id="participant_category"
                      role="tabpanel"
                      aria-labelledby="participant-category-listing"
                    >
                      <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
                        <h4 className="text-dark">Category Listing</h4>
                        <Button
                          className="btn btn-primary large-btn"
                          onClick={() => this.toggleCategory()}
                        >
                          SUBMIT CATEGORY
                        </Button>
                      </div>
                      <div className="shipment-table-listing table-responsive mt-2 w-100">
                        <table className="table">
                          <thead className="thead-dark">
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Category Name</th>
                              <th scope="col"></th>
                            </tr>
                          </thead>
                          <tbody>
                            {participant_categories.map((category, i) => {
                              return (
                                <tr key={i}>
                                  <td>{i + 1}</td>
                                  <td>{category.name}</td>
                                  <td>
                                    <i
                                      className="fa fa-pencil-alt"
                                      onClick={() =>
                                        this.setEditMode(
                                          "participant_category",
                                          i
                                        )
                                      }
                                    ></i>
                                    <i
                                      className="fa fa-trash"
                                      onClick={() =>
                                        this.setDeleteMode(
                                          "participant_category",
                                          i
                                        )
                                      }
                                    ></i>
                                  </td>
                                </tr>
                              );
                            })}
                            {participant_categories.length == 0 && (
                              <tr>
                                <td colSpan="3" className="text-center">
                                  No data available!!
                                </td>
                              </tr>
                            )}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <Modal
          toggle={this.toggleDelete}
          isOpen={deleteOpen}
          className="customModal"
        >
          <ModalHeader toggle={this.toggleDelete}></ModalHeader>
          <ModalBody className="text-center mb-5">
            <p>
              <strong>DELETE RECORD ?</strong>
            </p>
            <Button
              className="btn btn-primary large-btn"
              type="button"
              data-dismiss="modal"
              onClick={this.onDeleteEntry}
            >
              DELETE
            </Button>
          </ModalBody>
        </Modal>
        <Modal
          className="customModal document"
          isOpen={openParticipant}
          toggle={this.togglePrticipantModal}
        >
          <ModalHeader toggle={this.togglePrticipantModal}>
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              {editMode === "participant" ? "EDIT" : "ADD"} PARTICIPANT
            </h5>
          </ModalHeader>
          <ModalBody>
            <Formik
              initialValues={{
                participant_category_id: editMode
                  ? participant.participant_category_id
                  : "",
                username: editMode ? participant.username : "",
                email: editMode ? participant.email : "",
                password: "",
                repeat_password: "",
                official_name: editMode ? participant.official_name : "",
              }}
              validationSchema={AddParticipantschema}
              onSubmit={(values) => {
                this.setState({
                  participant: Object.assign({}, participant, {
                    participant_category_id: values.participant_category_id,
                    username: values.username,
                    email: values.email,
                    password: values.password,
                    repeat_password: values.repeat_password,
                    official_name: values.official_name,
                  }),
                });

                if (editMode === "participant") {
                  this.updateUser();
                } else {
                  this.onParticipantSubmit();
                  values.participant_category_id = "";
                  values.username = "";
                  values.email = "";
                  values.password = "";
                  values.repeat_password = "";
                  values.official_name = "";
                }
              }}
            >
              {({ errors, touched, handleChange, handleSubmit, values }) => {
                return (
                  <form className="form-container" onSubmit={handleSubmit}>
                    <div className="row ml-0 mr-0 content-block">
                      <div className="form-group col-md-12 p-0">
                        <label className="col-md-12 col-form-label pl-0">
                          Select Category
                        </label>
                        <CustomSelect
                          className="form-control"
                          name="participant_category_id"
                          // value={participant.participant_category_id || "0"}
                          value={values.participant_category_id}
                          onChange={handleChange}
                          // onChange={(event) => {
                          // 	this.setState({
                          // 		participant: Object.assign({}, participant, { participant_category_id: event.target.value })
                          // 	});
                          // }}
                          options={participant_categories}
                          defaultOptionText="Select Category"
                        />
                        {/* <option value="">Select Category</option>
                          {participant_categories.map((category, i) => {
                            return (
                              <option value={category.id} key={i}>
                                {category.name}
                              </option>
                            );
                          })}
                        </select> */}
                        {errors.participant_category_id &&
                        touched.participant_category_id ? (
                          <FormHelperMessage
                            message={errors.participant_category_id}
                            className="error"
                          />
                        ) : null}
                      </div>
                      <div className="form-group col-md-12 p-0">
                        <label
                          htmlFor="username"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Username
                        </label>
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          className="form-control"
                          // value={participant.username || ''}
                          value={values.username}
                          onChange={handleChange}
                          placeholder="Username"
                          // onChange={(event) => {
                          // 	this.setState({
                          // 		participant: Object.assign({}, participant, { username: event.target.value })
                          // 	});
                          // }}
                        />
                        {errors.username && touched.username ? (
                          <FormHelperMessage
                            message={errors.username}
                            className="error"
                          />
                        ) : null}
                      </div>
                      <div className="form-group col-md-12 p-0">
                        <label
                          htmlFor="email"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Email
                        </label>
                        <Input
                          type="text"
                          name="email"
                          id="email"
                          className="form-control"
                          // value={participant.email || ''}
                          value={values.email}
                          onChange={handleChange}
                          placeholder="Email"
                          // onChange={(event) => {
                          // 	this.setState({
                          // 		participant: Object.assign({}, participant, { email: event.target.value })
                          // 	});
                          // }}
                        />
                        {errors.email && touched.email ? (
                          <FormHelperMessage
                            message={errors.email}
                            className="error"
                          />
                        ) : null}
                      </div>
                      <div className="form-group col-md-12 p-0">
                        <label
                          htmlFor="password"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Password
                        </label>
                        <Input
                          type="text"
                          name="password"
                          id="password"
                          className="form-control"
                          placeholder="Password"
                          value={values.password}
                          onChange={handleChange}
                          // onChange={(event) => {
                          // 	this.setState({
                          // 		participant: Object.assign({}, participant, { password: event.target.value })
                          // 	});
                          // }}
                        />
                        {errors.password && touched.password ? (
                          <FormHelperMessage
                            message={errors.password}
                            className="error"
                          />
                        ) : null}
                      </div>
                      <div className="form-group col-md-12 p-0">
                        <label
                          htmlFor="repeat_password"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Repeat Password
                        </label>
                        <Input
                          type="text"
                          name="repeat_password"
                          id="repeat_password"
                          className="form-control"
                          placeholder="Repeat Password"
                          value={values.repeat_password}
                          onChange={handleChange}
                          // onChange={(event) => {
                          // 	this.setState({
                          // 		participant: Object.assign({}, participant, { repeat_password: event.target.value })
                          // 	});
                          // }}
                        />
                        {errors.repeat_password && touched.repeat_password ? (
                          <FormHelperMessage
                            message={errors.repeat_password}
                            className="error"
                          />
                        ) : null}
                      </div>
                      <div className="form-group col-md-12 p-0">
                        <label
                          htmlFor="official_name"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Official Name
                        </label>
                        <Input
                          type="text"
                          name="official_name"
                          id="official_name"
                          value={participant.official_name || ""}
                          className="form-control"
                          placeholder="Name"
                          value={values.official_name}
                          onChange={handleChange}
                          // onChange={(event) => {
                          // 	this.setState({
                          // 		participant: Object.assign({}, participant, { official_name: event.target.value })
                          // 	});
                          // }}
                        />
                        {errors.official_name && touched.official_name ? (
                          <FormHelperMessage
                            message={errors.official_name}
                            className="error"
                          />
                        ) : null}
                      </div>
                    </div>
                    <ModalFooter>
                      <Button
                        className="btn btn-primary large-btn"
                        // onClick={editMode === 'participant' ? this.updateUser : this.onParticipantSubmit}
                        type="submit"
                        // data-dismiss="modal"
                      >
                        {editMode === "participant" ? "UPDATE" : "INSERT"}
                      </Button>
                    </ModalFooter>
                  </form>
                );
              }}
            </Formik>
          </ModalBody>
        </Modal>
        <Modal
          className="customModal document"
          isOpen={openCategory}
          toggle={this.toggleCategory}
        >
          <ModalHeader toggle={this.toggleCategory}>
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              {editMode === "participant_category" ? "EDIT" : "ADD"} CATEGORY
            </h5>
          </ModalHeader>
          <ModalBody>
            <Formik
              initialValues={{
                name: participant_category.name || "",
              }}
              validationSchema={AddParticipantCategoryschema}
              onSubmit={(values) => {
                this.setState({
                  participant_category: Object.assign(
                    {},
                    participant_category,
                    { name: values.name }
                  ),
                });
                if (editMode === "participant_category") {
                  this.updateCategory();
                } else {
                  this.onCategorySubmit();
                  values.name = "";
                }
                $(".customModal").modal("hide");
              }}
            >
              {({ errors, touched, handleChange, handleSubmit, values }) => (
                <form onSubmit={handleSubmit}>
                  <div>
                    <div className="row ml-0 mr-0 content-block">
                      <div className="form-group col-md-12 p-0">
                        <label
                          htmlFor="name"
                          className="col-md-12 col-form-label pl-0"
                        >
                          Category Name
                        </label>
                        <Input
                          type="text"
                          name="name"
                          id="name"
                          className="form-control"
                          placeholder="Category Name"
                          // value={participant_category.name || ''}
                          value={values.name}
                          onChange={handleChange}
                          // onChange={(event) => {
                          // 	this.setState({
                          // 		participant_category: Object.assign({}, participant_category, { name: event.target.value }),
                          // 	});
                          // }}
                        />
                        {errors.name && touched.name ? (
                          <FormHelperMessage
                            message={errors.name}
                            className="error"
                          />
                        ) : null}
                      </div>
                    </div>
                  </div>
                  <ModalFooter>
                    <Button
                      data-dismiss="modal"
                      // onClick={editMode === 'participant_category' ? this.updateCategory : this.onCategorySubmit}
                      className="btn btn-primary large-btn"
                      type="submit"
                    >
                      {editMode === "participant_category"
                        ? "UPDATE"
                        : "INSERT"}
                    </Button>
                  </ModalFooter>
                </form>
              )}
            </Formik>
          </ModalBody>
        </Modal>
        <Footer />
      </div>
    );
  }
}

export default withAuth(ParticipantPage, { loginRequired: true });

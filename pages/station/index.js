import PropTypes from "prop-types";
import Head from "next/head";
import Header from "../../components/HomeHeader";
import Footer from "../../components/HomeFooter";
import Sidebar from "../../components/Sidebar";
import DeleteModal from "./DeleteModal";
import AddModal from "./AddModal";
import EditModal from "./EditModal";
import NProgress from "nprogress";
import notify from "../../lib/notifier";
import string from "../../utils/stringConstants/language/eng.json";
import withAuth from "../../lib/withAuth";
import {
  fetchRoads,
  addRoad,
  removeRoad,
  updateRoad,
} from "../../lib/api/road";
// updated
import Button from "../../components/common/form-elements/button/Button";
class RoadPage extends React.Component {
  static getInitialProps() {
    const roadPage = true;
    return { roadPage };
  }

  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
  };

  async componentDidMount() {
    NProgress.start();
    try {
      const roads = await fetchRoads();
      this.setState({ roads });
      NProgress.done();
    } catch (err) {
      this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
      NProgress.done();
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      roads: [],
      road: {},
      deleteMode: "",
      selectedIndex: "",
      editMode: "",
    };
  }

  // submit road function to check submitted details
  onRoadSubmit = (values) => {
    // event.preventDefault();
    const { road } = this.state;
    const { name, latitude, longitude, radius } = road;


  };

  // add road function
  addRoadData = async (data) => {
    NProgress.start();
    try {
      const road = await addRoad(data);
      let { roads } = this.state;
      roads.push(road);
      this.setState({ roads, road: {} });
      notify(`${string.station.roadAddedSucessfully}`);
    } catch (err) {
      console.log(err);
      notify(`${string.station.errorAddingRoad}`);
      NProgress.done();
    }
  };

  // Function to delete entry from popup
  onDeleteEntry = async (event) => {
    event.preventDefault();
    let { deleteMode, roads, selectedIndex } = this.state;
    if (deleteMode == "road") {
      // delete road data
      let roads_data = roads[selectedIndex];
      await removeRoad({ id: roads_data.id });
      roads.splice(selectedIndex, 1);
      this.setState({ roads });
      notify(`${string.station.roadDelatedSucessfully}`);
    }
  };

  // update road function
  updateRoad = async () => {
    NProgress.start();
    let { road, selectedIndex } = this.state;
    try {
      await updateRoad(road);
      let { roads } = this.state;
      roads[selectedIndex] = road;
      this.setState({ roads, road: {} });
      notify(`${string.station.roadUpdatedSucessfully}`);

    } catch (err) {
      console.log(err);
      notify(`${string.station.errorAddingRoad}`);
      NProgress.done();
    }
  };

  // set delete mode upon selecting delete icon
  setDeleteMode = (mode, i) => {
    if (mode) {
      this.setState({ deleteMode: mode });
      this.setState({ selectedIndex: i });
      $("#deleteModal").modal("show");
    }
  };

  setEditMode = (mode, i) => {
    if (mode) {
      this.setState({ editMode: mode });
      this.setState({ selectedIndex: i });
      if (mode == "road") {
        let { roads } = this.state;
        let road = roads[i];
        this.setState({ road });
        $("#editRoadModal").modal("show");
      }
    }
  };

  render() {
    const { user, roads, road } = this.state;
    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - {string.station.manageStation}</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <div
                    className="tab-pane fade show active mt-3 w-100"
                    id="road"
                    role="tabpanel"
                    aria-labelledby="road-listing"
                  >
                    <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
                      <h4 className="text-dark">{string.station.stationListing}</h4>
                      <Button
                        className="btn btn-primary large-btn"
                        data-toggle="modal"
                        data-target="#roadModal"
                      >
                        {string.station.submitStation}
                      </Button>
                    </div>
                    <div className="shipment-table-listing table-responsive mt-2 w-100">
                      <table className="table">
                        <thead className="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col"> {string.station.station}</th>
                            <th scope="col"> {string.shipment.latitude}</th>
                            <th scope="col">{string.shipment.longitude}</th>
                            <th scope="col">{string.shipment.radius}</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {roads.map((road, i) => {
                            return (
                              <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{road.name}</td>
                                <td>{road.longitude}</td>
                                <td>{road.latitude}</td>
                                <td>{road.radius}</td>
                                <td>
                                  <i
                                    className="fa fa-pencil-alt"
                                    onClick={() => this.setEditMode("road", i)}
                                  ></i>
                                  <i
                                    className="fa fa-trash"
                                    onClick={() =>
                                      this.setDeleteMode("road", i)
                                    }
                                  ></i>
                                </td>
                              </tr>
                            );
                          })}
                          {roads.length == 0 && (
                            <tr>
                              <td colSpan="5" className="text-center">
                                {string.cargo.noDataAvailable}

                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <div
          className="modal fade customModal document"
          id="deleteModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <DeleteModal onDeleteEntry={this.onDeleteEntry} />
        </div>
        <div
          className="modal fade customModal document"
          id="roadModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <AddModal
            road={road}
            state={this.setState.bind(this)}
            onRoadSubmit={this.onRoadSubmit.bind(this)}
          />
        </div>
        <div
          className="modal fade customModal document"
          id="editRoadModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <EditModal
            road={road}
            state={this.setState.bind(this)}
            updateRoad={this.updateRoad.bind(this)}
          />
        </div>
        <Footer />
      </div>
    );
  }
}

export default withAuth(RoadPage, { loginRequired: true });

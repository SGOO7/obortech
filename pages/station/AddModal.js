// updated
import React, { useState } from "react";
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";
import { useFormik } from "formik";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import * as Yup from "yup";
import string from "../../utils/stringConstants/language/eng.json";
function AddModal({ road, state, onRoadSubmit }) {


  console.log(state);
  const formik = useFormik({
    initialValues: {
      name: "",
      latitude: "",
      longitude: "",
      radius: ""
    },
    validationSchema: Yup.object({
      name: Yup.string().required(`Name ${string.errors.required}`),
      latitude: Yup.string().required(`Latitude ${string.errors.required}`),
      longitude: Yup.string().required(`Longitude ${string.errors.required}`),
      radius: Yup.string().required(`Radius ${string.errors.required}`),
    }),
    onSubmit: values => {

      alert(JSON.stringify(values, null, 2));
      state({
        road: Object.assign({}, road, {
          name: values.name,
          latitude: values.latitude,
          longitude: values.longitude,
          radius: values.radius
        }),
      });
      onRoadSubmit(values);

    }
  })

  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              {string.station.addStation}

            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">{string.cancelSign}</span>
            </Button>
          </div>
          <div className="modal-body">
            <form className="form-container" onSubmit={formik.handleSubmit}>
              <div className="row ml-0 mr-0 content-block">
                <div className="form-group col-md-12 p-0">
                  <label
                    htmlFor="name"
                    className="col-md-12 col-form-label pl-0"
                  >
                    {string.station.name}
                  </label>
                  <Input
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                    placeholder={string.station.name}
                    onChange={formik.handleChange}
                    value={formik.values.name}
                  />
                  {formik.errors.name ? (
                    <FormHelperMessage
                      className="err"
                      message={formik.errors.name}
                    />
                  ) : null}
                </div>


                <div className="form-group col-md-12 p-0">
                  <label
                    htmlFor="latitude"
                    className="col-md-12 col-form-label pl-0"
                  >
                    {string.latitude}
                  </label>
                  <Input
                    type="text"
                    name="latitude"
                    id="latitude"
                    className="form-control"
                    placeholder={string.latitude}
                    onChange={formik.handleChange}
                    value={formik.values.latitude}
                  />
                  {formik.errors.latitude ? (
                    <FormHelperMessage
                      className="err"
                      message={formik.errors.latitude}
                    />
                  ) : null}
                </div>


                <div className="form-group col-md-12 p-0">
                  <label
                    htmlFor="longitude"
                    className="col-md-12 col-form-label pl-0"
                  >
                    {string.longitude}
                  </label>
                  <Input
                    type="text"
                    name="longitude"
                    id="longitude"
                    className="form-control"
                    placeholder={string.longitude}
                    onChange={formik.handleChange}
                    value={formik.values.longitude}
                  />
                  {formik.errors.longitude ? (
                    <FormHelperMessage
                      className="err"
                      message={formik.errors.longitude}
                    />
                  ) : null}
                </div>


                <div className="form-group col-md-12 p-0">
                  <label
                    htmlFor="longitude"
                    className="col-md-12 col-form-label pl-0"
                  >
                    {string.radius}
                  </label>
                  <Input
                    type="text"
                    name="radius"
                    id="radius"
                    className="form-control"
                    placeholder={string.radius}
                    onChange={formik.handleChange}
                    value={formik.values.radius}
                  />
                  {formik.errors.radius ? (
                    <FormHelperMessage
                      className="err"
                      message={formik.errors.radius}
                    />
                  ) : null}
                </div>


              </div>
              <div className="modal-footer">
                <Button
                  className="btn btn-primary large-btn"
                  type="button"
                  // data-dismiss="modal"
                >
                  {string.insertBtnTxt}
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

AddModal.propTypes = {};

AddModal.defaultProps = {};

export default AddModal;

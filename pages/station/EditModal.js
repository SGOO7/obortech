// updated
import React, { useState } from "react";
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";
import { useFormik } from "formik";
import string from "../../utils/stringConstants/language/eng.json";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import * as Yup from "yup";
function EditModal({ road, state, updateRoad }) {
  const n = road.name;
  console.log(n);
  console.log(state);
  let formik = null;
  if (!!road?.name) {
    formik = useFormik({
      initialValues: {
        name: road.name || "",
        latitude: road.latitude || "",
        longitude: road.longitude || "",
        radius: road.radius || ""
      },
      validationSchema: Yup.object({
        name: Yup.string().required(`Name ${string.errors.required}`),
        latitude: Yup.string().required(`Latitude ${string.errors.required}`),
        longitude: Yup.string().required(`Longitude ${string.errors.required}`),
        radius: Yup.string().required(`Radius ${string.errors.required}`),
      }),
      onSubmit: values => {
        alert(JSON.stringify(values, null, 2));
        state({
          road: Object.assign({}, road, {
            name: values.name,
            latitude: values.latitude,
            longitude: values.longitude,
            radius: values.radius
          }),
        });
        updateRoad();
      }
    });
  }

  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              {string.station.editStation}

            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">{string.cancelSign}</span>
            </Button>
          </div>
          <div className="modal-body">
            <form className="form-container" onSubmit={formik?.handleSubmit}>
              <div className="row ml-0 mr-0 content-block">
                <div className="form-group col-md-12 p-0">
                  <label
                    htmlFor="name"
                    className="col-md-12 col-form-label pl-0"
                  >
                    {string.station.name}
                  </label>
                  <Input
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                    placeholder={string.station.name}
                    onChange={formik?.handleChange}
                    value={formik?.values.name}
                  />
                  {formik?.errors.name ? (
                    <FormHelperMessage
                      className="err"
                      message={formik?.errors.name}
                    />
                  ) : null}
                </div>
                <div className="form-group col-md-12 p-0">
                  <label
                    htmlFor="latitude"
                    className="col-md-12 col-form-label pl-0"
                  >
                    {string.latitude}
                  </label>
                  <Input
                    type="text"
                    name="latitude"
                    id="latitude"
                    className="form-control"
                    placeholder={string.latitude}
                    onChange={formik?.handleChange}
                    value={formik?.values.latitude}
                  />
                  {formik?.errors.latitude ? (
                    <FormHelperMessage
                      className="err"
                      message={formik?.errors.latitude}
                    />
                  ) : null}
                </div>
                <div className="form-group col-md-12 p-0">
                  <label
                    htmlFor="longitude"
                    className="col-md-12 col-form-label pl-0"
                  >
                    {string.longitude}
                  </label>
                  <Input
                    type="text"
                    name="longitude"
                    id="longitude"
                    value={road.longitude || ""}
                    className="form-control"
                    placeholder={string.longitude}
                    onChange={formik?.handleChange}
                    value={formik?.values.longitude}
                  />
                  {formik?.errors.longitude ? (
                    <FormHelperMessage
                      className="err"
                      message={formik?.errors.longitude}
                    />
                  ) : null}
                </div>
                <div className="form-group col-md-12 p-0">
                  <label
                    htmlFor="longitude"
                    className="col-md-12 col-form-label pl-0"
                  >
                    {string.radius}
                  </label>
                  <Input
                    type="text"
                    name="radius"
                    id="radius"
                    value={road.radius || ""}
                    className="form-control"
                    placeholder={string.radius}
                    onChange={formik?.handleChange}
                    value={formik?.values.radius}
                  />
                  {formik?.errors.radius ? (
                    <FormHelperMessage
                      className="err"
                      message={formik?.errors.radius}
                    />
                  ) : null}
                </div>
              </div>
              <div className="modal-footer">
                <Button
                  className="btn btn-primary large-btn"
                  type="submit"
                // onClick={updateRoad}
                //   data-dismiss="modal" 
                > 
                  {string.updateBtnTxt}
                 </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

EditModal.propTypes = {};

EditModal.defaultProps = {};

export default EditModal;

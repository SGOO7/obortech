import PropTypes from "prop-types";
import NProgress from "nprogress";
import Link from 'next/link';
import withAuth from "../../lib/withAuth";
import { fetchBorderInfo } from "../../lib/api/border-info";
import { fetchDropdown } from "../../lib/api/shipment";
import {
	_momentDateFormat,
	_momentDate,
	_momentGetDiff,
} from "../../utils/globalFunc";
import AuthPage from "../../components/wrapper/AuthPage";
import { UncontrolledTooltip } from "reactstrap";
import dynamic from "next/dynamic";
import { fetchLocationLogs, fetchLatestStats } from "../../lib/api/logs";
import statusCode from "../../utils/statusCodes";
import string from "../../utils/stringConstants/language/eng.json";
import AdvanceSelect from "../../components/common/form-elements/select/AdvanceSelect";
import Checkbox from "../../components/common/form-elements/checkbox";

const DynamicMap = dynamic(
	() => {
		return import("../../components/dashboard/dashboardMap");
	},
	{ ssr: false }
);

class IndexPage extends React.Component {

	static getInitialProps({ query }) {
		const indexPage = true;
		const shipment_id = query.shipment_id;
		return { indexPage, shipment_id };
	}

	static propTypes = {
		user: PropTypes.shape({
			_id: PropTypes.string,
		}),
	};

	static defaultProps = {
		user: null,
	};

	async componentDidMount() {
		NProgress.done();
		try {
			window.localStorage.setItem("shipment_id", this.props.shipment_id);

			// Fetch Shipment Details and Prepare Cargo, Container, Truck and Group dropdown accordingly
			const shipment_details = await fetchDropdown({
				shipment_id: this.props.shipment_id,
			});
			this.setState({ shipment_details: shipment_details });

			this._prepareDropdown(["temp", "hum", "sealing"]);
		} catch (err) {
			this.setState({
				loading: false,
				error: err.message || err.toString(),
			}); // eslint-disable-line
			NProgress.done();
		}
	}

	constructor(props) {
		super(props);

		this.state = {
			user: props.user || {},
			borderInfoArr: [],
			shipmentTripStats: {},
			format: "YYYY-MM-DD HH:mm:ss",
			activeDot: {},
			mapMarker: null,
			startMarker: null,
			endMarker: null,
			polylines: [],
			shipment_details: {},
			shipmentCargos: [],
			shipmentContainers: [],
			shipmentGroups: [],
			shipmentTrucks: [],
			selectedCargo: null,
			selectedContainer: null,
			selectedGroup: null,
			selectedTruck: null,
            selectedContainerValue: "",
            selectedGroupValue: "",
            selectedTruckValue: "",
            selectedCargoValue: "",
			selectedSelection: null,
			latestStats: { latestTemp: 0, latestHum: 0, sealingOpenCount: 0 },
			hum_alert: 0,
			temp_alert: 0,
			alertArr: [],
            sealingChecked: false,
            temperatureChecked: false,
			humidityChecked: false,
			filteredShipmentCargos: []
		};
	}

    //Handle checkout click
    _handleCheck = async (alert_selection) => {
        if(alert_selection == 'sealing'){
            await this.setState({sealingChecked: !this.state.sealingChecked});
        }
        if(alert_selection == 'temp'){
            await this.setState({temperatureChecked: !this.state.temperatureChecked});
        }
        if(alert_selection == 'hum'){
            await this.setState({humidityChecked: !this.state.humidityChecked});
        }
        this._showAlert();
    }

    //Show alert
	_showAlert = () => {
        let alertArr = [];
        if(this.state.sealingChecked){
            alertArr.push('sealing');
        }
        if(this.state.temperatureChecked){
            alertArr.push('temp');
        }
        if(this.state.humidityChecked){
            alertArr.push('hum');
        }
		this._prepareDropdown(alertArr);
	};

	_prepareDropdown = (alertArr) => {
		let shipmentCargos = [],
			shipmentContainers = [],
			shipmentGroups = [],
			shipmentTrucks = [];
		this.state.shipment_details.shipment_selections.map(
			(shipment_selection, i) => {
				let containerDetails =
					shipment_selection.selection_containers[0];
				let groupDetails = shipment_selection.selection_groups[0];
				let truckDetails = shipment_selection.selection_trucks[0];
				let alert = 0;
				alertArr.map((alert_selection) => {
					if (alert_selection == "temp") {
						alert += containerDetails.container.temp_alert.length;
					}
					if (alert_selection == "hum") {
						alert += containerDetails.container.hum_alert.length;
					}
					if (alert_selection == "sealing") {
						if (containerDetails.container.sealing_alert.length > 0) {
							alert += containerDetails.container.sealing_alert[0].open_count;
						}
					}
				});
				shipmentContainers.push({
					alert: alert,
					selectionID: containerDetails.selection_id,
					info: containerDetails,
					value: i + "_" + containerDetails.selection_id,
					label: containerDetails.container.containerID + " " + "(" + alert + ")",
				});
				shipmentGroups.push({
					alert: alert,
					selectionID: groupDetails.selection_id,
					value: groupDetails.group.id + "_" + groupDetails.selection_id,
					label: groupDetails.group.groupID + " " + "(" + alert + ")",
				});
				shipmentTrucks.push({
					alert: alert,
					selectionID: truckDetails.selection_id,
					value: truckDetails.truck.id + "_" + truckDetails.selection_id,
					label: truckDetails.truck.truckID + " " + "(" + alert + ")",
				});
				shipment_selection.selection_cargos.map((cargo) => {
					shipmentCargos.push({
						alert: alert,
						value: i + "_" + cargo.cargo.id + "_" + cargo.selection_id,
						label: cargo.cargo.cargoID + " " + "(" + alert + ")",
					});
				});
			}
		);

		// Sort them DESC order by alert
		shipmentCargos.sort(function(a, b) {
			return b.alert - a.alert;
		});
		shipmentContainers.sort(function(a, b) {
			return b.alert - a.alert;
		});
		shipmentGroups.sort(function(a, b) {
			return b.alert - a.alert;
		});
		shipmentTrucks.sort(function(a, b) {
			return b.alert - a.alert;
		});

		this.setState({
			shipmentCargos,
			shipmentContainers,
			shipmentGroups,
			shipmentTrucks,
		});

        //Show selected values in dropdown
        this._setSelectedValue('cargo', this.state.selectedCargoValue, shipmentCargos);
        this._setSelectedValue('container', this.state.selectedContainerValue, shipmentContainers);
        this._setSelectedValue('group', this.state.selectedGroupValue, shipmentGroups);
        this._setSelectedValue('truck', this.state.selectedTruckValue, shipmentTrucks);
	};

    _setSelectedValue = (type, preSelectedValue, valuesArr) => {
        if(preSelectedValue){
			let selected = valuesArr.filter(option => option.value === preSelectedValue.value);
			if(selected.length != 0){
				if(type == 'cargo'){
					this.setState({selectedCargoValue: {label: selected[0].label, value: selected[0].value}});
				}
				if(type == 'container'){
					this.setState({selectedContainerValue: {label: selected[0].label, value: selected[0].value}});
				}
				if(type == 'group'){
					this.setState({selectedGroupValue: {label: selected[0].label, value: selected[0].value}});
				}
				if(type == 'truck'){
					this.setState({selectedTruckValue: {label: selected[0].label, value: selected[0].value}});
				}
			}
        }
    }

	_fetchBorderInfo = async (container_id) => {
		if (!container_id) {
			return false;
		}
		NProgress.start();

        //Check all filter checkboxes
        await this.setState({sealingChecked: true});
        await this.setState({humidityChecked: true});
        await this.setState({temperatureChecked: true});
        this._showAlert();

		// Fetch Border Info details
		const borderInfo = await fetchBorderInfo({
			container_id: container_id,
			shipment_id: this.props.shipment_id,
		});
		// Fetch Location Points to draw on map
		this._fetchLocationLogs();
		// Fetch Latest Location to show below
		const latestStats = await fetchLatestStats({
			container: container_id,
			shipment: this.props.shipment_id,
		});
		this.setState({ latestStats: latestStats.data });

		if (borderInfo && borderInfo.length > 0) {
			this.setState({ borderInfo: this.normalizeRoadArr(borderInfo) });

			//Calculate total duration and time
			const shipmentTripStats = this.getShipmentTripStats(borderInfo);
			this.setState({ shipmentTripStats });

			$('[data-toggle="tooltip"]').tooltip();
		}
		NProgress.done();
	};

	_resetMap = () => {
		this.setState({
			mapData: {},
			mapMarker: null,
			startMarker: null,
			endMarker: null,
			polylines: [],
		});
	};

	_fetchLocationLogs = async () => {

		try {
			const locLogs = await fetchLocationLogs({
				container: this.state.selectedContainer,
				shipment: this.props.shipment_id,
			});
			if (locLogs?.code === 1) {
				this.setState({ mapData: locLogs.data });
				if (locLogs.data) {
					const { stations, locationlogs } = locLogs.data;
					if (stations.length > 0) {
						this.setState({
							startMarker: {
								name: stations[0]?.road.name,
								pos: [
									stations[0]?.road.latitude,
									stations[0]?.road.longitude,
								],
							},
							endMarker: {
								name: stations[stations.length - 1]?.road.name,
								pos: [
									stations[stations.length - 1]?.road.latitude,
									stations[stations.length - 1]?.road.longitude,
								],
							},
						});
					}
					if (locationlogs.length > 0) {
						const arr = [];
						locationlogs.map((loc, idx) => {
							let obj = {};
							(obj.fromLat = loc.latitude),
								(obj.fromLong = loc.longitude),
								(obj.toLat = locationlogs[idx + 1]?.latitude),
								(obj.toLong = locationlogs[idx + 1]?.longitude);
							arr.push(obj);
							obj = {};
						});
						this.setState({
							polylines: arr,
							mapMarker: [
								locationlogs[locationlogs.length - 1]?.latitude,
								locationlogs[locationlogs.length - 1]?.longitude,
							],
						});
					} else {
						this._resetMap();
					}
				}
			}
		} catch (err) {
			console.error("Err in _fetchLocationLogs => ", err);
		}
	};

	// Array normalization
	normalizeRoadArr = (borderInfo) => {
		const { format } = this.state;
		if (borderInfo?.length > 0) {
			try {
				const obj = {
					status_class: "disabled",
					total_distance_covered: 0,
				};
				const roadArr = borderInfo.map((info, i) => {
					if (info.road.inside.length > 0) {
						obj.status_class = "active";
						obj.border_in_datetime = _momentDateFormat(info.road.inside[0].createdAt,format);
						if (i <= 0) {
							obj.started_datetime = info.road.inside[0].createdAt;
						}
						if (info.road.inside.length > 1) {
							this.setState({
								activeDot: {
									...this.state.activeDot,
									[info.road.id]: 0,
								},
							});
						}
						info.road.inside.map((ins, idx) => {
							const trip_str = `<strong>Border In</strong><span>${_momentDateFormat(ins?.createdAt,this.state.format
							)}</span><br/><strong>Border Out</strong><span>${_momentDateFormat(info.road.outside[idx]?.createdAt,this.state.format)}</span><br/><strong>Traveled</strong><span>${info.road.outside[idx]?.travelled_distance > 0 ? parseFloat(info.road.outside[idx].travelled_distance).toFixed(1) : ""} km</span>`;
							this.setState({ [`trip-str-${info.road.id}-${idx}`]: trip_str});
						});
						if (info.road.outside.length > 0) {
							obj.status_class = "complete";

							obj.distance_covered = info.road.outside[0].travelled_distance > 0 ? parseFloat(info.road.outside[0].travelled_distance).toFixed(1)	: "";

							obj.total_distance_covered = parseFloat(obj.total_distance_covered) + parseFloat(obj.distance_covered);
							obj.last_border_outtime = _momentDate(info.road.outside[0].createdAt);

							const travelled_time = _momentGetDiff(
								obj.last_border_outtime,
								obj.border_in_datetime,
								"hours"
							);
							obj.border_out_datetime = _momentDateFormat(
								obj.last_border_outtime,
								format
							);

							obj.diff_in_days = _momentGetDiff(
								obj.last_border_outtime,
								obj.border_in_datetime,
								"days"
							);
							// obj.diff_in_days = Math.floor(travelled_time / 24);
							obj.diff_in_hours = Math.floor(
								travelled_time - obj.diff_in_days * 24
							);
							obj.travelled_time = travelled_time;

						} else {

                            obj.last_border_outtime = _momentDate(new Date());

							const travelled_time = _momentGetDiff(
								obj.last_border_outtime,
								obj.border_in_datetime,
								"hours"
							);
							obj.border_out_datetime = _momentDateFormat(obj.last_border_outtime, format);

							obj.diff_in_days = _momentGetDiff(
								obj.last_border_outtime,
								obj.border_in_datetime,
								"days"
							);

							obj.diff_in_hours = Math.floor(
								travelled_time - obj.diff_in_days * 24
							);

                            obj.diff_in_mins = _momentGetDiff(
								obj.last_border_outtime,
								obj.border_in_datetime,
								"minutes"
							);

							obj.travelled_time = travelled_time;
                        }


					} else {
						obj.status_class = "disabled";
						obj.border_in_datetime = "";
						if (i <= 0) {
							obj.started_datetime = "";
						}
						obj.distance_covered = 0;
						obj.total_distance_covered = 0;
						obj.last_border_outtime = "";

                        const travelled_time = "";
						obj.border_out_datetime = "";

						obj.diff_in_days = "";
						obj.diff_in_hours = "";
                        obj.diff_in_mins = "";
						obj.travelled_time = travelled_time;
					}
					return (info = {
						...obj,
						borderId: info.id,
						isActive: info.road.isActive,
						name: info.road.name,
						id: info.road.id,
						inside: info.road.inside,
						outside: info.road.outside,
					});
				});
				return roadArr;
			} catch (err) {
				console.error("Err in normalizeRoadArr => ", err);
			}
		}
	};

	//Show/hide trip info
	toggleTripInfo = (road_id, idx) => {
		this.setState({
			activeDot: {
				...this.state.activeDot,
				[road_id]: idx,
			},
		});
	};

	//Get shipment trip stats
	getShipmentTripStats = (borderInfo) => {
		try {
			const { format } = this.state;
			let started_datetime = "";
			let last_border_outtime = "";
			let total_distance_covered = 0;
			let started_datetime_formatted = 0;
			let total_diff_in_days = 0;
			let total_diff_in_hours = 0;
			let total_diff_in_mins = 0;

			borderInfo.map((info, i) => {

                if(info.road.inside.length <= 0){
                    return false;
                }

				if (i <= 0) {
					started_datetime = info.road.inside[0].createdAt;
				}

				const distance_covered =
					info.road.outside[0]?.travelled_distance > 0 ? parseFloat(info.road.outside[0]?.travelled_distance).toFixed(1): "";
				if (distance_covered) {
					total_distance_covered = parseFloat(total_distance_covered) + parseFloat(distance_covered);
				}
			});

            //If border out exists, otherwise take current date
            if(borderInfo[borderInfo.length - 1].road.outside.length > 0){
                borderInfo[borderInfo.length - 1].road.outside.map((out_road, j) => {
					last_border_outtime = out_road.createdAt;
				});
            }else{
                last_border_outtime = new Date();
            }

			if (last_border_outtime) {
				started_datetime_formatted = _momentDateFormat(started_datetime, format);
				const total_travelled_time = _momentGetDiff(last_border_outtime, started_datetime, "hours");

				total_diff_in_days = Math.floor(total_travelled_time / 24);
				const diff_in_hours = Math.floor(total_travelled_time - total_diff_in_days * 24);

				total_diff_in_hours = diff_in_hours > 60 ? Math.floor(diff_in_hours / 60) : diff_in_hours;
				total_diff_in_mins = _momentGetDiff(last_border_outtime, started_datetime, "minutes");
                total_diff_in_mins = (total_diff_in_mins - (total_travelled_time * 60));
			}

			const returnArr = {
				started_datetime_formatted,
				total_diff_in_days,
				total_diff_in_hours,
				total_diff_in_mins,
				total_distance_covered,
			};
			return returnArr;
		} catch (err) {
			console.error("Err in getShipmentTripStats => ", err);
		}
	};

	_setDropDownValues = (array, event, isCargo = false) => {
		let i = array.indexOf(event);
		if(i == -1){
			return false;
		}
		const {shipmentContainers, shipmentTrucks, shipmentGroups} = this.state;
		const container = shipmentContainers[i].info;
		this.setState({
			selectedContainerValue:shipmentContainers[i],
			selectedTruckValue:shipmentTrucks[i],
			selectedGroupValue:shipmentGroups[i],
			selectedContainer: container.container.id,
			hum_alert: container.container.hum_alert.length,
			temp_alert: container.container.temp_alert.length,
		});
		this._fetchBorderInfo(
			container.container.id
		);
		let shipmentCargos = [];
		if(!isCargo){
			this.state.shipment_details.shipment_selections.map(
				(shipment_selection, selectionIndex) => {
					if(shipment_selection.id == event.selectionID){
						shipment_selection.selection_cargos.map((cargo) => {
							let alert = 0;
							shipmentCargos.push({
								alert: event.alert,
								value: i + "_" + cargo.cargo.id + "_" + cargo.selection_id,
								label: cargo.cargo.cargoID + " " + "(" + event.alert + ")",
							});
						});
					}
				}
			);
			shipmentCargos.sort(function(a, b) {
				return b.alert - a.alert;
			});
			this.setState({
				// filteredShipmentCargos: shipmentCargos,
				selectedCargoValue: shipmentCargos[0]
			});
		}
	}

	_renderBorderItem = (info, idx) => {
		const { id: road_id, status_class, inside, outside, travelled_time, diff_in_days, diff_in_hours, diff_in_mins, name, border_in_datetime, border_out_datetime, distance_covered, } = info;
		const tooltip_str = (id) => {
			return (
				<>
					<strong>Border In</strong>
					<span>{_momentDateFormat(inside[id]?.createdAt, this.state.format)}</span>
					<br />
					<strong>Border Out</strong>
					<span>{_momentDateFormat(outside[id]?.createdAt, this.state.format)}</span>
					<br />
					<strong>Traveled</strong>
					<span>{outside[id]?.travelled_distance > 0 ? parseFloat(outside[id].travelled_distance).toFixed(1): ""}{" "}km</span>
				</>
			);
		};
		// const tooltipString
		return (
			<div id={"station-" + road_id} className={`col-xs-3 bs-wizard-step ${status_class}`}>
				{inside?.length > 1 && (
					<div className="in-out-state">
						{inside.map((it, idx) => {
							return (
								<span
									className={`trip-dots ${this.state.activeDot[road_id] === idx ? "active"	: "" }`}
									onClick={() =>
										this.toggleTripInfo(road_id, idx)
									}
								></span>
							);
						})}
					</div>
				)}

				<div className="progress">
					<div className="progress-bar"></div>
				</div>

				{inside.length > 1 ? (
					<>
						<a className="bs-wizard-dot trip-2 show-trip" id={`Tooltip-${road_id}_${this.state.activeDot[road_id]}`}
						>
							<span data-totalhours={travelled_time}>
								{_momentGetDiff(outside[this.state.activeDot[road_id]] ?.createdAt, inside[this.state.activeDot[road_id]] ?.createdAt, "days" )}{" "}d<br />
								{Math.floor(_momentGetDiff(outside[this.state.activeDot[road_id]] ?.createdAt, inside[this.state.activeDot[road_id]] ?.createdAt, "hours" ) - _momentGetDiff(outside[this.state.activeDot[road_id]] ?.createdAt, inside[this.state.activeDot[road_id]] ?.createdAt, "days" ) * 24)}{" "}h
							</span>
						</a>
						<UncontrolledTooltip
							placement="right"
							target={`Tooltip-${road_id}_${this.state.activeDot[road_id]}`}
						>
							{tooltip_str(this.state.activeDot[road_id])}
						</UncontrolledTooltip>
					</>
				) : (
					<a
						className="bs-wizard-dot trip-2 show-trip"
						data-toggle="tooltip"
						data-placement="right"
						id="hints"
						data-html="true"
						title={this.state[`trip-str-${road_id}-0`]}
					>
						{(diff_in_days && diff_in_days > 0) ? `${diff_in_days} d` : ""}{" "}
                        {(diff_in_days > 0) && <br />}
                        {(diff_in_hours && diff_in_hours > 0) ? `${diff_in_hours} h` : ""}
                        {(diff_in_mins && diff_in_mins < 60 && diff_in_hours < 1) ? `${diff_in_mins} m` : ""}
                        {(!diff_in_days && !diff_in_hours && !diff_in_mins) ? "0" : ""}
					</a>
				)}
				<div className="bs-wizard-info text-center">{name}</div>
			</div>
		);
	};

	render() {
		const {
			hum_alert,
			temp_alert,
			latestStats,
			selectedContainer,
			selectedContainerValue,
            selectedCargoValue,
            selectedGroupValue,
            selectedTruckValue,
			selectedSelection,
			shipmentCargos,
			shipmentContainers,
			shipmentGroups,
			shipmentTrucks,
			user,
			borderInfo,
			borderInfoArr,
			shipmentTripStats,
			mapData,
			center,
			mapMarker,
			startMarker,
			endMarker,
			polylines,
			filteredShipmentCargos,
		} = this.state;

		return (
			<AuthPage user={user} title={string.containerPageTitle}>
				<div className="container-fluid">
					{/* Content Row */}
					<div className={selectedContainer ? "row d-flex step-wrapper" : "d-none"}>
						<div className="steps col-xl-9">
							<div className="bs-wizard">
								{borderInfo && borderInfo.length > 0 &&
									borderInfo.map((info, i) => {
										return this._renderBorderItem(info, i);
									})}
								{borderInfoArr}
							</div>
						</div>
						{shipmentTripStats &&
							Object.keys(shipmentTripStats).length > 0 && (
								<div className="steps-total col-xl-3">
									<p>
										<strong>Total Duration:</strong>{" "}
										{(shipmentTripStats.total_diff_in_days ? shipmentTripStats.total_diff_in_days: "0")}{" "}
										days,{" "}
										{(shipmentTripStats.total_diff_in_hours) ? shipmentTripStats.total_diff_in_hours: "0"}{" "}
										hours,{" "}
										{(shipmentTripStats.total_diff_in_mins) ? shipmentTripStats.total_diff_in_mins: "0"}{" "}
										minutes
									</p>
									<p>
										<strong>Traveled km:</strong>{" "}
										{parseFloat(shipmentTripStats.total_distance_covered).toFixed(2)}{" "}km
									</p>
									<p>
										<strong>Started:</strong>{" "}
										{shipmentTripStats.started_datetime_formatted}{" "}GMT +8
									</p>
								</div>
							)}
					</div>
					<div className="row d-flex map-content">
						<div className="col-lg-5 pl-0">
							<form action="" method="">
								<div className="form-group row">
									<label htmlFor="email_address" className="col-md-4 col-form-label">Cargo:</label>
									<div className="col-md-8 position-relative">
										<AdvanceSelect
											placeholder="Select Cargo"
											options={filteredShipmentCargos.length > 0 ? filteredShipmentCargos : shipmentCargos}
                                            value={selectedCargoValue}
											name="cargo_id"
											onChange={(event) => {
												// set container value in dropdown
												let valueArr = event.value.split("_");
												this.setState({
													selectedCargo: valueArr[1],
                                                    selectedCargoValue: event,
													selectedSelection: valueArr[2]
												});
												this._setDropDownValues(shipmentCargos, event,true);
												// fetch container details and set map accordingly
												let container =
													shipmentContainers[
														shipmentCargos.indexOf(event)
													].info;

												if (valueArr[0]) {

												} else {
													this.setState({
														selectedContainer: "",
														hum_alert: 0,
														temp_alert: 0,
													});
													this._resetMap();
												}
											}}
										/>
									</div>
								</div>
								<div className="form-group row">
									<label htmlFor="email_address" className="col-md-4 col-form-label">Container:</label>
									<div className="col-md-8 position-relative">
										<AdvanceSelect
											placeholder="Select Container"
											options={shipmentContainers}
											value={selectedContainerValue}
											name="container_id"
											onChange={(event) => {
												this.setState({selectedContainerValue: event});
												let valueArr = event.value.split("_");
												if (valueArr[0]) {
													this._setDropDownValues(shipmentContainers, event);
												} else {
													this.setState({
														selectedContainer: "",
														hum_alert: 0,
														temp_alert: 0,
													});
													this._resetMap();
												}
											}}
										/>
									</div>
								</div>
								<div className="form-group row">
									<label htmlFor="email_address" className="col-md-4 col-form-label">Truck:</label>
									<div className="col-md-8 position-relative">
										<AdvanceSelect
											placeholder="Select Truck"
											options={shipmentTrucks}
                                            value={selectedTruckValue}
											name="truck_id"
											onChange={(event) => {
												let valueArr = event.value.split("_");
												if (valueArr[0]) {
													this._setDropDownValues(shipmentTrucks, event);
												} else {
													this.setState({
														selectedContainer: "",
														selectedTruck: "",
														selectedTruckValue: event,
														hum_alert: 0,
														temp_alert: 0,
													});
													this._resetMap();
												}
											}}
										/>
									</div>
								</div>
								<div className="form-group row">
									<label htmlFor="email_address" className="col-md-4 col-form-label">Group:</label>
									<div className="col-md-8 position-relative">
										<AdvanceSelect
											placeholder="Select Group"
											options={shipmentGroups}
                                            value={selectedGroupValue}
											name="group_id"
											onChange={(event) => {
												let valueArr = event.value.split("_");
												if (valueArr[0]) {
													this._setDropDownValues(shipmentGroups, event);
												} else {
													this.setState({
														selectedContainer: "",
														selectedGroup: "",
														selectedGroupValue: event,
														hum_alert: 0,
														temp_alert: 0,
													});
													this._resetMap();
												}
											}}
										/>
									</div>
								</div>
								<div className="form-group row">
									<label htmlFor="email_address" className="col-md-4 col-form-label">Show alerts:</label>
									<div className="col-md-8 tem-type">
										<span>
											<Checkbox
                                                checked={this.state.sealingChecked}
												type="checkbox"
                                                onChange={() =>{

                                                }}
												onClick={() =>
													this._handleCheck("sealing")
												}
											/>{" "}
											Sealing
										</span>
										<span>
											<Checkbox
                                                checked={this.state.temperatureChecked}
												type="checkbox"
                                                onChange={() =>{

                                                }}
												onClick={() =>
													this._handleCheck("temp")
												}
											/>{" "}
											Temperature
										</span>
										<span>
											<Checkbox
                                                checked={this.state.humidityChecked}
												type="checkbox"
                                                onChange={() =>{

                                                }}
												onClick={() =>
													this._handleCheck("hum")
												}
											/>{" "}
											Humidity
										</span>
									</div>
								</div>
							</form>
							{/* //form */}
							{/* mini stats */}
							<div className={selectedContainer ? "stats-wrapper row m-0" : "d-none" }>
								<div className="col-sm-6 br-0 bb-0">
									<Link href={"/analytics/"+this.props.shipment_id+"/"+selectedContainer}>
									    <a><p className="title">HUMIDITY</p></a>
                                    </Link>
									<p className="output">
										<span className="green-text">
											{latestStats.latestHum}%
										</span>{" "}
										<span className="text-red">
											({hum_alert})
										</span>
									</p>
								</div>
								<div className="col-sm-6 bb-0">
                                    <Link href={"/analytics/"+this.props.shipment_id+"/"+selectedContainer}>
                                        <a><p className="title">TEMPERATURE</p></a>
                                    </Link>
									<p className="output">
										<span className="green-text">
											{latestStats.latestTemp}°C
										</span>{" "}
										<span className="text-red">
											({temp_alert})
										</span>
									</p>
								</div>
								<div className="col-sm-6 br-0">
									<p className="title">SEALING</p>
									<p className="output">
										<span className="text-red">
											{latestStats.sealingOpenCount.toString()}{" "}
											opened
										</span>
									</p>
								</div>
								<div className="col-sm-6">
									<p className="title">SMART LOCK</p>
									<p className="output">
										<span className="green-text">N/A</span>
									</p>
								</div>
							</div>
							{/* //mini stats */}
						</div>
						<div className="col-lg-7 pr-0">
							<div className="map-wrapper">
								{polylines.length > 0 && (
									<DynamicMap
										mapMarker={mapMarker}
										polylines={polylines}
										startMarker={startMarker}
										endMarker={endMarker}
									/>
								)}
							</div>
						</div>
					</div>
				</div>
			</AuthPage>
		);
	}
}

export default withAuth(IndexPage, { loginRequired: true });

import PropTypes from "prop-types";
import Head from "next/head";
import Header from "../../components/HomeHeader";
import Footer from "../../components/HomeFooter";
import Sidebar from "../../components/Sidebar";
import DeleteModal from "./DeleteModal";
import AddModal from "./AddModal";
import EditModal from "./EditModal";
import NProgress from "nprogress";
import notify from "../../lib/notifier";
import withAuth from "../../lib/withAuth";
import string from "../../utils/stringConstants/language/eng.json";
import {
  fetchCargos,
  addCargo,
  removeCargo,
  updateCargo,
} from "../../lib/api/cargo";
// updated
import Button from "../../components/common/form-elements/button/Button";

class CargoPage extends React.Component {
  static getInitialProps() {
    const cargoPage = true;
    return { cargoPage };
  }

  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.string,
    }),
  };

  static defaultProps = {
    user: null,
  };

  async componentDidMount() {
    NProgress.start();
    try {
      const cargos = await fetchCargos();
      this.setState({ cargos });
      NProgress.done();
    } catch (err) {
      this.setState({ loading: false, error: err.message || err.toString() }); // eslint-disable-line
      NProgress.done();
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      user: props.user || {},
      cargos: [],
      cargo: {},
      deleteMode: "",
      selectedIndex: "",
      editMode: "",
    };
  }

  // submit cargo function to check submitted details
  onCargoSubmit = (event) => {
    // event.preventDefault();
    const { cargo } = this.state;
    const { cargoID } = cargo;

    // if (!cargoID) {
    // 	notify('CargoID is required');
    // 	return;
    // }

    this.addCargoData({ cargoID: cargoID });
  };

  // add cargo function
  addCargoData = async (data) => {
    NProgress.start();
    try {
      const cargo = await addCargo(data);
      let { cargos } = this.state;
      cargos.push(cargo);
      this.setState({ cargos, cargo: {} });
      notify(`${string.cargo.cargoAddedSucessfully}`);
      NProgress.done();
    } catch (err) {
      console.log(err);
      notify(`${string.cargo.errorAddingCargo}`);
      NProgress.done();
    }
  };

  // Function to delete entry from popup
  onDeleteEntry = async (event) => {
    event.preventDefault();
    let { deleteMode, cargos, selectedIndex } = this.state;
    if (deleteMode == "cargo") {
      // delete cargo data
      let cargos_data = cargos[selectedIndex];
      await removeCargo({ id: cargos_data.id });
      cargos.splice(selectedIndex, 1);
      this.setState({ cargos });
      notify(`${string.cargo.cargoDelatedSucessfully}`);
    }
  };

  // update cargo function
  updateCargo = async () => {
    NProgress.start();
    let { cargo, selectedIndex } = this.state;
    try {
      await updateCargo(cargo);
      let { cargos } = this.state;
      cargos[selectedIndex] = cargo;
      this.setState({ cargos, cargo: {} });
      notify(`${string.cargo.cargoUpdatedSucessfully}`);
      NProgress.done();
    } catch (err) {
      console.log(err);
      notify(`${string.cargo.errorAddingCargo}`);
      NProgress.done();
    }
  };

  // set delete mode upon selecting delete icon
  setDeleteMode = (mode, i) => {
    if (mode) {
      this.setState({ deleteMode: mode });
      this.setState({ selectedIndex: i });
      $("#deleteModal").modal("show");
    }
  };

  setEditMode = (mode, i) => {
    if (mode) {
      this.setState({ editMode: mode });
      this.setState({ selectedIndex: i });
      if (mode == "cargo") {
        let { cargos } = this.state;
        let cargo = cargos[i];
        this.setState({ cargo });
        $("#editCargoModal").modal("show");
      }
    }
  };

  render() {
    const { user, cargos, cargo } = this.state;
    return (
      <div id="page-top">
        <Head>
          <title>{process.env.APP_NAME} - {string.cargo.manageCargos}</title>
        </Head>
        {/* Page Wrapper */}
        <div id="wrapper">
          {/* Sidebar */}
          <Sidebar />
          {/* End of Sidebar */}
          {/* Content Wrapper */}
          <div id="content-wrapper" className="d-flex flex-column">
            {/* Main Content */}
            <div id="content">
              {/* Topbar */}
              <Header user={user} />
              {/* End of Topbar */}
              <div className="container-fluid">
                <div className="row d-flex shipment-listing">
                  <div
                    className="tab-pane fade show active mt-3 w-100"
                    id="cargo"
                    role="tabpanel"
                    aria-labelledby="cargo-listing"
                  >
                    <div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
                      <h4 className="text-dark">{string.cargo.cargosListing}</h4>
                      <Button
                        className="btn btn-primary large-btn"
                        data-toggle="modal"
                        data-target="#cargoModal"
                      >
                        {string.cargo.sUBMITcARGO}

                      </Button>
                    </div>
                    <div className="shipment-table-listing table-responsive mt-2 w-100">
                      <table className="table">
                        <thead className="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col"> {string.cargo.cargoID}</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {cargos.map((cargo, i) => {
                            return (
                              <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{cargo.cargoID}</td>
                                <td>
                                  <i
                                    className="fa fa-pencil-alt"
                                    onClick={() => this.setEditMode("cargo", i)}
                                  ></i>
                                  <i
                                    className="fa fa-trash"
                                    onClick={() =>
                                      this.setDeleteMode("cargo", i)
                                    }
                                  ></i>
                                </td>
                              </tr>
                            );
                          })}
                          {cargos.length == 0 && (
                            <tr>
                              <td colSpan="5" className="text-center">
                                {string.cargo.noDataAvailable}

                              </td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* End of Main Content */}
          </div>
          {/* End of Content Wrapper */}
        </div>
        {/* End of Page Wrapper */}
        <div
          className="modal fade customModal document"
          id="deleteModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <DeleteModal onDeleteEntry={this.onDeleteEntry} />
        </div>
        <div
          className="modal fade customModal document"
          id="cargoModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <AddModal
            state={this.setState.bind(this)}
            onCargoSubmit={this.onCargoSubmit.bind(this)}
          />
        </div>
        <div
          className="modal fade customModal document"
          id="editCargoModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <EditModal
            cargo={cargo}
            state={this.setState.bind(this)}
            updateCargo={this.updateCargo.bind(this)}
          />
        </div>
        <Footer />
      </div>
    );
  }
}

export default withAuth(CargoPage, { loginRequired: true });

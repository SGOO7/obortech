import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";

const AddCargoschema = Yup.object().shape({
  cargoID: Yup.string()
    .trim()
    .required(`${string.cargo.cargoID} ${string.errors.required}`),
});

function AddModal({ state, onCargoSubmit }) {
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              {string.cargo.addCargo}

            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">{string.cancelSign}</span>
            </Button>
          </div>
          <div className="modal-body">
            <Formik
              initialValues={{
                cargoID: "",
              }}
              validationSchema={AddCargoschema}
              onSubmit={(values) => {
                state({
                  cargo: Object.assign({}, cargo, { cargoID: values.cargoID }),
                });
                onCargoSubmit();
                $("#cargoModal").modal("hide");
                values.cargoID = "";
              }}
            >
              {({ errors, touched, handleChange, handleSubmit, values }) => (
                <form className="form-container" onSubmit={handleSubmit}>
                  <div className="row ml-0 mr-0 content-block">
                    <div className="form-group col-md-12 p-0">
                      <label
                        htmlFor="cargoID"
                        className="col-md-12 col-form-label pl-0"
                      >
                        {string.cargo.cargoId}

                      </label>
                      <Input
                        type="text"
                        name="cargoID"
                        id="cargoID"
                        className="form-control"
                        placeholder={string.cargo.cargoId}
                        value={values.cargoID}
                        onChange={handleChange}
                      // onChange={(event) => {
                      //     state({
                      //         cargo: Object.assign({}, cargo, { cargoID: event.target.value })
                      //     });
                      // }}
                      />
                      {errors.cargoID && touched.cargoID ? (
                        <FormHelperMessage
                          message={errors.cargoID}
                          className="error"
                        />
                      ) : null}
                    </div>
                  </div>
                  <div className="modal-footer">
                    <Button
                      className="btn btn-primary large-btn"
                      // onClick={onCargoSubmit}
                      type="submit"
                    // data-dismiss="modal"
                    >
                      {string.cargo.insert}

                    </Button>
                  </div>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

AddModal.propTypes = {};

AddModal.defaultProps = {};

export default AddModal;

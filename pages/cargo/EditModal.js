import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormHelperMessage from "../../components/common/form-elements/formHelperMessage";
import string from "../../utils/stringConstants/language/eng.json";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";

const EditCargoschema = Yup.object().shape({
  cargoID: Yup.string()
    .trim()
    .required(`${string.cargo.cargoId} ${string.errors.required}`),
});

function EditModal({ cargo, state, updateCargo }) {
  if (typeof window === "undefined") {
    return null;
  } else {
    return (
      <div className="modal-dialog modal-md" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title text-dark font-weight-bold"
              id="exampleModalLabel"
            >
              {string.cargo.editCargo}
            </h5>
            <Button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">{string.cancelSign}</span>
            </Button>
          </div>
          <div className="modal-body">
            <Formik
              enableReinitialize={true}
              initialValues={{
                cargoID: cargo.cargoID || "",
              }}
              validationSchema={EditCargoschema}
              onSubmit={(values) => {
                state({
                  cargo: Object.assign({}, cargo, { cargoID: values.cargoID }),
                });
                updateCargo();
                $("#editCargoModal").modal("hide");
              }}
            >
              {({ errors, touched, handleChange, handleSubmit, values }) => (
                <form className="form-container" onSubmit={handleSubmit}>
                  <div className="row ml-0 mr-0 content-block">
                    <div className="form-group col-md-12 p-0">
                      <label
                        htmlFor="cargoID"
                        className="col-md-12 col-form-label pl-0"
                      >
                        {string.cargo.cargoID}
                      </label>
                      <Input
                        type="text"
                        name="cargoID"
                        id="cargoID"
                        // value={cargo.cargoID || ""}
                        value={values.cargoID}
                        onChange={handleChange}
                        className="form-control"
                        placeholder={string.cargo.cargoId}
                      // onChange={(event) => {
                      //     state({
                      //         cargo: Object.assign({}, cargo, { cargoID: event.target.value })
                      //     });
                      // }}
                      />
                      {errors.cargoID && touched.cargoID ? (
                        <FormHelperMessage
                          message={errors.cargoID}
                          className="error"
                        />
                      ) : null}
                    </div>
                  </div>
                  <div className="modal-footer">
                    <Button
                      className="btn btn-primary large-btn"
                      // onClick={updateCargo}
                      type="submit"
                    // data-dismiss="modal"
                    >
                      {string.updateBtnTxt}
                    </Button>
                  </div>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

EditModal.propTypes = {};

EditModal.defaultProps = {};

export default EditModal;

import PropTypes from "prop-types";
import DeleteModal from "../../components/common/DeleteModal";
import AddEventCategoryModal from "../../components/categories/addEventCategoryModal";
import AddShipmentCategoryModal from "../../components/categories/addShipmentCategoryModal";
import AddEventModal from "../../components/categories/addEventModal";
import AddDocTypeModal from "../../components/categories/addTypeModal";
import EventCategoryTable from "../../components/categories/eventCategoryTable";
import ShipmentCategoryTable from "../../components/categories/shipmentCategoryTable";
import DocumentCategoryTable from "../../components/categories/documentCategoryTable";
import AddShipmentCategoryMapModal from "../../components/categories/addShipmentCategoryMapModal";
import AddDocumentCategoryMapModal from "../../components/categories/addDocumentCategoryMapModal";
import AddDocumentCategoryModal from "../../components/categories/addDocumentCategoryModal";
import NProgress from "nprogress";
import notify from "../../lib/notifier";
import string from "../../utils/stringConstants/language/eng.json";

import withAuth from "../../lib/withAuth";
import {
	fetchEventCategories,
	addEventCategory,
	removeEventCategory,
	updateEventCategory,
	updateEvent,
	addEvent,
	removeEvent,
} from "../../lib/api/event-category";
import {
	fetchShipmentCategories,
	addShipmentCategory,
    addShipmentDocumentCategory,
	addShipmentEventCategory,
	removeShipmentEventCategory,
    removeShipmentDocumentCategory,
	removeShipmentCategory,
	updateShipmentCategory,
} from "../../lib/api/shipment-category";
import {
	fetchDocumentCategories,
	addDocumentCategory,
	removeDocumentCategory,
	updateDocumentCategory,
} from "../../lib/api/document-category";

import AuthPage from "../../components/wrapper/AuthPage";
import { useEffect, useState } from "react";
// updated
import Button from "../../components/common/form-elements/button/Button";
import Input from "../../components/common/form-elements/input/Input";

const EventCategoryPage = (props) => {
	const { user } = props;
	const [event_categories, setEvent_categories] = useState([]);
	const [event, setEvent] = useState({});
	const [shipment_categories, setShipment_categories] = useState([]);
	const [document_categories, setDocument_categories] = useState([]);
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState("");
	const [eventCategory, setEventCategory] = useState({});
	const [shipmentCategory, setShipmentCategory] = useState({});
	const [deleteMode, setDeleteMode] = useState("");
	const [editMode, setEditMode] = useState("");
	const [selectedIndex, setSelectedIndex] = useState("");
	const [categoryIndex, setCategoryIndex] = useState("");
	const [eventCategoryModal, setEventCategoryModal] = useState(false);
	const [shipmentCategoryModal, setShipmentCategoryModal] = useState(false);
	const [documentCategoryModal, setDocumentCategoryModal] = useState(false);
	const [shipmentCategoryMapModal, setShipmentCategoryMapModal] = useState(false);
    const [documentCategoryMapModal, setDocumentCategoryMapModal] = useState(false);
	const [eventModal, setEventModal] = useState(false);
	const [docEventModal, setDocEventModal] = useState(false);
	const [values, setValues] = useState({});
	useEffect(() => {
		NProgress.start();
		_fetchEventCategories();
		_fetchShipmentCategories();
		_fetchDocumentCategories();
	}, []);

	const _fetchEventCategories = async () => {
		try {
			const eventCategories = await fetchEventCategories();
			setEvent_categories(eventCategories);
			NProgress.done();
		} catch (err) {
			setLoading(false);
			setError(err.message || err.toString());
			NProgress.done();
		}
	};

	const _fetchShipmentCategories = async () => {
		try {
			const shipmentCategories = await fetchShipmentCategories();
			setShipment_categories(shipmentCategories);
			NProgress.done();
		} catch (err) {
			setLoading(false);
			setError(err.message || err.toString());
			NProgress.done();
		}
	};

	const _fetchDocumentCategories = async () => {
		try {
			const documentCategories = await fetchDocumentCategories();
			setDocument_categories(documentCategories);
			NProgress.done();
		} catch (err) {
			setLoading(false);
			setError(err.message || err.toString());
			NProgress.done();
		}
	};

	//Add/Update Event Category
	const _onEventCategorySubmit = (e) => {
		const { name } = values;
		if (editMode === "eventCategory") {
			_updateEventCategory();
		} else {
			_addEventCategory(eventCategory);
		}
	};

	//Add/Update Shipment Category
	const _onShipmentCategorySubmit = (e) => {
		const { name } = values;
		if (editMode === "shipmentCategory") {
			_updateShipmentCategory();
		} else {
			_addShipmentCategory(name);
		}
	};

	//Add/Update Document Category
	const _onDocumentCategorySubmit = (e) => {
		if (editMode === "documentCategory") {
			_updateDocumentCategory();
		} else {
			_addDocumentCategory(name);
		}
	};

	//Map event category with shipment category
	const _onShipmentCategoryMapSubmit = (e) => {
		const { value } = values;
		_addShipmentEventCategory(value);
	};

    const _onDocumentCategoryMapSubmit = (e) => {
        const { value } = values;
		_addShipmentDocumentCategory(value);
    }

	const _onEventSubmit = (e) => {
		const { name } = values;
		if (editMode === "event" && selectedIndex != null) {
			_updateEvent();
		} else {
			_addEvent(event);
		}
	};

	const _onDocEventSubmit = (e) => {
		const { name } = values;
		if (editMode === "d_event" && selectedIndex != null) {
			_updateDocEvent();
		} else {
			_addDocEvent(event);
		}
	};

	// Function to delete entry from popup
	const _onDeleteEntry = async (e) => {
		e.preventDefault();
		// check which category to delete
		if (deleteMode == "event") {
			// delete event data
			let eventCategories = event_categories;
			let eventCategory = eventCategories[categoryIndex];
			let events_data = eventCategory.events;
			await removeEvent({ id: events_data[selectedIndex].id });
			events_data.splice(selectedIndex, 1);
			eventCategory.events = events_data;
			eventCategories[categoryIndex] = eventCategory;
			setEvent_categories(eventCategories);
			setDeleteMode("");
			notify("Event deleted successfully!");
		} else if (deleteMode == "d_event") {
			// delete event data
			let documentCategories = document_categories;
			let documentCategory = documentCategories[categoryIndex];
			let events_data = documentCategory.events;
			await removeEvent({ id: events_data[selectedIndex].id });
			events_data.splice(selectedIndex, 1);
			documentCategory.events = events_data;
			documentCategories[categoryIndex] = documentCategory;
			setDocument_categories(documentCategories);
			setDeleteMode("");
			notify("Event deleted successfully!");
		} else if (deleteMode == "eventCategory") {
			// delete event category data
			let category = event_categories[selectedIndex];
			await removeEventCategory({ id: category.id });
			event_categories.splice(selectedIndex, 1);
			setEvent_categories(event_categories);
			setDeleteMode("");
			_fetchShipmentCategories();
			notify("Category deleted successfully!");
		} else if (deleteMode == "shipmentCategory") {
			// delete shipment category data
			let category = shipment_categories[selectedIndex];
			await removeShipmentCategory({ id: category.id });
			shipment_categories.splice(selectedIndex, 1);
			setShipment_categories(shipment_categories);
			setDeleteMode("");
			notify("Category deleted successfully!");
		} else if (deleteMode == "eventCategoryMap") {
			// delete shipment category data
			let shipmentCategories = shipment_categories;
			let selectCategory = shipmentCategories[categoryIndex];
			let categoryData = selectCategory.shipment_event_categories;
			await removeShipmentEventCategory({
				id: categoryData[selectedIndex].id,
			});
			categoryData.splice(selectedIndex, 1);
			selectCategory.shipment_event_categories = categoryData;
			shipmentCategories[categoryIndex] = selectCategory;
			setShipment_categories(shipmentCategories);
			setDeleteMode("");
			notify("Category deleted successfully!");
		} else if (deleteMode == "documentCategory") {
			// delete document category data
			let category = document_categories[selectedIndex];
			await removeDocumentCategory({ id: category.id });
			document_categories.splice(selectedIndex, 1);
			setDocument_categories(document_categories);
			setDeleteMode("");
			notify("Category deleted successfully!");
		} else if (deleteMode == 'documentCategoryMap') {
			// delete shipment category data
			let shipmentCategories = shipment_categories;
			let selectCategory = shipmentCategories[categoryIndex];
			let categoryData = selectCategory.shipment_document_categories;
			await removeShipmentDocumentCategory({
				id: categoryData[selectedIndex].id,
			});
			categoryData.splice(selectedIndex, 1);
			selectCategory.shipment_document_categories = categoryData;
			shipmentCategories[categoryIndex] = selectCategory;
			setShipment_categories(shipmentCategories);
			setDeleteMode("");
			notify("Category deleted successfully!");
		}
	};

	//Add Shipment Event Category
	const _addShipmentEventCategory = async (data) => {
		NProgress.start();
		try {
			let categoryData = values;
			let shipmentCategories = shipment_categories;
			categoryData.shipment_category_id = shipmentCategories[categoryIndex].id;
			const categoryObj = await addShipmentEventCategory(categoryData);

			if (categoryObj.code == 1) {
				_fetchShipmentCategories();
				setValues({});
				_toggleShipmentCategoryMap({});
				notify("Category added successfully!");
			} else {
				notify(categoryObj.message);
			}
			NProgress.done();
		} catch (err) {
			notify("Error adding Category!");
			NProgress.done();
		}
	};

    const _addShipmentDocumentCategory = async () =>{
        NProgress.start();
		try {

            let categoryData = values;
			let shipmentCategories = shipment_categories;
			categoryData.shipment_category_id = shipmentCategories[categoryIndex].id;
			const categoryObj = await addShipmentDocumentCategory(values);

			if (categoryObj.code == 1) {
				_fetchShipmentCategories();
				setValues({});
				_toggleDocumentCategoryMap({});
				notify("Category added successfully!");
			} else {
				notify(categoryObj.message);
			}
			NProgress.done();
		} catch (err) {
			notify("Error adding Category!");
			NProgress.done();
		}
    }

	// add document category function
	const _addDocumentCategory = async (data) => {
		NProgress.start();
		try {
			const category = await addDocumentCategory(values);
			_fetchDocumentCategories();
			setValues({});
			_toggleDocumentCategory({});
			notify("Category added successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error adding category!");
			NProgress.done();
		}
	};

	// update document category function
	const _updateDocumentCategory = async () => {
		NProgress.start();
		try {
			const category = await updateDocumentCategory(values);
			_fetchDocumentCategories();
			setValues({});
			_toggleDocumentCategory({});
			notify("Category updated successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error adding category!");
			NProgress.done();
		}
	};

	// add event category function
	const _addEventCategory = async (data) => {
		NProgress.start();
		try {
			const category = await addEventCategory(values);
			_fetchEventCategories();
			setValues({});
			_toggleEventCategory({});
			notify("Category added successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error adding category!");
			NProgress.done();
		}
	};

	// update event category function
	const _updateEventCategory = async () => {
		NProgress.start();
		try {
			const category = await updateEventCategory(values);
			_fetchEventCategories();
			setValues({});
			_toggleEventCategory({});
			notify("Category updated successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error adding category!");
			NProgress.done();
		}
	};

	// add shipment category function
	const _addShipmentCategory = async (data) => {
		NProgress.start();
		try {
			const category = await addShipmentCategory(values);
			_fetchShipmentCategories();
			setValues({});
			_toggleShipmentCategory({});
			notify("Category added successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error adding category!");
			NProgress.done();
		}
	};

	// update shipment category function
	const _updateShipmentCategory = async () => {
		NProgress.start();
		try {
			await updateShipmentCategory(values);
			_fetchShipmentCategories();
			setValues({});
			_toggleShipmentCategory({});
			notify("Category updated successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error adding category!");
			NProgress.done();
		}
	};

	// add event function
	const _addEvent = async (data) => {
		NProgress.start();
		try {
			let eventData = values;
			let eventCategories = event_categories;
			eventData.event_category_id = eventCategories[categoryIndex].id;
			const event = await addEvent(eventData);
			_fetchEventCategories();
			setValues({});
			_toggleEvent({});
			notify("Event added successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error adding event!");
			NProgress.done();
		}
	};

	// add document event function
	const _addDocEvent = async (data) => {
		NProgress.start();
		try {
			let eventData = values;
			eventData.type = 'document';
			let documentCategories = document_categories;
			eventData.event_category_id = documentCategories[categoryIndex].id;
			const event = await addEvent(eventData);
			_fetchDocumentCategories();
			setValues({});
			_toggleDocEvent({});
			notify("Event added successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error adding event!");
			NProgress.done();
		}
	};

	// update event function
	const _updateEvent = async () => {
		NProgress.start();
		try {
			await updateEvent(values);
			_fetchEventCategories();
			setValues({});
			_toggleEvent({});
			notify("Event updated successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error updating event!");
			NProgress.done();
		}
	};

	// update doc event function
	const _updateDocEvent = async () => {
		NProgress.start();
		try {
			await updateEvent(values);
			_fetchDocumentCategories();
			setValues({});
			_toggleDocEvent({});
			notify("Event updated successfully!");
			NProgress.done();
		} catch (err) {
			notify("Error updating event!");
			NProgress.done();
		}
	};

	// set delete mode upon selecting delete icon
	const _setDeleteMode = (mode, i, cat) => {
		if (mode == "eventCategory") {
			setDeleteMode(mode);
			setSelectedIndex(i);
		} else if (mode == "event") {
			setDeleteMode(mode);
			setSelectedIndex(i);
			setCategoryIndex(cat);
		} else if (mode == "d_event") {
			setDeleteMode(mode);
			setSelectedIndex(i);
			setCategoryIndex(cat);
		} else if (mode == "shipmentCategory") {
			setDeleteMode(mode);
			setSelectedIndex(i);
		} else if (mode == "eventCategoryMap") {
			setDeleteMode(mode);
			setSelectedIndex(i);
			setCategoryIndex(cat);
        } else if (mode == 'documentCategoryMap'){
            setDeleteMode(mode);
			setSelectedIndex(i);
			setCategoryIndex(cat);
		} else if (mode == "documentCategory") {
			setDeleteMode(mode);
			setSelectedIndex(i);
		}
	};

	const _setEditMode = (mode, i, j, cat) => {
		if (mode) {
			setValues({ ...cat });
			setCategoryIndex(i);
			if (mode == "event") {
				if (j != null) {
					setEditMode(mode);
					setSelectedIndex(j);
					let events = event_categories[i].events;
					let event = events[i];
					setEvent(event);
					_toggleEvent(event);
				} else {
                    setEditMode("");
					setEvent({});
					_toggleEvent({});
				}
			} else if (mode == "d_event") {
				if (j != null) {
					setEditMode(mode);
					setSelectedIndex(j);
					let events = document_categories[i].events;
					let event = events[i];
					setEvent(event);
					_toggleDocEvent(event);
				} else {
					setEvent({});
                    setEditMode("");
					_toggleDocEvent({});
				}
			} else if (mode == "eventCategory") {
				setEditMode(mode);
				_toggleEventCategory(cat);
			} else if (mode == "shipmentCategory") {
				setEditMode(mode);
				_toggleShipmentCategory(cat);
			} else if (mode == "eventCategoryMap") {
				setEditMode(mode);
				_toggleShipmentCategoryMap(cat);
			} else if (mode == "documentCategory") {
				setEditMode(mode);
				_toggleDocumentCategory(cat);
			} else if (mode == 'documentCategoryMap') {
                setEditMode(mode);
				_toggleDocumentCategoryMap(cat);
            }
		}
	};

	const _toggleEvent = (e, cat) => {
		setEventModal(!eventModal);
	};

	const _toggleDocEvent = (e, cat) => {
		setDocEventModal(!docEventModal);
	};

	const _toggleEventCategory = (e, cat) => {
		setEventCategoryModal(!eventCategoryModal);
	};

	const _toggleShipmentCategory = (e, cat) => {
		setShipmentCategoryModal(!shipmentCategoryModal);
	};

	const _toggleDocumentCategory = (e, cat) => {
		setDocumentCategoryModal(!documentCategoryModal);
	};

	const _toggleShipmentCategoryMap = (e, cat) => {
		setShipmentCategoryMapModal(!shipmentCategoryMapModal);
	};

    const _toggleDocumentCategoryMap = (e,cat) => {
        setDocumentCategoryMapModal(!documentCategoryMapModal);
    }

	const _handleSubmitBtnClick = (type) => {
		if (type === "shipmentcat") {
			_toggleShipmentCategory();
		} else if (type == "eventcat") {
			_toggleEventCategory();
		} else if (type == "doccat") {
			_toggleDocumentCategory();
		}
		setEditMode("");
		setValues({});
	};

	return (
		<AuthPage user={user} title={string.categoriesPageTitle}>
			<div className="container-fluid">
				<div className="row d-flex shipment-listing">
					<ul
						className="nav nav-tabs w-100"
						id="myTab"
						role="tablist"
					>
						<li className="nav-item">
							<a
								className="nav-link active"
								id="shipmentCategory"
								data-toggle="tab"
								href="#shipmentCategories"
								role="tab"
								aria-controls="shipmentCategories"
								aria-selected="true"
							>
								{string.shipmentCatTitle}
							</a>
						</li>
						<li className="nav-item">
							<a
								className="nav-link"
								id="eventCategory"
								data-toggle="tab"
								href="#eventCategories"
								role="tab"
								aria-controls="eventCategories"
							>
								{string.eventCatTitle}
							</a>
						</li>
						<li className="nav-item">
							<a
								className="nav-link"
								id="eventCategory"
								data-toggle="tab"
								href="#documentCategories"
								role="tab"
								aria-controls="eventCategories"
							>
								{string.docCatTitle}
							</a>
						</li>
					</ul>
					<div className="tab-content w-100" id="myTabContent">
						<div
							className="tab-pane fade show active mt-3 w-100"
							id="shipmentCategories"
							role="tabpanel"
							aria-labelledby="event-listing"
						>
							<div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
								<h4 className="text-dark">
									{string.shipmentCatTitle}
								</h4>
								<Button
									className="btn btn-primary large-btn"
									onClick={() =>
										_handleSubmitBtnClick("shipmentcat")
									}
								>
									{string.submitCatBtn}
								</Button>
							</div>
							<div className="shipment-table-listing table-responsive mt-2 w-100">
								<ShipmentCategoryTable
									shipment_categories={shipment_categories}
									setEditMode={_setEditMode}
									setDeleteMode={_setDeleteMode}
									string={string}
								/>
							</div>
						</div>
						<div
							className="tab-pane fade mt-3 w-100"
							id="eventCategories"
							role="tabpanel"
							aria-labelledby="event-listing"
						>
							<div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
								<h4 className="text-dark">
									{string.eventCatTitle}
								</h4>
								<Button
									className="btn btn-primary large-btn"
									onClick={() =>
										_handleSubmitBtnClick("eventcat")
									}
								>
									{string.submitCatBtn}
								</Button>
							</div>
							<div className="shipment-table-listing table-responsive mt-2 w-100">
								<EventCategoryTable
									event_categories={event_categories}
									setEditMode={_setEditMode}
									setDeleteMode={_setDeleteMode}
									string={string}
								/>
							</div>
						</div>
						<div
							className="tab-pane fade mt-3 w-100"
							id="documentCategories"
							role="tabpanel"
							aria-labelledby="event-listing"
						>
							<div className="col-md-12 add-shipment d-flex align-items-center justify-content-between p-0 event-filter ">
								<h4 className="text-dark">
									{string.docCatTitle}
								</h4>
								<Button
									className="btn btn-primary large-btn"
									onClick={() =>
										_handleSubmitBtnClick("doccat")
									}
								>
									{string.submitCatBtn}
								</Button>
							</div>
							<div className="shipment-table-listing table-responsive mt-2 w-100">
								<DocumentCategoryTable
									document_categories={document_categories}
									setEditMode={_setEditMode}
									setDeleteMode={_setDeleteMode}
									string={string}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<DeleteModal
				toggle={() => setDeleteMode("")}
				isOpen={!!deleteMode}
				onDeleteEntry={_onDeleteEntry}
			/>
			<AddEventCategoryModal
				handleChange={({ target: { name, value } }) =>
					setValues({
						...values,
						[name]: value,
					})
				}
				onCategorySubmit={_onEventCategorySubmit}
				isOpen={eventCategoryModal}
				toggle={_toggleEventCategory}
				values={values}
				editMode={editMode}
				string={string}
			/>
			<AddShipmentCategoryModal
				handleChange={({ target: { name, value } }) =>
					setValues({
						...values,
						[name]: value,
					})
				}
				onCategorySubmit={_onShipmentCategorySubmit}
				isOpen={shipmentCategoryModal}
				toggle={_toggleShipmentCategory}
				values={values}
				string={string}
				editMode={editMode}
			/>
			<AddDocumentCategoryModal
				handleChange={({ target: { name, value } }) =>
					setValues({
						...values,
						[name]: value,
					})
				}
				onCategorySubmit={_onDocumentCategorySubmit}
				isOpen={documentCategoryModal}
				toggle={_toggleDocumentCategory}
				values={values}
				editMode={editMode}
				string={string}
			/>
			<AddShipmentCategoryMapModal
				setValues={setValues}
				onCategorySubmit={_onShipmentCategoryMapSubmit}
				isOpen={shipmentCategoryMapModal}
				toggle={_toggleShipmentCategoryMap}
				values={values}
				event_categories={event_categories}
				editMode={editMode}
				string={string}
			/>
            <AddDocumentCategoryMapModal
				setValues={setValues}
				onCategorySubmit={_onDocumentCategoryMapSubmit}
				isOpen={documentCategoryMapModal}
				toggle={_toggleDocumentCategoryMap}
				values={values}
				document_categories={document_categories}
				editMode={editMode}
				string={string}
			/>
			<AddEventModal
				handleChange={({ target: { name, value } }) =>
					setValues({
						...values,
						[name]: value,
					})
				}
				onEventSubmit={_onEventSubmit}
				isOpen={eventModal}
				toggle={_toggleEvent}
				values={values}
				string={string}
				editMode={selectedIndex != null ? editMode : ""}
			/>
			<AddDocTypeModal
				handleChange={({ target: { name, value } }) =>
					setValues({
						...values,
						[name]: value,
					})
				}
				onEventSubmit={_onDocEventSubmit}
				isOpen={docEventModal}
				toggle={_toggleDocEvent}
				values={values}
				string={string}
				editMode={selectedIndex != null ? editMode : ""}
			/>
		</AuthPage>
	);
};

EventCategoryPage.getInitialProps = (ctx) => {
	const eventCategoryPage = true;
	return { eventCategoryPage };
};

EventCategoryPage.propTypes = {
	user: PropTypes.shape({
		id: PropTypes.string,
	}),
};

EventCategoryPage.defaultProps = {
	user: null,
	eventCategory: {},
	shipmentCategory: {},
};

export default withAuth(EventCategoryPage, { loginRequired: true });

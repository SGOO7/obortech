export const sideMenuData = {
    statusCode: 200,
    message: 'Successfully fetched',
    data: [{
        "id": 9,
        "name": "Main Folder",
        "subFolder": [{
            "id": 10,
            "name": "Sub Folder A",
            "parent": 9,
            "shipments": [{
                "id": 12,
                "name": 'Sub shipment A',
                "parent": 10
            }]
        }, {
            "id": 11,
            "name": "Sub Folder B",
            "parent": 9,
            "shipments": [{
                "id": 13,
                "name": 'Sub Shipment B',
                "parent": 11
            }]
        }],
        "shipments": [{
            "id": 14,
            "name": 'Main Shipment',
            "parent": 9
        }]
    }]
}
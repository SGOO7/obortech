import moment from "moment";

export const _momentDate = (date = new Date()) => {
    return moment(date);
}

export const _momentDateFormat = (date = new Date(), format = "YYYY-DD-MM HH:mm:ss") => {
    return _momentDate(date).format(format);
}

export const _momentGetDiff = (startDate = new Date(), endDate, limit) => {
    return _momentDate(startDate).diff(_momentDate(endDate), limit)
}

export const _checkValidStatus = ({statusCode}) =>{
    return statusCode === 200;
}
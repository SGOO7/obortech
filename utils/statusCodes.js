const strings = require('./stringConstants/language/eng.json');

const statusCode = {
    success: 1,
    emptyData: { code: 400, message: strings.statusResponses.emptyData },
    unAuthorized: { code: 401, message: strings.statusResponses.unAuthorized },
    successData: { code: 200, message: strings.statusResponses.success },
    createdData: { code: 201, message: strings.statusResponses.created },
    notFound: { code: 404, message: strings.statusResponses.notFound },
    serverError: { code: 500, message: strings.statusResponses.serverError },
}

module.exports = statusCode;

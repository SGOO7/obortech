import { FETCH_CITIES_REQUEST, FETCH_CITIES_SUCCESS, FETCH_CITIES_FAILURE, ERROR_RESPONSE } from "../types";
import { getAllCitiesApi } from "../../lib/api/city";

const fetchCitiesRequest = () => {
    return {
      type: FETCH_CITIES_REQUEST
    }
}
  
const fetchCitiesSuccess = data => {
    return {
      type: FETCH_CITIES_SUCCESS,
      payload: data
    }
}

const fetchCitiesFailure = data => {
    return {
      type: FETCH_CITIES_FAILURE,
      payload: data
    }
}

export const getAllCities = code => async dispatch => {
    try {
        dispatch(fetchCitiesRequest())
        const response = await getAllCitiesApi(code);
        if(response.code === 200){
            dispatch(fetchCitiesSuccess(response.data))
        }else{
            dispatch(fetchCitiesFailure(response.data))
        }
    } catch (err) {
        dispatch(fetchCitiesFailure({ exception: err }))
    }
}

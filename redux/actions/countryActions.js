import { FETCH_COUNTRIES_REQUEST, FETCH_COUNTRIES_SUCCESS, FETCH_COUNTRIES_FAILURE, ERROR_RESPONSE } from "../types";
import { getAllCountriesApi } from "../../lib/api/country";

const fetchCountriesRequest = () => {
    return {
      type: FETCH_COUNTRIES_REQUEST
    }
}
  
const fetchCountriesSuccess = data => {
    return {
      type: FETCH_COUNTRIES_SUCCESS,
      payload: data
    }
}
  
const fetchCountriesFailure = data => {
    return {
      type: FETCH_COUNTRIES_FAILURE,
      payload: data
    }
}

export const getAllCountries = () => async dispatch => {
    try {
        dispatch(fetchCountriesRequest())
        const response = await getAllCountriesApi();
        if(response.code === 200){
            dispatch(fetchCountriesSuccess(response.data))
        }else{
            dispatch(fetchCountriesFailure(response.data))
        }
    } catch (err) {
        dispatch(fetchCountriesFailure({ exception: err }))
    }
}

import { FETCH_COUNTRIES_REQUEST, FETCH_COUNTRIES_SUCCESS, FETCH_COUNTRIES_FAILURE, ERROR_RESPONSE } from "../types";

const initialState = {
    loading: false,
    payload: null,
    error: null
}

export const countryReducer = (state = initialState, { payload, type }) => {
    switch(type) {
        case FETCH_COUNTRIES_REQUEST: {
            return {
                ...state,
                loading: true,
            }
        }
        case FETCH_COUNTRIES_SUCCESS: {
            return {
                ...state,
                loading: false,
                payload: payload,
                error: null
            }
        }
        case FETCH_COUNTRIES_FAILURE: {
            return {
                ...state,
                loading: false,
                payload: null,
                error: payload
            }
        }
        default:
            return state;
    }
}
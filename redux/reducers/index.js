import { combineReducers } from 'redux';
import { countryReducer } from './countryReducer';
import { cityReducer } from './cityReducer';
import { siginupReducer } from './signupReducer';
import { roleReducer } from './roleReducer';
import { userTypeReducer } from './userTypeReducer';
import { mobileVerificationReducer } from './mobileVerificationReducer'
import { emailVerificationReducer } from './emailVerificationReducer'
import { userCheckReducer } from "./userCheckReducer"
import { signupVerifiedUser } from "./signupVerifiedUserReducer"

const rootReducer = combineReducers({
    countries: countryReducer,
    cities: cityReducer,
    types: userTypeReducer,
    roles: roleReducer,
    signup: siginupReducer,
    mobile: mobileVerificationReducer,
    email:  emailVerificationReducer,
    user: userCheckReducer,
    signedUser: signupVerifiedUser
})

export default rootReducer

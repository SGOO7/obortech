import { FETCH_CITIES_REQUEST, FETCH_CITIES_SUCCESS, FETCH_CITIES_FAILURE, ERROR_RESPONSE } from "../types";

const initialState = {
    loading: false,
    payload: null,
    error: null
}

export const cityReducer = (state = initialState, { payload, type }) => {
    switch(type) {
        case FETCH_CITIES_REQUEST: {
            return {
                ...state,
                loading: true,
            }
        }
        case FETCH_CITIES_SUCCESS: {
            return {
                ...state,
                loading: false,
                payload: payload,
                error: null
            }
        }
        case FETCH_CITIES_FAILURE: {
            return {
                ...state,
                loading: false,
                payload: null,
                error: payload
            }
        }
        default:
            return state;
    }
}
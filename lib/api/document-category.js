import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/document-category';

export const addDocumentCategory = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchDocumentCategories = () =>
  sendRequest(`${BASE_PATH}/fetch`, {
    method: 'GET',
  });

export const removeDocumentCategory = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateDocumentCategory = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });
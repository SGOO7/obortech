import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/types';

export const getAllTypesApi = () =>
    sendRequest(`${BASE_PATH}/`, {
        method: 'GET',
    }
)
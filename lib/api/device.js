import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/device';

export const addDevice = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchDevices = () =>
  sendRequest(`${BASE_PATH}/fetch`, {
    method: 'GET',
  });

export const removeDevice = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateDevice = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });
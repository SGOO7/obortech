import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/countries';

export const getAllCountriesApi = () =>
  sendRequest(`${BASE_PATH}`, {
    method: 'GET',
  }
);

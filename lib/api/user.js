import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/user';

export const addUser = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchUsers = () =>
  sendRequest(`${BASE_PATH}/fetch`, {
    method: 'GET',
  });

export const removeUser = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateUser = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });
import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/event-category';

export const addEventCategory = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchEventCategories = () =>
  sendRequest(`${BASE_PATH}/fetch`, {
    method: 'GET',
  });

export const removeEventCategory = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateEventCategory = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });

export const addEvent = data =>
  sendRequest(`${BASE_PATH}/add-event`, {
    body: JSON.stringify(data),
  });

export const updateEvent = data =>
  sendRequest(`${BASE_PATH}/event-update`, {
    body: JSON.stringify(data),
  });

export const removeEvent = data =>
  sendRequest(`${BASE_PATH}/remove-event`, {
    body: JSON.stringify(data),
  });
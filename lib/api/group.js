import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/group';

export const addGroup = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchGroups = () =>
  sendRequest(`${BASE_PATH}/fetch`, {
    method: 'GET',
  });

export const removeGroup = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateGroup = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });
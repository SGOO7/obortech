import { _checkValidStatus } from "../../utils/globalFunc";
import { sideMenuData } from "../../utils/sideMenuData"

export const getSideMenu = (sideData) => {
    try{
        const res = sideData || sideMenuData;
        if(_checkValidStatus(res)) {
            return res.data;
        }else {
            return res;
        }
    } catch(err) {
        console.error("Err in getSideMenu => ", err);
        return err;
    }
}

export const addFolderSideMenu = (data) => {
    try{
        if(name){
            let oldArr = sideMenuData;
            if(data.type === 'subFolder'){
                oldArr = oldArr.map(it => {
                    if(data.parent === it.id) {
                        it.subFolder.push(data)
                    }
                    return it;
                })
            }else if(data.type === 'shipment') {
                if(data.isUnderSub){
                    oldArr = oldArr.map(it => {
                        it.subFolder.map(subIt => {
                            if(subIt.id === data.parent){
                                subIt.shipments.push(data);
                            }
                            return subIt;
                        })
                        return it;
                    })
                }else {
                    oldArr = oldArr.map(it => {
                        if (data.parent === it.id) {
                            it.shipments.push(data)
                        }
                        return it;
                    })
                }
            }
            return oldArr;
            // const newFolder = 
        }else {
            return sideMenuData;
        }
    }catch (err) {
        console.error("Err in addFolderSideMenu => ", err);
        return err;
    }
}
import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/signup';

export const verifyEmailIdApi = data =>
  sendRequest(`${BASE_PATH}/verify/email`, {
    method: 'POST',
    body: JSON.stringify(data),
  }
)

export const signupVerifedUserApi = data =>
    sendRequest(`${BASE_PATH}/user`, {
        method: 'POST',
        body: JSON.stringify(data),
    }
)

export const verifyUserExistsApi = data =>
    sendRequest(`${BASE_PATH}/verifyUser`, {
        method: 'POST',
        body: JSON.stringify(data),
    }
)

  
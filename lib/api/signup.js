import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/organization';

export const createOrgAndUserApi = data =>
    sendRequest(`${BASE_PATH}/orgUser`, {
        method: 'POST',
        body: JSON.stringify(data),
    }
)

export const existsByNameApi = name => 
    sendRequest(`${BASE_PATH}/existsByName?name=${name}`, {
        method: 'GET',
    }
)

export const existsByUsernameApi = username =>
    sendRequest(`${BASE_PATH}/existsByUsername?username=${username}`, {
        method: 'GET',
    }
)

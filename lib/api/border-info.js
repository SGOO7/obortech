import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/shipment-road';

export const fetchBorderInfo = data =>
    sendRequest(`${BASE_PATH}/fetch`, {
    body: JSON.stringify(data),
});

import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/cargo';

export const addCargo = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchCargos = () =>
  sendRequest(`${BASE_PATH}/fetch`, {
    method: 'GET',
  });

export const removeCargo = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateCargo = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });
import sendRequest from './sendRequest';
import sendRequestWithFile from './sendRequestWithFile';

const BASE_PATH = '/api/v1/shipment-event';

export const addShipmentEvent = data =>
    sendRequestWithFile(`${BASE_PATH}/add`, {
        body: data,
    });

export const fetchShipmentEvents = data =>
    sendRequest(`${BASE_PATH}/fetch`, {
        body: JSON.stringify(data),
    });

export const fetchShipmentDocuments = data =>
    sendRequest(`${BASE_PATH}/fetch-shipment-documents`, {
        body: JSON.stringify(data),
    });

export const addShipmentEventComment = data =>
    sendRequest(`${BASE_PATH}/add-comment`, {
        body: JSON.stringify(data),
    });

export const acceptShipmentEventDocument = data =>
    sendRequest(`${BASE_PATH}/accept-document`, {
        body: JSON.stringify(data),
    });

export const seenShipmentEventDocument = data =>
    sendRequest(`${BASE_PATH}/seen-document`, {
        body: JSON.stringify(data),
    });

export const removeShipmentEvent = data =>
    sendRequest(`${BASE_PATH}/remove`, {
        body: JSON.stringify(data),
    });

export const updateShipmentEvent = data =>
    sendRequest(`${BASE_PATH}/update`, {
        body: JSON.stringify(data),
    });

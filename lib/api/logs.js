import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/shipment-logs';


export const fetchLocationLogs = ({container, shipment}) =>
    sendRequest(`${BASE_PATH}/fetch/location?container_id=${container}&shipment_id=${shipment}`, {
        method: 'GET'
    })

export const fetchTemperatureLogs = ({container, shipment, start_date, end_date}) =>
    sendRequest(`${BASE_PATH}/fetch/temperature?container_id=${container}&shipment_id=${shipment}&start_date=${start_date}&end_date=${end_date}`, {
        method: 'GET'
    })

export const fetchHumidityLogs = ({container, shipment, start_date, end_date}) =>
    sendRequest(`${BASE_PATH}/fetch/humidity?container_id=${container}&shipment_id=${shipment}&start_date=${start_date}&end_date=${end_date}`, {
        method: 'GET'
    })

export const fetchLatestStats = ({container, shipment}) =>
    sendRequest(`${BASE_PATH}/fetch/lateststats?container_id=${container}&shipment_id=${shipment}`, {
        method: 'GET'
    })
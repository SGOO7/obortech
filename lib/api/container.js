import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/container';

export const addContainer = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchContainers = () =>
  sendRequest(`${BASE_PATH}/fetch`, {
    method: 'GET',
  });

export const removeContainer = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateContainer = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });
import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/participant-category';

export const addParticipantCategory = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchParticipantCategories = () =>
  sendRequest(`${BASE_PATH}/fetch`, {
    method: 'GET',
  });

export const removeParticipantCategory = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateParticipantCategory = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });
import sendRequest from './sendRequest';
import sendRequestWithFile from './sendRequestWithFile';

const BASE_PATH = '/api/v1/auth';

export const getBookList = () =>
  sendRequest(`${BASE_PATH}/books`, {
    method: 'GET',
  });

export const signup = data =>
  sendRequest(`${BASE_PATH}/signup`, {
    body: JSON.stringify(data),
  });

export const login = data =>
  sendRequest(`${BASE_PATH}/login`, {
    body: JSON.stringify(data),
  });

export const notification = data =>
  sendRequest(`${BASE_PATH}/notifications`, {
    body: JSON.stringify(data),
  });

export const markNotificationAsRead = data =>
  sendRequest(`${BASE_PATH}/marknotificationasread`, {
    body: JSON.stringify(data),
  });

export const forgotpassword = data =>
  sendRequest(`${BASE_PATH}/forgotpassword`, {
    body: JSON.stringify(data),
  });

export const resetpassword = data =>
  sendRequest(`${BASE_PATH}/resetpassword`, {
    body: JSON.stringify(data),
  });

export const fetchProfile = ({ slug }) =>
  sendRequest(`${BASE_PATH}/profile/${slug}`, {
    method: 'GET',
  });

export const saveProfile = data =>
  sendRequest(`${BASE_PATH}/profile`, {
    body: JSON.stringify(data),
  });

export const uploadImage = data =>
  sendRequestWithFile(`${BASE_PATH}/uploadimage`, {
    body: data,
  });

export const sendOtpApi = data =>
  sendRequest(`${BASE_PATH}/sendotp`, {
    method: 'POST',
    body: JSON.stringify(data),
  });

  
export const verifyOtpApi = data =>
  sendRequest(`${BASE_PATH}/verifyotp`, {
    method: 'POST',
    body: JSON.stringify(data),
  });

// const BASE_PATH_SIGNUP = '/api/v1/signup';

// export const verifyEmailIdApi = data =>
//   sendRequest(`${BASE_PATH_SIGNUP}/verify/email`, {
//     method: 'POST',
//     body: JSON.stringify(data),
//   }
// )
export default function getRootUrl() {
  const ROOT_URL = process.env.SITE_URL;
  return ROOT_URL;
}
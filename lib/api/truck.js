import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/truck';

export const addTruck = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchTrucks = () =>
  sendRequest(`${BASE_PATH}/fetch`, {
    method: 'GET',
  });

export const removeTruck = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateTruck = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });
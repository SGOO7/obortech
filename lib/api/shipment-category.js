import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/shipment-category';

export const addShipmentCategory = data =>
    sendRequest(`${BASE_PATH}/add`, {
        body: JSON.stringify(data),
    });

export const addShipmentEventCategory = data =>
    sendRequest(`${BASE_PATH}/addShipmentEventCategory`, {
        body: JSON.stringify(data),
    });

export const addShipmentDocumentCategory = data =>
    sendRequest(`${BASE_PATH}/addShipmentDocumentCategory`, {
        body: JSON.stringify(data),
    });

export const fetchShipmentCategories = () =>
    sendRequest(`${BASE_PATH}/fetch`, {
        method: 'GET',
    });

export const fetchShipmentDocumentCategories = data =>
    sendRequest(`${BASE_PATH}/fetchShipmentDocumentCategories`, {
        body: JSON.stringify(data),
    });

export const fetchShipmentEventCategories = data =>
    sendRequest(`${BASE_PATH}/fetchShipmentEventCategories`, {
        body: JSON.stringify(data),
    });

export const removeShipmentCategory = data =>
    sendRequest(`${BASE_PATH}/remove`, {
        body: JSON.stringify(data),
    });

export const removeShipmentEventCategory = data =>
    sendRequest(`${BASE_PATH}/removeShipmentEventCategory`, {
        body: JSON.stringify(data),
    });

export const removeShipmentDocumentCategory = data =>
    sendRequest(`${BASE_PATH}/removeShipmentDocumentCategory`, {
        body: JSON.stringify(data),
    });

export const updateShipmentCategory = data =>
    sendRequest(`${BASE_PATH}/update`, {
        body: JSON.stringify(data),
    });
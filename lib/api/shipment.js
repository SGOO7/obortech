import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/shipment';

export const addShipment = data =>
    sendRequest(`${BASE_PATH}/add`, {
        body: JSON.stringify(data),
    });

export const fetchShipments = () =>
    sendRequest(`${BASE_PATH}/fetch`, {
        method: 'GET',
    });

export const fetchShipment = data =>
    sendRequest(`${BASE_PATH}/fetchOne`, {
        body: JSON.stringify(data),
    });

export const fetchDropdown = data =>
    sendRequest(`${BASE_PATH}/fetchDropdown`, {
        body: JSON.stringify(data),
    });

export const removeShipment = data =>
    sendRequest(`${BASE_PATH}/remove`, {
        body: JSON.stringify(data),
    });

export const updateShipment = data =>
    sendRequest(`${BASE_PATH}/update`, {
        body: JSON.stringify(data),
    });

export const startTracking = data =>
    sendRequest(`${BASE_PATH}/start-tracking`, {
        body: JSON.stringify(data),
    });

export const stopTracking = data =>
    sendRequest(`${BASE_PATH}/stop-tracking`, {
        body: JSON.stringify(data),
    });

/* Shipment Folder Routes */
export const fetchSideMenuShipments = () =>
    sendRequest(`${BASE_PATH}/folder-shipments`, {
        method: 'GET',
    });

export const fetchFolders = () =>
    sendRequest(`${BASE_PATH}/fetch-folders`, {
        method: 'GET',
    });

export const addFolder = data =>
    sendRequest(`${BASE_PATH}/create-folder`, {
        body: JSON.stringify(data),
    });

export const updateFolder = data =>
    sendRequest(`${BASE_PATH}/update-folder`, {
        body: JSON.stringify(data),
    });

export const removeFolder = data =>
    sendRequest(`${BASE_PATH}/remove-folder`, {
        body: JSON.stringify(data),
    });

export const assignShipment = data =>
    sendRequest(`${BASE_PATH}/assign-shipment-folder`, {
        body: JSON.stringify(data),
    });

export const releaseShipment = data =>
    sendRequest(`${BASE_PATH}/remove-shipment-folder`, {
        body: JSON.stringify(data),
    });

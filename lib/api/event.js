import sendRequest from './sendRequest';

const BASE_PATH = '/api/v1/event';

export const addEvent = data =>
  sendRequest(`${BASE_PATH}/add`, {
    body: JSON.stringify(data),
  });

export const fetchEvents = data =>
  sendRequest(`${BASE_PATH}/fetch`, {
    body: JSON.stringify(data),
  });

export const removeEvent = data =>
  sendRequest(`${BASE_PATH}/remove`, {
    body: JSON.stringify(data),
  });

export const updateEvent = data =>
  sendRequest(`${BASE_PATH}/update`, {
    body: JSON.stringify(data),
  });
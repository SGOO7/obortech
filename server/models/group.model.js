module.exports = (sequelize, Sequelize) => {
    const Group = sequelize.define("group", {
        groupID: {
            type: Sequelize.STRING
        }
    });

    return Group;
};
module.exports = (sequelize, Sequelize) => {
    const Device = sequelize.define("device", {
        deviceID: {
            type: Sequelize.STRING
        },
        is_available: {
            type: Sequelize.INTEGER,
            defaultValue: 1
        }
    });

    return Device;
};
module.exports = (sequelize, Sequelize) => {
    const SelectionDevice = sequelize.define("selection_device", {
        selection_id: {
            type: Sequelize.INTEGER
        },
        device_id: {
            type: Sequelize.INTEGER
        },
        data_interval: {
            type: Sequelize.STRING
        }
    });
    
    return SelectionDevice;
};
module.exports = (sequelize, Sequelize) => {
    const ShipmentCategory = sequelize.define("shipment_category", {
        name: {
            type: Sequelize.STRING
        }
    });

    return ShipmentCategory;
};
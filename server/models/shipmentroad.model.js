module.exports = (sequelize, Sequelize) => {
    const ShipmentRoad = sequelize.define("shipment_road", {
        shipment_id: {
            type: Sequelize.INTEGER
        },
        road_id: {
            type: Sequelize.INTEGER
        },
        order: {
            type: Sequelize.INTEGER
        }
    });
    
    return ShipmentRoad;
};
module.exports = (sequelize, Sequelize) => {
    const Organization = sequelize.define("organization", {
        name: {
            type: Sequelize.STRING
        },
        // country: {
        //     type: Sequelize.INTEGER
        // },
        // city: {
        //     type: Sequelize.INTEGER
        // },
        streetAddress: {
            type: Sequelize.STRING
        },
    });
    return Organization;
};

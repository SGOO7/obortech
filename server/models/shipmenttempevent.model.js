module.exports = (sequelize, Sequelize) => {
    const ShipmentTempEvent = sequelize.define("shipment_temp_event", {
        event_id: {
            type: Sequelize.INTEGER
        },
        container_id: {
            type: Sequelize.INTEGER
        },
        shipment_id: {
            type: Sequelize.INTEGER
        },
        road_id: {
            type: Sequelize.INTEGER
        },
        current_temp: {
            type: Sequelize.FLOAT
        },
        current_hum: {
            type: Sequelize.FLOAT
        },
        is_deleted: {
            type: Sequelize.INTEGER
        }
    });
    
    return ShipmentTempEvent;
};
module.exports = (sequelize, Sequelize) => {
    const DocumentAcceptedUser = sequelize.define("document_accepted_user", {
        shipment_event_id: {
            type: Sequelize.INTEGER
        },
        user_id: {
            type: Sequelize.INTEGER
        }
    });
    
    return DocumentAcceptedUser;
};
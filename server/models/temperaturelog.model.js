module.exports = (sequelize, Sequelize) => {
    const TemperatureLog = sequelize.define("temperature_log", {
        shipment_id: {
            type: Sequelize.INTEGER
        },
        container_id: {
            type: Sequelize.INTEGER
        },
        temperature: {
            type: Sequelize.FLOAT
        }
    });
    
    return TemperatureLog;
};
module.exports = (sequelize, Sequelize) => {
    const DocumentSeenUser = sequelize.define("document_seen_user", {
        shipment_event_id: {
            type: Sequelize.INTEGER
        },
        user_id: {
            type: Sequelize.INTEGER
        }
    });
    
    return DocumentSeenUser;
};
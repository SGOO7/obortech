module.exports = (sequelize, Sequelize) => {
    const EventDocumentUser = sequelize.define("event_document_user", {
        shipment_event_id: {
            type: Sequelize.INTEGER
        },
        user_id: {
            type: Sequelize.INTEGER
        }
    });
    
    return EventDocumentUser;
};
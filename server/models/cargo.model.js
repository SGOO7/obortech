module.exports = (sequelize, Sequelize) => {
    const Cargo = sequelize.define("cargo", {
        cargoID: {
            type: Sequelize.STRING
        },
        is_available:{
            type: Sequelize.INTEGER,
            defaultValue: 1
        }
    });

    return Cargo;
};
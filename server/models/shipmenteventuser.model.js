module.exports = (sequelize, Sequelize) => {
    const ShipmentEventUser = sequelize.define("shipment_event_user", {
        shipment_event_id: {
            type: Sequelize.INTEGER
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        created_by: {
            type: Sequelize.INTEGER
        },
        viewed: {
            type: Sequelize.BOOLEAN
        }
    });
    
    return ShipmentEventUser;
};
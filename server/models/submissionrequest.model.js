module.exports = (sequelize, Sequelize) => {
    const SubmissionRequest = sequelize.define("submission_request", {
        worker_id: {
            type: Sequelize.INTEGER
        },
        shipment_id: {
            type: Sequelize.INTEGER
        },
        shipment_event_id: {
            type: Sequelize.INTEGER
        },
        container_id: {
            type: Sequelize.INTEGER
        },
        cargo_id: {
            type: Sequelize.INTEGER
        },
        event_id: {
            type: Sequelize.INTEGER
        },
        event_type: {
            type: Sequelize.STRING
        },
        is_viewed: {
            type: Sequelize.INTEGER
        },
        is_submitted: {
            type: Sequelize.INTEGER
        }
    });
    return SubmissionRequest;
};

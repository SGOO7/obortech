module.exports = (sequelize, Sequelize) => {
	const EventCategory = sequelize.define("event_category", {
		name: {
			type: Sequelize.STRING,
		},
		isReadOnly: {
			type: Sequelize.INTEGER,
		},
	});

	return EventCategory;
};
module.exports = (sequelize, Sequelize) => {
    const SubmissionRequestUser = sequelize.define("submission_request_user", {
        user_id: {
            type: Sequelize.INTEGER
        },
        submission_id: {
            type: Sequelize.INTEGER
        }
    });
    return SubmissionRequestUser;
};
module.exports = (sequelize, Sequelize) => {
    const ShipmentEventComment = sequelize.define("shipment_event_comment", {
        shipment_event_id: {
            type: Sequelize.INTEGER
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        comment: {
            type: Sequelize.STRING
        }
    });
    
    return ShipmentEventComment;
};
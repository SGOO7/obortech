module.exports = (sequelize, Sequelize) => {
    const ShipmentEventCategory = sequelize.define("shipment_event_category", {
        shipment_category_id: {
            type: Sequelize.INTEGER
        },
        event_category_id: {
            type: Sequelize.INTEGER
        }
    });

    return ShipmentEventCategory;
};
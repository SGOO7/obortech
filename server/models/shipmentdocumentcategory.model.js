module.exports = (sequelize, Sequelize) => {
    const ShipmentDocumentCategory = sequelize.define("shipment_document_category", {
        shipment_category_id: {
            type: Sequelize.INTEGER
        },
        document_category_id: {
            type: Sequelize.INTEGER
        }
    });

    return ShipmentDocumentCategory;
};
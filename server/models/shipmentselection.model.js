module.exports = (sequelize, Sequelize) => {
    const ShipmentSelection = sequelize.define("shipment_selection", {
        shipment_id: {
            type: Sequelize.INTEGER
        }
    });
    
    return ShipmentSelection;
};
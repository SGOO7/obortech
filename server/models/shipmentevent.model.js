module.exports = (sequelize, Sequelize) => {
    const ShipmentEvent = sequelize.define("shipment_event", {
        event_id: {
            type: Sequelize.INTEGER
        },
        container_id: {
            type: Sequelize.INTEGER
        },
        shipment_id: {
            type: Sequelize.INTEGER
        },
        road_id: {
            type: Sequelize.INTEGER
        },
        attachment: {
            type: Sequelize.STRING
        },
        attachment_type: {
            type: Sequelize.INTEGER
        },
        file_hash: {
            type: Sequelize.STRING
        },
        isActive: {
            type: Sequelize.INTEGER
        }
    });

    return ShipmentEvent;
};

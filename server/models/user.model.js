module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("user", {
        official_name: {
            type: Sequelize.STRING
        },
        participant_category_id: {
            type: Sequelize.INTEGER
        },
        organization_id: {
            type: Sequelize.INTEGER
        },
        role_id: {
            type: Sequelize.INTEGER
        },
        user_type_id: {
            type: Sequelize.INTEGER
        },
        isEmailVerified: {
            type: Sequelize.BOOLEAN
        },
        isPhoneVerified: {
            type: Sequelize.BOOLEAN
        },
        username: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        mobile: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: 1
        },
        isAdmin: {
            type: Sequelize.INTEGER,
            defaultValue: 2
        }
    });
    return User;
};
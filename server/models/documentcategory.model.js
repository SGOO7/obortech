module.exports = (sequelize, Sequelize) => {
    const DocumentCategory = sequelize.define("document_category", {
        name: {
            type: Sequelize.STRING
        }
    });

    return DocumentCategory;
};
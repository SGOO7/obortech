module.exports = (sequelize, Sequelize) => {
    const Event = sequelize.define("event", {
        event_category_id: {
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING
        },
        type: {
            type: Sequelize.STRING,
            defaultValue: 'event'
        }
    });

    return Event;
};
module.exports = (sequelize, Sequelize) => {
    const SealingDetail = sequelize.define("sealing_detail", {
        shipment_id: {
            type: Sequelize.INTEGER
        },
        container_id: {
            type: Sequelize.INTEGER
        },
        event_id: {
            type: Sequelize.INTEGER
        },
        status: {
            type: Sequelize.STRING
        },
        open_count: {
            type: Sequelize.INTEGER
        },
        close_count: {
            type: Sequelize.INTEGER
        },
        is_active: {
            type: Sequelize.INTEGER
        }
    });

    return SealingDetail;
};
module.exports = (sequelize, Sequelize) => {
    const City = sequelize.define("city", {
        Name: {
            type: Sequelize.STRING
        },
        CountryCode: {
            type: Sequelize.STRING
        },
        District: {
            type: Sequelize.INTEGER
        },
    });

    return City;
};

// -- cities --
// id
// Name
// CountryCode
// District
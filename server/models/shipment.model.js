module.exports = (sequelize, Sequelize) => {
    const Shipment = sequelize.define("shipment", {
        name: {
            type: Sequelize.STRING
        },
        shipment_category_id: {
            type: Sequelize.INTEGER
        },
        document_category_id: {
            type: Sequelize.INTEGER
        },
        temperature_alert_min: {
            type: Sequelize.STRING
        },
        temperature_alert_max: {
            type: Sequelize.STRING
        },
        temperature_alert_interval: {
            type: Sequelize.STRING
        },
        temperature_allowed_occurances: {
            type: Sequelize.STRING
        },
        humidity_alert_min: {
            type: Sequelize.STRING
        },
        humidity_alert_max: {
            type: Sequelize.STRING
        },
        humidity_alert_interval: {
            type: Sequelize.STRING
        },
        humidity_allowed_occurances: {
            type: Sequelize.STRING
        },
        ambience_threshold: {
            type: Sequelize.STRING
        },
        draft: {
            type: Sequelize.INTEGER
        },
        isActive: {
            type: Sequelize.INTEGER
        }
    });

    Shipment.associate = models => {
        Shipment.hasMany(models.shipment_selection);
    }

    return Shipment;

    // return {shipment, shipment_participant, shipment_selection, selection_device, selection_cargo, selection_container, selection_truck};
};
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const env = process.env.dev == false ? "production" : "development";
const config = require(path.join(__dirname, '../../', 'config', 'config.json'))[env];
const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: config.dialect,
    logging: false,
    pool: {
        max: 100,
        min: 0,
        acquire: "30000",
        idle: "10000"
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./user.model.js")(sequelize, Sequelize);
db.organizations = require("./organization.model.js")(sequelize, Sequelize);
db.participants = require("./participant.model.js")(sequelize, Sequelize);
db.participant_categories = require("./participantcategory.model.js")(sequelize, Sequelize);
db.event_categories = require("./eventcategory.model.js")(sequelize, Sequelize);
db.shipment_events = require("./shipmentevent.model.js")(sequelize, Sequelize);
db.shipment_temp_events = require("./shipmenttempevent.model.js")(sequelize, Sequelize);
db.sealing_details = require("./sealingdetail.model.js")(sequelize, Sequelize);
db.document_categories = require("./documentcategory.model.js")(sequelize, Sequelize);
db.roads = require("./road.model.js")(sequelize, Sequelize);
db.cargos = require("./cargo.model.js")(sequelize, Sequelize);
db.groups = require("./group.model.js")(sequelize, Sequelize);
db.devices = require("./device.model.js")(sequelize, Sequelize);
db.trucks = require("./truck.model.js")(sequelize, Sequelize);
db.containers = require("./container.model.js")(sequelize, Sequelize);
db.apikey = require("./apikey.model.js")(sequelize, Sequelize);
db.events = require("./event.model.js")(sequelize, Sequelize);
db.shipments = require("./shipment.model.js")(sequelize, Sequelize);
db.shipment_categories = require("./shipmentcategory.model.js")(sequelize, Sequelize);
db.shipment_participants = require("./shipmentparticipant.model.js")(sequelize, Sequelize);
db.shipment_selections = require("./shipmentselection.model.js")(sequelize, Sequelize);
db.shipment_roads = require("./shipmentroad.model.js")(sequelize, Sequelize);
db.shipment_events = require("./shipmentevent.model.js")(sequelize, Sequelize);
db.selection_devices = require("./selectiondevice.model.js")(sequelize, Sequelize);
db.selection_cargos = require("./selectioncargo.model.js")(sequelize, Sequelize);
db.selection_containers = require("./selectioncontainer.model.js")(sequelize, Sequelize);
db.selection_trucks = require("./selectiontruck.model.js")(sequelize, Sequelize);
db.selection_groups = require("./selectiongroup.model.js")(sequelize, Sequelize);
db.shipment_event_users = require("./shipmenteventuser.model.js")(sequelize, Sequelize);
db.shipment_event_comments = require("./shipmenteventcomment.model.js")(sequelize, Sequelize);
db.document_accepted_users = require("./documentaccepteduser.model.js")(sequelize, Sequelize);
db.document_seen_users = require("./documentseenuser.model.js")(sequelize, Sequelize);
db.event_document_users = require("./eventdocumentuser.model.js")(sequelize, Sequelize);
db.station_border_info = require("./stationborderinfo.model.js")(sequelize, Sequelize);
db.shipment_folders = require("./shipmentfolder.model.js")(sequelize, Sequelize);
db.locationlog_logs = require("./locationlog.model.js")(sequelize, Sequelize);
db.humidity_logs = require("./humiditylog.model.js")(sequelize, Sequelize);
db.temperature_logs = require("./temperaturelog.model.js")(sequelize, Sequelize);
db.shipment_event_categories = require("./shipmenteventcategory.model.js")(sequelize, Sequelize);
db.shipment_document_categories = require("./shipmentdocumentcategory.model.js")(sequelize, Sequelize);
db.device_api_logs = require("./deviceapilog.model.js")(sequelize, Sequelize);
db.workers = require("./worker.model.js")(sequelize, Sequelize);
db.roles = require("./role.model.js")(sequelize, Sequelize);
db.user_types = require("./user_type.model.js")(sequelize, Sequelize);
db.submission_requests = require("./submissionrequest.model.js")(sequelize, Sequelize);
db.submission_request_recipients = require("./submissionrequestrecipient.model.js")(sequelize, Sequelize);
db.submission_request_users = require("./submissionrequestuser.model.js")(sequelize, Sequelize);
db.temp_number_verification = require("./tempnumberverification.model.js")(sequelize, Sequelize);

// READ SEQUEL DUMP MODEL
db.countries = require("./countries.model.js")(sequelize, Sequelize);
db.cities = require("./cities.model.js")(sequelize, Sequelize);

// Associations
db.roles.hasMany(db.workers, {foreignKey: 'role_id'});
db.workers.belongsTo(db.roles, {foreignKey: 'role_id'});
db.submission_requests.hasMany(db.submission_request_recipients, {foreignKey: 'submission_id'});
db.submission_request_recipients.belongsTo(db.submission_requests, {foreignKey: 'submission_id'});
db.submission_requests.hasMany(db.submission_request_users, {foreignKey: 'submission_id'});
db.submission_request_users.belongsTo(db.submission_requests, {foreignKey: 'submission_id'});
db.submission_requests.belongsTo(db.workers, {foreignKey: 'worker_id'});
db.submission_requests.belongsTo(db.shipments, {foreignKey: 'shipment_id'});
db.submission_requests.belongsTo(db.containers, {foreignKey: 'container_id'});
db.submission_requests.belongsTo(db.cargos, {foreignKey: 'cargo_id'});
db.submission_requests.belongsTo(db.events, {foreignKey: 'event_id'});
db.submission_requests.belongsTo(db.shipment_events, {foreignKey: 'shipment_event_id'});
db.shipment_categories.hasMany(db.shipment_event_categories, {foreignKey: 'shipment_category_id'});
db.shipment_event_categories.belongsTo(db.shipment_categories, {foreignKey: 'shipment_category_id'});
db.shipment_event_categories.belongsTo(db.event_categories, {foreignKey: 'event_category_id'});

db.shipment_categories.hasMany(db.shipment_document_categories, {foreignKey: 'shipment_category_id'});

db.shipment_document_categories.belongsTo(db.shipment_categories, {foreignKey: 'shipment_category_id'});
db.shipment_document_categories.belongsTo(db.document_categories, {foreignKey: 'document_category_id'});
db.event_categories.hasMany(db.events, {foreignKey: 'event_category_id'});
db.document_categories.hasMany(db.events, {foreignKey: 'event_category_id'});

db.containers.hasMany(db.shipment_events, {foreignKey: 'container_id', as: 'temp_alert'});
db.containers.hasMany(db.shipment_events, {foreignKey: 'container_id', as: 'hum_alert'});
db.containers.hasMany(db.sealing_details, {foreignKey: 'container_id', as: 'sealing_alert'});
db.containers.hasMany(db.sealing_details, {foreignKey: 'container_id'});
db.users.belongsTo(db.participant_categories, { foreignKey: 'participant_category_id' });
//For Organizations
db.users.belongsTo(db.organizations, { foreignKey: 'organization_id' });
db.users.belongsTo(db.roles, { foreignKey: 'role_id' });
db.users.belongsTo(db.user_types, { foreignKey: 'user_type_id' });
db.organizations.belongsTo(db.countries, { foreignKey: 'country_id' });
db.organizations.belongsTo(db.cities, { foreignKey: 'city_id' });
//End
db.shipments.hasMany(db.shipment_selections, {foreignKey: 'shipment_id'});
db.shipments.hasMany(db.shipment_roads, {foreignKey: 'shipment_id'});
db.shipments.hasMany(db.shipment_participants, {foreignKey: 'shipment_id'});
db.shipment_participants.belongsTo(db.users, {foreignKey: 'participant_id'});
db.shipment_participants.belongsTo(db.shipments, {foreignKey: 'shipment_id'});
db.shipments.belongsTo(db.shipment_categories, {foreignKey: 'shipment_category_id'});
db.shipment_selections.hasMany(db.selection_cargos, {foreignKey: 'selection_id'});
db.selection_cargos.belongsTo(db.cargos, {foreignKey: 'cargo_id'});
db.shipment_selections.hasMany(db.selection_containers, {foreignKey: 'selection_id'});
db.selection_containers.belongsTo(db.containers, {foreignKey: 'container_id'});
db.shipment_selections.hasMany(db.selection_groups, {foreignKey: 'selection_id'});
db.selection_groups.belongsTo(db.groups, {foreignKey: 'group_id'});
db.shipment_selections.hasMany(db.selection_devices, {foreignKey: 'selection_id'});
db.selection_devices.belongsTo(db.devices, {foreignKey: 'device_id'});
db.shipment_selections.hasMany(db.selection_trucks, {foreignKey: 'selection_id'});
db.selection_trucks.belongsTo(db.trucks, {foreignKey: 'truck_id'});
db.shipment_events.hasMany(db.shipment_event_comments, {foreignKey: 'shipment_event_id'});
db.shipment_events.hasMany(db.shipment_event_users, {foreignKey: 'shipment_event_id'});
db.shipment_events.hasMany(db.document_accepted_users, {foreignKey: 'shipment_event_id'});
db.shipment_events.hasMany(db.document_seen_users, {foreignKey: 'shipment_event_id'});
db.shipment_events.hasMany(db.event_document_users, {foreignKey: 'shipment_event_id'});
db.shipment_event_users.belongsTo(db.shipment_events, {foreignKey: 'shipment_event_id'})
db.shipment_event_users.belongsTo(db.users, {foreignKey: 'created_by'});
db.shipment_folders.hasMany(db.shipment_folders, {as: 'subFolders', foreignKey: 'parent'});
db.shipment_folders.hasMany(db.shipment_folders, {as: 'shipments', foreignKey: 'parent'});
db.shipment_folders.belongsTo(db.shipments, {foreignKey: 'shipment_id'});
db.shipment_events.belongsTo(db.events, {foreignKey: 'event_id'});
db.shipment_events.belongsTo(db.roads, {foreignKey: 'road_id'});
db.document_accepted_users.belongsTo(db.shipment_events, {foreignKey: 'shipment_event_id'});
db.document_accepted_users.belongsTo(db.users, {foreignKey: 'user_id'});
db.document_seen_users.belongsTo(db.shipment_events, {foreignKey: 'shipment_event_id'});
db.document_seen_users.belongsTo(db.users, {foreignKey: 'user_id'});
db.event_document_users.belongsTo(db.shipment_events, {foreignKey: 'shipment_event_id'});
db.event_document_users.belongsTo(db.users, {foreignKey: 'user_id'});
db.shipment_roads.belongsTo(db.roads, {foreignKey: 'road_id'});
db.shipment_event_comments.belongsTo(db.users, {foreignKey: 'user_id'});
db.station_border_info.belongsTo(db.roads, {foreignKey: 'station_id'});
db.roads.hasMany(db.station_border_info, {foreignKey: 'station_id', as: 'inside'});
db.roads.hasMany(db.station_border_info, {foreignKey: 'station_id', as: 'outside'});
// READ SEQUEL DUMP ASSOCIATIONS
db.cities.belongsTo(db.countries, {foreignKey: 'country_id'});

module.exports = db;

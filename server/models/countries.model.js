module.exports = (sequelize, Sequelize) => {
    const Country = sequelize.define("country", {
        Code: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        Name: {
            type: Sequelize.STRING
        },
        Continent: {
            type: Sequelize.STRING
        },
        Region: {
            type: Sequelize.STRING
        },
        Capital: {
            type: Sequelize.INTEGER
        },
        Code2: {
            type: Sequelize.STRING
        },
    });

    return Country;
};

// -- contries --
// Code
// Name
// Continent
// Region
// Capital
// Code2

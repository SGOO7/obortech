module.exports = (sequelize, Sequelize) => {
    const ShipmentFolder = sequelize.define("shipment_folder", {
        shipment_id: {
            type: Sequelize.INTEGER,
            defaultValue: null,
        },
        name: {
            type: Sequelize.STRING,
            defaultValue: null,
        },
        parent: {
            type: Sequelize.INTEGER,
            defaultValue: 0,
        }
    });

    return ShipmentFolder;
};

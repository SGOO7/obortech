module.exports = (sequelize, Sequelize) => {
    const LocationLog = sequelize.define("location_log", {
        shipment_id: {
            type: Sequelize.INTEGER
        },
        container_id: {
            type: Sequelize.INTEGER
        },
        latitude: {
            type: Sequelize.STRING
        },
        longitude: {
            type: Sequelize.STRING
        }
    });

    return LocationLog;
};
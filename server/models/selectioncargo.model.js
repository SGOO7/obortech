module.exports = (sequelize, Sequelize) => {
    const SelectionCargo = sequelize.define("selection_cargo", {
        selection_id: {
            type: Sequelize.INTEGER
        },
        cargo_id: {
            type: Sequelize.INTEGER
        }
    });
    
    return SelectionCargo;
};
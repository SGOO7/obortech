module.exports = (sequelize, Sequelize) => {
    const ParticipantCategory = sequelize.define("participant_category", {
        name: {
            type: Sequelize.STRING
        }
    });

    ParticipantCategory.associate = models => {
        ParticipantCategory.hasMany(models.participant);
    }

    return ParticipantCategory;
};
module.exports = (sequelize, Sequelize) => {
    const Cargo = sequelize.define("apikey", {
        type: {
            type: Sequelize.STRING
        },
        value: {
            type: Sequelize.STRING
        }
    });

    return Cargo;
};
module.exports = (sequelize, Sequelize) => {
    const ShipmentParticipant = sequelize.define("shipment_participant", {
        shipment_id: {
            type: Sequelize.INTEGER
        },
        participant_id: {
            type: Sequelize.INTEGER
        }
    });

    return ShipmentParticipant;
};
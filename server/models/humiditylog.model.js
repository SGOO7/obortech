module.exports = (sequelize, Sequelize) => {
    const HumidityLog = sequelize.define("humidity_log", {
        shipment_id: {
            type: Sequelize.INTEGER
        },
        container_id: {
            type: Sequelize.INTEGER
        },
        humidity: {
            type: Sequelize.FLOAT
        }
    });
    
    return HumidityLog;
};
const cron = require('node-cron');
const { shipment_roads } = require('./models');

// Load MySQL Models
const db = require("./models");
const Shipment = db.shipments;
const ShipmentSelection = db.shipment_selections;
const SelectionDevice = db.selection_devices;
const SelectionContainer = db.selection_containers;
const Device = db.devices;
const ShipmentRoad = db.shipment_roads;
const Road = db.roads;
const ShipmentParticipant = db.shipment_participants;
const Users = db.users;

const Op = db.Sequelize.Op;
const cronapp = require('./cronapp.js');
const helper = require('./helpers/cron-helper.js');
require('dotenv').config();

const tempAlertEventId      = process.env.tempAlertEventId; //Temperature alert event id
const humidityAlertEventId  = process.env.humidityAlertEventId; //Humidity alert event id

const cronJobs = [];

async function runCronJob(){
    // get shipments with active tracking with their devices and start cron jobs for all shipments
    const shipments = await Shipment.findAll({
        include: [
            {
                model:ShipmentSelection,
                include: [
                    {
                        model: SelectionDevice,
                        include: [
                            {model: Device}
                        ]
                    },
                    {
                        model: SelectionContainer
                    }
                ]
            },
            {
                model: ShipmentRoad,
                include: [
                    {model: Road}
                ]
            },
            {
                model: ShipmentParticipant,
                include: [
                    {
                        model: Users,
                        required: true
                    },
                ],
            },
        ],
        where: {isActive: 1}
    });
    shipments.map((shipment,i)=>{
        const shipment_id = shipment.id;
        cronJobs[shipment_id] = [];
        shipment.shipment_selections.map((shipment_selection,j)=>{
            shipment_selection.selection_devices.map((device,k)=>{
                let container_id = shipment_selection.selection_containers[k].container_id;

                let task_device = cron.schedule('*/'+(device.data_interval)+' * * * *', () =>  {
                    cronapp.fetchData(device.device.deviceID,shipment,container_id,0);
                });
                cronJobs[shipment_id].push(task_device);

                let task_temp_alert = cron.schedule('*/'+(shipment.temperature_alert_interval/(shipment.temperature_allowed_occurances+2))+' * * * *', () =>  {
                    helper.verifyOccurancesAndAddToShipmentEvent(shipment.shipment_participants, 0, tempAlertEventId, container_id, shipment.id, 'temperature', shipment.temperature_allowed_occurances);
                });
                cronJobs[shipment_id].push(task_temp_alert);

                let task_humidity_alert = cron.schedule('*/'+(shipment.humidity_alert_interval/(shipment.humidity_allowed_occurances+2))+' * * * *', () =>  {
                    helper.verifyOccurancesAndAddToShipmentEvent(shipment.shipment_participants, 0, humidityAlertEventId, container_id, shipment.id, 'humidity', shipment.humidity_allowed_occurances);
                });
                cronJobs[shipment_id].push(task_humidity_alert);
            });
        });
    });
}

async function addCronJob(shipment_id){
    cronJobs[shipment_id] = [];
    const shipment = await Shipment.findOne({
        include: [
            {
                model:ShipmentSelection,
                include: [
                    {
                        model: SelectionDevice,
                        include: [
                            {model: Device}
                        ]
                    },
                    {
                        model: SelectionContainer
                    }
                ],
            },
            {
                model: ShipmentRoad,
                include: [
                    {model: Road}
                ]
            },
            {
                model: ShipmentParticipant,
                include: [
                    {
                        model: Users,
                        required: true
                    },
                ],
            },
        ],
        where: {
            id: shipment_id
        }
    });
    shipment.shipment_selections.map((shipment_selection,j)=>{
        shipment_selection.selection_devices.map((device,k)=>{
            let container_id = shipment_selection.selection_containers[k].container_id;

            // Run it for the first time to fetch data
            cronapp.fetchData(device.device.deviceID,shipment,container_id,0);

            let task_device = cron.schedule('*/'+(device.data_interval)+' * * * *', () =>  {
                cronapp.fetchData(device.device.deviceID,shipment,container_id,0);
            });
            cronJobs[shipment_id].push(task_device);

            let task_temp_alert = cron.schedule('*/'+(shipment.temperature_alert_interval/(shipment.temperature_allowed_occurances+2))+' * * * *', () =>  {
                helper.verifyOccurancesAndAddToShipmentEvent(shipment.shipment_participants, 0, tempAlertEventId, container_id, shipment.id, 'temperature', shipment.temperature_allowed_occurances);
            });
            cronJobs[shipment_id].push(task_temp_alert);

            let task_humidity_alert = cron.schedule('*/'+(shipment.humidity_alert_interval/(shipment.humidity_allowed_occurances+2))+' * * * *', () =>  {
                helper.verifyOccurancesAndAddToShipmentEvent(shipment.shipment_participants, 0, humidityAlertEventId, container_id, shipment.id, 'humidity', shipment.humidity_allowed_occurances);
            });
            cronJobs[shipment_id].push(task_humidity_alert);
        });
    });
}

async function removeCronJob(shipment_id){
    const tasks = cronJobs[shipment_id];
    tasks.map((task) => {
        task.stop();
    });
}

async function getCronJobs(){
    return cronJobs;
}

async function stopCronJob(){
    const task = cronJobs[0];
    task.stop();
}

exports.runCronJob = runCronJob;
exports.getCronJobs = getCronJobs;
exports.stopCronJob = stopCronJob;
exports.addCronJob = addCronJob;
exports.removeCronJob = removeCronJob;

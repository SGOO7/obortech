/**
 * Copyright 2017 IBM All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an 'AS IS' BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

'use strict';
const request = require("request");
const helper = require('./helpers/cron-helper.js');
require('dotenv').config();

async function fetchData(device_name, shipment, container_id, user_id){

    let options = {
        method: 'POST',
        url: process.env.beeURL,
        qs: { apikey: process.env.beeAPIKey },
        headers:
            {
                'Postman-Token': 'fabe49e5-2582-4318-b383-d235075b89c9',
                'cache-control': 'no-cache'
            }
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        const jsonBody = JSON.parse(body);

        jsonBody.forEach(function(obj){

            if(obj.bee_number == device_name){

                helper.saveDeviceLog(obj, device_name);
                helper.saveLocationLogs(obj, shipment, container_id);
                helper.saveTemperatureLogs(obj, shipment, container_id);
                helper.saveHumidityLogs(obj, shipment, container_id);
                helper.saveBorderInfo(obj, shipment, user_id, container_id);
                helper.generateTempAlert(obj, shipment, user_id, device_name, container_id);
                helper.generateHumidityAlert(obj, shipment, user_id, device_name, container_id);
                helper.generateSealingAlert(obj, shipment, user_id, device_name, container_id);

            }
        })
    });
};

exports.fetchData = fetchData;

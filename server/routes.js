// const User = require('./models/User');
const LRUCache = require('lru-cache');

module.exports = function({ server, app }){
  const handle = app.getRequestHandler();
  const ssrCache = new LRUCache({
    max: 100, // 100 items
    maxAge: 1000 * 60 * 60, // 1 hr
  });

  function getCacheKey(req) {
    if (req.user) {
      return `${req.url}${req.user.id}`;
    }
    return `${req.url}`;
  }

  async function renderAndCache(req, res, pagePath, queryParams) {
    const key = getCacheKey(req);

    // If we have a page in the cache, let's serve it
    if (ssrCache.has(key)) {
      res.setHeader('x-cache', 'HIT');
      res.send(ssrCache.get(key));
      return;
    }

    try {
      // If not let's render the page into HTML
      const html = await app.renderToHTML(req, res, pagePath, queryParams);

      // Something is wrong with the request, let's skip the cache
      if (res.statusCode !== 200) {
        res.send(html);
        return;
      }

      // Let's cache this page
      ssrCache.set(key, html);

      res.setHeader('x-cache', 'MISS');
      res.send(html);
    } catch (err) {
      app.renderError(err, req, res, pagePath, queryParams);
    }
  }

  server.get('/robots.txt', (req, res) => {
    res.type('text/plain')
    res.send("User-agent: *\ndisallow: /");
  });

  server.get('/restaurant/list-owner', (req, res) => {
    app.render(req, res, '/restaurant/list-owner');
  });

  server.get('/restaurant/add-owner', (req, res) => {
    app.render(req, res, '/restaurant/add-owner');
  });

  server.get('/restaurant/add', (req, res) => {
    app.render(req, res, '/restaurant/add');
  });

  server.get('/restaurant/:filter', (req, res) => {
    const {filter} = req.params;
    app.render(req, res, '/restaurant/list', { filter });
  });

  server.get('/venue/add', (req, res) => {
    app.render(req, res, '/venue/add');
  });

  server.get('/venue/:filter', (req, res) => {
    const {filter} = req.params;
    app.render(req, res, '/venue/list', { filter });
  });

  server.get('/about/:slug', (req, res) => {
    app.render(req, res, '/about');
  });

  const URL_MAP = {
    '/my-books': '/customer/my-books',
  };

  server.get('*', (req, res) => {
    const url = URL_MAP[req.path];
    if (url) {
      app.render(req, res, url);
    } else {
      handle(req, res);
    }
  });
}
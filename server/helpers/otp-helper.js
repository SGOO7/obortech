//Add Shipment Helper
require('dotenv').config();
const twilio = require('twilio');

// Load Models
const db = require("../models");
const Worker = db.workers;
const TempNumberVerification = db.temp_number_verification;

//Twilio
const TWILIO_ACCOUNT_SID = process.env.TWILIO_ACCOUNT_SID;
const TWILIO_AUTH_TOKEN = process.env.TWILIO_AUTH_TOKEN;
const TWILIO_FROM_NUMBER = process.env.TWILIO_FROM_NUMBER;
const OTP_CHAR_LENGTH = process.env.OTP_CHAR_LENGTH;

//Send otp to web
const _sendOtpToWeb = async (reqObj) => {

    let response;

    try{
        await TempNumberVerification.create({
            otp: reqObj.verificationCode,
            number: reqObj.toNumber
        });

        //Send OTP in SMS
        const otpResponse = await _sendOtp(reqObj.verificationCode, reqObj.countrycode, reqObj.toNumber);
        if(otpResponse.success){
            response = {data: {
                code: 1,
                message: 'SMS sent successfully'
            }, code: 200};
        } else {
            response = {data: {
                code: 2,
                message: otpResponse.message
            }, code: 400};
        }

    } catch (error) {
        response = {data: {
            code: 2,
            message: error.message || err.toString()
        }, code: 500};
    }

    return response;
}

//Send otp to mobile
const _sendOtpToMobile = async (reqObj) => {

    let response;

    try{

        const worker = await Worker.findOne({
            where: { phone: reqObj.toNumber }
        });

        if (worker) {

            await worker.update({
                otp: reqObj.verificationCode
            });

            //Send OTP in SMS
            const otpResponse = await _sendOtp(reqObj.verificationCode, reqObj.countrycode, reqObj.toNumber);
            if(otpResponse.success){
                response = {data: {
                    code: 1,
                    message: 'SMS sent successfully'
                }, code: 200};
            } else {
                response = {data: {
                    code: 2,
                    message: otpResponse.message
                }, code: 400};
            }

        } else {
            response = {data: {
                code: 2,
                message: 'Phone number is not registered'
            }, code: 400};
        }

    } catch (error) {
        response = {data: {
            code: 2,
            message: error.message || err.toString()
        }, code: 500};
    }

    return response;
}

//Send OTP via Twilio
const _sendOtp = async (verificationCode, countrycode, toNumber) => {

    try {

        const client = new twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
        const message = await client.messages.create({
            body: 'Your OBORTECH verification code is: ' + verificationCode,
            to: countrycode + toNumber,  // Text this number
            from: TWILIO_FROM_NUMBER // From a valid Twilio number
        });

        return {
            success: true,
            message: message
        }

    } catch (error) {
        return {
            success: false,
            message: error.message || err.toString()
        }
    }
}

//Verify mobile OTP
const _verifyMobileOtp = async (reqObj) => {

    let response;

    try{

        const verificationCode = reqObj.verificationCode;
        const toNumber = reqObj.toNumber;

        const worker = await Worker.findOne({
            where: { phone: toNumber, otp: verificationCode }
        });

        if (worker) {

            await worker.update({
                otp: "",
                is_verified: 1
            })

            response = {data: {
                code: 1,
                message: 'Phone number verified successfully'
            }, code: 200};

        } else {
            response = {data: {
                code: 2,
                message: 'OTP is invalid!'
            }, code: 400};
        }

    } catch (error) {
        response = {data: {
            code: 2,
            message: error.message || err.toString()
        }, code: 500};
    }

    return response;
}

//Verify web OTP
const _verifyWebOtp = async (reqObj) => {

    let response;

    try{

        const verificationCode = reqObj.verificationCode;
        const toNumber = reqObj.toNumber;

        const tempNumber = await TempNumberVerification.findOne({
            where: { number: toNumber, otp: verificationCode }
        });

        if (tempNumber) {

            await tempNumber.destroy({
                where: { number: toNumber, otp: verificationCode }
            })

            response = {data: {
                code: 1,
                message: 'Phone number verified successfully'
            }, code: 200};

        } else {
            response = {data: {
                code: 2,
                message: 'OTP is invalid!'
            }, code: 400};
        }

    } catch (error) {
        response = {data: {
            code: 2,
            message: error.message || err.toString()
        }, code: 500};
    }

    return response;
}

// Function to generate OTP
const _generateotp = () => {
    var digits = '0123456789';
    let OTP = '';
    for (let i = 0; i < OTP_CHAR_LENGTH; i++ ) {
        OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
}

exports._sendOtpToWeb = _sendOtpToWeb;
exports._sendOtpToMobile = _sendOtpToMobile;
exports._verifyMobileOtp = _verifyMobileOtp;
exports._verifyWebOtp = _verifyWebOtp;
exports._generateotp = _generateotp;
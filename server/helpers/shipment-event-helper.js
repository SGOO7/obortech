//Add Shipment Helper
const db = require("../models");
const path = require("path");
const exec = require('await-exec');
const fs = require('fs');
const fsp = fs.promises;
const AWS = require('aws-sdk');
const md5File = require('md5-file')

const ShipmentEvent = db.shipment_events;
const ShipmentEventUser = db.shipment_event_users;
const ShipmentComment = db.shipment_event_comments;
const EventDocumentUser = db.event_document_users;

AWS.config.loadFromPath(path.join(__dirname, '../../', 'config', 's3_config.json'));
const s3Bucket = new AWS.S3({ params: { Bucket: 'blockchaindoc' } });

//Add Shipment Event
const _addShipmentEvent = async (req) => {

    //Upload file to S3 bucket
    let attachment = '';
    let file_hash = '';
    let event_type = req.body.type;
    let attachment_type = (event_type == 'document') ? 2 : 1; //1 for image and 2 for document

    if (req.files.file) {
        response = await uploadFile(req.files.file);
        attachment = response.s3_location;
        file_hash = md5File.sync(response.pdf_file_path);
    }

    //If event start date exists, in case of normal event
    let user_id = req.body.user_id;
    let event_data = {
        event_id: req.body.event_id,
        container_id: req.body.container_id,
        shipment_id: req.body.shipment_id,
        attachment: attachment,
        attachment_type: attachment_type,
        file_hash: file_hash,
        isActive: 1
    };

    //If start date is selected
    if (req.body.start_date && req.body.start_date != 'null') {
        event_data.createdAt = req.body.start_date
    }

    return ShipmentEvent.create(event_data).then(async function (shipmentevent) {

        if (shipmentevent) {

            //Link users with events
            //It will be used for who can see your event
            //Push current logged in user id in the array to link event
            _addEventUsers(req, user_id, shipmentevent.id);

            //If document event, Link users with document
            //it will be used for who can view your document
            if(event_type == 'document'){
                _addDocumentUsers(req, user_id, shipmentevent.id);
            }

            //Add first comment
            if (req.body.comment) {
                ShipmentComment.create({
                    shipment_event_id: shipmentevent.id,
                    comment: req.body.comment,
                    user_id: req.body.user_id
                })
                .then(shipmentComment => {
                });
            }

            //Send alert to network
            // let alert_time = moment().format();
            // sendAlertToNetwork(req.body.cargo_id, req.body.event_name, alert_time);

            return shipmentevent;

        } else {
            return { error: 'Error in insert new record' };
        }
    });
}

//Update shipment Event
const _updateShipmentEvent = async (req) => {

    //Upload file to S3 bucket
    let attachment = '';
    let file_hash = '';
    let event_type = req.body.type;
    let attachment_type = (event_type == 'document') ? 2 : 1; //1 for image and 2 for document

    if (req.files.file) {
        response = await uploadFile(req.files.file);
        attachment = response.s3_location;
        file_hash = md5File.sync(response.pdf_file_path);
    }

    //If event start date exists, in case of normal event
    let user_id = req.body.user_id;
    let event_data = {
        event_id: req.body.event_id,
        container_id: req.body.container_id,
        shipment_id: req.body.shipment_id,
        attachment: attachment,
        attachment_type: attachment_type,
        file_hash: file_hash,
        isActive: 1
    };

    //If start date is selected
    if (req.body.start_date) {
        event_data.createdAt = req.body.start_date
    }

    return ShipmentEvent.update(event_data, { where: { id: req.body.id } }).then(async function (shipmentevent) {
        if (shipmentevent) {

            //Link users with events
            //It will be used for who can see your event
            //Push current logged in user id in the array to link event
            _addEventUsers(req, user_id, shipmentevent.id);

            //If document event, Link users with document
            //it will be used for who can view your document
            if(event_type == 'document'){
                _addDocumentUsers(req, user_id, shipmentevent.id);
            }

            //Add first comment
            if (req.body.comment) {
                ShipmentComment.create({
                    shipment_event_id: shipmentevent.id,
                    comment: req.body.comment,
                    user_id: req.body.user_id
                })
                .then(shipmentComment => {
                });
            }

            //Send alert to network
            // let alert_time = moment().format();
            // sendAlertToNetwork(req.body.cargo_id, req.body.event_name, alert_time);

            return shipmentevent;

        } else {
            return { error: 'Error in insert new record' };
        }
    });

}


//Add shipment event users
const _addEventUsers = (req, user_id, eventid) => {

    if(req.body.selectedEventUsers){
        let selectedEventUsers = req.body.selectedEventUsers.split(',');
        if (selectedEventUsers.length > 0) {
            let shipmentEventUsers = [];
            for (let i = 0; i < selectedEventUsers.length; i++) {
                shipmentEventUsers.push({
                    shipment_event_id: eventid,
                    user_id: selectedEventUsers[i],
                    created_by: user_id
                });
            }
            ShipmentEventUser.bulkCreate(shipmentEventUsers).then(function (shipment_event_users) {
            })
        }
    }

    if(user_id){
        ShipmentEventUser.create({
            shipment_event_id: eventid,
            user_id: user_id,
            created_by: user_id
        });
    }
}

//Add shipment document users
const _addDocumentUsers = (req, user_id, eventid) => {

    if(req.body.selectedDocumentUsers){
        let selectedDocumentUsers = req.body.selectedDocumentUsers.split(',');
        if (selectedDocumentUsers.length > 0) {
            let eventDocumentUsers = [];
            for (let i = 0; i < selectedDocumentUsers.length; i++) {
                eventDocumentUsers.push({
                    shipment_event_id: eventid,
                    user_id: selectedDocumentUsers[i]
                });
            }
            EventDocumentUser.bulkCreate(eventDocumentUsers).then(function (event_document_users) {
            })
        }
    }

    if(user_id){
        EventDocumentUser.create({
            shipment_event_id: eventid,
            user_id: user_id
        });
    }
}

//Upload File to AWS S3
const uploadFile = async (filedata) => {

    if (filedata) {

        let upload_path = 'static/upload/';
        let oldpath = filedata.path;
        let clean_file_name = filedata.name.replace(/[|&;$%@"<>()+, ]/g, "");
        let destpath = upload_path + clean_file_name;
        let pdf_file_path = '';
        let returnPath = '';

        // Read the file
        await fsp.readFile(oldpath).then(async (data) => {

            // Write the file
            const bufdata = Buffer.from(data);
            await fsp.writeFile(destpath, bufdata).then(async (data) => {

                if (filedata.type == 'image/jpeg') {

                    pdf_file_path = destpath.replace('.jpg', '.pdf');
                    destpath = destpath.replace('.jpg', '');

                    //Convert image file to pdf
                    await exec(`python img2pdf.py ${destpath}.jpg ${pdf_file_path}`).then(async (error, stdout, stderr) => {

                        if (error.stderr) {
                            console.error(`stderr: ${stderr}`);
                        }

                        //Delete image file
                        await fsp.unlink(destpath + '.jpg').then(async (data) => {

                            //Upload PDF file to S3 bucket
                            if (pdf_file_path) {
                                returnPath = await _uploadToS3(pdf_file_path);
                            }

                        });

                    });

                } else if (filedata.type == 'image/png') {

                    pdf_file_path = destpath.replace('.png', '.pdf');
                    destpath = destpath.replace('.png', '');

                    //Convert png to jpg
                    await exec(`python img2pdf.py ${destpath}.png ${destpath}.jpg`).then(async (error, stdout, stderr) => {

                        //Convert image file to pdf
                        await exec(`python img2pdf.py ${destpath}.jpg ${pdf_file_path}`).then(async (error, stdout, stderr) => {

                            if (error.stderr) {
                                console.error(`stderr: ${stderr}`);
                            }

                            //Delete image file
                            await fsp.unlink(destpath + '.jpg');
                            await fsp.unlink(destpath + '.png').then(async (data) => {

                                //Upload PDF file to S3 bucket
                                if (pdf_file_path) {
                                   returnPath = await _uploadToS3(pdf_file_path);
                                }

                            });

                        });
                    });

                } else {
                    pdf_file_path = destpath;
                    returnPath = await _uploadToS3(pdf_file_path);
                }

            });
        });

        return returnPath;
    }
};

//Upload file to S3 bucket
const _uploadToS3 = async (pdf_file_path) => {

    try {

        let uploadParams = {};
        let fileStream = fs.createReadStream(pdf_file_path);
        fileStream.on('error', function (err) {
            console.error('File Error', err);
        });
        uploadParams.Body = fileStream;
        uploadParams.Key = path.basename(pdf_file_path);
        uploadParams.ACL = 'public-read';
        uploadParams.ContentType = 'application/pdf';

        const response = await s3Bucket.upload(uploadParams).promise();
        const returnObj = {
            s3_location: response.Location,
            pdf_file_path: pdf_file_path
        };

        return returnObj;

    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
}

/*
    Send Alert To Network

    This function will send the alert details to the blockchain
    network to keep record, we will get and show these events in
    blockchain explorer
*/
function sendAlertToNetwork(cargo_id, alert_type, alert_time) {

    /*var options = {
        method: 'POST',
        url: 'http://35.222.242.109:3000/api/addUser',
        headers:
        {
            'postman-token': '966af8e3-97e4-e15d-fa6c-c6eba9ca0049',
            'cache-control': 'no-cache',
            'content-type': 'application/json'
        },
        body:
        {
            orgName: 'org1',
            userName: 'appUser',
            cargoID: cargo_id,
            Alert: { type: alert_type, timeStamp: alert_time }
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        console.log(body);
    });*/
}


exports._addShipmentEvent = _addShipmentEvent;
exports._updateShipmentEvent = _updateShipmentEvent;

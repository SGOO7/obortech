//Cron Helper
const moment = require('moment');
const GeoPoint = require('geopoint');
const db = require("../models");
require('dotenv').config();

const ShipmentEvent = db.shipment_events;
const ShipmentTempEvent = db.shipment_temp_events;
const EventCategory = db.event_categories;
const SealingDetail = db.sealing_details;
const ShipmentEventUser = db.shipment_event_users
const StationBorderInfo = db.station_border_info;
const LocationLog = db.locationlog_logs;
const HumidityLog = db.humidity_logs;
const TemperatureLog = db.temperature_logs;
const DeviceApiLog = db.device_api_logs;

const Op = db.Sequelize.Op;

const minDistanceToSaveLog = process.env.minDistanceToSaveLog;
const tempAlertEventId = process.env.tempAlertEventId;
const humidityAlertEventId = process.env.humidityAlertEventId;
const sealOpenAlertEventId = process.env.sealOpenAlertEventId;
const sealLockAlertEventId = process.env.sealLockAlertEventId;
const borderInEventId = process.env.borderInEventId;
const borderOutEventid = process.env.borderOutEventid;

/*
    Save Device Logs

    This fucntion will capture the current location, temperarure
    humidity, ambience from device and save them for debugging purpose
*/
async function saveDeviceLog(obj, device_name) {

    try {

        DeviceApiLog.create({
            device_name: device_name,
            latitude: parseFloat(obj.state.geo.lat),
            longitude: parseFloat(obj.state.geo.lng),
            address: obj.state.geo.address,
            provider: obj.state.geo.provider,
            temperature: parseFloat(obj.state.temperature.original_temp),
            humidity: parseFloat(obj.state.HUM),
            ambience: parseFloat(obj.state.AMB)
        }).then(function (device) {
            console.log('Device log saved successfully!');
        });

    } catch (error) {
        console.error(`Error in saving device logs: ${error}`);
    }
}

/*
    Save Location Logs

    This fucntion will capture the current location from device
    and save it to show in analytics dashboard
*/
async function saveLocationLogs(obj, shipmentRow, container_id) {

    try {

        //Save logs for SHIPMENT
        if (shipmentRow) {

            LocationLog.findOne({
                where: {
                    container_id: container_id,
                    shipment_id: shipmentRow.id
                },
                order: [
                    ['id', 'DESC']
                ],
                limit: 1
            }).then((locationRow) => {

                if (locationRow) {

                    let current_position = new GeoPoint(parseFloat(obj.state.geo.lat), parseFloat(obj.state.geo.lng));
                    let last_postiion = new GeoPoint(parseFloat(locationRow.latitude), parseFloat(locationRow.longitude));
                    let covered_distance = current_position.distanceTo(last_postiion, true); //In Km
                    let covered_distance_meters = (parseFloat(covered_distance).toFixed(2) * 1000); //Convert km into meters

                    //If covered distance is greater than 500 meter
                    if (Math.round(covered_distance_meters) > minDistanceToSaveLog) {

                        LocationLog.create({
                            shipment_id: shipmentRow.id,
                            container_id: container_id,
                            latitude: parseFloat(obj.state.geo.lat),
                            longitude: parseFloat(obj.state.geo.lng)
                        }).then(function (locationlog) {
                            console.log('Location log saved successfully!');
                        });
                    }

                } else {

                    LocationLog.create({
                        shipment_id: shipmentRow.id,
                        container_id: container_id,
                        latitude: parseFloat(obj.state.geo.lat),
                        longitude: parseFloat(obj.state.geo.lng)
                    }).then(function (locationlog) {
                        console.log('Location log saved successfully!');
                    });
                }
            });
        }

    } catch (error) {
        console.error(`Error in saving location logs: ${error}`);
    }
}

/*
    Save Temperature Logs

    This fucntion will capture the current temperature from device
    and save it to show in analytics dashboard
*/
async function saveTemperatureLogs(obj, shipmentRow, container_id) {

    try {

        //Save logs for SHIPMENT
        if (shipmentRow) {

            //Save temperature logs
            TemperatureLog.findOrCreate({
                where: {
                    shipment_id: shipmentRow.id,
                    container_id: container_id,
                    temperature: parseFloat(obj.state.temperature.original_temp)
                },
                defaults: {
                    shipment_id: shipmentRow.id,
                    container_id: container_id,
                    temperature: parseFloat(obj.state.temperature.original_temp)
                }
            }).then(function (result, created) {
                console.log('Temperature log saved successfully!');
            })
        }

    } catch (error) {
        console.error(`Error in saving temperature logs: ${error}`);
    }
}

/*
    Save Humidity Logs

    This fucntion will capture the current humidity from device
    and save it to show in analytics dashboard
*/
async function saveHumidityLogs(obj, shipmentRow, container_id) {

    try {

        //Save logs for SHIPMENT
        if (shipmentRow) {

            //Save humidity logs
            HumidityLog.findOrCreate({
                where: {
                    shipment_id: shipmentRow.id,
                    container_id: container_id,
                    humidity: parseFloat(obj.state.HUM)
                },
                defaults: {
                    shipment_id: shipmentRow.id,
                    container_id: container_id,
                    humidity: parseFloat(obj.state.HUM)
                }
            }).then(function (result, created) {
                console.log('Humidity log saved successfully!');
            })
        }

    } catch (error) {
        console.error(`Error in saving humidity logs: ${error}`);
    }
}

/*
    Save Border Info

    This fucntion will capture the current location from
    device and save it to show in Border info on dashboard
*/
async function saveBorderInfo(obj, shipmentRow, user_id, container_id) {

    try {

        if (shipmentRow) {

            let current_position = new GeoPoint(parseFloat(obj.state.geo.lat), parseFloat(obj.state.geo.lng));

            //Get and save truck's current position
            for (let i = 0; i < shipmentRow.shipment_roads.length; i++) {

                let shipmentroad = shipmentRow.shipment_roads[i].road;

                //Getting distance from shipment starting point to current position
                let starting_postiion = new GeoPoint(parseFloat(shipmentroad.latitude), parseFloat(shipmentroad.longitude));
                let covered_distance = current_position.distanceTo(starting_postiion, true); //In Km
                let covered_distance_meters = (parseFloat(covered_distance).toFixed(2) * 1000); //Convert km into meters
                let position_radius = shipmentroad.radius;

                //If position is inside
                if (Math.round(covered_distance_meters) <= Math.round(position_radius)) {
                    _addInsideData(shipmentRow.shipment_participants, shipmentroad.id, user_id, shipmentRow.id, container_id, covered_distance_meters);
                }

                //If position is outside
                if (Math.round(covered_distance_meters) > Math.round(position_radius)) {
                    _addOutsideData(shipmentRow.shipment_participants, shipmentroad.id, user_id, shipmentRow.id, container_id, covered_distance_meters);
                }
            }
        }

    } catch (error) {
        console.error(`Error in saving device data: ${error}`);
    }
}

/*
    Generate Temperature Alert

    This fucntion will capture the current temperature
    from device and generate an alert if temperature is
    breached from the min/max limit defined in shipment
*/
async function generateTempAlert(obj, shipmentRow, user_id, device_name, container_id) {

    try {

        if (shipmentRow) {

            let min_temp_limit = shipmentRow.temperature_alert_min;
            let max_temp_limit = shipmentRow.temperature_alert_max;
            let current_temp = obj.state.temperature.original_temp;

            //Check if event already exists
            ShipmentTempEvent.findOne({
                where: {
                    event_id: tempAlertEventId,
                    container_id: container_id,
                    shipment_id: shipmentRow.id
                },
                order: [
                    ['id', 'DESC']
                ],
                limit: 1
            }).then((eventRow) => {

                if (!eventRow || (eventRow && eventRow.current_temp != current_temp)) {

                    //If current temp is less than than min limit or greater than max limit
                    if ((current_temp < min_temp_limit) || (current_temp > max_temp_limit)) {

                        ShipmentTempEvent.create({
                            event_id: tempAlertEventId,
                            container_id: container_id,
                            shipment_id: shipmentRow.id,
                            current_temp: current_temp,
                            isActive: 1
                        }).then(function (shipmentevent) {
                            console.log('Temperature event saved successfully!');
                        });
                    }
                }
            });
        }

    } catch (error) {
        console.log(`Error in saving device data: ${error}`);
    }
}

/*
    Generate Humidity Alert

    This fucntion will capture the current humidity from
    the device and generate an alert if humidity is less
    or greater than the min/max limit defined in the shipment
*/
async function generateHumidityAlert(obj, shipmentRow, user_id, device_name, container_id) {

    try {

        //If shipment found
        if (shipmentRow) {

            let min_hum_limit = shipmentRow.humidity_alert_min;
            let max_hum_limit = shipmentRow.humidity_alert_max;
            let current_hum = obj.state.HUM;

            ShipmentTempEvent.findOne({
                where: {
                    event_id: humidityAlertEventId,
                    container_id: container_id,
                    shipment_id: shipmentRow.id
                },
                order: [
                    ['id', 'DESC']
                ],
                limit: 1
            }).then(eventRow => {

                if (!eventRow || (eventRow && eventRow.current_hum != current_hum)) {

                    //If current humidity is less than min limit or greater than max limit
                    if ((current_hum < min_hum_limit) || (current_hum > max_hum_limit)) {

                        ShipmentTempEvent.create({
                            event_id: humidityAlertEventId,
                            container_id: container_id,
                            shipment_id: shipmentRow.id,
                            current_hum: current_hum,
                            isActive: 1
                        }).then(function (shipmentevent) {
                            console.log('Humidity event saved successfully!');
                        });
                    }
                }
            })
        }

    } catch (error) {
        console.error(`Error in saving device data: ${error}`);
    }

}

/*
    Generate Sealing Alert

    This fucntion will capture the current lock status from
    the device and generate an alert if the lock is opened or
    locked and also record the count of how many time lock is opened
    and closed for that shipment container
*/
async function generateSealingAlert(obj, shipmentRow, user_id, device_name, container_id) {

    try {

        let seal_alert_min = 0;
        let lock_status = "Locked";
        let prev_lock_status = 'Locked';
        let event_time = moment().format();
        let open_count = 0;
        let close_count = 0;

        //If shipments found
        if (shipmentRow) {

            let seal_alert_min = shipmentRow.ambience_threshold;
            let cargo_id = shipmentRow.cargo_id;

            //Get current seal status
            if (parseFloat(obj.state.AMB) > seal_alert_min) {
                lock_status = "Opened";
            }

            //Get sealing
            SealingDetail.findOne({
                where: {
                    shipment_id: shipmentRow.id,
                    container_id: container_id
                },
                order: [
                    ['id', 'DESC']
                ]
            }).then(sealingdetail => {

                if (sealingdetail) {

                    let prev_lock_status = sealingdetail.status;
                    open_count = sealingdetail.open_count;
                    close_count = sealingdetail.close_count;

                    if (prev_lock_status != lock_status) {

                        if (lock_status == "Opened") {

                            open_count++;

                            ShipmentEvent.create({
                                event_id: sealOpenAlertEventId,
                                container_id: container_id,
                                shipment_id: shipmentRow.id,
                                isActive: 1
                            }).then(function (shipmentevent) {

                                //Add shipment event user
                                if (shipmentevent) {
                                    const bulkEntries = [];
                                    shipmentRow.shipment_participants.map((participant) => {
                                        bulkEntries.push({
                                            shipment_event_id: shipmentevent.id,
                                            user_id: participant.participant_id
                                        })
                                    });
                                    ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {

                                        console.log('Sealing event saved successfully!');

                                        //Add sealing details
                                        SealingDetail.create({
                                            event_id: sealOpenAlertEventId,
                                            shipment_id: shipmentRow.id,
                                            container_id: container_id,
                                            status: lock_status,
                                            open_count: open_count,
                                            close_count: close_count,
                                            is_active: 1
                                        }).then(function () {
                                            //sendAlertToNetwork(cargo_id, event_name, event_time);
                                        })
                                    })
                                }
                            });
                        }

                        if (lock_status == "Locked") {

                            close_count++;

                            ShipmentEvent.create({
                                event_id: sealLockAlertEventId,
                                container_id: container_id,
                                shipment_id: shipmentRow.id,
                                isActive: 1
                            }).then(function (shipmentevent) {
                                const bulkEntries = [];
                                shipmentRow.shipment_participants.map((participant) => {
                                    bulkEntries.push({
                                        shipment_event_id: shipmentevent.id,
                                        user_id: participant.participant_id
                                    })
                                });
                                //Add shipment event user
                                if (shipmentevent) {
                                    ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {

                                        console.log('Sealing event saved successfully!');

                                        //Add sealing details
                                        SealingDetail.create({
                                            event_id: sealLockAlertEventId,
                                            shipment_id: shipmentRow.id,
                                            container_id: container_id,
                                            status: lock_status,
                                            open_count: open_count,
                                            close_count: close_count,
                                            is_active: 1
                                        }).then(function () {
                                            //sendAlertToNetwork(cargo_id, event_name, event_time);
                                        })

                                    })
                                }

                            });
                        }

                    }

                } else {

                    if (prev_lock_status != lock_status) {

                        if (lock_status == "Opened") {

                            open_count++;

                            ShipmentEvent.create({
                                event_id: sealOpenAlertEventId,
                                container_id: container_id,
                                shipment_id: shipmentRow.id,
                                isActive: 1
                            }).then(function (shipmentevent) {
                                const bulkEntries = [];
                                shipmentRow.shipment_participants.map((participant) => {
                                    bulkEntries.push({
                                        shipment_event_id: shipmentevent.id,
                                        user_id: participant.participant_id
                                    })
                                });
                                //Add shipment event user
                                if (shipmentevent) {
                                    ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {

                                        console.log('Sealing event saved successfully!');

                                        //Add sealing details
                                        SealingDetail.create({
                                            event_id: sealOpenAlertEventId,
                                            shipment_id: shipmentRow.id,
                                            container_id: container_id,
                                            status: lock_status,
                                            open_count: open_count,
                                            close_count: close_count,
                                            is_active: 1
                                        }).then(function () {
                                            //sendAlertToNetwork(cargo_id, event_name, event_time);
                                        })
                                    })
                                }
                            });
                        }

                        if (lock_status == "Locked") {

                            close_count++;

                            ShipmentEvent.create({
                                event_id: sealLockAlertEventId,
                                container_id: container_id,
                                shipment_id: shipmentRow.id,
                                isActive: 1
                            }).then(function (shipmentevent) {
                                const bulkEntries = [];
                                shipmentRow.shipment_participants.map((participant) => {
                                    bulkEntries.push({
                                        shipment_event_id: shipmentevent.id,
                                        user_id: participant.participant_id
                                    })
                                });
                                //Add shipment event user
                                if (shipmentevent) {
                                    ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {

                                        console.log('Sealing event saved successfully!');

                                        //Add sealing details
                                        SealingDetail.create({
                                            event_id: sealLockAlertEventId,
                                            shipment_id: shipmentRow.id,
                                            container_id: container_id,
                                            status: lock_status,
                                            open_count: open_count,
                                            close_count: close_count,
                                            is_active: 1
                                        }).then(function () {
                                            //sendAlertToNetwork(cargo_id, event_name, event_time);
                                        })

                                    })
                                }
                            });
                        }
                    }
                }
            });
        }

    } catch (error) {
        console.error(`Error in saving device data: ${error}`);
    }
}

/*
    Check current breaches and check against occurances

    This functions will check current temporary alerts again allowed occurances.
    If temp alerts are past allowed occurances then it will create actual shipment event entry.
*/
async function verifyOccurancesAndAddToShipmentEvent(shipment_participants, user_id, event_id, container_id, shipment_id, alert_type, allowed_occurances) {

    let alert_time = moment().format();

    ShipmentTempEvent.findAll({
        where: {
            event_id: event_id,
            container_id: container_id,
            shipment_id: shipment_id,
        }
    }).then(function (data) {

        if (data.length >= allowed_occurances) {

            //Check if event already exists
            ShipmentEvent.findOne({
                where: {
                    event_id: event_id,
                    container_id: container_id,
                    shipment_id: shipment_id
                },
                order: [
                    ['id', 'DESC']
                ],
                limit: 1
            }).then(function (shipmentevent) {

                if (shipmentevent) {

                    //If tempreture event, no duplicate event should be added for same temp
                    if (data.event_id == tempAlertEventId && data.current_temp != shipmentevent.current_temp) {

                        ShipmentEvent.create({
                            event_id: event_id,
                            container_id: container_id,
                            shipment_id: shipment_id,
                            current_temp: data.current_temp,
                            current_hum: data.current_hum,
                            isActive: 1
                        }).then(function (shipmentevent) {
                            const bulkEntries = [];
                            shipment_participants.map((participant) => {
                                bulkEntries.push({
                                    shipment_event_id: shipmentevent.id,
                                    user_id: participant.participant_id
                                })
                            });
                            ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {
                                //sendAlertToNetwork(cargo_id, alert_type, alert_time);
                            })
                        });
                    }

                    //If humidity event, no duplicate event should be added for same humidity
                    if (data.event_id == humidityAlertEventId && data.current_hum != shipmentevent.current_hum) {

                        ShipmentEvent.create({
                            event_id: event_id,
                            container_id: container_id,
                            shipment_id: shipment_id,
                            current_temp: data.current_temp,
                            current_hum: data.current_hum,
                            isActive: 1
                        }).then(function (shipmentevent) {
                            const bulkEntries = [];
                            shipment_participants.map((participant) => {
                                bulkEntries.push({
                                    shipment_event_id: shipmentevent.id,
                                    user_id: participant.participant_id
                                })
                            });
                            ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {
                                //sendAlertToNetwork(cargo_id, alert_type, alert_time);
                            })
                        });
                    }

                } else {

                    ShipmentEvent.create({
                        event_id: event_id,
                        container_id: container_id,
                        shipment_id: shipment_id,
                        current_temp: data.current_temp,
                        current_hum: data.current_hum,
                        isActive: 1
                    }).then(function (shipmentevent) {
                        const bulkEntries = [];
                        shipment_participants.map((participant) => {
                            bulkEntries.push({
                                shipment_event_id: shipmentevent.id,
                                user_id: participant.participant_id
                            })
                        });
                        ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {
                            //sendAlertToNetwork(cargo_id, alert_type, alert_time);
                        })
                    });
                }
            });
        }
    });
}

/*
    Send Alert To Network

    This function will send the alert details to the blockchain
    network to keep record, we will get and show these events in
    blockchain explorer
*/
function _sendAlertToNetwork(cargo_id, alert_type, alert_time) {

    /*var options = {
        method: 'POST',
        url: 'http://35.222.242.109:3000/api/addUser',
        headers:
        {
            'postman-token': '966af8e3-97e4-e15d-fa6c-c6eba9ca0049',
            'cache-control': 'no-cache',
            'content-type': 'application/json'
        },
        body:
        {
            orgName: 'org1',
            userName: 'appUser',
            cargoID: cargo_id,
            Alert: { type: alert_type, timeStamp: alert_time }
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        console.log(body);
    });*/
}

/*
    Add Border Inside Record

    This function will be used to insert border data
    in table when truck will go inside any position

*/
function _addInsideData(shipment_participants, station_id, user_id, shipment_id, container_id, covered_distance) {

    //Check if record already exists
    StationBorderInfo.findOne({
        where: {
            station_id: station_id,
            shipment_id: shipment_id,
            container_id: container_id
        },
        order: [
            ['id', 'DESC']
        ],
        limit: 1
    }).then(stationrow => {

        //Check if outside record exists for same road, then add new inside
        //Truck might have come second time to same location after existing
        if (!stationrow) {

            //Create new record if not exists
            StationBorderInfo.create({
                station_id: station_id,
                shipment_id: shipment_id,
                container_id: container_id,
                position: 'inside',
                travelled_distance: covered_distance,
            }).then(function (borderrow) {

                //Check if event already exists
                ShipmentEvent.findOne({
                    where: {
                        event_id: borderInEventId,
                        container_id: container_id,
                        shipment_id: shipment_id,
                        road_id: station_id,
                    }
                }).then(shipmentEventRow => {

                    if (!shipmentEventRow) {

                        //Create border event
                        ShipmentEvent.create({
                            event_id: borderInEventId,
                            container_id: container_id,
                            shipment_id: shipment_id,
                            road_id: station_id,
                            isActive: 1
                        }).then(function (shipmentevent) {
                            if (shipmentevent) {
                                const bulkEntries = [];
                                shipment_participants.map((participant) => {
                                    bulkEntries.push({
                                        shipment_event_id: shipmentevent.id,
                                        user_id: participant.participant_id
                                    })
                                });
                                //Add shipment event to user
                                ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {
                                    console.log('Border event saved successfully!');
                                })

                            }
                        });
                    }
                });
            });

        } else if (stationrow && stationrow.position == 'outside') {

            //If outside record exists
            StationBorderInfo.create({
                station_id: station_id,
                shipment_id: shipment_id,
                container_id: container_id,
                position: 'inside',
                travelled_distance: covered_distance,
            }).then(function (borderrow) {

                //Create border event
                ShipmentEvent.create({
                    event_id: borderInEventId,
                    container_id: container_id,
                    shipment_id: shipment_id,
                    road_id: station_id,
                    isActive: 1
                }).then(function (shipmentevent) {
                    if (shipmentevent) {
                        const bulkEntries = [];
                        shipment_participants.map((participant) => {
                            bulkEntries.push({
                                shipment_event_id: shipmentevent.id,
                                user_id: participant.participant_id
                            })
                        });
                        //Add shipment event to user
                        ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {
                            console.log('Border event saved successfully!');
                        })

                    }
                });
            });
        }
    });
}

/*
    Add Border Outside Record

    This function will be used to insert border data
    in table when truck will go outside of any position

*/
function _addOutsideData(shipment_participants, station_id, user_id, shipment_id, container_id, covered_distance) {

    //Check if inside record exists for this position
    StationBorderInfo.findOne({
        where: {
            station_id: station_id,
            shipment_id: shipment_id,
            container_id: container_id
        },
        order: [
            ['id', 'DESC']
        ],
        limit: 1
    }).then(insideposrow => {

        //If latest record is for border inside
        if (insideposrow && insideposrow.position == 'inside') {

            //If inside record exists
            StationBorderInfo.create({
                station_id: station_id,
                shipment_id: shipment_id,
                container_id: container_id,
                position: 'outside',
                travelled_distance: covered_distance,
            }).then(function (borderrow) {

                //Create border event
                ShipmentEvent.create({
                    event_id: borderOutEventid,
                    container_id: container_id,
                    shipment_id: shipment_id,
                    road_id: station_id,
                    isActive: 1
                }).then(function (shipmentevent) {
                    if (shipmentevent) {
                        const bulkEntries = [];
                        shipment_participants.map((participant) => {
                            bulkEntries.push({
                                shipment_event_id: shipmentevent.id,
                                user_id: participant.participant_id
                            })
                        });
                        //Add shipment event to user
                        ShipmentEventUser.bulkCreate(bulkEntries).then(function (shipmenteventuser) {
                            console.log('Border event saved successfully!');
                        })

                    }
                });
            });
        }
    })
}

exports.saveDeviceLog = saveDeviceLog;
exports.saveTemperatureLogs = saveTemperatureLogs;
exports.saveHumidityLogs = saveHumidityLogs;
exports.saveLocationLogs = saveLocationLogs;
exports.saveBorderInfo = saveBorderInfo;
exports.generateTempAlert = generateTempAlert;
exports.generateHumidityAlert = generateHumidityAlert;
exports.generateSealingAlert = generateSealingAlert;
exports.verifyOccurancesAndAddToShipmentEvent = verifyOccurancesAndAddToShipmentEvent;

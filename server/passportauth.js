// Load dependencies
const passport    = require('passport');
const md5 = require("md5");
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require("passport-jwt");
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

// Load Models
const db = require("./models");
const Worker = db.workers;

passport.use('local-worker', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    },
    function (username, password, cb) {

        //this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT
        return Worker.findOne({
            where: { username: username }
        })
        .then(user => {

            if (!user) {
                return cb(null, false, 'User not found.');
            }

            if (user.password != md5(password)) {
                return cb(null, false, 'Password does not match.');
            }

            let userObj = {
                id: user.id,
                first_name: user.first_name,
                last_name: user.last_name,
                email: user.email,
                phone: user.phone,
                status: user.isActive
            }

            return cb(null, userObj, 'Logged In Successfully');
        })
        .catch(err => cb(err));
    }
));

passport.use('jwt', new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : 'your_jwt_secret'
    },
    function (jwtPayload, cb) {

        return Worker.findOne({
            where: { id: jwtPayload.id }
        })
        .then(user => {
            return cb(null, user);
        })
        .catch(err => {
            return cb(err);
        });
    }
));


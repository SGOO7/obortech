function routesWithSlug({ server, app }) {

    // Auth Routes
    server.get('/verify-email/:token', async (req, res) => {
        const { token } = req.params;
        let verified = await User.verify({token});
        app.render(req, res, '/verify-email', {verified});
    });

    server.get('/reset-password/:token', (req, res) => {
        const { token } = req.params;
        app.render(req, res, '/reset-password', { token });
    });

    // User Route
    server.get('/user/edit/:id', (req, res) => {
        const { id } = req.params;
        app.render(req, res, '/user/edit', { id });
    });

    // Admin Routes
    server.get('/admin/edit/:id', (req, res) => {
        const { id } = req.params;
        app.render(req, res, '/admin/edit', { id });
    });

    // Shipment Events Routes
    server.get('/event/:shipment_id', (req, res) => {
        const { shipment_id } = req.params;
        app.render(req, res, '/event', { shipment_id });
    });

    // Shipment Documents Routes
    server.get('/document/:shipment_id', (req, res) => {
        const { shipment_id } = req.params;
        app.render(req, res, '/document', { shipment_id });
    });

    // Shipment Routes
    server.get('/containers/:shipment_id', (req, res) => {
        const { shipment_id } = req.params;
        app.render(req, res, '/containers', { shipment_id });
    });

    // Analytics Route
    server.get('/analytics/:shipment_id/:container_id', (req, res) => {
        const { shipment_id, container_id } = req.params;
        app.render(req, res, '/analytics', { shipment_id, container_id });
    });

}

module.exports = routesWithSlug;
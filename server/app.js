const express = require('express');
const session = require('express-session');
const compression = require('compression');
const mysqlSessionStore = require('express-mysql-session');
const next = require('next');
const helmet = require('helmet');
const routesWithSlug = require('./routesWithSlug');
const routesWithCache = require('./routesWithCache');
const sitemapAndRobots = require('./sitemapAndRobots');
const fs = require('fs');
const bodyParser = require('body-parser');
const path = require("path");
const cors = require('cors');
const auth = require('./google');
const api = require('./api');
const cronmanager = require('./cronmanager.js');
const logger = require('./logs');

require('./passportauth');
require('dotenv').config();

const dev = (process.env.dev == 'true');
const port = process.env.port;
const ROOT_URL = dev ? `http://localhost:${port}` : process.env.SITE_URL;

const sessionSecret = process.env.SESSION_SECRET;

const app = next({ dev });
const handle = app.getRequestHandler();

const db = require("./models");
db.sequelize.sync();

app.prepare().then(async () => {

    const server = express();
    server.use(cors());

    server.use(helmet());
    server.use(compression());
    server.use(express.json());
    server.use(bodyParser.urlencoded({ extended: false }));

    // Run Cron Jobs
    cronmanager.runCronJob();

    // give all Nextjs's request to Nextjs server
    server.get('/_next/*', (req, res) => {
        handle(req, res);
    });

    server.get('/static/*', (req, res) => {
        handle(req, res);
    });

    const env = process.env.dev ? "development" : "production";
    const config = require(path.join(__dirname, '../', 'config', 'config.json'))[env];
    const MysqlStore = mysqlSessionStore(session);
    const sess = {
        name: 'nextadmin.sid',
        secret: sessionSecret,
        store: new MysqlStore({
            host: config.host,
            port: 3306,
            user: config.username,
            password: config.password,
            database: config.database
        }),
        resave: false,
        saveUninitialized: false,
        cookie: {
            httpOnly: true,
            maxAge: 14 * 24 * 60 * 60 * 1000, // expires in 14 days
        },
    };

    server.use(session(sess));

    auth({ server, ROOT_URL });
    api(server);

    routesWithSlug({ server, app });
    routesWithCache({ server, app });
    sitemapAndRobots({ server });

    server.get('/', (req, res) => {
        res.redirect(301, '/shipment');
    });

    require('./routes')({ server, app });

    server.listen(port, (err) => {
        if (err) throw err;
        logger.info(`> Ready on ${ROOT_URL}`);
    });
});
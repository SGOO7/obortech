// Load dependencies
const express = require('express');
const md5 = require('md5');

// Load MySQL Models
const db = require("../models");
const EventCategory = db.event_categories;
const Events = db.events;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
    if (!req.user) {
        res.status(401).json({ error: 'Unauthorized' });
        return;
    }
    next();
});

// Fetch EventCategory code
router.get('/fetch', async (req, res) => {
    try {
        const eventcategories = await EventCategory.findAll({
            include: [
                {
                    model: Events,
                    where: { type: 'event' },
                    required: false
                }
            ]
        });
        res.json(eventcategories);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Add EventCategory code
router.post('/add', (req, res) => {
    try {
        return EventCategory.create({
            name: req.body.name
        }).then(function (event) {
            if (event) {
                res.json(event);
            } else {
                res.json({ error: 'Error in insert new record' });
            }
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Update EventCategory code
router.post('/update', (req, res) => {
    try {
        return EventCategory.update(
            req.body,
            { where: { id: req.body.id } }
        )
            .then(record => {
                res.json(record);
            })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Remove EventCategory code
router.post('/remove', (req, res) => {
    try {
        return EventCategory.destroy({
            where: { id: req.body.id }
        })
            .then(record => {
                res.json(record);
            });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Add Event code
router.post('/add-event', (req, res) => {
    const type = req.body.type ? req.body.type : 'event';
    try {
        return Events.create({
            type: type,
            name: req.body.name,
            event_category_id: req.body.event_category_id
        })
            .then(record => {
                res.json(record);
            })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Update Event code
router.post('/event-update', (req, res) => {
    try {
        return Events.update(
            req.body,
            { where: { id: req.body.id } }
        )
            .then(record => {
                res.json(record);
            })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Remove Event code
router.post('/remove-event', (req, res) => {
    try {
        return Events.destroy({
            where: { id: req.body.id }
        })
            .then(record => {
                res.json(record);
            })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

module.exports = router;
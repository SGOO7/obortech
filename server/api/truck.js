// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const Truck = db.trucks;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
    if (!req.user) {
        res.status(401).json({ error: 'Unauthorized' });
        return;
    }
    next();
});

// Fetch Trucks code
router.get('/fetch', async (req, res) => {
    try {
        const trucks = await Truck.findAll();
        res.json(trucks);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Add Truck code
router.post('/add', (req, res) => {
    try {
        return Truck.create({
            truckID: req.body.truckID,
        }).then(function (truck) {
            if (truck) {
                res.json(truck);
            } else {
                res.json({ error: 'Error in insert new record' });
            }
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Update Truck code
router.post('/update', (req, res) => {
    try {
        return Truck.update(
            req.body,
            { where: { id: req.body.id } }
        )
            .then(truck => {
                if (truck) {
                    res.json({ status: true });
                } else {
                    res.json({ error: 'Error in insert new record' });
                }
            })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Remove Truck code
router.post('/remove', (req, res) => {
    try {
        return Truck.destroy({
            where: { id: req.body.id }
        })
            .then(record => {
                res.json({ status: true });
            });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

module.exports = router;
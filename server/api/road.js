// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const Road = db.roads;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
  if (!req.user) {
    res.status(401).json({ error: 'Unauthorized' });
    return;
  }
  next();
});

// Fetch Roads code
router.get('/fetch', async (req, res) => {
  try {
    const roads = await Road.findAll();
    res.json(roads);
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Add Road code
router.post('/add', (req, res) => {
  try {
    return Road.create({
      name: req.body.name,
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      radius: req.body.radius
    }).then(function (road) {
      if (road) {
        res.json(road);
      } else {
        res.json({ error: 'Error in insert new record' });
      }
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Update Road code
router.post('/update', (req, res) => {
  try {
    return Road.update(
      req.body, 
      {where: { id: req.body.id } }
    )
    .then(record => {
      res.json(record);
    })
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Remove Road code
router.post('/remove', (req, res) => {
  try {
    return Road.destroy({
      where: { id: req.body.id }
    })
    .then(record => {
      res.json(record);
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

module.exports = router;
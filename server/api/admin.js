// Load dependencies
const express = require('express');
const { fork } = require('child_process');

// Load Mongo Models
// const User = require('../models/User');

// Define global variables
const logger = require('../logs');
const router = express.Router();

router.use((req, res, next) => {
  if (!req.user || !req.user.isAdmin) {
    res.status(401).json({ error: 'Unauthorized' });
    return;
  }
  next();
});

router.post('/addAdmin', async (req, res) => {
  try {
    let userdata = req.body;
    userdata.createdAt = new Date();
    const user = await User.create(userdata);
    res.json(user);
  } catch (err) {
    logger.error(err);
    res.json({ error: err.message || err.toString() });
  }
});

router.post('/updateAdmin', async (req, res) => {
  try {
    const user = await User.findOneAndUpdate(
      { _id: req.body._id },
      { $set: req.body }
    );
    res.json(user);
  } catch (err) {
    logger.error(err);
    res.json({ error: err.message || err.toString() });
  }
});

router.post('/toggleStatus', async (req, res) => {
  try {
    const user = await User.findOneAndUpdate(
      { _id: req.body._id },
      { $set: {status:!req.body.status} }
    );
    res.json(user);
  } catch (err) {
    logger.error(err);
    res.json({ error: err.message || err.toString() });
  }
});

router.post('/deleteAdmin', async (req, res) => {
  try {
    await User.find(
      { _id: req.body._id },
      { isAdmin:1 }
    ).remove();
    res.json({status:'success'});
  } catch (err) {
    logger.error(err);
    res.json({ error: err.message || err.toString() });
  }
});

router.get('/admins', async (req, res) => {
  try {
    const restaurant_users = await User.find({isAdmin:1});
    res.json(restaurant_users);
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

router.get('/admin/:id', async (req, res) => {
  try {
    const restaurant_user = await User.findOne({ _id: req.params.id });
    res.json(restaurant_user);
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

module.exports = router;
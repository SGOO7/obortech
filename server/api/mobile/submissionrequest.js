// Load dependencies
const express = require("express");
const router = express.Router();
const passport    = require('passport');
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();

// Load Models
const db = require("../../models");
const ShipmentEventHelper = require('../../helpers/shipment-event-helper.js');
const SubmissionRequest = db.submission_requests;
const SubmissionRequestRecipient = db.submission_request_recipients;
const SubmissionRequestUser = db.submission_request_users;
const ShipmentEvent = db.shipment_events;
const ShipmentComment = db.shipment_event_comments;
const Shipment = db.shipments;
const Container = db.containers;
const Cargo = db.cargos;
const Event = db.events;

router.use((req, res, next) => {
    passport.authenticate('jwt', {session: false})(req, res, next);
});

// Fetch worker submission requests
router.get('/fetch', async (req, res) => {

    let worker_id = parseInt(req.query.worker_id);

    try {

        const submissionrequests = await SubmissionRequestRecipient.findAll({
            attributes: [],
            include: [
                {
                    model:SubmissionRequest,
                    attributes: ['id', 'shipment_id', 'container_id', 'event_type', 'createdAt', 'is_viewed', 'is_submitted'],
                    include: [
                        {
                            model: Shipment,
                            attributes: ['name', 'is_completed']
                        },
                        {
                            model: Container,
                            attributes: [['containerID', 'name']]
                        },
                        {
                            model: Cargo,
                            attributes: [['cargoID', 'name']]
                        },
                        {
                            model: Event,
                            attributes: ['name']
                        },
                        {
                            model: SubmissionRequestUser,
                            attributes: ['user_id']
                        },
                        {
                            model: ShipmentEvent,
                            attributes: ['id', 'attachment'],
                            include: [
                                {
                                    model: ShipmentComment,
                                    attributes: ['comment'],
                                    order: [
                                        ['id','ASC']
                                    ],
                                    limit: 1
                                }
                            ]
                        }
                    ]
                },
            ],
            where : {
                worker_id: worker_id
            },
            order: [
                ['id', 'DESC']
            ]
        });
        res.status(200).json({
            code: 1,
            message: 'Submission requests fetched successfully',
            data: submissionrequests
        });
    } catch (err) {
        res.status(500).json({
            code: 2,
            message: err.message || err.toString()
        });
    }
});

//update submission request on view
router.post("/view", (req, res) => {

    let request_id = parseInt(req.body.id);

	try {
		return SubmissionRequest.update({
            is_viewed: 1
        }, {
			where: { id: request_id },
		}).then((record) => {
			res.status(200).json({
                code: 1,
                message: 'Submission request viewed successfully',
                data: record
            });
		});
	} catch (err) {
		res.status(500).json({
            code: 2,
            message: err.message || err.toString()
        });
	}
});

// Add Shipment Event
router.post('/add', multipartMiddleware, async (req, res) => {

    try {

        //Add Event
        const response = await ShipmentEventHelper._addShipmentEvent(req);

        //Change status of submission request
        await SubmissionRequest.update({
            is_submitted: 1,
            shipment_event_id: response.id
        }, {
			where: { id: req.body.submission_id },
		});

        res.status(200).json({
            code: 1,
            message: 'Event added successfully',
            data: response,
            request_body: req.body,
            request_files: req.files
        });

    } catch (err) {
        res.status(500).json({
            code: 2,
            message: err.message || err.toString()
        });
    }
});

module.exports = router;

// Load dependencies
const express = require("express");
const cron = require("node-cron");

// Load MySQL Models
const db = require("../models");
const Device = db.devices;
const Shipment = db.shipments;
const ShipmentFolder = db.shipment_folders;
const ShipmentParticipant = db.shipment_participants;
const ShipmentSelection = db.shipment_selections;
const ShipmentRoad = db.shipment_roads;
const ShipmentEvent = db.shipment_events;
const SealingDetail = db.sealing_details;
const SelectionDevice = db.selection_devices;
const SelectionCargo = db.selection_cargos;
const SelectionContainer = db.selection_containers;
const SelectionTruck = db.selection_trucks;
const SelectionGroup = db.selection_groups;
const LocatoinLog = db.locationlog_logs;
const Cargo = db.cargos;
const Container = db.containers;
const Group = db.groups;
const Users = db.users;
const Truck = db.trucks;
const Road = db.roads;
const ShipmentCategory = db.shipment_categories;
const ShipmentDocumentCategory = db.shipment_document_categories;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();
const cronmanager = require("../cronmanager.js");
const { route } = require("next/dist/next-server/server/router");

router.use((req, res, next) => {
	if (!req.user) {
		res.status(401).json({ error: "Unauthorized" });
		return;
	}
	next();
});

// Fetch All Shipment code
router.get("/fetch", async (req, res) => {
	let data = {};
	try {
		const shipments = await Shipment.findAll({
			include: [
				{
					model: ShipmentSelection,
					include: [
						{
							model: SelectionCargo,
							include: [
								{
									model: Cargo,
								},
							],
						},
						{
							model: SelectionContainer,
							include: [
								{
									model: Container,
								},
							],
						},
						{
							model: SelectionGroup,
							include: [
								{
									model: Group,
								},
							],
						},
						{
							model: SelectionDevice,
							include: [
								{
									model: Device,
								},
							],
						},
						{
							model: SelectionTruck,
							include: [
								{
									model: Truck,
								},
							],
						},
					],
				},
				{ model: ShipmentRoad },
				{
					model: ShipmentParticipant,
					include: [
						{
							model: Users,
						},
					]
				},
			],
		});
		res.json(shipments);
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

// Fetch One Shipment via ID
router.post("/fetchOne", async (req, res) => {
	let data = {};
	let shipment_id = req.body.shipment_id;
	try {
		const shipment = await Shipment.findOne({
			include: [
				{
					model: ShipmentSelection,
					include: [
						{
							model: SelectionCargo,
							include: [
								{
									model: Cargo,
								},
							],
						},
						{
							model: SelectionContainer,
							include: [
								{
									model: Container,
								},
							],
						},
						{
							model: SelectionGroup,
							include: [
								{
									model: Group,
								},
							],
						},
						{
							model: SelectionDevice,
							include: [
								{
									model: Device,
								},
							],
						},
						{
							model: SelectionTruck,
							include: [
								{
									model: Truck,
								},
							],
						},
					],
				},
                 {
                    model: ShipmentCategory,
                    include: [
                        {
                            model: ShipmentDocumentCategory
                        }
                    ]
                },
				{
					model: ShipmentRoad,
					include: [
						{
							model: Road,
						},
					],
				},
				{
					model: ShipmentParticipant,
					include: [
						{
							model: Users,
							required: true
						},
					],
				},
			],
			where: { id: shipment_id },
		});
		res.json(shipment);
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

// Fetch Dropdown
router.post("/fetchDropdown", async (req, res) => {
	let data = {};
	let shipment_id = req.body.shipment_id;
	try {
		const shipment = await Shipment.findOne({
			attributes: ["id"],
			include: [
				{
					model: ShipmentSelection,
					include: [
						{
							model: SelectionCargo,
							include: [
								{
									model: Cargo,
								},
							],
						},
						{
							model: SelectionContainer,
							include: [
								{
									model: Container,
									include: [
										{
											model: ShipmentEvent,
											required: false,
											as: "temp_alert",
											where: {
												event_id: 26,
											},
											attributes: ["id"],
										},
										{
											model: ShipmentEvent,
											required: false,
											as: "hum_alert",
											where: {
												event_id: 25,
											},
											attributes: ["id"],
										},
										{
											model: SealingDetail,
											required: false,
											as: "sealing_alert",
											attributes: ["id", "open_count"],
										},
									],
								},
							],
						},
						{
							model: SelectionGroup,
							include: [
								{
									model: Group,
								},
							],
						},
						{
							model: SelectionTruck,
							include: [
								{
									model: Truck,
								},
							],
						},
					],
				},
			],
			where: { id: shipment_id },
		});
		res.json(shipment);
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

// Add Device code
router.post("/add", (req, res) => {
	try {
		return Shipment.create({
			name: req.body.name,
			shipment_category_id: req.body.shipment_category_id,
			document_category_id: req.body.document_category_id,
			temperature_alert_min: req.body.temperature_alert_min,
			temperature_alert_max: req.body.temperature_alert_max,
			temperature_alert_interval: req.body.temperature_alert_interval,
			temperature_allowed_occurances: req.body.temperature_allowed_occurances,
			humidity_alert_min: req.body.humidity_alert_min,
			humidity_alert_max: req.body.humidity_alert_max,
			humidity_alert_interval: req.body.humidity_alert_interval,
			humidity_allowed_occurances: req.body.humidity_allowed_occurances,
			ambience_threshold: req.body.ambience_threshold,
			draft: req.body.draft,
		}).then(async function(shipment) {
			if (shipment) {
				await _addShipmentSelectionData(req, shipment.id);
                res.json(shipment);
			} else {
				res.json({ error: "Error in insert new record" });
			}
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

// Update Shipment code
router.post("/update", async (req, res) => {

    try {

        await _makeResourceAvailable(req.body.id);
        await ShipmentRoad.destroy({ where: { shipment_id: req.body.id } });
		await ShipmentParticipant.destroy({ where: { shipment_id: req.body.id } });
		await ShipmentSelection.destroy({ where: { shipment_id: req.body.id } })

        Shipment.update({
                name: req.body.name,
                shipment_category_id: req.body.shipment_category_id,
                document_category_id: req.body.document_category_id,
                temperature_alert_min: req.body.temperature_alert_min,
                temperature_alert_max: req.body.temperature_alert_max,
                temperature_alert_interval: req.body.temperature_alert_interval,
                temperature_allowed_occurances: req.body.temperature_allowed_occurances,
                humidity_alert_min: req.body.humidity_alert_min,
                humidity_alert_max: req.body.humidity_alert_max,
                humidity_alert_interval: req.body.humidity_alert_interval,
                humidity_allowed_occurances: req.body.humidity_allowed_occurances,
                ambience_threshold: req.body.ambience_threshold,
                draft: req.body.draft,
                isActive: 0,
            },
            { where: { id: req.body.id } }
        ).then(async function(shipment) {
            if (shipment) {
                await _addShipmentSelectionData(req, req.body.id);
                res.json(shipment);
            } else {
                res.json({ error: "Error in insert new record" });
            }
        });
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

//Add shipment selection data
const _addShipmentSelectionData = async (req, shipment_id) => {

    let shipmentRoad = [];
    let selectedRoads = req.body.selectedRoads;

    for (let i = 0; i < selectedRoads.length; i++) {
        shipmentRoad.push({
            shipment_id: shipment_id,
            road_id: selectedRoads[i].road_id,
            order: i + 1,
        });
    }

    await ShipmentRoad.bulkCreate(shipmentRoad);
    let shipmentParticipants = [];
    let selectedParticipants = req.body.selectedParticipants;
    for (let i = 0; i < selectedParticipants.length; i++) {
        shipmentParticipants.push({
            shipment_id: shipment_id,
            participant_id: selectedParticipants[i].value,
        });
    }
    await ShipmentParticipant.bulkCreate(shipmentParticipants);

    const pSelectoins = req.body.selections.map( async (selection, i) => {

        const shipment_selection = await ShipmentSelection.create({
            shipment_id: shipment_id,
        })

        let shipmentCargo = [];
        let cargos = selection.cargos;
        for (let i = 0; i < cargos.length; i++) {
            shipmentCargo.push({
                selection_id: shipment_selection.id,
                cargo_id: cargos[i].value,
            });
            await Cargo.update(
                { is_available: 0 },
                { where: { id: cargos[i].value } }
            )
        }
        SelectionCargo.bulkCreate(shipmentCargo);

        let shipmentContainer = [];
        let containers = selection.containers;
        for (let i = 0; i < containers.length; i++) {
            shipmentContainer.push({
                selection_id: shipment_selection.id,
                container_id: containers[i].value,
            });
            await Container.update(
                { is_available: 0 },
                { where: { id: containers[i].value } }
            )
        }

        await SelectionContainer.bulkCreate(shipmentContainer);

        await SelectionTruck.create({
            selection_id: shipment_selection.id,
            truck_id: selection.truck_id,
        });

        await Truck.update(
            { is_available: 0 },
            { where: { id: selection.truck_id } }
        )

        await SelectionDevice.create({
            selection_id: shipment_selection.id,
            device_id: selection.device_id,
            data_interval: selection.device_data_interval,
        })

        await Device.update(
            { is_available: 0 },
            { where: { id: selection.device_id } }
        )

        await SelectionGroup.create({
            selection_id: shipment_selection.id,
            group_id: selection.group_id,
        });
    })

    return await Promise.all(pSelectoins);
}

//Make resources available
const _makeResourceAvailable = async (shipment_id) => {

    const selections = await ShipmentSelection.findAll({
        where: { shipment_id: shipment_id },
    });

    const pArray = selections.map(async (selection, i) => {
        //Truck
        SelectionTruck.findOne({
            where: { selection_id: selection.id },
        }).then(async function(selectiontruck){
            await Truck.update(
                { is_available: 1 },
                { where: { id: selectiontruck.truck_id } }
            )
        });
        //Cargo
        SelectionCargo.findOne({
            where: { selection_id: selection.id },
        }).then(async function(selectioncargo){
            await Cargo.update(
                { is_available: 1 },
                { where: { id: selectioncargo.cargo_id } }
            )
        });
        //Device
        SelectionDevice.findOne({
            where: { selection_id: selection.id },
        }).then(async function(selectiondevice){
            await Device.update(
                { is_available: 1 },
                { where: { id: selectiondevice.device_id } }
            )
        });
        //Container
        SelectionContainer.findOne({
            where: { selection_id: selection.id },
        }).then(async function(selectioncontainer){
            await Container.update(
                { is_available: 1 },
                { where: { id: selectioncontainer.container_id } }
            )
        });
    })
    return await Promise.all(pArray);
}

// Remove Shipment code
router.post("/remove", async (req, res) => {
	try {
        await _makeResourceAvailable(req.body.id);
		return ShipmentSelection.destroy({
			where: { shipment_id: req.body.id },
		}).then((shipment_selection) => {
			Shipment.destroy({
				where: { id: req.body.id },
			}).then((shipment) => {
				ShipmentFolder.destroy({
					where: { shipment_id: req.body.id },
				}).then((shipment) => {
					res.json(shipment);
				});
			});
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/start-tracking", async (req, res) => {
	try {
		// Change isActive flag
		Shipment.update(
			{
				isActive: 1,
			},
			{
				where: {
					id: req.body.id,
				},
			}
		).then(async function(result) {
			if (result) {
				// add cron and start it
				cronmanager.addCronJob(req.body.id);
				res.json(result);
			}
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/stop-tracking", (req, res) => {
	try {
		// Change isActive flag
		Shipment.update(
			{
				isActive: 0,
			},
			{
				where: {
					id: req.body.id,
				},
			}
		).then(async function(result) {
			if (result) {
				// stop cron
				cronmanager.removeCronJob(req.body.id);
				res.json(result);
			}
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

/* Shipment Folder Relate APIs -- Start */
router.get("/fetch-folders", async (req, res) => {
	try {
		const folders = await ShipmentFolder.findAll({
			where: {
				parent: null,
			},
			include: [
				{
					model: ShipmentFolder,
					required: false,
					where: {
						shipment_id: null,
					},
					attributes: ["id", "name", "parent"],
					as: "subFolders",
					include: [
						{
							model: ShipmentFolder,
							required: false,
							where: {
								shipment_id: {
									[Op.ne]: null,
								},
							},
							attributes: ["id", "parent"],
							include: [
								{
									model: Shipment,
									attributes: ["id", "name"],
								},
							],
							as: "shipments",
						},
					],
				},
				{
					model: ShipmentFolder,
					required: false,
					where: {
						shipment_id: {
							[Op.ne]: null,
						},
					},
					attributes: ["id", "name", "parent"],
					include: [
						{
							model: Shipment,
							attributes: ["id", "name"],
						},
					],
					as: "shipments",
				},
			],
			attributes: ["id", "name"],
		});
		res.json(folders);
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

// Function to fetch shipment for logged in user to display in sidemenu
router.get("/folder-shipments", async (req, res) => {

    try {

        let shipments = [];

        //Check if logged in user is Admin
        const user = await Users.findByPk(req.session.passport.user);
        if(user.role_id == process.env.ADMIN){
            shipments = await ShipmentParticipant.findAll({
                include: [
                    {
                        model: Shipment
                    }
                ],
                group: ['shipment_id']
            })

        } else {

            shipments = await ShipmentParticipant.findAll({
                include: [
                    {
                        model: Shipment
                    }
                ],
                where: {
                    participant_id: req.session.passport.user
                }
            })
        }

		res.json(shipments);
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/create-folder", (req, res) => {
	try {
		ShipmentFolder.create({
			parent: req.body.parent,
			name: req.body.name,
		})
			.then(function(result) {
				if (result) {
					res.json(result);
				}
			})
			.catch((err) => {
				res.json({ error: err.message || err.toString() });
			});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/remove-folder", (req, res) => {
	try {
		ShipmentFolder.destroy({
			where: { id: req.body.id },
		}).then((result) => {
			if (result) {
				ShipmentFolder.destroy({
					where: { parent: req.body.id },
				}).then((result) => {
					res.json(result);
				});
			} else {
				res.json(result);
			}
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/update-folder", (req, res) => {
	try {
		ShipmentFolder.update(
			{
				name: req.body.name,
			},
			{
				where: {
					id: req.body.id,
				},
			}
		).then(function(result) {
			if (result) {
				res.json(result);
			}
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/assign-shipment-folder", (req, res) => {
	try {
		ShipmentFolder.create({
			parent: req.body.parent,
			shipment_id: req.body.shipment_id,
		}).then(function(result) {
			if (result) {
				res.json(result);
			}
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/remove-shipment-folder", (req, res) => {
	try {
		ShipmentFolder.destroy({
			where: {
				id: req.body.shipment_id,
				parent: req.body.parent,
			},
		}).then((result) => {
			if (result) {
				res.json(result);
			}
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

module.exports = router;

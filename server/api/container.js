// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const Container = db.containers;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
  if (!req.user) {
    res.status(401).json({ error: 'Unauthorized' });
    return;
  }
  next();
});

// Fetch Containers code
router.get('/fetch', async (req, res) => {
  try {
    const containers = await Container.findAll();
    res.json(containers);
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Add Container code
router.post('/add', (req, res) => {
  try {
    return Container.create({
        containerID: req.body.containerID
    }).then(function (container) {
      if (container) {
        res.json(container);
      } else {
        res.json({ error: 'Error in insert new record' });
      }
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Update Container code
router.post('/update', (req, res) => {
  try {
    return Container.update(
      req.body, 
      {where: { id: req.body.id } }
    )
    .then(container => {
      if (container) {
        res.json({status:true});
      } else {
        res.json({ error: 'Error in insert new record' });
      }
    })
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Remove Container code
router.post('/remove', (req, res) => {
  try {
    return Container.destroy({
      where: { id: req.body.id }
    })
    .then(record => {
      res.json({status:true});
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

module.exports = router;
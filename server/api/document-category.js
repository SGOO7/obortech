// Load dependencies
const express = require('express');
const md5 = require('md5');

// Load MySQL Models
const db = require("../models");
const DocumentCategory = db.document_categories;
const Events = db.events;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
  if (!req.user) {
    res.status(401).json({ error: 'Unauthorized' });
    return;
  }
  next();
});

// Fetch DocumentCategory code
router.get('/fetch', async (req, res) => {
  try {
    const documentcategories = await DocumentCategory.findAll({
      include:[
        {
            model: Events,
            required: false,
            where: {type:'document'}
        }
      ]
    });
    res.json(documentcategories);
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Add DocumentCategory code
router.post('/add', (req, res) => {
  try {
    return DocumentCategory.create({
      name: req.body.name
    }).then(function (document) {
      if (document) {
        res.json(document);
      } else {
        res.json({ error: 'Error in insert new record' });
      }
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Update DocumentCategory code
router.post('/update', (req, res) => {
  try {
    return DocumentCategory.update(
      req.body,
      {where: { id: req.body.id } }
    )
    .then(record => {
      res.json(record);
    })
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Remove DocumentCategory code
router.post('/remove', (req, res) => {
  try {
    return DocumentCategory.destroy({
      where: { id: req.body.id }
    })
    .then(record => {
      res.json(record);
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

module.exports = router;
// Load dependencies
const express = require('express');
const md5 = require('md5');
const statusCode = require('../../utils/statusCodes');

// Load MySQL Models
const db = require("../models");
const User = db.users;
const ParticipantCategory = db.participant_categories;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
    if (!req.user) {
        res.status(statusCode.unAuthorized.code).json({ error: 'Unauthorized', code: statusCode.unAuthorized.code, message: statusCode.unAuthorized.message });
        return;
    }
    next();
});

// Fetch Participants code
router.get('/fetch', async (req, res) => {
    try {
        const participants = await User.findAll({
            include: [ParticipantCategory]
        });
        res.json(participants);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Add User code
router.post('/add', (req, res) => {
    try {
        return User.create({
            participant_category_id: req.body.participant_category_id,
            password: md5(req.body.password),
            email: req.body.email,
            username: req.body.username,
            official_name: req.body.official_name
        }).then(function (participant) {
            if (participant) {
                res.json(participant);
            } else {
                res.json({ error: 'Error in insert new record' });
            }
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Add Org User code
router.post('/orgUser', (req, res) => {
    try {
        return User.create({
            organization_id: req.body.organization_id,
            role_id: req.body.role_id,
            user_type_id: req.body.user_type_id,
            password: md5(req.body.password),
            email: req.body.email,
            username: req.body.username,
            // official_name: req.body.official_name
        }).then(function (participant) {
            if (participant) {
                res.status(200).json({ code: statusCode.success.code, data: participant, message: statusCode.success.message });
            } else {
                res.json({ error: 'Error in insert new record' });
            }
        });
    } catch (err) {
        console.error("Error in creating org user => ", err)
        res.json({ error: err.message || err.toString() });
    }
});

// Update User code
router.post('/update', (req, res) => {
    try {
        return User.update(
            req.body,
            { where: { id: req.body.id } }
        )
            .then(record => {
                res.json(record);
            })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Remove User code
router.post('/remove', (req, res) => {
    try {
        return User.destroy({
            where: { id: req.body.id }
        })
            .then(record => {
                res.json(record);
            });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// email verification api
// http://localhost:4000/api/v1/user/verify?emailId=santoshjsutar@gmail.com
router.get('/verify', async (req, res) => {
  const { emailId } = req.query
  const query = { attributes: ['id', 'email', 'isEmailVerified'], where: { email: emailId } } 
  try {
    const user = await User.findOne(query);
    if(user !== null ) {
      user.isEmailVerified = true;
      const updatedUser = await User.update(user, {where: { id: user.id }})
      if(updatedUser !== null)
        res.status(200).json({ code: statusCode.successData.code, data: { isEmailIdVerifed: user.isEmailVerified }, message: statusCode.successData.message });
    }
    else {
      res.status(404).json({ code: statusCode.notFound.code, data: { isEmailIdVerifed: user.isEmailVerified }, message: statusCode.notFound.message });
    }
  } catch (err) {
      res.json({ error: err.message || err.toString() });
  }
});

module.exports = router;
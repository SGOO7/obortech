// Load dependencies
const express = require('express');
const multipart = require('connect-multiparty');

// Load MySQL Models
const db = require("../models");
const shipmentEventHelper = require('../helpers/shipment-event-helper.js');
const shipmentEvent = db.shipment_events;
const ShipmentEventUser = db.shipment_event_users;
const ShipmentComment = db.shipment_event_comments;
const Event = db.events;
const DocumentAcceptedUser = db.document_accepted_users;
const DocumentSeenUser = db.document_seen_users;
const User = db.users;
const Road = db.roads;
const EventDocumentUser = db.event_document_users;

const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();
const multipartMiddleware = multipart();

router.use((req, res, next) => {
    /*if (!req.user) {
        res.status(401).json({ error: 'Unauthorized' });
        return;
    }*/
    next();
});

// Fetch Shipment Events
/*It will fetch event visible to that user and
will also fetch events visible to all users*/
router.post('/fetch', async (req, res) => {

    let user_id = req.body.user_id;
    let participant_id = req.body.participant_id;
    let event_category = req.body.event_category;
    let shipment_id = req.body.shipment_id;
    let container_id = req.body.container_id;
    let mark_as_view = req.body.mark_as_view != undefined ? req.body.mark_as_view : true;

    //Event Category Filter
    const where_eventCategory = {};
    if (event_category != 0) {
        where_eventCategory.event_category_id = {[Op.in]: event_category};
    }

    // Container ID Filter
    const where_cotnainer = {};
    where_cotnainer.shipment_id = shipment_id;
    if(container_id){
        where_cotnainer.container_id = container_id;
    }

    //Event Participant Filter
    let where_participant = {};

    //Fetch all [manual + global] events created and shared by selected user
    if (participant_id == 0) {
        where_participant.user_id = { [Op.or]: [user_id, 0] };

        //Fetch all events created by user
    } else if (participant_id == 1) {
        where_participant = {
            user_id: { [Op.or]: [participant_id, 0] },
            created_by: { [Op.or]: [participant_id, 0] }
        };

        //Fetch all [manual] events created and shared to a selected user
    } else {
        where_participant = {
            user_id: user_id,
            created_by: participant_id
        };
    }

    if(req.body.viewed != undefined){
        where_participant.viewed = req.body.viewed
    }

    try {
        const shipmentevents = await ShipmentEventUser.findAll({
            include: [
                {
                    model: User
                },
                {
                    model: shipmentEvent,
                    where: where_cotnainer,
                    include: [
                        {
                            model: Event,
                            where: where_eventCategory
                        },
                        {
                            model: Road,
                        },
                        {
                            model: ShipmentComment,
                            include: [
                                {
                                    model: User
                                }
                            ]
                        },
                        {
                            model: DocumentAcceptedUser,
                            include: [
                                {
                                    model: User
                                }
                            ]
                        },
                        {
                            model: DocumentSeenUser,
                            include:[
                                {
                                    model: User
                                }
                            ]
                        },
                        {
                            model: EventDocumentUser
                        }
                    ]
                }
            ],
            where: where_participant,
            order: [
                ['id', 'DESC']
            ]
        });
        if(mark_as_view){
            shipmentevents.map(async (event) => {
                if(!event.viewed){
                    await ShipmentEventUser.update({
                        viewed:1
                    },{
                        where:{
                            viewed: 0,
                            user_id: user_id
                        }
                    })
                }
            })
        }
        res.json(shipmentevents);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

//Fetch shipment documents
router.post('/fetch-shipment-documents', async (req, res) => {

    //Shipment and Container filter
    const where_event = {
        shipment_id: req.body.shipment_id,
        container_id: req.body.container_id,
        attachment_type: 2 //2 for documents
    };

    //Event Participant Filter
    let where_user = {
        user_id: req.body.user_id
    };

    try {
        const shipmentDocuments = await EventDocumentUser.findAll({
            include: [
                {
                    model: User,
                    attributes: ['id', 'official_name']
                },
                {
                    model: shipmentEvent,
                    where: where_event,
                    include: [
                        {
                            model: ShipmentComment,
                            include: [
                                {
                                    model: User,
                                    attributes: ['id', 'official_name']
                                }
                            ],
                        },
                        {
                            model: Event
                        },
                        {
                            model: DocumentAcceptedUser,
                            include: [
                                {
                                    model: User,
                                    attributes: ['id', 'official_name']
                                }
                            ]
                        },
                        {
                            model: DocumentSeenUser,
                            include:[
                                {
                                    model: User,
                                    attributes: ['id', 'official_name']
                                }
                            ]
                        },
                    ]
                }
            ],
            where: where_user,
            order: [
                ['id', 'DESC']
            ],
        });
        res.json(shipmentDocuments);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Add Shipment Event
router.post('/add', multipartMiddleware, async (req, res) => {

    try {

        const response = await shipmentEventHelper._addShipmentEvent(req);
        res.json(response);

    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Update Shipment Event
router.post('/update', multipartMiddleware, async (req, res) => {

    try {

        await ShipmentEventUser.destroy({where: {shipment_event_id: req.body.id}});
        await ShipmentComment.destroy({where: {shipment_event_id: req.body.id}});
        await DocumentAcceptedUser.destroy({where: {shipment_event_id: req.body.id}});
        await DocumentSeenUser.destroy({where: {shipment_event_id: req.body.id}});
        await EventDocumentUser.destroy({where: {shipment_event_id: req.body.id}});

        const response = await shipmentEventHelper._updateShipmentEvent(req);
        res.json(response);

    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Remove Shipment Event
router.post('/remove', async (req, res) => {
    try {
        await ShipmentEventUser.destroy({where: {shipment_event_id: req.body.id}});
        await ShipmentComment.destroy({where: {shipment_event_id: req.body.id}});
        await DocumentAcceptedUser.destroy({where: {shipment_event_id: req.body.id}});
        await DocumentSeenUser.destroy({where: {shipment_event_id: req.body.id}});
        return shipmentEvent.destroy({
            where: { id: req.body.id }
        })
            .then(shipmentevent => {
                res.json(shipmentevent);
            })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Add Document Comment
router.post('/add-comment', async (req, res) => {
    try {
        return ShipmentComment.create({
            shipment_event_id: req.body.shipment_event_id,
            comment: req.body.comment,
            user_id: req.body.user_id
        })
            .then(shipmentcomment => {
                res.json(shipmentcomment);
            })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Accept Document
router.post('/accept-document', async (req, res) => {
    try {
        return DocumentAcceptedUser.create({
            shipment_event_id: req.body.shipment_event_id,
            user_id: req.body.user_id
        })
        .then(documentaccepteduser => {
            res.json(documentaccepteduser);
        }).catch((error) => {
            console.error(error);
          });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Seen Document
router.post('/seen-document', async(req, res) => {
    try {
        return DocumentSeenUser.create({
            shipment_event_id: req.body.shipment_event_id,
            user_id: req.body.user_id
        })
        .then(documentseenuser => {
            res.json(documentseenuser);
        })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

module.exports = router;

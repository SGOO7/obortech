// Load dependencies
const express = require('express');
const md5 = require('md5');
const statusCode = require('../../utils/statusCodes');

// Load MySQL Models
const db = require("../models");
const Organization = db.organizations;
const User = db.users;

// Define global variables
const router = express.Router();

// Add Org User code
router.post('/orgUser', async (req, res) => {
    try {
        const response = await Organization.create({
            name: req.body.name,
            streetAddress: req.body.streetAddress,
            CountryCode: req.body.country_id,
            city_id: req.body.city_id,
        });
        if (response && response.dataValues){
            return User.update({
                organization_id: response.dataValues.id,
                role_id: req.body.role_id,
                user_type_id: req.body.user_type_id,
                password: md5(req.body.password),
                // email: req.body.email,
                username: req.body.username,
                // isEmailVerified: req.body.isEmailVerified,
                // isPhoneVerified: req.body.isPhoneVerified,
                official_name: req.body.username
            }, { where: { id: req.body.id }}
            ).then(function (user) {
                if (user) {
                    res.status(200).json({ code: statusCode.successData.code, data: { user, message: "Oraganization created and user linked" }, message: statusCode.successData.message });
                } else {
                    res.status(400).json({ code: statusCode.emptyData.code, data: { message: "Failed to create org & link User" }, message: statusCode.emptyData.message });
                }
            });
        }else {
            res.status(400).json(statusCode.emptyData);
        }
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// API for checking the organization name already exists in DB
router.get('/existsByName', async (req, res) => {
    const { name } = req.query;
    const query = { attributes: ['name'], where: { name } } 
    try {
        const org = await Organization.findOne(query);
        if(org && org.dataValues )
            res.status(200).json({ code: statusCode.successData.code, data: { nameExists: true }, message: statusCode.successData.message });
        else 
            res.status(404).json({ code: statusCode.notFound.code, data: { nameExists: false }, message: statusCode.notFound.message });
    }catch (err) {
        res.json({ error: err.message || err.toString() });
    }
})

// API for checking the username already exists in DB
router.get('/existsByUsername', async (req, res) => {
    const { username } = req.query;
    const query = { attributes: ['username'], where: { username } } 
    try {
        const user = await User.findOne(query);
        if(user && user.dataValues )
            res.status(200).json({ code: statusCode.successData.code, data: { usernameExists: true }, message: statusCode.successData.message });
        else 
            res.status(404).json({ code: statusCode.notFound.code, data: { usernameExists: false }, message: statusCode.notFound.message });
    }catch (err) {
        res.json({ error: err.message || err.toString() });
    }
})

module.exports = router;

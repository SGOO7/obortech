// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const LocatoinLog = db.locationlog_logs;
const TemperatureLog = db.temperature_logs;
const HumidityLog = db.humidity_logs;
const SealingDetail = db.sealing_details;
const ShipmentRoad = db.shipment_roads;
const Road = db.roads;

// Define global variables
const router = express.Router();
const Op = db.Sequelize.Op;

router.use((req, res, next) => {
    if (!req.user) {
        res.status(401).json({ error: 'Unauthorized' });
        return;
    }
    next();
});

// Fetch Shipment Logs
router.get('/fetch/location', async (req, res) => {

    let shipment_id = req.query.shipment_id;
    let container_id = req.query.container_id;

    try {

        //Locatoin logs
        const locationLogs = await LocatoinLog.findAll({
            attributes: ['latitude', 'longitude'],
            where : {
                shipment_id: shipment_id,
                container_id: container_id
            },
            order: [
                ['id', 'ASC']
            ]
        });

        //Shipment road stations
        const stations = await ShipmentRoad.findAll({
            attributes: [],
            include: [
                {
                    model: Road,
                    attributes: [ 'name', 'latitude', 'longitude']
                }
            ],
            where : {
                shipment_id: shipment_id
            },
            order: [
                ['id', 'ASC']
            ]
        });

        res.status(200).json({
            code: 1,
            message: 'Logs fetched successfully',
            data: {
                locationlogs: locationLogs,
                stations: stations
            }
        });
    } catch (err) {
        res.status(500).json({
            code: 2,
            message: err.message || err.toString()
        });
    }
});

// Fetch Temperature Logs
router.get('/fetch/temperature', async (req, res) => {

    let shipment_id = req.query.shipment_id;
    let container_id = req.query.container_id;
    let start_date = req.query.start_date;
    let end_date = req.query.end_date;
    let whereObj = {
        shipment_id: shipment_id,
        container_id: container_id
    }

    if(start_date != 'null' && end_date != 'null'){
        whereObj.createdAt = {
            [Op.between]: [start_date, end_date]
        }
    }

    try {

        //Temperature logs
        const temperatureLogs = await TemperatureLog.findAll({
            attributes: ['temperature', 'createdAt'],
            where : whereObj,
            order: [
                ['id', 'ASC']
            ]
        });

        res.status(200).json({
            code: 1,
            message: 'Logs fetched successfully',
            data: temperatureLogs
        });
    } catch (err) {
        res.status(500).json({
            code: 2,
            message: err.message || err.toString()
        });
    }
});

// Fetch Humidity Logs
router.get('/fetch/humidity', async (req, res) => {

    let shipment_id = req.query.shipment_id;
    let container_id = req.query.container_id;
    let start_date = req.query.start_date;
    let end_date = req.query.end_date;

    let whereObj = {
        shipment_id: shipment_id,
        container_id: container_id
    }
    if(start_date != 'null' && end_date != 'null'){
        whereObj.createdAt = {
            [Op.between]: [start_date, end_date]
        }
    }

    try {

        //Humidity logs
        const humidityLogs = await HumidityLog.findAll({
            attributes: ['humidity', 'createdAt'],
            where : whereObj,
            order: [
                ['id', 'ASC']
            ]
        });

        res.status(200).json({
            code: 1,
            message: 'Logs fetched successfully',
            data: humidityLogs
        });
    } catch (err) {
        res.status(500).json({
            code: 2,
            message: err.message || err.toString()
        });
    }
});

// Fetch Latest Temperature Logs
router.get('/fetch/lateststats', async (req, res) => {

    let shipment_id = req.query.shipment_id;
    let container_id = req.query.container_id;

    try {

        const latestTemp = await TemperatureLog.findOne({
            attributes: ['temperature'],
            where : {
                shipment_id: shipment_id,
                container_id: container_id
            },
            order: [
                ['id', 'DESC']
            ]
        });

        const latestHum = await HumidityLog.findOne({
            attributes: ['humidity'],
            where : {
                shipment_id: shipment_id,
                container_id: container_id
            },
            order: [
                ['id', 'DESC']
            ]
        });

        const sealingOpenCount = await SealingDetail.findOne({
            attributes: ['open_count'],
            where : {
                shipment_id: shipment_id,
                container_id: container_id
            },
            order: [
                ['id', 'DESC']
            ]
        });

        res.status(200).json({
            code: 1,
            message: 'Logs fetched successfully',
            data: {
                latestTemp: latestTemp ? latestTemp.temperature : 0,
                latestHum: latestHum ? latestHum.humidity : 0,
                sealingOpenCount: sealingOpenCount ? sealingOpenCount.open_count : 0
            }
        });
    } catch (err) {
        res.status(500).json({
            code: 2,
            message: err.message || err.toString()
        });
    }
});

module.exports = router;
// Load dependencies
const express = require("express");
const md5 = require("md5");

// Load MySQL Models
const db = require("../models");
const ParticipantCategory = db.participant_categories;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
	if (!req.user) {
		res.status(401).json({ error: "Unauthorized" });
		return;
	}
	next();
});

// Fetch ParticipantCategory code
router.get("/fetch", async (req, res) => {
	try {
		const participantcategories = await ParticipantCategory.findAll();
		res.json(participantcategories);
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

// Add ParticipantCategory code
router.post("/add", (req, res) => {
	try {
		return ParticipantCategory.create({
			name: req.body.name,
		}).then(function(participant) {
			if (participant) {
				res.json(participant);
			} else {
				res.json({ error: "Error in insert new record" });
			}
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

// Update ParticipantCategory code
router.post("/update", (req, res) => {
	try {
		return ParticipantCategory.update(req.body, {
			where: { id: req.body.id },
		}).then((record) => {
			res.json(record);
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

// Remove ParticipantCategory code
router.post("/remove", (req, res) => {
	try {
		return ParticipantCategory.destroy({
			where: { id: req.body.id },
		}).then((record) => {
			res.json(record);
		});
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

module.exports = router;

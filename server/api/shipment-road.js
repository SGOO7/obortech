// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const ShipmentRoad = db.shipment_roads;
const Road = db.roads;
const StationBorderInfo = db.station_border_info;

// Define global variables
const router = express.Router();
const Op = db.Sequelize.Op;

router.use((req, res, next) => {
    if (!req.user) {
        res.status(401).json({ error: 'Unauthorized' });
        return;
    }
    next();
});

// Fetch Shipment Roads
router.post('/fetch', async (req, res) => {

    let shipment_id = parseInt(req.body.shipment_id);
    let container_id = parseInt(req.body.container_id);

    try {
        const shipmentroad = await ShipmentRoad.findAll({
            include: [
                {
                    model: Road,
                    include: [{
                        model: StationBorderInfo,
                        as: 'inside',
                        where: {
                            position: 'inside',
                            shipment_id: shipment_id,
                            container_id: container_id
                        },
                        order: [
                            ['id', 'ASC']
                        ],
                        limit: 5
                    },{
                        model: StationBorderInfo,
                        as: 'outside',
                        where: {
                            position: 'outside',
                            shipment_id: shipment_id,
                            container_id: container_id
                        },
                        order: [
                            ['id', 'ASC']
                        ],
                        limit: 5
                    }]
                }
            ],
            where : {
                shipment_id: shipment_id
            }
        });
        res.json(shipmentroad);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

module.exports = router;
// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const Event = db.events;

const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
    if (!req.user) {
        res.status(401).json({ error: 'Unauthorized' });
        return;
    }
    next();
});

// Fetch Events code
router.post('/fetch', async (req, res) => {

    let categories = req.body.categories;
    let eventtype = req.body.type;

    try {
        const events = await Event.findAll({
            where: {
                event_category_id: {[Op.in]: categories},
                type: eventtype
            }
        });
        res.json(events);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Add Event code
router.post('/add', (req, res) => {
    try {
        return Event.create({
            event_category_id: req.body.category_id,
            name: req.body.event_name,
        }).then(function (event) {
            if (event) {
                res.json(event);
            } else {
                res.json({ error: 'Error in insert new record' });
            }
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Update Event code
router.post('/update', (req, res) => {
    try {
        return Event.update(
            req.body,
            {
                where: {
                    id: req.body.id
                }
            }
        )
        .then(record => {
            res.json(record);
        })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Remove Event code
router.post('/remove', (req, res) => {
    try {
        return Event.destroy({
            where: { id: req.body.id }
        })
        .then(record => {
            res.json(record);
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

module.exports = router;
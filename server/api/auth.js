// Load dependencies
const express = require("express");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const fs = require("fs");
const AWS = require("aws-sdk");
const md5 = require("md5");
const multipart = require("connect-multiparty");

// Load Models
const db = require("../models");
const otpHelper = require("../helpers/otp-helper.js");
const User = db.users;

// Define global variables
const sendEmail = require("../mail");
const logger = require("../logs");
const multipartMiddleware = multipart();
const router = express.Router();
const server = express();

router.post("/sendotp", async (req, res) => {

    try {

        //Create a save OTP
        let response;
        const reqObj = {
            verificationCode: otpHelper._generateotp(),
            toNumber: req.body.number,
            countrycode: req.body.countrycode
        };

        //If request is sent from mobile app
        if(req.body.ismobile == 'true'){
            response = await otpHelper._sendOtpToMobile(reqObj);
        } else  {
            response = await otpHelper._sendOtpToWeb(reqObj);
        }

        res.status(response.code).json(response.data);

	} catch (err) {
		res.status(500).json({
            code: 2,
            message: err.message || err.toString()
        });
	}
});

router.post("/verifyotp", async (req, res) => {

    try {

        //Create a save OTP
        let response;
        const reqObj = {
            verificationCode: req.body.otp,
            toNumber: req.body.number,
            countrycode: req.body.countrycode
        };

        //If request is sent from mobile app
        if(req.body.ismobile == 'true'){
            response = await otpHelper._verifyMobileOtp(reqObj);
        } else  {
            response = await otpHelper._verifyWebOtp(reqObj);
        }

        res.status(response.code).json(response.data);

	} catch (err) {
		res.status(500).json({
            code: 2,
            message: err.message || err.toString()
        });
	}
});

router.get("/profile/:slug", async (req, res) => {
	try {
		const user = await User.findOne({ _id: req.params.slug });
		user.password = null;
		res.json(user);
	} catch (err) {
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/profile", async (req, res) => {
	try {
		if (req.body.password) {
			const user = await User.findOneAndUpdate(
				{ _id: req.body._id },
				{
					$set: {
						first_name: req.body.first_name,
						last_name: req.body.last_name,
						phone: req.body.phone,
						password: md5(req.body.password),
					},
				}
			);
			user.password = null;
			res.json(user);
		} else {
			const user = await User.findOneAndUpdate(
				{ _id: req.body._id },
				{
					$set: {
						first_name: req.body.first_name,
						last_name: req.body.last_name,
						phone: req.body.phone,
					},
				}
			);
			user.password = null;
			res.json(user);
		}
	} catch (err) {
		logger.error(err);
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/uploadimage", multipartMiddleware, async (req, res) => {
	try {
		const userdetails = JSON.parse(req.body.user);
		let tmp_path = req.files.file.path;
		await fs.readFile(tmp_path, async function(err, data) {
			const s3 = new AWS.S3({
				accessKeyId: process.env.Amazon_accessKeyId,
				secretAccessKey: process.env.Amazon_secretAccessKey,
				region: process.env.Amazon_region,
			});
			const params = {
				Bucket: "obortech-data", // pass your bucket name
				Key:
					"profile_images/" +
					userdetails._id +
					"/" +
					req.files.file.name, // file will be saved as testBucket/contacts.csv
				Body: data,
				ACL: "public-read",
				ContentType: "image/jpeg",
			};
			await s3.upload(params, async function(s3Err, data) {
				const response = {};
				const user = await User.findOneAndUpdate(
					{ _id: userdetails._id },
					{ $set: { profile_pic: data.Location } },
					{ fields: "profile_pic", new: true }
				);
				response.image = data.Location;
				res.json(response);
			});
		});
	} catch (err) {
		logger.error(err);
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/signup", async (req, res) => {
	try {
		var user = await User.findOne({
			where: { username: req.body.username },
		});
		if (user) {
			res.json({ error: "User with same username already exist." });
			return;
		}
		var user = await User.findOne({ where: { email: req.body.email } });
		if (user) {
			res.json({ error: "User with same email already exist." });
			return;
		}

		// generate email verification token
		let verification_token =
			new Date().valueOf() + "_" + Math.round(Math.random() * 10);
		verification_token = Buffer.from(verification_token).toString("base64");
		req.body.verification_token = verification_token;
		req.body.email_verified = false;
		req.body.password = md5(req.body.password);

		await User.create(req.body)
			.then((data) => {
				res.json(data);
			})
			.catch((err) => {
				res.status(500).send({
					message:
						err.message ||
						"Some error occurred while creating the Tutorial.",
				});
			});
	} catch (err) {
		logger.error(err);
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/forgotpassword", async (req, res) => {
	try {
		const user = await User.forgotpassword(req.body);
		res.json(user);
	} catch (err) {
		logger.error(err);
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/notifications", async (req, res) => {
	try {
		if (req.body.readAll != undefined && req.body.readAll == true) {
			await Notifications.updateMany({ read: 0 }, { $set: { read: 1 } });
			const notifications = await Notifications.find().sort({
				createdAt: -1,
			});
			res.json(notifications);
		} else {
			const notifications = await Notifications.find(req.body).sort({
				createdAt: -1,
			});
			res.json(notifications);
		}
	} catch (err) {
		logger.error(err);
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/marknotificationasread", async (req, res) => {
	try {
		let notifications = await Notifications.findOneAndUpdate(
			{ _id: req.body.notification_id },
			{ $set: { read: 1 } }
		);
		res.json(notifications);
	} catch (err) {
		logger.error(err);
		res.json({ error: err.message || err.toString() });
	}
});

router.post("/resetpassword", async (req, res) => {
	try {
		const user = await User.resetpassword(req.body);
		res.json(user);
	} catch (err) {
		logger.error(err);
		res.json({ error: err.message || err.toString() });
	}
});

passport.use('local-user', new LocalStrategy(
		{
			usernameField: "username",
			passwordField: "password",
			passReqToCallback: true,
		},
		async function(req, username, password, done) {
			password = md5(password);
			let user = await User.findOne({
				where: { email: username, password: password },
			});
			if (!user) {
				return done(null, false, { message: "Incorrect credentials." });
			} else {
				if (user.status == 0) {
					return done(null, false, {
						message: "User is not active.",
					});
				}
				return done(null, user);
			}
		}
	)
);

router.post("/login", passport.authenticate("local-user"), function(req, res) {
	res.json(req.user);
});

passport.serializeUser((user, done) => {
	done(null, user.id);
});

passport.deserializeUser((id, done) => {
	User.findById(id, User.publicFields(), (err, user) => {
		done(err, user);
	});
});

server.use(passport.initialize());
server.use(passport.session());

module.exports = router;

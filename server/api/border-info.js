// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const StationBorderInfo = db.station_border_info;
const Road = db.roads;
const RoadMap = db.road_maps;

// Define global variables
const router = express.Router();
const Op = db.Sequelize.Op;

router.use((req, res, next) => {
    if (!req.user) {
        res.status(401).json({ error: 'Unauthorized' });
        return;
    }
    next();
});

// Fetch Station Border Info
router.post('/fetch', async (req, res) => {

    let shipment_id = req.body.shipment_id;
    let container_id = req.body.container_id;

    try {
        const stationborderinfo = await StationBorderInfo.findAll({
            include: [
                {
                    model: Road,
                    include: [
                        {
                            model: RoadMap,
                            as: 'inside',
                            where: {
                                position: {[Op.or]: ['inside']}
                            },
                            order: [
                                ['id', 'ASC']
                            ],
                            limit: 1
                        },
                        {
                            model: RoadMap,
                            as: 'outside',
                            where: {
                                position: {[Op.or]: ['outside']}
                            },
                            order: [
                                ['id', 'DESC']
                            ],
                            limit: 1
                        }
                    ]
                }
            ],
            where : {
                shipment_id: shipment_id,
                container_id: container_id
            }
        });
        res.json(stationborderinfo);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

module.exports = router;
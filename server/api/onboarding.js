// Load dependencies
const express = require("express");
const nodemailer = require('nodemailer');
const statusCode = require('../../utils/statusCodes');
const cipher = require('../../utils/encrypt')
// const cipher = require('../../static/templates/email-html/logo.png')
// const md5 = require('md5');

const fs = require('fs');
const path = require("path");
var handlebars = require('handlebars');

// const passportJWT = require("passport-jwt");
// const JWTStrategy   = passportJWT.Strategy;
// const ExtractJWT = passportJWT.ExtractJwt;

// Load MySQL Models
const db = require("../models");
// const emailSender = require("../server/sendMail");

const User = db.users;

// Define global variables
const router = express.Router();

// EMAIL & OTHER CONFIGURATIONS
const DEVELOPMENT = process.env.dev;
const PORT = process.env.port;
const CIPHER_SALT = process.env.CIPHER_SALT
const MAIL_HOST_NAME = process.env.MAIL_HOST_NAME
const MAIL_PORT_NO = process.env.MAIL_PORT_NO
const MAIL_SECURE = process.env.MAIL_SECURE
const MAIL_USERNAME = process.env.MAIL_USERNAME
const MAIL_PASSWORD = process.env.MAIL_PASSWORD
const MAIL_EMAIL_ID = process.env.MAIL_EMAIL_ID

// Check for existing user & verification
router.post('/verifyUser', async (req, res) => {
  try {
        const { email, mobile } = req.body
        const query = { attributes: ['id', 'email', 'mobile', 'isEmailVerified', 'isPhoneVerified', 'password'], where: { email, mobile } } 
        const isUserVerified = await User.findOne(query);
        
        if(isUserVerified && isUserVerified.dataValues) {
            if(isUserVerified.dataValues.isEmailVerified && isUserVerified.dataValues.isPhoneVerified) {
              if(isUserVerified.dataValues.password)
                res.status(200).json({ code: statusCode.successData.code, data: { user: isUserVerified.dataValues, message: "Onboarding process completed, please login", level: 5 }, message: statusCode.successData.message });
              else
                res.status(200).json({ code: statusCode.successData.code, data: { user: isUserVerified.dataValues, message: "User exists & verified", level: 3 }, message: statusCode.successData.message });
            }
            if(isUserVerified.dataValues.isEmailVerified)
              res.status(200).json({ code: statusCode.successData.code, data: { user: isUserVerified.dataValues, message: "User exists & only EmailId verified", level: 2 }, message: statusCode.successData.message });
            if(isUserVerified.dataValues.isPhoneVerified)
              res.status(200).json({ code: statusCode.successData.code, data: { user: isUserVerified.dataValues, message: "User exists & only Mobile No verified", level: 2 }, message: statusCode.successData.message });
            res.status(200).json({ code: statusCode.successData.code, data: { user: isUserVerified.dataValues, message: "User exists & not verified", level: 2 }, message: statusCode.successData.message });
        } else {
            res.status(400).json({ code: statusCode.emptyData.code, data: { message: "User doesn't exists", level: 1 }, message: statusCode.emptyData.message });
        }
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Add new user to db
router.post('/user', async (req, res) => {
  try {
        const { email, mobile, isEmailVerified, isPhoneVerified } = req.body
        return User.create({
            organization_id: -1,
            role_id: -1,
            user_type_id: -1,
            mobile: mobile,
            email: email,
            isEmailVerified: isEmailVerified,
            isPhoneVerified: isPhoneVerified,
        }).then(function (user) {
            if (user && user.dataValues) {
              res.status(200).json({ code: statusCode.successData.code, data: { user: user.dataValues, message: "User created successfully" }, message: statusCode.successData.message });
            } else {
              res.status(400).json({ code: statusCode.emptyData.code, data: { message: "Failed to create user" }, message: statusCode.emptyData.message });
            }
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// send mail api
router.post("/verify/email", async (req, res) =>{
    const { emailId, mobile, numVerified } = req.body
    // const ROOT_URL = DEVELOPMENT ? "http://localhost:4000" : "https://www.obortech.io";
    const ROOT_URL = DEVELOPMENT ?  `http://localhost:${PORT}` : process.env.SITE_URL;
    // const code = `id=${emailId}&mobile=${mobile}&verified=${true}`
    const code = JSON.stringify({
      id: emailId,
      mobile: mobile,
      numVerified,
      idVerified: true,
    })
    
    // To create a cipher
    const encoder = cipher(CIPHER_SALT)

    //Then cipher any text:
    const encoded = encoder(code)
    
    const verificationLink = `${ROOT_URL}/login?email=${encoded}`

    const emailSender = nodemailer.createTransport({
        host: MAIL_HOST_NAME,
        port: MAIL_PORT_NO,
        secure: MAIL_SECURE,
        auth: {
            user: MAIL_USERNAME,
            pass: MAIL_PASSWORD
        }
    });
    const filePath = path.join(__dirname, '../../static/templates/email-html/activate-your-account.html');
    const source = fs.readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);
    const replacements = {
      verificationLink: verificationLink
    };
    const htmlToSend = template(replacements);
    const logoPath = path.join(__dirname, '../../static/templates/email-html/logo.png');
    console.log(logoPath)
    const message = {
        from: MAIL_EMAIL_ID,
        to: emailId,
        subject: 'Email verfication link - OBORTECH',
        html: htmlToSend,
        attachments: [{
          filename: 'logo.png',
          path: logoPath,
          cid: 'support@obortech.io'
        }]
    };

    emailSender.sendMail(message, function(err, info) {
        if (err) {
          res.status(200).json({ code: statusCode.successData.code, data: { user: isUserVerified.dataValues, message: "User exists & only EmailId verified", level: 2 }, message: statusCode.successData.message });
          res.status(statusCode.notFound.code).json({ code: statusCode.notFound.code, message: statusCode.notFound.message, data: err });
        } else {
          res.status(statusCode.successData.code).json({ code: statusCode.successData.code, message: statusCode.successData.message, data: info });
        }
    });
})


// email verification api
// http://localhost:4000/api/v1/signup/verify?emailId=santoshjsutar@gmail.com

// localhost:4000/login?email=fsgdfg&phone=1234567890&isVerfied=true
router.get('/verify', async (req, res) => {
    const { emailId } = req.query
    const query = { attributes: ['id', 'email', 'isEmailVerified'], where: { email: emailId } } 
    try {
      const user = await User.findOne(query);
      if(user && user.dataValues && Object.keys(dataValues).length) {
        user.dataValues.isEmailVerified = true;
        const updateUser = await User.update(user.dataValues, {where: { id: user.dataValues.id }})
        const updatedUser = await User.findOne({ attributes: ['isEmailVerified'], where: { id: user.dataValues.id }});
        const isEmailIdVerified = updatedUser.dataValues.isEmailVerified
        if(updateUser.length > 0 && updatedUser.dataValues.isEmailVerified)
            res.status(200).json({ code: statusCode.successData.code, data: { isEmailIdVerified }, message: statusCode.successData.message });
        else
            res.status(400).json({ code: statusCode.emptyData.code, data: { isEmailIdVerified }, message: statusCode.emptyData.message });
      }
      else {
        res.status(404).json({ code: statusCode.notFound.code, data: { isEmailIdVerified: user.dataValues.isEmailVerified }, message: statusCode.notFound.message });
      }
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
  });
  

module.exports = router;


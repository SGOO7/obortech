// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const Cargo = db.cargos;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
  if (!req.user) {
    res.status(401).json({ error: 'Unauthorized' });
    return;
  }
  next();
});

// Fetch Cargos code
router.get('/fetch', async (req, res) => {
  try {
    const cargos = await Cargo.findAll();
    res.json(cargos);
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Add Cargo code
router.post('/add', (req, res) => {
  try {
    return Cargo.create({
      cargoID: req.body.cargoID,
    }).then(function (cargo) {
      if (cargo) {
        res.json(cargo);
      } else {
        res.json({ error: 'Error in insert new record' });
      }
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Update Cargo code
router.post('/update', (req, res) => {
  try {
    return Cargo.update(
      req.body, 
      {where: { id: req.body.id } }
    )
    .then(record => {
      res.json(record);
    })
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Remove Cargo code
router.post('/remove', (req, res) => {
  try {
    return Cargo.destroy({
      where: { id: req.body.id }
    })
    .then(record => {
      res.json(record);
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

module.exports = router;
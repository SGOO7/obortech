// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const Device = db.devices;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
  if (!req.user) {
    res.status(401).json({ error: 'Unauthorized' });
    return;
  }
  next();
});

// Fetch Devices code
router.get('/fetch', async (req, res) => {
  try {
    const devices = await Device.findAll();
    res.json(devices);
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Add Device code
router.post('/add', async (req, res) => {
  try {
    const device_exist = await Device.findOne({
      where: {deviceID:req.body.deviceID}
    });
    if(device_exist){
      res.json({ error: 'Device with same name already exists!!' });
    } else{
      return Device.create({
        deviceID: req.body.deviceID,
      }).then(function (device) {
        if (device) {
          res.json(device);
        } else {
          res.json({ error: 'Error in insert new record' });
        }
      });
    }
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Update Device code
router.post('/update', async(req, res) => {
  try {
    return Device.update(
      req.body, 
      {where: { id: req.body.id } }
    )
    .then(record => {
      res.json(record);
    })
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Remove Device code
router.post('/remove', (req, res) => {
  try {
    return Device.destroy({
      where: { id: req.body.id }
    })
    .then(record => {
      res.json(record);
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

module.exports = router;
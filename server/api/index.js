function api(server) {
    server.use('/api/v1/admin', require('./admin'));
    server.use('/api/v1/user', require('./user'));
    server.use('/api/v1/organization', require('./organization'));
    server.use('/api/v1/auth', require('./auth'));
    server.use('/api/v1/event', require('./event'));
    server.use('/api/v1/participant', require('./participant'));
    server.use('/api/v1/road', require('./road'));
    server.use('/api/v1/cargo', require('./cargo'));
    server.use('/api/v1/shipment', require('./shipment'));
    server.use('/api/v1/shipment-event', require('./shipment-event'));
    server.use('/api/v1/shipment-category', require('./shipment-category'));
    server.use('/api/v1/shipment-road', require('./shipment-road'));
    server.use('/api/v1/shipment-logs', require('./shipment-logs'));
    server.use('/api/v1/border-info', require('./border-info'));
    server.use('/api/v1/group', require('./group'));
    server.use('/api/v1/device', require('./device'));
    server.use('/api/v1/truck', require('./truck'));
    server.use('/api/v1/container', require('./container'));
    server.use('/api/v1/participant-category', require('./participant-category'));
    server.use('/api/v1/event-category', require('./event-category'));
    server.use('/api/v1/document-category', require('./document-category'));
    server.use('/api/v1/api-key', require('./api-key'));
    server.use('/api/v1/signup', require('./onboarding'));
    server.use('/api/v1/countries', require('./country'))
    server.use('/api/v1/cities', require('./city'))
    server.use('/api/v1/roles', require('./role'));
    server.use('/api/v1/types', require('./user_type'));

    //Mobile apis
    server.use('/api/v1/worker', require('./mobile/worker'));
    server.use('/api/v1/submissionrequest', require('./mobile/submissionrequest'));

}

module.exports = api;
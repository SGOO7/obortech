// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const Group = db.groups;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
  if (!req.user) {
    res.status(401).json({ error: 'Unauthorized' });
    return;
  }
  next();
});

// Fetch Groups code
router.get('/fetch', async (req, res) => {
  try {
    const groups = await Group.findAll();
    res.json(groups);
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Add Group code
router.post('/add', (req, res) => {
  try {
    return Group.create({
      groupID: req.body.groupID,
    }).then(function (group) {
      if (group) {
        res.json(group);
      } else {
        res.json({ error: 'Error in insert new record' });
      }
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Update Group code
router.post('/update', (req, res) => {
  try {
    return Group.update(
      req.body, 
      {where: { id: req.body.id } }
    )
    .then(record => {
      res.json(record);
    })
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

// Remove Group code
router.post('/remove', (req, res) => {
  try {
    return Group.destroy({
      where: { id: req.body.id }
    })
    .then(record => {
      res.json(record);
    });
  } catch (err) {
    res.json({ error: err.message || err.toString() });
  }
});

module.exports = router;
// Load dependencies
const express = require('express');

// Load MySQL Models
const db = require("../models");
const ShipmentCategory = db.shipment_categories;
const ShipmentEventCategory = db.shipment_event_categories;
const ShipmentDocumentCategory = db.shipment_document_categories;
const DocumentCategory = db.document_categories;
const EventCategory = db.event_categories;
const Op = db.Sequelize.Op;

// Define global variables
const router = express.Router();

router.use((req, res, next) => {
    if (!req.user) {
        res.status(401).json({ error: 'Unauthorized' });
        return;
    }
    next();
});

// Fetch ShipmentCategory
router.get('/fetch', async (req, res) => {
    try {
        const categories = await ShipmentCategory.findAll({
            include: [
                {
                    model: ShipmentEventCategory,
                    attributes: ['id','shipment_category_id','event_category_id'],
                    include:[
                        {
                            model: EventCategory,
                            attributes: ['name'],
                        }
                    ]
                },
                {
                    model: ShipmentDocumentCategory,
                    attributes: ['id','shipment_category_id','document_category_id'],
                    include:[
                        {
                            model: DocumentCategory,
                            attributes: ['name'],
                        }
                    ]
                }
            ]
        });
        res.json(categories);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Fetch ShipmentEventCategory
router.post('/fetchShipmentEventCategories', async (req, res) => {
    try {
        const categories = await ShipmentEventCategory.findAll({
            include: [
                {
                    model: EventCategory
                }
            ],
            where: {
                shipment_category_id: req.body.shipment_category_id
            }
        });
        res.json(categories);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

// Fetch ShipmentDocumentCategory
router.post('/fetchShipmentDocumentCategories', async (req, res) => {
    try {
        const categories = await ShipmentDocumentCategory.findAll({
            include: [
                {
                    model: DocumentCategory
                }
            ],
            where: {
                shipment_category_id: req.body.shipment_category_id
            }
        });
        res.json(categories);
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

//Add ShipmentCategory
router.post('/add', (req, res) => {
    try {
        return ShipmentCategory.create({
            name: req.body.name
        }).then(function (event) {
            if (event) {
                res.json(event);
            } else {
                res.json({ error: 'Error in insert new record' });
            }
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

//Add addShipmentDocumentCategory
router.post('/addShipmentDocumentCategory', async (req, res) => {
    try {

        const existingCategory = await ShipmentDocumentCategory.findOne({
            where: {
                shipment_category_id: req.body.shipment_category_id,
                document_category_id: req.body.value
            }
        });

        //Check if category already existing
        if(!existingCategory){

            return ShipmentDocumentCategory.create({
                shipment_category_id: req.body.shipment_category_id,
                document_category_id: req.body.value
            }).then(function (category) {
                if (category) {
                    res.json({
                        code: 1,
                        data: {
                            category: category
                        }
                    });
                } else {
                    res.json({
                        code: 2,
                        message: "Error in insert new record!"
                    });
                }
            });

        } else {
            res.json({
                code: 2,
                message: "Category already existing!"
            });
        }

    } catch (err) {
        res.json({
            code: 2,
            message: err.message || err.toString()
        });
    }
});

//Add ShipmentEventCategory
router.post('/addShipmentEventCategory', async (req, res) => {
    try {

        const existingCategory = await ShipmentEventCategory.findOne({
            where: {
                shipment_category_id: req.body.shipment_category_id,
                event_category_id: req.body.value
            }
        });

        //Check if category already existing
        if(!existingCategory){

            return ShipmentEventCategory.create({
                shipment_category_id: req.body.shipment_category_id,
                event_category_id: req.body.value
            }).then(function (category) {
                if (category) {
                    res.json({
                        code: 1,
                        data: {
                            category: category
                        }
                    });
                } else {
                    res.json({
                        code: 2,
                        message: "Error in insert new record!"
                    });
                }
            });

        } else {
            res.json({
                code: 2,
                message: "Category already existing!"
            });
        }

    } catch (err) {
        res.json({
            code: 2,
            message: err.message || err.toString()
        });
    }
});

//Update ShipmentCategory
router.post('/update', (req, res) => {
    try {
        return ShipmentCategory.update(
            req.body,
            {where: { id: req.body.id }
        }).then(record => {
            res.json(record);
        })
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

//Remove ShipmentCategory
router.post('/remove', (req, res) => {
    try {
        return ShipmentCategory.destroy({
            where: { id: req.body.id }
        })
        .then(record => {
            res.json(record);
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

//Remove removeShipmentEventCategory
router.post('/removeShipmentEventCategory', (req, res) => {
    try {
        return ShipmentEventCategory.destroy({
            where: { id: req.body.id }
        })
        .then(record => {
            res.json(record);
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

//Remove removeShipmentDocumentCategory
router.post('/removeShipmentDocumentCategory', (req, res) => {
    try {
        return ShipmentDocumentCategory.destroy({
            where: { id: req.body.id }
        })
        .then(record => {
            res.json(record);
        });
    } catch (err) {
        res.json({ error: err.message || err.toString() });
    }
});

module.exports = router;